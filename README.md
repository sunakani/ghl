# GHL

ゲームプログラマーである筆者が、DirectX12による描画や開発テストを行うために用意したプロジェクトです。

![起動イメージ](https://gitlab.com/sunakani/ghl/-/wikis/uploads/d1dc7acaa3f2d2f68dfdb999aedc1952/sample.jpg)

## ビルドについて
Visual Studioでのビルドを想定しています。
クローンを取得後、GHL.slnを開きソリューションのビルドを行ってください。

※　サブモジュールが複数あるためクローンの際は --recursive を使ってサブモジュールごと取得してください。

`git clone git@gitlab.com:sunakani/ghl.git --recursive`

※　すでに取得していた場合は `git submodule update --init` でサブモジュールを取得できます。

**現在ビルド可能な構成は、Debug / x64 の組み合わせだけになります。**

GameAddinプロジェクトで証明書関連のエラーが出る場合は、GameAddinプロジェクトのプロパティの署名を選択し、「ClickOnceマニュフェストに署名する」の右側にある「ファイルを選択」から

\GHL\Tool\Excel\GameAddin\GameAddin_TemporaryKey.pfx

を選択するか、その下の「テスト証明書の作成」を行ってください。テスト証明書作成の場合、設定は何もせずにそのままOKで通ります。

## 起動方法
Gameプロジェクトをスタートアッププロジェクトに設定したのち、デバッグ実行してください。

### 動作確認環境
- Ryzen 5 5600g + NVIDIA GeForce GTX 1660 ti 
- Ryzen 3 3100 + NVIDIA GeForce GTX 1660 ti
- Intel Core i5 8250U + Intel UHD Graphics 620

## プロジェクトの概要

### Game
ゲーム本体のプロジェクト。MMD用のモデル、モーションデータを使い、3Dゲームのキャラクターとして移動等のアクションを実行できます。

操作方法
- W,A,S,Dキー
    - 操作キャラの移動
- ESCキー
    - 終了
- Oキー
    - デバッグモードに切り替え
- マウス移動
    - カメラの移動
- マウス左クリック
    - 操作キャラの攻撃
        - 攻撃中に再度クリックすることでコンボがつながります。

### GHL
ライブラリプロジェクト。主にDirectX12による描画機能を提供しています。

### GameAddin
ゲームで使用するバイナリデータやインクルードファイルの作成を支援するツール用のプロジェクト。VSTOでエクセルのアドインとしてC#にて記述されています。使い方などはWikiにまとめます。

https://gitlab.com/sunakani/ghl/-/wikis/home#%E3%82%A8%E3%82%AF%E3%82%BB%E3%83%AB%E3%82%A2%E3%83%89%E3%82%A4%E3%83%B3
