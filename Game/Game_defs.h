#pragma once

#include "GHL.h"
#include "data/Data.h"
#include "inc/data_format_SCharacterPlacementData.h"
#include "inc/data_format_SCharacterActionData.h"
#include "inc/data_format_SCharacterActionTransit.h"
#include "inc/data_format_SCharacterActionEvent.h"

class CCamera;
class CLight;
class CFilePathManager;
class CLoadManager;
class CDebugFigure;
class CPlayerManager;
class CCharacterManager;
class CDebugMenu;

// ゲームインスタンスの更新に対して外部から渡されるパラメータ.
struct SGameUpdateIO
{
  struct In {
  };
  In in;
  struct Out {
    bool bQuit{};
  };
  Out out;
};

// 広範囲で利用されるオブジェクトの参照.
struct SGameInfo {
  float               viewport_width      {800.f};
  float               viewport_height     {600.f};
  HWND                hWnd                {};
  ghl::HTextureBuffer hShadowDepthBuffer  {};
  ghl::HTextureBuffer hSceneRenderTarget  {};
  ghl::HTextureBuffer hSceneDepthBuffer   {};
  CFilePathManager*   pFilePathManager    {nullptr};
  CLoadManager*       pLoadManager        {nullptr};
  CDebugFigure*       pDebugFigure        {nullptr};
  CPlayerManager*     pPlayerManager      {nullptr};
  CCharacterManager*  pCharacterManager   {nullptr};
  CDebugMenu*         pDebugMenu          {nullptr};
  void Clear() {
    viewport_width    = 800.f;
    viewport_height   = 600.f;
    hWnd = NULL;
    hShadowDepthBuffer.Invalidate();
    hSceneRenderTarget.Invalidate();
    hSceneDepthBuffer.Invalidate();
    pFilePathManager  = nullptr;
    pLoadManager      = nullptr;
    pDebugFigure      = nullptr;
    pPlayerManager    = nullptr;
    pCharacterManager = nullptr;
    pDebugMenu        = nullptr;
  }
};

// 各所の更新で使用されるパラメータ.
struct SGameUpdateParam {
  float                       fDeltaTime      {0.f};
  SGameInfo*                  pGameInfo       {nullptr};
  const CCamera*              pCamera         {nullptr};
  const CLight*               pLight          {nullptr};
  DirectX::XMMATRIX           matView         {DirectX::XMMatrixIdentity()};
  DirectX::XMMATRIX           matPersProj     {DirectX::XMMatrixIdentity()};
  DirectX::XMMATRIX           matOrthProj     {DirectX::XMMatrixIdentity()};
  DirectX::XMMATRIX           matLightCamera  {DirectX::XMMatrixIdentity()};
  const SGameUpdateIO*        pIO             {nullptr};
};

// キャラクターのシェーダリソースビューのインデックスごとの説明.
enum class EIndexOfSRV {
  eCharaColor,
  eCharaSpherical,
  eCharaAdditionalSpherical,
  eCharaToon,
  eCharaShadow,
  Num
};

// 色(RGBA).
using AFloatColor = std::array<float,4>;
const AFloatColor& GetClearColor();

#if defined(_DEBUG)
#include "DebugFont.h"
#endif

// プレイヤーを識別する番号.
enum class EPlayerIndex
{
	None = -1,
	P1   = 0,
	Num
};

// 角度系のヘルパー.
// 角度度を-180度〜180度に補正して扱う.
float adjust_angle_y(float angle_y);
float calc_diff_to_close_target(float adjusted_target_angle, float adjusted_source_angle);

// 各種定数.
enum {

  // キャラクター更新スレッド数.
  eCharacterUpdateThreadNum = 8,

};

// 表示モード.
enum class EGameDisplayMode {
  eNone,
  eNormal,
  eWithDebugDialog,
};

