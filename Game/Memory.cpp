#include "Memory.h"

// デフォルト.
CHeap s_heap;
CHeap& GetDefaultHeap() { return s_heap; }

// 確保領域のヘッダー.
struct SAllocateHeader {
  CHeap* pHeap{};
  size_t size{};
  uint32_t signature{0};
};
constexpr uint32_t MEMORY_HEADER_SIGNATURE = 1234567890;

// 確保領域の終端を示すマーク. 
struct SAllocateEndMarker {
  uint32_t signature{0};
};
constexpr uint32_t MEMORY_END_MARK = 987654321;

// グローバルnew/deleteのオーバーロード.
void* operator new(std::size_t size, CHeap* pHeap)
{
  return pHeap->Allocate(size);
}
void* operator new(std::size_t size) {
  return ::operator new(size, &GetDefaultHeap());
}
void operator delete(void* p) noexcept 
{
  SAllocateHeader* pHeader = reinterpret_cast<SAllocateHeader*>(static_cast<char*>(p) - sizeof(SAllocateHeader));
  _DEBUG_BREAK(pHeader->signature == MEMORY_HEADER_SIGNATURE);
  pHeader->pHeap->Deallocate(p);
}

// CHeap.
void* CHeap::Allocate(size_t size)
{
  size_t allocate_size = size + sizeof(SAllocateHeader) + sizeof(SAllocateEndMarker);
  char* p = static_cast<char*>(malloc(allocate_size));

  SAllocateHeader* pHeader = reinterpret_cast<SAllocateHeader*>(p);
  pHeader->pHeap = this;
  pHeader->size = size;
  pHeader->signature = MEMORY_HEADER_SIGNATURE;

  SAllocateEndMarker* pEnd = reinterpret_cast<SAllocateEndMarker*>(p + sizeof(SAllocateHeader) + size);
  pEnd->signature = MEMORY_END_MARK;

  AddAllocation(size);
  void* pReturnAddress = p + sizeof(SAllocateHeader);
  return pReturnAddress;
}
void CHeap::Deallocate(void* p)
{
  SAllocateHeader* pHeader = reinterpret_cast<SAllocateHeader*>(static_cast<char*>(p) - sizeof(SAllocateHeader));
  _DEBUG_BREAK(pHeader->signature == MEMORY_HEADER_SIGNATURE);

  SAllocateEndMarker* pEnd = reinterpret_cast<SAllocateEndMarker*>(static_cast<char*>(p) + pHeader->size);
  _DEBUG_BREAK(pEnd->signature == MEMORY_END_MARK);

  RemoveAllocation(pHeader->size);
  free(pHeader);
}
