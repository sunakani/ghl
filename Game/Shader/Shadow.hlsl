
cbuffer cbuff0          : register(b0)
{
  matrix matView;
  matrix matPersProj;
  matrix matLightCamera;
  matrix matShadow;
  float3 eye;
};
cbuffer cbuff1          : register(b1)
{
  matrix matWorld;
  matrix localBoneMatrices[256];
};

struct VSOutput {
  float4 svpos  :SV_POSITION;
};

VSOutput VS(
    float4 pos        : POSITION
  , float4 normal     : NORMAL
  , float2 uv         : TEXCOORD
  , min16uint2 boneno : BONENO
  , min16uint weight  : WEIGHT
)
{
  VSOutput output;
  float w = weight * 0.01f;
  matrix bm = localBoneMatrices[boneno[0]] * w + localBoneMatrices[boneno[1]] * (1 - w);
  pos = mul(bm, pos);
  pos = mul(matWorld, pos);
  pos = mul(matLightCamera, pos);
  output.svpos = pos;
  return output;
}

