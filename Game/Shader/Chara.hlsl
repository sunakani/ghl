
struct VSInput {
  float4      pos     : POSITION;
  float4      normal  : NORMAL;
  float2      uv      : TEXCOORD;
  min16uint2  boneno  : BONENO;
  min16uint   weight  : WEIGHT;
  uint        instNo  : SV_InstanceID;
  min16uint   edge    : EDGEFLAG;
};

struct VSOutput {
  float4  svpos   : SV_POSITION;
  float4  normal  : NORMAL0;
  float4  vnormal : NORMAL1;
  float2  uv      : TEXCOORD;
  float3  ray     : VECTOR;
  uint    instNo  : SV_InstanceID;
  float4  tpos    : TPOS;
};

// t.
Texture2D<float4> tex           : register(t0);
Texture2D<float4> sph           : register(t1);
Texture2D<float4> spa           : register(t2);
Texture2D<float4> toon          : register(t3);
Texture2D<float4> lightDepthTex : register(t4);

// s.
SamplerState smp                  : register(s0);
SamplerState smpToon              : register(s1);
SamplerComparisonState smpShadow  : register(s2);

// b.
cbuffer cbuff0          : register(b0)
{
  matrix matView;
  matrix matPersProj;
  matrix matLightCamera;
  matrix matShadow;
  float3 eye;
};
cbuffer cbuff1          : register(b1)
{
  matrix matWorld;
  matrix localBoneMatrices[256];
};
cbuffer Material        : register(b2)
{
  float3  diffuse;
  float   alpha;
  float3  specular;
  float   specularity;
  float3  ambient;
};

// 入力された頂点座標をワールド系に変換.
float4 TransformWorldPosition(float4 inPosition, VSInput In)
{
  float w = In.weight * 0.01f;
  matrix bm = localBoneMatrices[In.boneno[0]] * w + localBoneMatrices[In.boneno[1]] * (1 - w);
  inPosition = mul(bm, inPosition);
  inPosition = mul(matWorld, inPosition);
  return inPosition;
}

// 頂点シェーダ(通常).
VSOutput VS(VSInput In)
{
  VSOutput output;
  float4 pos = TransformWorldPosition(In.pos, In);
  output.tpos = mul(matLightCamera, pos);
  if (In.instNo == 1) {
    pos = mul(matShadow, pos);
  }
  pos = mul(matView, pos);
  pos = mul(matPersProj, pos);
  output.svpos = pos;
  output.uv = In.uv;
  In.normal.w = 0.f;
  output.normal = mul(matWorld, In.normal);
  output.vnormal = mul(matView, output.normal);
  output.ray = normalize(pos.xyz - eye);
  output.instNo = In.instNo;
  return output;
}

// ピクセルシェーダ(通常).
float4 PS(VSOutput input) : SV_TARGET
{
  if (input.instNo == 1) {
    return float4(0.f,0.f,0.f,1.f);
  }
  float3 light = normalize(float3(1, -1, 1));
  float3 lightColor = float3(1, 1, 1);

  float3 posFromLightVP = input.tpos.xyz / input.tpos.w;
  float2 shadowUV = (posFromLightVP.xy + float2(1,-1)) * float2(0.5,-0.5);
  float value = posFromLightVP.z - 0.005f;
  float depthFromLight = lightDepthTex.SampleCmp(smpShadow, shadowUV, value);
  float shadowWeight = lerp(0.5f, 1.f, depthFromLight);
  float diffuseB = dot(-light, input.normal);
  diffuseB *= shadowWeight;

  float3 refLight = normalize(reflect(light, input.normal.xyz));
  float specularB = pow(saturate(dot(refLight, -input.ray)), specularity);

  float2 sphereMapUV = (input.vnormal.xy + float2(1, -1)) * float2(0.5, -0.5);

  float4 texColor = tex.Sample(smp, input.uv);

  float4 toonDif = toon.Sample(smpToon, float2(0, 1.f - diffuseB));

  return
    max(
      saturate(
        toonDif
        * float4(diffuse, alpha)
        * texColor
        * sph.Sample(smp, sphereMapUV)
      )
      + saturate(
        spa.Sample(smp, sphereMapUV) * texColor
        + float4(specularB * specular.rgb, 1)
      )
      , float4(texColor * ambient, 1)
    );
}

// 頂点シェーダ(アウトライン).
VSOutput VS_Outline(VSInput In)
{
  VSOutput output;
  float4 pos = TransformWorldPosition(In.pos, In);
  float4x4 mtxVP = mul(matPersProj, matView);
  pos = mul(mtxVP, pos);
  // 輪郭線.
  // 0が有効の意味になっているので注意.
  if (In.edge == 0)
  {
    float4 basePos    = pos;
    float4 offseted   = float4(In.pos.xyz + In.normal, 1);
    float4 outlinePos = mul(mtxVP, TransformWorldPosition(offseted, In));
    float4 vec        = normalize(outlinePos - basePos);
    pos = basePos + vec * 0.001 * basePos.w;
  }
  output.svpos = pos;
  return output;
}

// ピクセルシェーダ(アウトライン).
float4 PS_Outline(VSOutput input) : SV_TARGET
{
  return float4(0.f,0.f,0.f,1.f);
}
