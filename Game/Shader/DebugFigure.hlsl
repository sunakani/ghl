
struct VSOutput {
  float4 svpos  :SV_POSITION;
};

// b.
cbuffer cbuff0          : register(b0)
{
  matrix  matWorld;
  matrix  matView;
  matrix  matProj;
  float3  diffuse;
  float   alpha;
  float   thickness;
};

VSOutput VS(
  float4 pos        : POSITION
)
{
  VSOutput output;
  pos = mul(matWorld, pos);
  pos = mul(matView, pos);
  pos = mul(matProj, pos);
  output.svpos = pos;
  return output;
}

VSOutput VS_Line(
  float4 pos        : POSITION
)
{
  VSOutput output;
  pos = mul(matView, pos);
  pos = mul(matProj, pos);
  output.svpos = pos;
  return output;
}

struct GSOutput
{
	float4 pos  : SV_POSITION;
};

[maxvertexcount(6)]
void GS_Line(
  line VSOutput input[2],
  inout TriangleStream<GSOutput> output
)
{
  float4 from = input[0].svpos;
  float4 to   = input[1].svpos;
  float2 dir      = normalize(input[1].svpos.xy - input[0].svpos.xy);
  float4 cross_up = float4(-dir.y,  dir.x, 0.f, 0.f);

  float offset = thickness * 0.1f;
  {
    GSOutput element;
    element.pos = from + cross_up * offset;
    output.Append(element);
  }
  {
    GSOutput element;
    element.pos = from - cross_up * offset;
    output.Append(element);
  }
  {
    GSOutput element;
    element.pos = to + cross_up * offset;
    output.Append(element);
  }
  output.RestartStrip();
  {
    GSOutput element;
    element.pos = to + cross_up * offset;
    output.Append(element);
  }
  {
    GSOutput element;
    element.pos = from - cross_up * offset;
    output.Append(element);
  }
  {
    GSOutput element;
    element.pos = to - cross_up * offset;
    output.Append(element);
  }
  output.RestartStrip();
	//for (int i=0; i<2; i++)
	//{
  //  float offset = thickness / 2.0f;
  //  {
  //    GSOutput element;
  //    element.pos = input[i].svpos + float4(0.f, offset, 0.0f, 0.0f);
  //    output.Append(element);
  //  }
  //  {
  //    GSOutput element;
  //    element.pos = input[i].svpos + float4(0.f, -offset, 0.0f, 0.0f);
  //    output.Append(element);
  //  }
  //  {
  //    GSOutput element;
  //    element.pos = input[(i + 1) % 2].svpos + float4(0.f, offset * sign(i - 1), 0.0f, 0.0f);
  //    output.Append(element);
  //  }
  //  output.RestartStrip();
	//}
}

float4 PS(VSOutput input) : SV_TARGET
{
  return float4(diffuse, alpha);
}
