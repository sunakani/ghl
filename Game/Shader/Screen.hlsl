
Texture2D<float4> tex       : register(t0);
Texture2D<float4> texDepth  : register(t1);
SamplerState      smp       : register(s0);

struct VSOutput {
  float4 svpos : SV_POSITION;
  float2 uv    : TEXCOORD;
};

VSOutput VS(
  float4 pos        : POSITION,
  float2 uv         : TEXCOORD
)
{
  VSOutput op;
  op.svpos = pos;
  op.uv = uv;
  return op;
}

float4 PS(VSOutput input) : SV_TARGET
{
  // テクスチャ情報.
  float w, h, levels;
  tex.GetDimensions(0, w, h, levels);
  float dx = 1.f / w;
  float dy = 1.f / h;

  // 基本カラー.
  float4 color = tex.Sample(smp, input.uv);

  //float dep = pow(color, 0.2f);
  //color = float4(dep, dep, dep, 1.f);

  // グレースケール.
  //float Y = dot(color.rgb, float3(0.299, 0.587, 0.114));
  //color = float4(Y,Y,Y,Y);

  // 反転.
  //color = float4(1.f - color.r, 1.f - color.g, 1.f - color.b, color.a);

  // 階調調整.
  //color = float4(color.rgb - fmod(color.rgb, 0.25), color.a);

  // ぼかし.
  //float4 tmp = float4(0,0,0,0);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx, -2*dy));
  //tmp += tex.Sample(smp, input.uv + float2(    0, -2*dy));
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx, -2*dy));
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx,     0));
  //tmp += tex.Sample(smp, input.uv + float2(    0,     0));
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx,     0));
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx,  2*dy));
  //tmp += tex.Sample(smp, input.uv + float2(    0,  2*dy));
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx,  2*dy));
  //color = tmp / 9.f;

  // エンボス.
  //float4 tmp = float4(0,0,0,0);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx, -2*dy)) * ( 2);
  //tmp += tex.Sample(smp, input.uv + float2(    0, -2*dy)) * ( 1);
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx, -2*dy)) * ( 0);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx,     0)) * ( 1);
  //tmp += tex.Sample(smp, input.uv + float2(    0,     0)) * ( 1);
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx,     0)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx,  2*dy)) * ( 0);
  //tmp += tex.Sample(smp, input.uv + float2(    0,  2*dy)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx,  2*dy)) * (-2);
  //float Y = dot(tmp.rgb, float3(0.299, 0.587, 0.114));
  //color = float4(Y,Y,Y,color.a);

  // シェープネス.
  //float4 tmp = float4(0,0,0,0);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx, -2*dy)) * ( 0);
  //tmp += tex.Sample(smp, input.uv + float2(    0, -2*dy)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx, -2*dy)) * ( 0);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx,     0)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2(    0,     0)) * ( 5);
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx,     0)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx,  2*dy)) * ( 0);
  //tmp += tex.Sample(smp, input.uv + float2(    0,  2*dy)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx,  2*dy)) * ( 0);
  //float Y = dot(tmp.rgb, float3(0.299, 0.587, 0.114));
  //color = float4(Y,Y,Y,color.a);

  // エッジ抽出.
  //float4 tmp = float4(0,0,0,0);
  //tmp += tex.Sample(smp, input.uv + float2(    0, -2*dy)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2(-2*dx,     0)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2(    0,     0)) * ( 4);
  //tmp += tex.Sample(smp, input.uv + float2( 2*dx,     0)) * (-1);
  //tmp += tex.Sample(smp, input.uv + float2(    0,  2*dy)) * (-1);
  //float Y = dot(tmp.rgb, float3(0.299, 0.587, 0.114));
  //Y = pow(1.f - Y, 10.f);
  //Y = step(0.2f, Y);
  //color = float4(Y,Y,Y,color.a);

  // 最終加工.
  float4 modified = color;

  return modified;
}

