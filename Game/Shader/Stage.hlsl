
struct VSInputFloor {
  float4      pos     : POSITION;
  float4      normal  : NORMAL;
  float2      uv      : TEXCOORD;
};

struct VSOutputFloor {
  float4  svpos   : SV_POSITION;
  float4  normal  : NORMAL0;
  float4  vnormal : NORMAL1;
  float2  uv      : TEXCOORD;
  float4  tpos    : TPOS;
};

// t.
Texture2D<float4> texShadowMap : register(t0);
Texture2D<float4> texColor     : register(t1);

// s.
SamplerComparisonState smpShadowMap : register(s0);
SamplerState           smp          : register(s1);

// b.
cbuffer cbuff0          : register(b0)
{
  matrix matWorld;
  matrix matView;
  matrix matProj;
  matrix matLightCamera;
};

// 頂点シェーダ(床).
VSOutputFloor VS_Floor(VSInputFloor In)
{
  VSOutputFloor output;
  float4 pos = mul(matWorld, In.pos);
  output.tpos = mul(matLightCamera, pos);
  pos = mul(matView, pos);
  pos = mul(matProj, pos);
  output.svpos = pos;
  output.uv = In.uv;
  return output;
}

// ピクセルシェーダ(床).
float4 PS_Floor(VSOutputFloor input) : SV_TARGET
{
  float4 color = texColor.Sample(smp, input.uv);

  float3 posFromLightVP = input.tpos.xyz / input.tpos.w;
  float2 shadowUV = (posFromLightVP.xy + float2(1,-1)) * float2(0.5,-0.5);
  float value = posFromLightVP.z - 0.005f;
  float depthFromLight = texShadowMap.SampleCmp(smpShadowMap, shadowUV, value);
  float shadowWeight = lerp(0.5f, 1.f, depthFromLight);

  color *= shadowWeight;

  return color;
}
