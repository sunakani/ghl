#include "StageManager.h"
#include "LoadFile.h"

CStageManager::CStageManager()
  : m_root_signature()
  , m_pso()
  , m_floor()
{}
void CStageManager::Init(const SGameInfo& info)
{
	InitPSO();
  InitFloor(info);
}
void CStageManager::Term(const SGameInfo& info)
{
  TermFloor(info);
	TermPSO();
}
void CStageManager::InitPSO()
{
	// ルートシグネチャ.
  ghl::SCreateRootSignatureParam root_sig_param;
  {
    auto& rp = root_sig_param.root_parameters;
    rp.resize(2);
    rp[0].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 0, 1});  // b0.
    rp[1].table.push_back({ghl::EDescriptorType::eShaderResourceView, 0, 0, 2});  // t0,t1.
    auto& smp = root_sig_param.sampler_parameters;
    smp.push_back({0, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp}); // s0.
    smp.push_back({1, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp}); // s1.
  }
  m_root_signature.hRootSignature = gi().RootSig_Create(root_sig_param);

  // PSO.1231
  auto& pso = m_pso;

  // シェーダ.
  LPCWSTR shader_dir = L"Shader\\";
  WCHAR path[256];
  swprintf_s(path, L"%s\\%s", shader_dir, L"Stage.hlsl");
  pso.hVS = gi().Shader_CreateByCompileFile(path, "VS_Floor", "vs_5_0");
  pso.hPS = gi().Shader_CreateByCompileFile(path, "PS_Floor", "ps_5_0");

  // パイプラインステート.
  ghl::SCreatePiplelineStateParam pipeline_state_param;
  {
    auto& vi = pipeline_state_param.vertex_input_rayout_parameters;
    vi.push_back({"POSITION", 0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0});
    vi.push_back({"NORMAL",   0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0});
    vi.push_back({"TEXCOORD", 0, ghl::EGraphInfraFormat::eR32G32_Float,    0});
    pipeline_state_param.hVS = pso.hVS;
    pipeline_state_param.hPS = pso.hPS;
    pipeline_state_param.hRootSignature = m_root_signature.hRootSignature;
    pipeline_state_param.eFillMode = ghl::EFillMode::eSolid;
    pipeline_state_param.eCullMode = ghl::ECullMode::eNone;
    pipeline_state_param.bEnableBlend = true;
  }
  pso.hState = gi().PipeState_Create(pipeline_state_param);
}
void CStageManager::TermPSO()
{
  m_pso.Destroy();
  m_root_signature.Destroy();
}
void CStageManager::InitFloor(const SGameInfo& info)
{
  constexpr float length = 1000.f;
  SStageFloorVertex vertices[] = {
    { {-length,  0.f,  length },  { 0.f,  1.f,  0.f },  { 0.f,  0.f } },
    { {-length,  0.f, -length },  { 0.f,  1.f,  0.f },  { 0.f,  1.f } },
    { { length,  0.f,  length },  { 0.f,  1.f,  0.f },  { 1.f,  0.f } },
    { { length,  0.f, -length },  { 0.f,  1.f,  0.f },  { 1.f,  1.f } },
  };
  m_floor.hVB = gi().Vertex_CreateBuffer(sizeof(vertices[0]), _countof(vertices));
  m_floor.hVV = gi().Vertex_CreateView(m_floor.hVB, sizeof(vertices[0]), _countof(vertices));
  gi().Vertex_UpdateBuffer(m_floor.hVB, vertices, sizeof(vertices[0]), _countof(vertices));

  // ヒープ.
  m_floor.hDescHeap = gi().DescHeap_Create(ghl::EDescriptorHeapType::eCBV_SRV_UAV, 3, true);

  // ヒープ[0]:定数バッファ.
  m_floor.hCB = gi().Const_CreateBuffer(sizeof(SConstantBufferForStageFloor));
  gi().DescHeap_PlaceConstView(m_floor.hDescHeap, m_floor.hCB, 0);

  // ヒープ[1]:シャドウマップテクスチャ.
  gi().DescHeap_PlaceTextureView(m_floor.hDescHeap, info.hShadowDepthBuffer, 1, ghl::EGraphInfraFormat::eR32_Float);

  // カラーテクスチャ.
  if (auto* pTexture = info.pLoadManager->LoadTexture(ELoadFile::STAGE_FLOOR_TEX, info)) {
    m_floor.hTex = pTexture->GetBuffer();
  }
  else {
    m_floor.hTex = gi().Texture_GetWhite();
  }
  gi().DescHeap_PlaceTextureView(m_floor.hDescHeap, m_floor.hTex, 2);
}
void CStageManager::TermFloor(const SGameInfo& info)
{
  if (m_floor.hVB) {
    if (m_floor.hVV) {
      gi().Vertex_DestroyView(m_floor.hVB, m_floor.hVV);
    }
    gi().Vertex_DestroyBuffer(m_floor.hVB);
  }
  if (m_floor.hCB) {
    gi().Const_DestroyBuffer(m_floor.hCB);
  }
  if (m_floor.hTex) {
    info.pLoadManager->UnloadTexture(ELoadFile::STAGE_FLOOR_TEX);
    m_floor.hTex.Invalidate();
  }
  if (m_floor.hDescHeap) {
    gi().DescHeap_Destroy(m_floor.hDescHeap);
  }
}
void CStageManager::Update(SGameUpdateParam& update_param)
{
  m_floor.cbuffer.matView     = update_param.matView;
  m_floor.cbuffer.matPersProj = update_param.matPersProj;
  m_floor.cbuffer.matWorld    = DirectX::XMMatrixIdentity();
  m_floor.cbuffer.matLightCamera = update_param.matLightCamera;
  gi().Const_UpdateBuffer(m_floor.hCB, &m_floor.cbuffer, sizeof(m_floor.cbuffer));
}
void CStageManager::Draw()
{
  gi().Cmd_RootSig_Set(m_root_signature.hRootSignature);
  gi().Cmd_Set_PrimitiveTopology(ghl::EPrimitiveTopology::eTriangleStrip);

  gi().Cmd_PipeState_Set(m_pso.hState);
  if (m_floor.hVB && m_floor.hVV)
  {
    gi().Cmd_Vertex_Set(m_floor.hVB, m_floor.hVV);
    gi().Cmd_DescHeap_Set(m_floor.hDescHeap);
    gi().Cmd_DescHeap_SetRootParam(m_floor.hDescHeap, 0, 0);
    gi().Cmd_DescHeap_SetRootParam(m_floor.hDescHeap, 1, 1);
    gi().Cmd_Draw_Instanced(4, 1, 0, 0);
  }
}
