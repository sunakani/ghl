﻿#include "Game.h"
#include "Game_defs.h"
#include "Graphic.h"
#include "Memory.h"

#include "DebugMenu.h"

#include "imgui_impl_win32.h"
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

CGameObject::CGameObject()
  : m_game_info()
  , m_camera()
  , m_light()
  , m_fAspect(1.f)
  , m_file_path_manager()
  , m_load_manager()
  , m_character_manager()
  , m_debug_figure()
  , m_stage_manager()
  , m_bEnableVSync(true)
  , m_update_count(0)
  , m_update_tick_time(0.f)
  , m_update_fps(0.f)
  , m_draw_tick_time(0.f)
  , m_draw_fps(0.f)
  , m_frequency()
  , m_player_manager()
  , m_local_player_index(EPlayerIndex::None)
  , m_screen_object_manager()
  , m_eGameDisplayMode(EGameDisplayMode::eWithDebugDialog)
  , m_pso_chara()
  , m_pso_chara_outline()
  , m_pso_shadow()
  , m_rs_chara()
  , m_rs_shadow()
  , m_viewport_default()
  , m_viewport_shadow()
  , m_scissorrect_default()
  , m_scissorrect_shadow()
  , m_hSceneDepthBuffer()
  , m_hSceneRenderTargetBuffer()
  , m_hSceneDescHeapForDSV()
  , m_hSceneDescHeapForRTV()
  , m_hScreenDepthBuffer()
  , m_hScreenDescHeapForDSV()
  , m_hShadowDepthBuffer()
{}

CGameObject::~CGameObject()
{}

void CGameObject::Init(HWND hWnd)
{
  // ライブラリ初期化パラメータ.
  ghl::SInitParam initParam;
  initParam.width           = 1920;
  initParam.height          = 1080;
  initParam.hWnd            = hWnd;
  initParam.data_dir        = L"Data\\";
  initParam.unBackBufferNum = 2;

  // Windowサイズ調整.
  {
    RECT rc1;
    RECT rc2;
    GetWindowRect(hWnd, &rc1);
    GetClientRect(hWnd, &rc2);
    LONG sx = initParam.width;
    LONG sy = initParam.height;
    sx += ((rc1.right - rc1.left) - (rc2.right - rc2.left));
    sy += ((rc1.bottom - rc1.top) - (rc2.bottom - rc2.top));

    // Windowが画面の中央に来るように移動.
    RECT monitor_rect{};
    SystemParametersInfo(SPI_GETWORKAREA,0,&monitor_rect,0);
    int x = std::max(static_cast<int>((monitor_rect.right - monitor_rect.left) / 2 - (rc1.right - rc1.left) / 2), 0);
    int y = std::max(static_cast<int>((monitor_rect.bottom - monitor_rect.top) / 2 - (rc1.bottom - rc1.top) / 2), 0);
    SetWindowPos(hWnd, NULL, x, y, sx, sy, SWP_SHOWWINDOW);
  }

  // ライブラリ生成.
  ghl::InitLibrary(initParam);

  uint16_t shadowmap_size = 1024;

  // ビューポート.
  m_viewport_default.Width    = static_cast<float>(initParam.width);
  m_viewport_default.Height   = static_cast<float>(initParam.height);
  m_viewport_default.TopLeftX = 0;
  m_viewport_default.TopLeftY = 0;
  m_viewport_default.MaxDepth = 1.f;
  m_viewport_default.MinDepth = 0.f;
  m_viewport_shadow.Width    = static_cast<float>(shadowmap_size);
  m_viewport_shadow.Height   = static_cast<float>(shadowmap_size);
  m_viewport_shadow.TopLeftX = 0;
  m_viewport_shadow.TopLeftY = 0;
  m_viewport_shadow.MaxDepth = 1.f;
  m_viewport_shadow.MinDepth = 0.f;

  // シザー矩形.
  m_scissorrect_default.top     = 0;
  m_scissorrect_default.left    = 0;
  m_scissorrect_default.right   = m_scissorrect_default.left  + initParam.width;
  m_scissorrect_default.bottom  = m_scissorrect_default.top   + initParam.height;
  m_scissorrect_shadow.top     = 0;
  m_scissorrect_shadow.left    = 0;
  m_scissorrect_shadow.right   = m_scissorrect_shadow.left  + shadowmap_size;
  m_scissorrect_shadow.bottom  = m_scissorrect_shadow.top   + shadowmap_size;

  // グラフィックパイプライン.
  InitCharaPipeline();
  InitShadowPipeline();

  // 深度バッファ.
  m_hSceneDepthBuffer     = gi().Texture_CreateDepthStencilBuffer(initParam.width, initParam.height, ghl::EGraphInfraFormat::eR32_Typeless);
  m_hShadowDepthBuffer    = gi().Texture_CreateDepthStencilBuffer(shadowmap_size, shadowmap_size, ghl::EGraphInfraFormat::eR32_Typeless);
  m_hSceneDescHeapForDSV  = gi().DescHeap_Create(ghl::EDescriptorHeapType::eDSV, 2, false);
  gi().DescHeap_PlaceDepthStencilView(m_hSceneDescHeapForDSV, m_hSceneDepthBuffer,  0, ghl::EGraphInfraFormat::eD32_Float);
  gi().DescHeap_PlaceDepthStencilView(m_hSceneDescHeapForDSV, m_hShadowDepthBuffer, 1, ghl::EGraphInfraFormat::eD32_Float);
  m_hScreenDepthBuffer    = gi().Texture_CreateDepthStencilBuffer(initParam.width, initParam.height, ghl::EGraphInfraFormat::eD32_Float);
  m_hScreenDescHeapForDSV = gi().DescHeap_Create(ghl::EDescriptorHeapType::eDSV, 1, false);
  gi().DescHeap_PlaceDepthStencilView(m_hScreenDescHeapForDSV, m_hScreenDepthBuffer, 0, ghl::EGraphInfraFormat::eD32_Float);

  // レンダーターゲット.
  ghl::SCreateTextureBufferParam texCreateParam = {
    initParam.width,
    initParam.height,
    ghl::EResourceHeapType::eDefault,
    ghl::EResourceCpuPageProp::eUnknown,
    ghl::EResourceMemoryPool::eUnknown,
    ghl::EGraphInfraFormat::eR8G8B8A8_UNorm,
    ghl::eRES_FLAG_ALLOW_RENDER_TARGET,
    ghl::EResourceState::ePixcelShaderResource,
    &GetClearColor()
  };
  m_hSceneRenderTargetBuffer = gi().Texture_CreateBuffer(texCreateParam);
  m_hSceneDescHeapForRTV = gi().DescHeap_Create(ghl::EDescriptorHeapType::eRTV, 1, false);
  gi().DescHeap_PlaceRenderTargetView(m_hSceneDescHeapForRTV, m_hSceneRenderTargetBuffer, 0);

  // アスペクト比.
  m_fAspect = static_cast<float>(initParam.width) / static_cast<float>(initParam.height);

  // VSync.
  m_bEnableVSync = true;

  QueryPerformanceFrequency(&m_frequency);

  // 各管理者初期化.
  m_file_path_manager.Init();
  m_load_manager.Init();
  m_character_manager.Init();
  m_debug_figure.Init();
  m_player_manager.Init();
  m_player_manager.ActivatePlayer(EPlayerIndex::P1);
  m_local_player_index = EPlayerIndex::P1;

  // DebugMenu.
  m_debug_menu.Init(hWnd);

  // ゲーム情報.
  m_game_info.viewport_width      = m_viewport_default.Width;
  m_game_info.viewport_height     = m_viewport_default.Height;
  m_game_info.hWnd                = hWnd;
  m_game_info.hShadowDepthBuffer  = m_hShadowDepthBuffer;
  m_game_info.hSceneRenderTarget  = m_hSceneRenderTargetBuffer;
  m_game_info.hSceneDepthBuffer   = m_hSceneDepthBuffer;
  m_game_info.pFilePathManager    = &m_file_path_manager;
  m_game_info.pLoadManager        = &m_load_manager;
  m_game_info.pDebugFigure        = &m_debug_figure;
  m_game_info.pPlayerManager      = &m_player_manager;
  m_game_info.pCharacterManager   = &m_character_manager;
  m_game_info.pDebugMenu          = &m_debug_menu;

  // カメラ.
  m_camera.Init();

  // ライト.
  m_light.Init();

  // 配置情報.
  if (auto* pPlacementDataPath = m_file_path_manager.GetPath(ELoadFile::DB_PLACE_CHARA)) {
    std::ifstream ifs(pPlacementDataPath, std::ios::in | std::ios::binary);
    _DEBUG_BREAK(ifs);
    if (ifs) {
    	ghl::SBinary bin;
      ifs.seekg(0, std::ios::end);
      size_t size = ifs.tellg();
      bin.bin.resize(size);
      ifs.seekg(0, std::ios::beg);
      ifs.read(bin.bin.data(),size);
      ifs.close();
      m_db_character_placement.Init(bin.bin);
    }
  }

  // キャラ準備.
  for (size_t i=0; i<m_db_character_placement.Num(); ++i) {
    if (auto* pData = m_db_character_placement.GetByIndex(i)) {
      auto eChara = static_cast<ECharacter>(pData->CharaType);
      m_character_manager.LoadCharacter(eChara, m_game_info);
    }
  }

  // キャラ配置.
  // 配置座標については。上方向がY+、奥行き方向がZ+の左手系.
  for (size_t i=0; i<m_db_character_placement.Num(); ++i) {
    if (auto* pData = m_db_character_placement.GetByIndex(i)) {
      SCharacterCreateParam charaCreateParam;
      charaCreateParam.eId = static_cast<ECharacter>(pData->CharaType);
      charaCreateParam.initial_pos = DirectX::XMFLOAT3(
        static_cast<float>(pData->initial_pos_x),
        static_cast<float>(pData->initial_pos_y),
        static_cast<float>(pData->initial_pos_z));
      charaCreateParam.initial_rot_y  = DirectX::XMConvertToRadians(pData->initial_angle_y);
      m_character_manager.CreateCharacter(charaCreateParam, m_game_info);
    }
  }
  m_character_manager.SetPlayerByIndex(0, m_local_player_index);  // 先頭キャラを操作プレイヤーとする.

  // プレイヤーとカメラの関連付け.
  m_camera.SetTargetPlayer(m_local_player_index);

  // ステージ.
  m_stage_manager.Init(m_game_info);

  // スクリーンオブジェクト(2D).
  m_screen_object_manager.Init(m_game_info, m_eGameDisplayMode);
}

void CGameObject::Term()
{
  if (m_hSceneRenderTargetBuffer) {
    gi().Texture_DestroyBuffer(m_hSceneRenderTargetBuffer);
  }
  if (m_hSceneDescHeapForRTV) {
    gi().DescHeap_Destroy(m_hSceneDescHeapForRTV);
  }

  TermCharaPipeline();
  TermShadowPipeline();

  // 深度バッファ.
  if (m_hSceneDepthBuffer) {
    gi().Texture_DestroyBuffer(m_hSceneDepthBuffer);
  }
  if (m_hShadowDepthBuffer) {
    gi().Texture_DestroyBuffer(m_hShadowDepthBuffer);
  }
  if (m_hSceneDescHeapForDSV) {
    gi().DescHeap_Destroy(m_hSceneDescHeapForDSV);
  }
  if (m_hScreenDepthBuffer) {
    gi().Texture_DestroyBuffer(m_hScreenDepthBuffer);
  }
  if (m_hScreenDescHeapForDSV) {
    gi().DescHeap_Destroy(m_hScreenDescHeapForDSV);
  }

  m_light.Term();
  m_camera.Term();

  for (size_t i=0; i<m_db_character_placement.Num(); ++i) {
    if (auto* pData = m_db_character_placement.GetByIndex(i)) {
      auto eChara = static_cast<ECharacter>(pData->CharaType);
      m_character_manager.UnloadCharacter(eChara, m_game_info);
    }
  }
  m_character_manager.Term(m_game_info);

  m_screen_object_manager.Term(m_game_info);

  m_stage_manager.Term(m_game_info);

  m_load_manager.Term();
  m_file_path_manager.Term();
  m_debug_figure.Term();
  m_player_manager.Term();

  m_db_character_placement.Term();

  m_debug_menu.Term();

  // ライブラリ破棄.
  ghl::TermLibrary();
}

/**************************************************/
/*	キャラ用のPSO.
***************************************************/
void CGameObject::InitCharaPipeline()
{
  // ルートシグネチャ.
  ghl::SCreateRootSignatureParam root_sig_param;
  {
    auto& rp = root_sig_param.root_parameters;
    rp.resize(3);
    rp[0].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 0, 1}); // b0(ビュー行列など).
    rp[1].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 1, 1}); // b1(キャラ).
    rp[2].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 2, 1}); // b2(キャラマテリアル).
    rp[2].table.push_back({ghl::EDescriptorType::eShaderResourceView, 1, 0, e2ut(EIndexOfSRV::Num)}); // t0,t1,t2,t3,t4.

    // ◆ルートパラメータとレンジについて漸く理解できたので熱くメモ.
    // まずディスクリプタヒープはCBV_SRV_UAV、Sampler、RTV、DSVと複数の種類があるが、
    // 同じ種類のヒープを同時にセットすることができない.
    // 例えば、ヒープ0がCBV_SRV_UAV、ヒープ1がCBV_SRV_UAVの時に0と1を同時にセットすると未定義動作となる.
    // そこで、ヒープ0をCBV_SRV_UAVとして、各種ディスクリプタは基本的にここに配置する.
    // しかし、ヒープ上にある複数のディスクリプタのうち、どこからどこまでをシェーダで何として扱ってほしいかがまだわからないので、
    // それを指定してあげる必要がある.
    // この時必要な情報は、ディスクリプタの先頭アドレスと、そこから何個分をシェーダのレジスターどこからアクセスできるようにするか、という情報である.
    // なので、情報を指定するための関数には２つ引数があり、１つが先頭アドレスとなる.
    // これは、ヒープの先頭からディスクリプタ分のオフセットを指定してあげることで、そこが先頭となる.
    // そして、その先頭から何個分をどのレジスタに割り当てるかについては、
    // 予めテーブルに詳細を記載しておき、それを指定することで適用できる.
    // このテーブルを複数まとめて保持しているのがルートパラメータである.
    // 具体的には、
    //  指定した先頭から１つ分を定数バッファとし、b0を割り当てる.
    //  指定した先頭から１つ分を定数バッファとし、b1を割り当てる.
    //  指定した先頭から１つ分を定数バッファとし、さらに指定した先頭から１つオフセットを足した部分から４つ分をテクスチャとする.
    // といった感じ。

    auto& smp = root_sig_param.sampler_parameters;
    smp.push_back({0, ghl::ETextureAddressMode::eWrap,  ghl::ETextureAddressMode::eWrap,  ghl::ETextureAddressMode::eWrap }); // s0.
    smp.push_back({1, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp}); // s1.
    smp.push_back({2, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp, ghl::ETextureAddressMode::eClamp, ghl::EFilter::eCompMinMagMipLinear, ghl::EComparisonFunc::eLessEqual, 1 }); // s2.
  }
  m_rs_chara.hRootSignature = gi().RootSig_Create(root_sig_param);

  // 頂点レイアウト.
  ghl::SVertexInputRayout vert_rayout[] = {
    {"POSITION", 0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0}, //12.
    {"NORMAL",   0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0}, //12.
    {"TEXCOORD", 0, ghl::EGraphInfraFormat::eR32G32_Float,    0}, //8.
    {"BONENO",   0, ghl::EGraphInfraFormat::eR16G16_UInt,     0}, //4.
    {"WEIGHT",   0, ghl::EGraphInfraFormat::eR8_UInt,         0}, //1.
    {"EDGEFLAG", 0, ghl::EGraphInfraFormat::eR8_UInt,         0}, //1.
  };

  // PSO(通常).
  {
    auto& pso = m_pso_chara;
    // シェーダ.
    LPCWSTR shader_dir = L"Shader\\";
    WCHAR path[256];
    swprintf_s(path, L"%s\\%s", shader_dir, L"Chara.hlsl");
    pso.hVS = gi().Shader_CreateByCompileFile(path, "VS", "vs_5_0");
    swprintf_s(path, L"%s\\%s", shader_dir, L"Chara.hlsl");
    pso.hPS = gi().Shader_CreateByCompileFile(path, "PS", "ps_5_0");
    ghl::SCreatePiplelineStateParam pipeline_state_param;
    {
      for (auto& elem : vert_rayout) {
        pipeline_state_param.vertex_input_rayout_parameters.push_back(elem);
      }
      pipeline_state_param.hVS = pso.hVS;
      pipeline_state_param.hPS = pso.hPS;
      pipeline_state_param.hRootSignature = m_rs_chara.hRootSignature;
      pipeline_state_param.eFillMode = ghl::EFillMode::eSolid;
      pipeline_state_param.eCullMode = ghl::ECullMode::eNone;   // 服の裏側などで必要なので裏面も描画.
      pipeline_state_param.bEnableBlend = true;
    }
    pso.hState = gi().PipeState_Create(pipeline_state_param);
  }

  // PSO(輪郭).
  {
    auto& pso = m_pso_chara_outline;
    // シェーダ.
    LPCWSTR shader_dir = L"Shader\\";
    WCHAR path[256];
    swprintf_s(path, L"%s\\%s", shader_dir, L"Chara.hlsl");
    pso.hVS = gi().Shader_CreateByCompileFile(path, "VS_Outline", "vs_5_0");
    swprintf_s(path, L"%s\\%s", shader_dir, L"Chara.hlsl");
    pso.hPS = gi().Shader_CreateByCompileFile(path, "PS_Outline", "ps_5_0");
    ghl::SCreatePiplelineStateParam pipeline_state_param;
    {
      for (auto& elem : vert_rayout) {
        pipeline_state_param.vertex_input_rayout_parameters.push_back(elem);
      }
      pipeline_state_param.hVS = pso.hVS;
      pipeline_state_param.hPS = pso.hPS;
      pipeline_state_param.hRootSignature = m_rs_chara.hRootSignature;
      pipeline_state_param.eFillMode = ghl::EFillMode::eSolid;
      pipeline_state_param.eCullMode = ghl::ECullMode::eFront;  // 裏面だけ必要.
    }
    pso.hState = gi().PipeState_Create(pipeline_state_param);
  }
}
void CGameObject::TermCharaPipeline()
{
  m_pso_chara.Destroy();
  m_pso_chara_outline.Destroy();
  m_rs_chara.Destroy();
}

/**************************************************/
/*	シャドウマップ用のPSO.
***************************************************/
void CGameObject::InitShadowPipeline()
{
  // ルートシグネチャ.
  ghl::SCreateRootSignatureParam root_sig_param;
  {
    auto& rp = root_sig_param.root_parameters;
    rp.resize(3);
    rp[0].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 0, 1}); // b0(ビュー行列など).
    rp[1].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 1, 1}); // b1(キャラ).
    rp[2].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 2, 1}); // b2(キャラマテリアル).
    auto& smp = root_sig_param.sampler_parameters;
  }
  m_rs_shadow.hRootSignature = gi().RootSig_Create(root_sig_param);

  // PSO.
  auto& pso = m_pso_shadow;

  // シェーダ.
  LPCWSTR shader_dir = L"Shader\\";
  WCHAR path[256];
  swprintf_s(path, L"%s\\%s", shader_dir, L"Shadow.hlsl");
  pso.hVS = gi().Shader_CreateByCompileFile(path, "VS", "vs_5_0");

  // パイプラインステート.
  ghl::SCreatePiplelineStateParam pipeline_state_param;
  {
    auto& vi = pipeline_state_param.vertex_input_rayout_parameters;
    vi.push_back({"POSITION", 0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0});
    vi.push_back({"NORMAL",   0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0});
    vi.push_back({"TEXCOORD", 0, ghl::EGraphInfraFormat::eR32G32_Float,    0});
    vi.push_back({"BONENO",   0, ghl::EGraphInfraFormat::eR16G16_UInt,     0});
    vi.push_back({"WEIGHT",   0, ghl::EGraphInfraFormat::eR8_UInt,         0});
    pipeline_state_param.hVS = pso.hVS;
    pipeline_state_param.hPS = pso.hPS;
    pipeline_state_param.hRootSignature = m_rs_shadow.hRootSignature;
    pipeline_state_param.unRenderTargetNum = 0;
    pipeline_state_param.eGIFormatRT = ghl::EGraphInfraFormat::eUnknown;
    pipeline_state_param.bEnableBlend = true;
  }
  pso.hState = gi().PipeState_Create(pipeline_state_param);
}
void CGameObject::TermShadowPipeline()
{
  m_pso_shadow.Destroy();
  m_rs_shadow.Destroy();
}

// 毎フレの更新.
void CGameObject::Update(SGameUpdateIO& io)
{
  // ライブラリの更新関数を呼び出すと、内部で求めた更新時間を引数にして登録関数をコールバックしてくれる.
  ghl::UpdateLibrary([this, &io](float fDeltaTime) {
    CallbackUpdate(fDeltaTime, io);
    });
}

// 毎フレの更新.
// 大きくゲーム側の更新と描画更新に分かれる.
void CGameObject::CallbackUpdate(float fDeltaTime, SGameUpdateIO& io)
{
  ++m_update_count;
  // 計測.
  LARGE_INTEGER begin_update;
  QueryPerformanceCounter(&begin_update);
  // ロジック更新.
  Update(fDeltaTime, io);
  // 計測.
  LARGE_INTEGER end_update;
  QueryPerformanceCounter(&end_update);
  m_update_tick_time = static_cast<float>(end_update.QuadPart - begin_update.QuadPart) / m_frequency.QuadPart;
  if (m_update_tick_time > FLT_EPSILON) {
    m_update_fps = 1.f / m_update_tick_time;
  }
  // 中断時はここで抜ける.
  if (io.out.bQuit) {
    return;
  }
  // 計測.
  LARGE_INTEGER begin_draw;
  QueryPerformanceCounter(&begin_draw);
  // 描画.
  Draw();
  // 計測.
  LARGE_INTEGER end_draw;
  QueryPerformanceCounter(&end_draw);
  m_draw_tick_time = static_cast<float>(end_draw.QuadPart - begin_draw.QuadPart) / m_frequency.QuadPart;
  if (m_draw_tick_time > FLT_EPSILON) {
    m_draw_fps = 1.f / m_draw_tick_time;
  }
}

// プロシージャ.
void CGameObject::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam);
}

void CGameObject::Update(float fDeltaTime, SGameUpdateIO& io)
{
  // デバッグ表示.
  CDebugDisp::Inst().Clear();
  CDebugDisp::Inst().SetPosition(0, 0);
  CDebugDisp::Inst().SetColor({0.f, 0.f, 0.f, 1.f});
  CDebugDisp::Inst().Printf("[Frame ] Count=%u, FPS=%.2f\n", m_update_count, ghl::GetFps());
 
  CDebugDisp::Inst().SetPosition(0, 40);
  CDebugDisp::Inst().SetColor({0.f, 0.f, 0.f, 1.f});
  CDebugDisp::Inst().Printf("[Update] Delta=%.6f, FPS=%.2f\n", m_update_tick_time, m_update_fps);

  CDebugDisp::Inst().SetPosition(0, 80);
  CDebugDisp::Inst().SetColor({0.f, 0.f, 0.f, 1.f});
  CDebugDisp::Inst().Printf("[Draw  ] Delta=%.6f, FPS=%.2f\n", m_draw_tick_time, m_draw_fps);

  CDebugDisp::Inst().SetPosition(0, 120);
  CDebugDisp::Inst().SetColor({0.f, 0.f, 0.f, 1.f});
  CDebugDisp::Inst().Printf("[D-MEM ] B=%u, KB=%u, Count=%u\n", GetDefaultHeap().GetAllocatedSize(), GetDefaultHeap().GetAllocatedSize() / 1024, GetDefaultHeap().GetAllocatedCount());

  CDebugDisp::Inst().SetPosition(0, 160);
  CDebugDisp::Inst().SetColor({1.f, 1.f, 1.f, 1.f});
  CDebugDisp::Inst().Printf("VSYNC      : %s\n", m_bEnableVSync ? "ON" : "OFF");

  CDebugDisp::Inst().SetPosition(700, 0);
  CDebugDisp::Inst().SetColor({1.f, 0.8f, 0.3f, 1.f});
  CDebugDisp::Inst().Printf("W.A.S.D key : MOVE CHARA\n");

  CDebugDisp::Inst().SetPosition(700, 40);
  CDebugDisp::Inst().SetColor({1.f, 0.8f, 0.3f, 1.f});
  CDebugDisp::Inst().Printf("ESC key     : QUIT GAME\n");

  CDebugDisp::Inst().SetPosition(700, 80);
  CDebugDisp::Inst().SetColor({1.f, 0.8f, 0.3f, 1.f});
  CDebugDisp::Inst().Printf("O key       : TOGGLE DEBUG MODE\n");

  CDebugDisp::Inst().SetPosition(700, 120);
  CDebugDisp::Inst().SetColor({1.f, 0.8f, 0.3f, 1.f});
  CDebugDisp::Inst().Printf("MOUSE       : MOVE CAMERA\n");

  CDebugDisp::Inst().SetPosition(700, 160);
  CDebugDisp::Inst().SetColor({1.f, 0.8f, 0.3f, 1.f});
  CDebugDisp::Inst().Printf("MOUSE L     : ATTACK\n");

  // 更新引数.
  SGameUpdateParam update_param = {};
  update_param.fDeltaTime = fDeltaTime;
  update_param.pGameInfo = &m_game_info;
  update_param.pIO = &io;

  // 入力.
  m_player_manager.UpdateActivePlayerInput(update_param);
  if (auto* p1Input = m_player_manager.GetActivePlayerInput(EPlayerIndex::P1)) {
    // エスケープ.
    if (p1Input->IsKeybordKeyPressed(ghl::EKey::ESC)) {
      io.out.bQuit = true;
      return;
    }
    // 表示モード切替.
    if (p1Input->IsKeybordKeyPressed(ghl::EKey::O)) {
      auto prev = m_eGameDisplayMode;
      auto next =
        prev == EGameDisplayMode::eNormal ? EGameDisplayMode::eWithDebugDialog :
        prev == EGameDisplayMode::eWithDebugDialog ? EGameDisplayMode::eNormal :
        EGameDisplayMode::eNone;
      if (prev != next) {
        OnChangedGameDispMode(prev, next);
        m_eGameDisplayMode = next;
      }
    }
  }

  // デバッグ1.
  m_debug_figure.Update1st(update_param);

  // カメラ.
  m_camera.Update(update_param);
  update_param.pCamera = &m_camera;
  update_param.pLight = &m_light;
  {
    auto& cs = m_camera.GetStatus();
    update_param.matView = DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&cs.eye), DirectX::XMLoadFloat3(&cs.at), DirectX::XMLoadFloat3(&cs.up));
    update_param.matPersProj = DirectX::XMMatrixPerspectiveFovLH(cs.fov, cs.aspect, cs.clip_near, cs.clip_far);
    update_param.matOrthProj = DirectX::XMMatrixOrthographicLH(m_viewport_default.Width, m_viewport_default.Height, 0.f, 1.f);
  }
  
  // ライト.
  {
    using namespace DirectX;
    XMFLOAT4 planeVec(0.f, 1.f, 0.f, 0.f);
    XMFLOAT3 paraLightInverse = {-m_light.GetParallelLightDirection().x, -m_light.GetParallelLightDirection().y, -m_light.GetParallelLightDirection().z};
    {
      auto& cs = m_camera.GetStatus();
      auto targetPos = XMLoadFloat3(&cs.at);
      auto normLightDirection = XMVector3Normalize(XMLoadFloat3(&paraLightInverse));
      float scale = XMVector3Length(XMVectorSubtract(XMLoadFloat3(&cs.at), XMLoadFloat3(&cs.eye))).m128_f32[0];
      auto lightPos = XMVectorAdd(targetPos, XMVectorScale(normLightDirection, scale));
      XMVECTOR upVec = {0.f, 1.f, 0.f, 0.f};
      float w = 128.f;
      float h = 128.f;
      float clip_near = 1.f;
      float clip_far  = 100.f;
      update_param.matLightCamera = XMMatrixLookAtLH(lightPos, targetPos, upVec) * XMMatrixOrthographicLH(w, h, clip_near, clip_far);
    }
  }

  // キャラ.
  m_character_manager.Update(update_param);

  // ステージ.
  m_stage_manager.Update(update_param);

  // UI.
  m_screen_object_manager.Update(update_param);
  m_screen_object_manager.PreDraw(update_param);

  // デバッグ2.
  m_debug_figure.Update2nd(update_param);
}

void CGameObject::Draw()
{
  // 描画先を変更.
  gi().Cmd_RenderTarget_Set(ghl::HDescHeap(), 0, m_hSceneDescHeapForDSV, 1);
  gi().Cmd_DepthStencil_Clear(m_hSceneDescHeapForDSV, 1);
  gi().Cmd_Set_Viewports(m_viewport_shadow);
  gi().Cmd_Set_ScissorRects(m_scissorrect_shadow);
  gi().Cmd_Set_PrimitiveTopology(ghl::EPrimitiveTopology::eTriangleList);

  // シャドウマップ生成.
  {
    gi().Cmd_RootSig_Set(m_rs_shadow.hRootSignature);
    gi().Cmd_PipeState_Set(m_pso_shadow.hState);
    m_character_manager.DrawShadow();
  }

  // 描画先を変更.
  gi().Cmd_Texture_TransState(m_hSceneRenderTargetBuffer, ghl::EResourceState::ePixcelShaderResource, ghl::EResourceState::eRenderTarget);
  gi().Cmd_RenderTarget_Set(m_hSceneDescHeapForRTV, 0, m_hSceneDescHeapForDSV, 0);
  gi().Cmd_RenderTarget_Clear(m_hSceneDescHeapForRTV, GetClearColor());
  gi().Cmd_DepthStencil_Clear(m_hSceneDescHeapForDSV, 0);
  gi().Cmd_Set_Viewports(m_viewport_default);
  gi().Cmd_Set_ScissorRects(m_scissorrect_default);

  // シーン.
  {
    // ステージ.
    m_stage_manager.Draw();

    // キャラ.
    gi().Cmd_RootSig_Set(m_rs_chara.hRootSignature);
    gi().Cmd_Set_PrimitiveTopology(ghl::EPrimitiveTopology::eTriangleList);
    gi().Cmd_PipeState_Set(m_pso_chara.hState);
    m_character_manager.Draw();

    // キャラ輪郭.
    gi().Cmd_PipeState_Set(m_pso_chara_outline.hState);
    m_character_manager.DrawOutline();

    // デバッグ.
    m_debug_figure.Draw();

    // 2D.
    m_screen_object_manager.Draw();

    // デバッグ.
    CDebugDisp::Inst().Draw(m_viewport_default);
  }

  // 今までレンダーターゲットだったテクスチャを今後はシェーダで参照できるように変更する.
  gi().Cmd_Texture_TransState(m_hSceneRenderTargetBuffer, ghl::EResourceState::eRenderTarget, ghl::EResourceState::ePixcelShaderResource);

  // レンダーターゲットをバックバッファに戻す.
  gi().Cmd_BackBuffer_ChangeStateFromPresentToRenderTarget();
  gi().Cmd_BackBuffer_SetToRenderTarget(m_hScreenDescHeapForDSV, 0);
  gi().Cmd_BackBuffer_Clear(GetClearColor());
  gi().Cmd_DepthStencil_Clear(m_hScreenDescHeapForDSV, 0);
  gi().Cmd_Set_Viewports(m_viewport_default);
  gi().Cmd_Set_ScissorRects(m_scissorrect_default);

  // スクリーン.
  {
    // 矩形バッファに上で描画した内容をテクスチャとして張り付ける.
    m_screen_object_manager.DrawSceneQuad();
  }

  // imgui.
  if (m_eGameDisplayMode == EGameDisplayMode::eWithDebugDialog)
  {
    m_debug_menu.UpdateImGui();
  }

  // バックバッファの用途を表示先に切り替える.
  gi().Cmd_BackBuffer_ChangeStateFromRenderTargetToPresent();

  // コマンドの終了、実行、次の準備.
  gi().CloseCommand();
  gi().ExecuteCommand();
  gi().WaitCommandFinished();
  gi().ClearCommand();

  // フリップ.
  gi().SwapChain_Present(m_bEnableVSync);

  // グラフィックメモリクリア.
  gi().ClearGraphicMemory();
}

void CGameObject::OnChangedGameDispMode(EGameDisplayMode prev, EGameDisplayMode next)
{
  if (next == EGameDisplayMode::eNormal) {
    m_screen_object_manager.ChangeDispModeDefault();
  }
  else if (next == EGameDisplayMode::eWithDebugDialog) {
    m_screen_object_manager.ChangeDispModeWithDebug();
  }
}
