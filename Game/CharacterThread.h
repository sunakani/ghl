#pragma once

#include "Game_defs.h"

class CCharacter;

class CCharacterUpdateThread : public ghl::CThread
{
public:

	CCharacterUpdateThread();

	void Init();
	void Term();

	void ResetTargets();
	void AddTarget(CCharacter* pChara);
	size_t GetTargetNum() const { return m_targets.size(); }

	void SetUpdateParam(const SGameUpdateParam& update_param) { m_update_param = update_param; }

	void Update();

private:
	static unsigned ThreadFunc(void* pArg);

private:
	using Super = ghl::CThread;

	std::vector<CCharacter*> m_targets;

	SGameUpdateParam m_update_param;
};

