#include "Player.h"

CPlayer::CPlayer()
	: m_pInput(nullptr)
{}
void CPlayer::Init()
{
	m_pInput = new ghl::CInput();
	m_pInput->Init();
}
void CPlayer::Term()
{
	if (m_pInput != nullptr) {
		m_pInput->Term();
		delete m_pInput;
		m_pInput = nullptr;
	}
}
void CPlayer::UpdateInput(const SGameUpdateParam& update_param)
{
	if (m_pInput != nullptr) {
		m_pInput->UpdateKeybordState();
		m_pInput->UpdateMouseState(update_param.pGameInfo->hWnd);
	}
}

CPlayerManager::CPlayerManager()
{}
void CPlayerManager::Init()
{
	for (auto& elem : m_players) {
		elem.bActive = false;
		elem.player.Init();
	}
}
void CPlayerManager::Term()
{
	for (auto& elem : m_players) {
		elem.bActive = false;
		elem.player.Term();
	}
}
void CPlayerManager::ActivatePlayer(EPlayerIndex ePlayer)
{
	auto index = e2ut(ePlayer);
	if (index >= 0 && index < m_players.size()) {
		m_players.at(index).bActive = true;
	}
}
void CPlayerManager::DeactivatePlayer(EPlayerIndex ePlayer)
{
	auto index = e2ut(ePlayer);
	if (index >= 0 && index < m_players.size()) {
		m_players.at(index).bActive = false;
	}
}
void CPlayerManager::UpdateActivePlayerInput(const SGameUpdateParam& update_param)
{
	for (auto& elem : m_players) {
		if (elem.bActive) {
			elem.player.UpdateInput(update_param);
		}
	}
}
const ghl::CInput* CPlayerManager::GetActivePlayerInput(EPlayerIndex ePlayer) const
{
	auto index = e2ut(ePlayer);
	if (index >= 0 && index < m_players.size()) {
		auto& target = m_players.at(index);
		if (target.bActive) {
			return target.player.GetInput();
		}
	}
	return nullptr;
}


