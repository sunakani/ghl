#pragma once

#include "GHL.h"
#include "input/Input.h"
#include "Game_defs.h"

namespace ghl {
class CInput;
}
class CCharacter;

/************************************************************/
/*	プレイヤークラス.
*		キャラクターではなくユーザーごとの処理を行うクラス.
*		例えば入力の管理などを行う.
*************************************************************/
class CPlayer
{
public:
	CPlayer();
	void Init();
	void Term();
	void UpdateInput(const SGameUpdateParam& update_param);
	const ghl::CInput* GetInput() const { return m_pInput; }
private:
	ghl::CInput*	m_pInput{};
};

/************************************************************/
/*	プレイヤー管理クラス.
*		複数のプレイヤーをまとめるが、当面は一人用.
*************************************************************/
class CPlayerManager
{
public:
	CPlayerManager();
	void Init();
	void Term();

	// インデックスで指定したプレイヤーを有効/無効化する.
	void ActivatePlayer(EPlayerIndex ePlayer);
	void DeactivatePlayer(EPlayerIndex ePlayer);

	// 入力関連.
	void UpdateActivePlayerInput(const SGameUpdateParam& update_param);
	const ghl::CInput* GetActivePlayerInput(EPlayerIndex ePlayer) const;

private:
	struct SPlayer {
		bool bActive{false};
		CPlayer player;
	};
	std::array<SPlayer,e2ut(EPlayerIndex::Num)> m_players;
};
