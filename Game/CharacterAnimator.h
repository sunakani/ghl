#pragma once

#include "GHL.h"
#include "LoadFile.h"

class CCharacter;

/************************************************/
/*  キャラクターのモーションを管理.
*   モーションデータをもとにキャラクターの骨の回転やオフセットを決める.
*   IKや補間なども行う.
*************************************************/
class CCharacterAnimator
{
public:
	CCharacterAnimator();

  void Init(std::weak_ptr<std::vector<::SMotion>> motion_set, std::weak_ptr<SSkeleton> skeleton);
	void Term();

  void Update(const SGameUpdateParam& update_param, const CCharacter& chara);
  void UpdateBone(ACharacterBoneMatrices& localBoneMatrices);

  const DirectX::XMVECTOR& GetRootTranslate() const { return m_root_translate; }

  bool IsCurrentMotionFinished(int32_t id) const;

private:
  // IK(CCD-IK)関連.
  void IKSolve(const SSkeleton& skeleton, const SMotion& motion);
  void SolveCCDIK(const SSkeleton& skeleton, const SSkeleton::SIK& ik, const SMotion& motion);

  void OnBeginMotion(int32_t id, uint32_t action_uid, float interp_time);
  void OnEndMotion();

private:
  struct SBone
  {
    const SSkeleton::SBoneNode* pNode{nullptr};
    SBone* pParent{nullptr};
    DirectX::XMVECTOR localTranslate{}; // 親からの移動量.
    DirectX::XMVECTOR localInitialTranslate{};
    DirectX::XMVECTOR localRotation{};
    DirectX::XMMATRIX localMatrix{};
    void UpdateMatrix()
    {
      auto mat = DirectX::XMMatrixRotationQuaternion(localRotation) * DirectX::XMMatrixTranslationFromVector(localTranslate);
      if (pParent != nullptr) {
        mat *= pParent->localMatrix;
      }
      localMatrix = mat;
    }
  };
  void RecursiveInitBones(SBone* pBone);
  void UpdateLocalMatrices(SBone* pBone);

private:
  std::weak_ptr<std::vector<::SMotion>> m_motion_set;
  float                                 m_motion_elapsed_time;
  float                                 m_motion_frame;
  std::weak_ptr<SSkeleton>              m_skeleton;
  std::vector<SBone>                    m_bones;
  std::list<SBone*>                     m_root_bones;
  uint32_t                              m_current_action_uid;
  int32_t                               m_current_motion_index;
  size_t                                m_motion_num;

  // 補間用の情報.
  struct SInterp
  {
    struct SBone {
      DirectX::XMVECTOR translate{};
      DirectX::XMVECTOR rotation{};
    };
    std::vector<SBone> sourceBones;
    bool bProcessing{};
    float duration{};
    float elapsed{};
    float rate{};
  };
  SInterp                               m_interp;

  // ルートの移動量.
  // モーションの作りに合わせて無効化したりする.
  DirectX::XMVECTOR m_root_translate{};
};

