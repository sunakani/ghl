#include "CharacterAnimator.h"
#include "input/Input.h"
#include "Character.h"

CCharacterAnimator::CCharacterAnimator()
  : m_motion_set()
  , m_motion_elapsed_time()
  , m_motion_frame()
  , m_skeleton()
  , m_bones()
  , m_root_bones()
  , m_current_action_uid(0xFFFFFFFF)
  , m_current_motion_index(-1)
  , m_motion_num()
{}

void CCharacterAnimator::Init(std::weak_ptr<std::vector<::SMotion>> motion_set, std::weak_ptr<SSkeleton> skeleton)
{
  m_motion_set = motion_set;
  auto shared_motion_set = m_motion_set.lock();
  if (auto* pMotionSet = shared_motion_set.get()) {
    m_motion_num = pMotionSet->size();
  }
  m_current_action_uid = 0xFFFFFFFF;
  m_current_motion_index = -1;

  m_skeleton  = skeleton;

  m_interp.bProcessing = false;
  m_interp.duration = 0.f;
  m_interp.elapsed = 0.f;
  m_interp.rate = 0.f;

  // 骨初期化.
  auto shared_skeleton = m_skeleton.lock();
  if (auto* pSkeleton = shared_skeleton.get()) {
    // 骨データとキャラクターが使う実際の骨情報を関連付け.
    m_bones.resize(pSkeleton->boneNum);
    m_interp.sourceBones.resize(pSkeleton->boneNum);
    for (int i=0; i<pSkeleton->boneNum; ++i) {
      m_bones[i].pNode = pSkeleton->boneIdx2Node[i];
    }
    for (int i=0; i<pSkeleton->boneNum; ++i) {
      if (m_bones[i].pParent == nullptr) {
        RecursiveInitBones(&m_bones[i]);
      }
    }
    // センターを親としない骨を集める.
    for (int i=0; i<m_bones.size(); ++i) {
      if (m_bones[i].pParent == nullptr) {
        m_root_bones.push_back(&m_bones[i]);
      }
    }
    // 骨のローカル行列を計算.
    for (auto* pRoot : m_root_bones) {
      if (pRoot != nullptr) {
        UpdateLocalMatrices(pRoot);
      }
    }
  }

  OnBeginMotion(0, 0xFFFFFFFF, 0);
}

void CCharacterAnimator::Term()
{}

void CCharacterAnimator::Update(const SGameUpdateParam& update_param, const CCharacter& chara)
{
  m_motion_elapsed_time += update_param.fDeltaTime;

  auto& action_state = chara.GetActionController().GetActionState();
  if (action_state.pCurrent != nullptr) {
    if (action_state.uid != m_current_action_uid) {
      OnEndMotion();
      OnBeginMotion(action_state.pCurrent->pData->motion_id, action_state.uid, action_state.interp_time);
    }
  }

  float scale = 1.f;
  if (action_state.pCurrent != nullptr) {
    scale = action_state.pCurrent->pData->update_scale;
  }
  m_motion_frame = 30.f * m_motion_elapsed_time * scale;

  if (m_interp.bProcessing) {
    m_interp.elapsed += update_param.fDeltaTime;
    if (m_interp.duration > FLT_EPSILON) {
      m_interp.rate = m_interp.elapsed / m_interp.duration;
      if (m_interp.rate >= 1.f) {
        m_interp.bProcessing = false;
      }
    }
    else {
      m_interp.bProcessing = false;
    }
  }
}

void CCharacterAnimator::OnBeginMotion(int32_t id, uint32_t action_uid, float interp_time)
{
  m_current_motion_index = id;
  m_current_action_uid = action_uid;
  m_motion_elapsed_time = 0.f;
  m_motion_frame = 0.f;
  // 補間.
  if (interp_time > 0) {
    m_interp.bProcessing = true;
    m_interp.duration = interp_time;
  }
  else {
    m_interp.bProcessing = false;
    m_interp.duration = 0;
  }
  m_interp.elapsed = 0.f;
  m_interp.rate = 0.f;
//  DebugPrint("OnBeginMotion\n");
}

void CCharacterAnimator::OnEndMotion()
{
  m_current_motion_index = -1;
  m_current_action_uid = 0xFFFFFFFF;
  // 現在の骨の情報を保存し補間に利用する.
  for (size_t i=0; i<m_bones.size(); ++i) {
    m_interp.sourceBones[i].translate = m_bones[i].localTranslate;
    m_interp.sourceBones[i].rotation = m_bones[i].localRotation;
  }
  m_interp.bProcessing = false;
  m_interp.duration = 0.f;
  m_interp.elapsed = 0.f;
//  DebugPrint("OnEndMotion\n");
}

float get_y_from_x_on_bezier(float x, const DirectX::XMFLOAT2& a, const DirectX::XMFLOAT2& b, uint8_t n)
{
  if (a.x == a.y && b.x == b.y) {
    return x;
  }
  float t = x;
  const float k0 = 1.f + 3.f * a.x - 3.f * b.x;
  const float k1 = 3.f * b.x - 6.f * a.x;
  const float k2 = 3.f * a.x;
  constexpr float epsilon = FLT_EPSILON;  // もう少し大きくても違いは出ないかも. 要調整.
  for (int i=0; i<n; ++i) {
    auto ft = k0 * (t * t * t) + k1 * (t * t) + k2 * (t) - x;
    if (ft <= epsilon && ft >= -epsilon) {
      break;
    }
    t -= (ft * 0.5f);
  }
  auto r = 1.f - t;
  return t * t * t + 3.f * t * t * r * b.y + 3.f * t * r * r * a.y;
}

void CCharacterAnimator::UpdateBone(ACharacterBoneMatrices& localBoneMatrices)
{
  auto shared_skeleton = m_skeleton.lock();
  if (auto* pSkeleton = shared_skeleton.get()) {
    auto shared_motion_set = m_motion_set.lock();
    if (m_current_motion_index >= 0) {
      if (auto* pMotion = &shared_motion_set.get()->at(m_current_motion_index)) {
        // モーションデータから全ボーンのローカル座標と回転を更新する.
        for (auto& bonemotion : pMotion->plots_by_bones) {
          auto found = pSkeleton->boneNodeTable.find(bonemotion.first);
          if (found != pSkeleton->boneNodeTable.end()) {
            if (auto* pTargetBoneMatrix = &localBoneMatrices[(*found).second.boneIdx])
            {
              auto& motions = bonemotion.second;
              // 後ろから探索し、現在のモーションフレームを挟むキー区間を探す.
              auto rit = std::find_if(motions.rbegin(), motions.rend(), [this](const ::SMotion::SPlot& motion) { return motion.frameNo <= m_motion_frame; });
              if (rit == motions.rend()) {
                continue;
              }
              DirectX::XMVECTOR rotation = {};
              DirectX::XMVECTOR offset = XMLoadFloat3(&rit->offset);
              auto it = rit.base(); // 順方向基準での一つ先を取得.
              if (it != motions.end()) {
                auto t = static_cast<float>(m_motion_frame - rit->frameNo) / static_cast<float>(it->frameNo - rit->frameNo);
                auto modified_t = get_y_from_x_on_bezier(t, it->p1, it->p2, 12);
                rotation = DirectX::XMQuaternionSlerp(rit->quaternion, it->quaternion, modified_t);
                offset = DirectX::XMVectorLerp(offset, XMLoadFloat3(&it->offset), t);
              }
              else {
                rotation = rit->quaternion;
              }
              if (auto* pBone = &m_bones[(*found).second.boneIdx]) {
                auto translate = DirectX::XMVectorAdd(offset, pBone->localInitialTranslate);
                if (m_interp.bProcessing) {
                  if (auto* pInterpSource = &m_interp.sourceBones[(*found).second.boneIdx]) {
                    float t = m_interp.rate;
                    translate = DirectX::XMVectorLerp(pInterpSource->translate, translate, t);
                    rotation = DirectX::XMQuaternionSlerp(pInterpSource->rotation, rotation, t);
                  }
                }
                pBone->localTranslate = translate;
                pBone->localRotation = rotation;
              }
            }
          }
        }
        // 基底骨を更新し、再帰的にすべて更新する.
        for (auto* pRoot : m_root_bones) {
          if (pRoot != nullptr) {
            UpdateLocalMatrices(pRoot);
          }
        }
        // IK.
        IKSolve(*pSkeleton, *pMotion);
      }
    }
    // 転送準備.
    for (int i=0; i<m_bones.size(); ++i) {
      localBoneMatrices[i] = pSkeleton->bindMatrices[i] * m_bones[i].localMatrix;
    }
    if (localBoneMatrices.size() > 0) {
      m_root_translate = localBoneMatrices[0].r[3];
    }
  }
}
void CCharacterAnimator::IKSolve(const SSkeleton& skeleton, const SMotion& motion)
{
  for (auto& data : skeleton.ikDatum) {
    SolveCCDIK(skeleton, data, motion);
  }
}
void CCharacterAnimator::SolveCCDIK(const SSkeleton& skeleton, const SSkeleton::SIK& ik, const SMotion& motion)
{
  // IKボーン(エフェクタ).
  if (auto* pEffectorBone = &m_bones.at(ik.boneIdx))
  {
    // 目標ボーン(ターゲット).
    if (auto* pTargetBone = &m_bones.at(ik.targetIdx))
    {
      // 繰り返し.
      for (uint16_t ite=0; ite<ik.iterations; ++ite)
      {
        for (int i=0; i<ik.nodeIdxes.size(); ++i)
        {
          auto chain_index = ik.nodeIdxes[i];
          if (auto* pChain = skeleton.boneIdx2Node[chain_index]) {
            auto* pChainBone = &m_bones[chain_index];
       
            auto matChain    = pChainBone->localMatrix;
            auto matChainInv = DirectX::XMMatrixInverse(nullptr, matChain);

            auto pos_effector = DirectX::XMVector3Transform(pEffectorBone->localMatrix.r[3],  matChainInv);
            auto pos_target   = DirectX::XMVector3Transform(pTargetBone->localMatrix.r[3],    matChainInv);

            auto lenSq = DirectX::XMVectorGetX(DirectX::XMVector3LengthSq(DirectX::XMVectorSubtract(pos_effector, pos_target)));
            if (lenSq < 0.0001f) {
              return;
            }

            if (DirectX::XMVectorGetX(DirectX::XMVector3LengthSq(pos_effector)) < FLT_EPSILON) {
              continue;
            }
            if (DirectX::XMVectorGetX(DirectX::XMVector3LengthSq(pos_target)) < FLT_EPSILON) {
              continue;
            }
            auto vec_to_effector  = DirectX::XMVector3Normalize(pos_effector);
            auto vec_to_target    = DirectX::XMVector3Normalize(pos_target);

            auto dot = DirectX::XMVectorGetX(DirectX::XMVector3Dot(vec_to_effector, vec_to_target));
            dot = std::min(1.0f, std::max(-1.0f, dot));
            float radian = acosf(dot);
            if (radian < 0.0001f) {
              continue;
            }
            auto limitAngle = ik.limit;
            radian = std::min(limitAngle, std::max(-limitAngle, radian));
            if (radian < 0.0001f) {
              continue;
            }

            auto axis = DirectX::XMVector3Normalize(DirectX::XMVector3Cross(vec_to_target, vec_to_effector));
            if (DirectX::XMVectorGetX(DirectX::XMVector3LengthSq(axis)) < FLT_EPSILON) {
              continue;
            }

            if (pChainBone->pNode->knee)
            {
              auto rotation = DirectX::XMQuaternionRotationAxis(axis, radian);
              auto eulerAngle = ghl::GetQuaternionToEulerXYZ(rotation);
              eulerAngle.x = std::min(-0.002f, std::max(-DirectX::XM_PI, eulerAngle.x));
              eulerAngle.y = 0;
              eulerAngle.z = 0;
              rotation = ghl::MakeQuaternionEuler(eulerAngle);
              rotation = DirectX::XMQuaternionMultiply(rotation, pChainBone->localRotation);
              pChainBone->localRotation = DirectX::XMQuaternionNormalize(rotation);
            }
            else
            {
              auto rotation = DirectX::XMQuaternionRotationAxis(axis, radian);
              rotation = DirectX::XMQuaternionMultiply(rotation, pChainBone->localRotation);
              pChainBone->localRotation = DirectX::XMQuaternionNormalize(rotation);
            }

            // 自身を先頭として、先端に向かって更新する.
            for (int j=i; j>= 0; --j)
            {
              auto childe_chain_index = ik.nodeIdxes[j];
              m_bones[childe_chain_index].UpdateMatrix();
            }

            pEffectorBone->UpdateMatrix();
            pTargetBone->UpdateMatrix();
          }
        }
      }
    }
  }
}

void CCharacterAnimator::RecursiveInitBones(SBone* pBone)
{
  if (pBone != nullptr) {
    if (pBone->pNode != nullptr) {
      auto trans = pBone->pNode->startPos;
      if (pBone->pParent != nullptr) {
        trans = trans - pBone->pParent->pNode->startPos;
      }
      pBone->localTranslate = XMLoadFloat3(&trans);
      pBone->localInitialTranslate = pBone->localTranslate;
#if defined(_DEBUG) && 0
      DebugPrintf("[%3d]%s:initialTrans{%.1f,%.1f,%.1f,%.1f}\n", pBone->pNode->boneIdx, pBone->pNode->debugName.c_str(), pBone->localInitialTranslate.m128_f32[0], pBone->localInitialTranslate.m128_f32[1], pBone->localInitialTranslate.m128_f32[2], pBone->localInitialTranslate.m128_f32[3]);
#endif
      for (auto* pChild : pBone->pNode->children) {
        if (auto* pChildBone = &m_bones[pChild->boneIdx]) {
          pChildBone->pParent = pBone;
          RecursiveInitBones(pChildBone);
        }
      }
    }
  }
}

void CCharacterAnimator::UpdateLocalMatrices(SBone* pBone)
{
  if (pBone != nullptr) {
    pBone->UpdateMatrix();
    for (auto* pChild : pBone->pNode->children) {
      if (auto* pChildBone = &m_bones[pChild->boneIdx]) {
        UpdateLocalMatrices(pChildBone);
      }
    }
  }
}

bool CCharacterAnimator::IsCurrentMotionFinished(int32_t id) const
{
  if (m_current_motion_index >= 0) {
    if (m_current_motion_index == id) {
      auto shared_motion_set = m_motion_set.lock();
      if (auto* pMotion = &shared_motion_set.get()->at(m_current_motion_index)) {
        if (m_motion_frame > pMotion->duration) {
          return true;
        }
      }
    }
  }
  return false;
}

