#include "CharacterThread.h"
#include "Character.h"

CCharacterUpdateThread::CCharacterUpdateThread()
	: m_targets()
{}

void CCharacterUpdateThread::Init()
{
	Super::Init(ThreadFunc, L"CharacterUpdate");
	m_targets.clear();
}

void CCharacterUpdateThread::Term()
{
	Super::Term();
	m_targets.clear();
	m_targets.shrink_to_fit();
}

void CCharacterUpdateThread::ResetTargets()
{
	m_targets.clear();
}

void CCharacterUpdateThread::AddTarget(CCharacter* pChara)
{
	m_targets.push_back(pChara);
}

unsigned CCharacterUpdateThread::ThreadFunc(void* pArg)
{
	while(1) {
		CCharacterUpdateThread* pThread = static_cast<CCharacterUpdateThread*>(pArg);
		pThread->WaitBegin();
		pThread->Update();
		pThread->SetEnd();
	}
	return 0;
}

void CCharacterUpdateThread::Update()
{
	for (auto* pChara : m_targets) {
		if (pChara != nullptr) {
      pChara->Update(m_update_param);
      pChara->PreDraw(m_update_param);
    }
	}
}

