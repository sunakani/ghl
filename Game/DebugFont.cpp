#include "DebugFont.h"
#include "Graphic.h"

static const float sk_default_font_size = 0.6f;

CDebugDisp& CDebugDisp::Inst() {
	static CDebugDisp inst;
	return inst;
}
CDebugDisp::CDebugDisp()
  : m_scale(1.f)
  , m_color{0.f, 0.f, 0.f, 1.f}
  , m_position{0.f,0.f}
{}
CDebugDisp::~CDebugDisp()
{}
void CDebugDisp::SetScale(float scale)
{
  m_scale = scale;
}
void CDebugDisp::SetColor(const AFloatColor& color)
{
  m_color = color;
}
void CDebugDisp::SetPosition(float x, float y)
{
  m_position.x = x;
  m_position.y = y;
}
void CDebugDisp::Register(const char (&buf)[eMaxStringLength])
{
  SRegister* pDest = nullptr;
  for (auto& elem : m_registered) {
    if (elem.use == false) {
      pDest = &elem;
      break;
    }
  }
  if (pDest != nullptr) {
    pDest->use = true;
    pDest->color = m_color;
    pDest->position = m_position;
    pDest->size = sk_default_font_size * m_scale;
    std::memcpy(pDest->texts, buf, eMaxStringLength);
    if (pDest->texts[eTailIndexOfString] != '\0') {
      pDest->texts[eTailIndexOfString] = '\0';
    }
  }
}
void CDebugDisp::Draw(const ghl::SViewPort& vp)
{
  gi().DebugFont_Begin(vp);
  for (auto& r : m_registered)
  {
    if (r.use) {
      gi().DebugFont_DrawString(
        r.texts,
        r.position,
        DirectX::XMFLOAT4(r.color.data()),
        r.size
      );
    }
  }
  gi().DebugFont_End();
}
void CDebugDisp::Clear()
{
  for (auto& r : m_registered)
  {
    r.use = false;
  }
  m_scale = 1.f;
  m_color = {0.f, 0.f, 0.f, 1.f};
  m_position = {0.f,0.f};
}
