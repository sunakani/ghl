#pragma once

#include "GHL.h"
#include "Game_defs.h"

/************************************************************/
/*	HUDデバッグ文字列クラス.
*		シングルトンに対してPrintfすることで、その時の設定で画面表示.
*************************************************************/
class CDebugDisp
{
public:
	CDebugDisp(const CDebugDisp&) = delete;
	CDebugDisp(CDebugDisp&&) = delete;
	CDebugDisp& operator=(const CDebugDisp&) = delete;
	CDebugDisp& operator=(CDebugDisp&&) = delete;
	static CDebugDisp& Inst();

	void SetScale(float scale);
	void SetColor(const AFloatColor& color);
	void SetPosition(float x, float y);

	enum {
		eMaxStringLength = 256,
		eTailIndexOfString = eMaxStringLength - 1,
	};

	template<typename... Args>
	void Printf(const char* format, Args... args)
	{
		int len = std::snprintf(nullptr, 0, format, args...);
		if (len <= 0) {
			return;
		}
		len += 1;
		len = std::min<int>(len, eMaxStringLength);
		char buf[eMaxStringLength];
		std::snprintf(buf, len, format, args...);
		Register(buf);
	}

	void Draw(const ghl::SViewPort& vp);
	void Clear();

private:
	CDebugDisp();
	~CDebugDisp();
	void Register(const char (&buf)[eMaxStringLength]);

	float								m_scale{};
	AFloatColor					m_color{};
	DirectX::XMFLOAT2		m_position{};

	struct SRegister
	{
		bool								use{false};
		float								size{};
		AFloatColor					color{};
		DirectX::XMFLOAT2		position{};
		char								texts[eMaxStringLength]{};
	};
	std::array<SRegister,128> m_registered;
};
