#pragma once

#include "Game_defs.h"
#include "Character.h"
#include "CharacterManager.h"
#include "Camera.h"
#include "Light.h"
#include "Graphic.h"
#include "LoadFile.h"
#include "DebugFigure.h"
#include "StageManager.h"
#include "Player.h"
#include "ScreenObjectManager.h"
#include "DebugMenu.h"

/**************************************************/
/*	ゲーム本体.
*   メイン関数でこのクラスをインスタンス化して
*   更新メソッドを呼び出すことでゲームが起動する.
***************************************************/
class CGameObject
{
  DISALLOW_COPY_AND_ASSIGN(CGameObject);
public:
  CGameObject();
  ~CGameObject();
public:
  void Init(HWND hWnd);
  void Term();
  void Update(SGameUpdateIO& exParam);
  void CallbackUpdate(float fDeltaTime, SGameUpdateIO& io);
  static void WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
private:
  void Update(float fDeltaTime, SGameUpdateIO& io);
  void Draw();

private:
  // 各パイプラインステートの初期化.
  // 徐々にゲームクラスからは剥がしていく.
  void InitCharaPipeline();
  void InitShadowPipeline();
  void TermCharaPipeline();
  void TermShadowPipeline();

  // ゲーム表示モード切替時.
  void OnChangedGameDispMode(EGameDisplayMode prev, EGameDisplayMode next);

private:
  SGameInfo                 m_game_info;
  CCamera                   m_camera;
  CLight                    m_light;
  float                     m_fAspect;
  CFilePathManager          m_file_path_manager;
  CLoadManager              m_load_manager;
  CCharacterManager         m_character_manager;
  CDebugFigure              m_debug_figure;
  CStageManager             m_stage_manager;
  bool                      m_bEnableVSync;
  uint32_t                  m_update_count;
  float                     m_update_tick_time;
  float                     m_update_fps;
  float                     m_draw_tick_time;
  float                     m_draw_fps;
  LARGE_INTEGER             m_frequency;
  CPlayerManager            m_player_manager;
  EPlayerIndex              m_local_player_index; // この端末のプレイヤー.
  CScreenObjectManager      m_screen_object_manager;
  EGameDisplayMode          m_eGameDisplayMode;
  CDebugMenu                m_debug_menu;

  // 配置データ.
  ghl::CDatabase<SCharacterPlacementData> m_db_character_placement;

  // グラフィックパイプラインステート.
  SPipeline m_pso_chara;
  SPipeline m_pso_chara_outline;
  SPipeline m_pso_shadow;

  // ルートシグネチャ.
  SRootSignature  m_rs_chara;
  SRootSignature  m_rs_shadow;

  // ビューポート.
  ghl::SViewPort  m_viewport_default;
  ghl::SViewPort  m_viewport_shadow;

  // シザー矩形.
  ghl::SRect      m_scissorrect_default;
  ghl::SRect      m_scissorrect_shadow;

  //------------------
  // シーン.
  //------------------
  // バッファ.
  ghl::HTextureBuffer m_hSceneDepthBuffer;
  ghl::HTextureBuffer m_hSceneRenderTargetBuffer;
  // ヒープ.
  ghl::HDescHeap      m_hSceneDescHeapForDSV; // 深度.
  ghl::HDescHeap      m_hSceneDescHeapForRTV; // レンダーターゲット.

  //------------------
  // スクリーン.
  //------------------
  // バッファ.
  ghl::HTextureBuffer m_hScreenDepthBuffer;
  // ヒープ.
  ghl::HDescHeap      m_hScreenDescHeapForDSV;

  //------------------
  // シャドウマップ.
  //------------------
  // バッファ.
  ghl::HTextureBuffer m_hShadowDepthBuffer;
};
