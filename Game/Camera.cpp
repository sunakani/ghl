#include "Camera.h"
#include "Game_defs.h"
#include "CharacterManager.h"
#include "Character.h"
#include "Player.h"
#include "DebugMenu.h"

namespace {

// 初期値.
// 必要に合わせて外部化(ステートごとのデータベースがあるとよい).
constexpr float kLength   = 50.f;
constexpr float kClipNear = 1.f;
constexpr float kClipFar  = 1000.f;
constexpr float kFov      = DirectX::XM_PIDIV4;
constexpr float kAngleXMin= DirectX::XMConvertToRadians(-5);
constexpr float kAngleXMax= DirectX::XMConvertToRadians(80);

}

CCameraStateDefault::CCameraStateDefault()
  : ghl::IState<CCamera>()
  , m_param()
  , m_bInit(false)
{
  m_param.fLength = kLength;
  m_fAngleY = -DirectX::XM_PIDIV2;
  m_fAngleX = DirectX::XMConvertToRadians(10.f);
}
void CCameraStateDefault::OnBegin(CCamera& context)
{
  m_param.fLength = kLength;
  m_fAngleY = -DirectX::XM_PIDIV2;
  m_fAngleX = DirectX::XMConvertToRadians(10.f);
  m_bInit = false;
}
void CCameraStateDefault::OnEnd(CCamera& context)
{
}
void CCameraStateDefault::OnEvent(CCamera& context, ghl::AStateEvent eEvent, const void* pArg)
{
  if (eEvent == eCameraEventIdUpdate)
  {
    if (auto* pUpdateParam = static_cast<const SGameUpdateParam*>(pArg))
    {
      if (auto* pTargetPlayer = pUpdateParam->pGameInfo->pCharacterManager->GetPlayerCharacter(context.GetTargetPlayer()))
      {
        // 初回.
        if (m_bInit == false)
        {
          m_param.fLength = kLength;
          DirectX::XMFLOAT3 at = GetPositionToAttention(*pTargetPlayer);

          m_fAngleY = pTargetPlayer->GetAngleY();
          m_fAngleX = ClampT(m_fAngleX, kAngleXMin, kAngleXMax);

          DirectX::XMFLOAT3 ofs = GetTrailOffset(m_param.fLength, m_fAngleY, m_fAngleX);
          DirectX::XMFLOAT3 eye = at;
          eye += ofs;
          DirectX::XMFLOAT3 up(0, 1, 0);
          context.SetEye(eye);
          context.SetAt(at);
          context.SetUp(up);
          m_bInit = true;
        }

        // 元の値.
        const auto& prev = context.GetStatus();

        // 更新.
        float target_angle_y = m_fAngleY;
        float target_angle_x = m_fAngleX;
        if (auto* pInput = pUpdateParam->pGameInfo->pPlayerManager->GetActivePlayerInput(context.GetTargetPlayer())) {
          int32_t mouse_move_x = 0;
          int32_t mouse_move_y = 0;
          pInput->GetMouseMove(&mouse_move_x, &mouse_move_y);
          if (std::abs(mouse_move_x) > FLT_EPSILON) {
            target_angle_y += (-1.f * pUpdateParam->fDeltaTime * mouse_move_x);
          }
          if (std::abs(mouse_move_y) > FLT_EPSILON) {
            target_angle_x += (1.f * pUpdateParam->fDeltaTime * mouse_move_y);
          }
        }
        target_angle_x = ClampT(target_angle_x, kAngleXMin, kAngleXMax);
        m_fAngleY = target_angle_y;
        m_fAngleX = target_angle_x;

        DirectX::XMFLOAT3 target_at = GetPositionToAttention(*pTargetPlayer);
        auto at  = prev.at;
        at.x += (target_at.x - prev.at.x) * 0.1f;
        at.y += (target_at.y - prev.at.y) * 0.1f;
        at.z += (target_at.z - prev.at.z) * 0.1f;

        DirectX::XMFLOAT3 ofs = GetTrailOffset(m_param.fLength, m_fAngleY, m_fAngleX);
        DirectX::XMFLOAT3 eye = at;
        eye += ofs;
        DirectX::XMFLOAT3 up(0, 1, 0);

        context.SetEye(eye);
        context.SetAt(at);
        context.SetUp(up);

        float fov = kFov;
        auto& debug = pUpdateParam->pGameInfo->pDebugMenu->GetCamera();
        if (debug.bEnabled) {
          fov = DirectX::XMConvertToRadians(debug.contents.fov);
        }
        context.SetFov(fov);

        context.SetClipNear(kClipNear);
        context.SetClipFar(kClipFar);
      }
    }
  }
}
void CCameraStateDefault::Transit(CCamera& context, ghl::IStateChanger& changer)
{
}
DirectX::XMFLOAT3 CCameraStateDefault::GetPositionToAttention(const CCharacter& chara) const
{
  DirectX::XMFLOAT3 pos = chara.GetUpperPosition();
  pos.y += 0.f;
  pos += chara.GetFrontDir() * 5.f;
  return pos;
}
DirectX::XMFLOAT3 CCameraStateDefault::GetTrailOffset(float len, float angle_y, float angle_x) const
{
  return {
    len * std::cosf(angle_x) * std::cosf(angle_y - DirectX::XM_PIDIV2),
    len * std::sinf(angle_x),
    len * std::cosf(angle_x) * std::sinf(angle_y - DirectX::XM_PIDIV2)
  };
}

CCameraStateLockOn::CCameraStateLockOn()
  : ghl::IState<CCamera>()
{}
void CCameraStateLockOn::OnBegin(CCamera& context)
{
}
void CCameraStateLockOn::OnEnd(CCamera& context)
{
}
void CCameraStateLockOn::OnEvent(CCamera& context, ghl::AStateEvent eEvent, const void* pArg)
{
}
void CCameraStateLockOn::Transit(CCamera& context, ghl::IStateChanger& changer)
{
}


CCamera::CCamera()
  : m_stateMachine(this)
  , m_status{}
  , m_target_player(EPlayerIndex::None)
{}

void CCamera::Init()
{
  m_stateMachine.Init(eCameraStateDefault);
  m_status.eye        = {0, 15, -30};
  m_status.at         = {0, 10, 0};
  m_status.up         = {0, 1, 0};
  m_status.fov        = DirectX::XM_PIDIV4;
  m_status.aspect     = 16.f / 9.f;
  m_status.clip_near  = kClipNear;
  m_status.clip_far   = kClipFar;
}

void CCamera::Term()
{
  m_stateMachine.Term();
}

void CCamera::Update(const SGameUpdateParam& update)
{
  m_stateMachine.Transit();
  m_stateMachine.Update();
  m_stateMachine.Event(eCameraEventIdUpdate, &update);
}

