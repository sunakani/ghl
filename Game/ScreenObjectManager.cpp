#include "ScreenObjectManager.h"
#include "Player.h"

static const DirectX::XMFLOAT2 sk_big_viewport    = {1920, 1080};
static const DirectX::XMFLOAT2 sk_small_viewport  = {1536, 864};

CScreenObjectManager::CScreenObjectManager()
  : m_pso()
  , m_rootsig()
  , m_test_object()
  , m_hTestTexture()
  , m_auto_transit()
  , m_scene_quad()
	, m_curr_disp_mode(EGameDisplayMode::eNormal)
	, m_next_disp_mode(EGameDisplayMode::eNone)
{}

void CScreenObjectManager::Init(const SGameInfo& info, EGameDisplayMode eDispMode)
{
	InitPipeline();

  // テスト用テクスチャ.
  if (auto* pTexture = info.pLoadManager->LoadTexture(ELoadFile::UI_TEST, info)) {
    m_hTestTexture = pTexture->GetBuffer();
  }
  else {
    m_hTestTexture = gi().Texture_GetWhite();
  }

  // テスト用UI.
  m_test_object.Init(info, EScreenObjectType::eSimple);
  m_test_object.SetColorTexture(m_hTestTexture);
  m_test_object.SetPos(-400.f,-400.f);
  m_test_object.SetSize(100.f, 200.f);

  // 表示モード関連.
  m_auto_transit.Init();
  m_curr_disp_mode = eDispMode;
	m_next_disp_mode = EGameDisplayMode::eNone;

  // シーンテクスチャを張り付ける矩形.
  m_scene_quad.Init(info, EScreenObjectType::eSceneQuad);
  m_scene_quad.SetColorTexture(info.hSceneRenderTarget);
  m_scene_quad.SetDepthTexture(info.hSceneDepthBuffer);
  auto& viewport = m_curr_disp_mode == EGameDisplayMode::eWithDebugDialog ? sk_small_viewport : sk_big_viewport;
  float x = (info.viewport_width  - viewport.x) - (info.viewport_width   * 0.5f);
  float y = (info.viewport_height - viewport.y) - (info.viewport_height  * 0.5f);
  m_scene_quad.SetPos(x, y);
  m_scene_quad.SetSize(viewport);
}

void CScreenObjectManager::Term(const SGameInfo& info)
{
  m_scene_quad.Term(info);
  m_test_object.Term(info);

  if (m_hTestTexture) {
    info.pLoadManager->UnloadTexture(ELoadFile::UI_TEST);
    m_hTestTexture.Invalidate();
  }

	TermPipeline();
}

void CScreenObjectManager::InitPipeline()
{
  // ルートシグネチャ.
  ghl::SCreateRootSignatureParam root_sig_param;
  {
    auto& rp = root_sig_param.root_parameters;
    rp.resize(2);
    rp[0].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 0, 1}); // b0,
    rp[1].table.push_back({ghl::EDescriptorType::eShaderResourceView, 0, 0, 2}); // t0,t1,
    auto& smp = root_sig_param.sampler_parameters;
    smp.push_back({0, ghl::ETextureAddressMode::eWrap,  ghl::ETextureAddressMode::eWrap,  ghl::ETextureAddressMode::eWrap }); // s0.
  }
  m_rootsig.hRootSignature = gi().RootSig_Create(root_sig_param);

  // PSO.

  // シェーダ.
  LPCWSTR shader_dir = L"Shader\\";
  WCHAR path[256];
  swprintf_s(path, L"%s\\%s", shader_dir, L"ScreenObject.hlsl");
  m_pso.hVS = gi().Shader_CreateByCompileFile(path, "VS", "vs_5_0");
  swprintf_s(path, L"%s\\%s", shader_dir, L"ScreenObject.hlsl");
  m_pso.hPS = gi().Shader_CreateByCompileFile(path, "PS", "ps_5_0");

  // パイプラインステート.
  ghl::SCreatePiplelineStateParam pipeline_state_param;
  {
    auto& vi = pipeline_state_param.vertex_input_rayout_parameters;
    vi.push_back({"POSITION", 0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0});
    vi.push_back({"TEXCOORD", 0, ghl::EGraphInfraFormat::eR32G32_Float,    0});
    pipeline_state_param.hVS = m_pso.hVS;
    pipeline_state_param.hPS = m_pso.hPS;
    pipeline_state_param.hRootSignature = m_rootsig.hRootSignature;
    //pipeline_state_param.eCullMode = ghl::ECullMode::eNone;
  }
  m_pso.hState = gi().PipeState_Create(pipeline_state_param);
}

void CScreenObjectManager::TermPipeline()
{
  m_pso.Destroy();
  m_rootsig.Destroy();
}

void CScreenObjectManager::Update(const SGameUpdateParam& update_param)
{
  m_test_object.Update(update_param);

  if (m_next_disp_mode != EGameDisplayMode::eNone && m_curr_disp_mode != m_next_disp_mode)
  {
    m_auto_transit.Init();

    m_auto_transit.bProcess   = true;
    m_auto_transit.pos_prev   = m_scene_quad.GetPos();
    m_auto_transit.size_prev  = m_scene_quad.GetSize();
    m_auto_transit.duration   = 0.2f;

    auto& info = *update_param.pGameInfo;

    if (m_next_disp_mode == EGameDisplayMode::eNormal)
    {
      float x = (info.viewport_width  - sk_big_viewport.x) - (info.viewport_width   * 0.5f);
      float y = (info.viewport_height - sk_big_viewport.y) - (info.viewport_height  * 0.5f);
      m_auto_transit.pos_next.x = x;
      m_auto_transit.pos_next.y = y;
      m_auto_transit.size_next.x = sk_big_viewport.x;
      m_auto_transit.size_next.y = sk_big_viewport.y;
    }
    else if (m_next_disp_mode == EGameDisplayMode::eWithDebugDialog) {
      float x = (info.viewport_width  - sk_small_viewport.x) - (info.viewport_width   * 0.5f);
      float y = (info.viewport_height - sk_small_viewport.y) - (info.viewport_height  * 0.5f);
      m_auto_transit.pos_next.x = x;
      m_auto_transit.pos_next.y = y;
      m_auto_transit.size_next.x = sk_small_viewport.x;
      m_auto_transit.size_next.y = sk_small_viewport.y;
    }
    m_curr_disp_mode = m_next_disp_mode;
    m_next_disp_mode = EGameDisplayMode::eNone;
  }

  if (m_auto_transit.bProcess)
  {
    m_auto_transit.elapsed += update_param.fDeltaTime;
    if (m_auto_transit.elapsed >= m_auto_transit.duration) {
      m_scene_quad.SetPos(m_auto_transit.pos_next);
      m_scene_quad.SetSize(m_auto_transit.size_next);
      m_auto_transit.Init();
    }
    else {
      float t = m_auto_transit.elapsed / m_auto_transit.duration;
      float x = (m_auto_transit.pos_next.x - m_auto_transit.pos_prev.x) * t + m_auto_transit.pos_prev.x;
      float y = (m_auto_transit.pos_next.y - m_auto_transit.pos_prev.y) * t + m_auto_transit.pos_prev.y;
      float w = (m_auto_transit.size_next.x - m_auto_transit.size_prev.x) * t + m_auto_transit.size_prev.x;
      float h = (m_auto_transit.size_next.y - m_auto_transit.size_prev.y) * t + m_auto_transit.size_prev.y;
      m_scene_quad.SetPos(x, y);
      m_scene_quad.SetSize(w, h);
    }
  }

  m_scene_quad.Update(update_param);
}

void CScreenObjectManager::PreDraw(const SGameUpdateParam& update_param)
{
  m_test_object.PreDraw(update_param);
  m_scene_quad.PreDraw(update_param);
}

void CScreenObjectManager::Draw()
{
  gi().Cmd_RootSig_Set(m_rootsig.hRootSignature);
  gi().Cmd_PipeState_Set(m_pso.hState);
  gi().Cmd_Set_PrimitiveTopology(ghl::EPrimitiveTopology::eTriangleStrip);
  m_test_object.Draw();
}

void CScreenObjectManager::DrawSceneQuad()
{
  gi().Cmd_RootSig_Set(m_rootsig.hRootSignature);
  gi().Cmd_PipeState_Set(m_pso.hState);
  gi().Cmd_Set_PrimitiveTopology(ghl::EPrimitiveTopology::eTriangleStrip);
  m_scene_quad.Draw();
}

void CScreenObjectManager::ChangeDispModeDefault()
{
  m_next_disp_mode = EGameDisplayMode::eNormal;
}

void CScreenObjectManager::ChangeDispModeWithDebug()
{
  m_next_disp_mode = EGameDisplayMode::eWithDebugDialog;
}
