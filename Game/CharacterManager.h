#pragma once

#include "Character_defs.h"
#include "Game_defs.h"
#include "CharacterThread.h"

class CCharacter;

/************************************************/
/*  キャラクターのインスタンスの管理.
*		キャラデータの管理.
*************************************************/
class CCharacterManager
{
public:
	CCharacterManager();
	~CCharacterManager();
	void Init();
	void Term(SGameInfo& info);

	// 更新.
	void Update(SGameUpdateParam& update_param);

	// 各種描画.
	void DrawShadow();
	void Draw();
	void DrawOutline();

	// キャラクターのリソースをロード/アンロード.
	void LoadCharacter(ECharacter eChara, SGameInfo& info);
	void UnloadCharacter(ECharacter eChara, SGameInfo& info);

	// キャラクター生成に必要なアーキタイプ(リソースへの参照セット)を取得.
	const SCharacterArchetype* GetCharacterArchetype(ECharacter eChara) const;

	// キャラクター生成.
	void CreateCharacter(const SCharacterCreateParam& param, const SGameInfo& info);

	// プレイヤーとの紐づけ.
	void SetPlayerByIndex(size_t index, EPlayerIndex ePlayer);
	const CCharacter* GetPlayerCharacter(EPlayerIndex ePlayer) const;

private:
	struct SUnit {
		SUnit(ECharacter chara, ELoadFile model, ELoadFile motion_set, ELoadFile action)
			: data(chara, model, motion_set, action)
			, archetype()
			, count(0)
		{}
		SCharacterData data;
		SCharacterArchetype archetype;
		int16_t count{0};
	};
private:
	void _LoadCharacter(SUnit& unit, SGameInfo& info);
	void _UnloadCharacter(SUnit& unit, SGameInfo& info);
	void LoadModel(SUnit& unit, SGameInfo& info);
	void LoadMotion(SUnit& unit, SGameInfo& info);
	void LoadAction(SUnit& unit, SGameInfo& info);

private:
	std::map<ECharacter,SUnit>											m_datum;
	std::list<CCharacter*>													m_characters;
	std::array<CCharacter*,e2ut(EPlayerIndex::Num)>	m_player_characters;
	std::array<CCharacterUpdateThread,eCharacterUpdateThreadNum> m_update_threads;
};
