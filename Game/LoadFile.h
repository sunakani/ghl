#pragma once

#include "GHL.h"
#include "DebugFigure.h"

struct SGameInfo;

// ファイルID.
#define DEF_LOAD_FILE_START() \
enum class ELoadFile { \
	None = 0,
#define DEF_LOAD_FILE(id, value,...) id = value,
#define DEF_LOAD_FILE_END() };
#include "inc/db_load_file.inc"
#undef DEF_LOAD_FILE_START
#undef DEF_LOAD_FILE
#undef DEF_LOAD_FILE_END

/************************************************************/
/*	ファイルIDとそのパスの情報を管理するクラス.
*************************************************************/
class CFilePathManager
{
public:
	CFilePathManager();
	~CFilePathManager();
	void Init();
	void Term();
	const char* GetPath(ELoadFile eFile) const;
	static const char* GetModelDir() { return "Data/Model/"; }
	static const char* GetToonDir() { return "Data/Toon/"; }
private:
	std::unordered_map<ELoadFile,const char*> m_map;
};

/**************************************************/
/*	テクスチャのロード. バッファの管理.
***************************************************/
class CLoaderTexture
{
public:
	CLoaderTexture();
	void Init();
	void Term();
	void Load(ELoadFile eFile, const SGameInfo& info);
	void Load(size_t hash, const char* pPath, const SGameInfo& info);
	void Unload();
	bool IsLoaded() const;
	ghl::HTextureBuffer GetBuffer() const { return m_hBuffer; }
	int16_t GetRefCount() const { return m_nRefCount; }
	size_t GetHash() const { return m_hash; }
	bool IsManagedWithHash() const { return m_bManagedWithHash; }
private:
	ELoadFile						m_eFile;
	int16_t							m_nRefCount;
	bool								m_bManagedWithHash;
	size_t							m_hash;
	ghl::HTextureBuffer	m_hBuffer;
#if defined(_DEBUG)
	std::string					m_debug_path;
#endif
};

/**************************************************/
/*	頂点バッファとインデックスバッファの管理.
***************************************************/
class CLoaderVertices
{
public:
	CLoaderVertices();
	void Init();
	void Term();
	void Load(ELoadFile eFile, const SGameInfo& info);
	void Load(ELoadFile eFile, FILE* fp, const SGameInfo& info);
	void Unload();
	bool IsLoaded() const;
	ghl::HVertexBuffer GetVertexBuffer() const { return m_hVertexBuffer; }
	ghl::HIndexBuffer GetIndexBuffer() const { return m_hIndexBuffer; }
	uint32_t GetVertexNum() const { return m_unVertexNum; }
	uint32_t GetIndexNum() const { return m_unIndexNum; }
	uint16_t GetOneVertexBytes() const { return m_unOneVertexBytes; }
	uint16_t GetOneIndexBytes() const { return m_unOneIndexBytes; }
private:
	ELoadFile						m_eFile;
	int16_t							m_nRefCount;
	ghl::HVertexBuffer	m_hVertexBuffer;
	ghl::HIndexBuffer		m_hIndexBuffer;
	uint32_t						m_unVertexNum;
	uint32_t						m_unIndexNum;
	uint16_t						m_unOneVertexBytes;
	uint16_t						m_unOneIndexBytes;
};

/**************************************************/
/*	スケルトンの管理.
***************************************************/
enum class EBoneType : uint8_t {
	eRotation,
	eRotAndMove,
	eIK,
	eUndefined,
	eIKChild,
	eRotationChild,
	eIKDestination,
	eInvisible,
};
struct SSkeleton {
	uint16_t boneNum{0};
  struct SBoneNode {
		size_t									hash					{0};
    int32_t									boneIdx				{-1};
		EBoneType								boneType			{EBoneType::eRotation};
		uint16_t								ikParentBone	{0};
    DirectX::XMFLOAT3				startPos			{0.f, 0.f, 0.f};
		SBoneNode*							parent				{nullptr};
		bool										knee					{false};	// 膝骨は、IK処理に特別な制限をかける.
    std::vector<SBoneNode*> children			{};
#if defined(_DEBUG)
		std::string	debugName;
#endif
  };
  std::map<size_t,SBoneNode>			boneNodeTable;
	std::vector<SBoneNode*>					boneIdx2Node;
	std::vector<DirectX::XMMATRIX>	bindMatrices;
	struct SIK {
		uint16_t boneIdx{0};
		uint16_t targetIdx{0};
		uint16_t iterations{0};
		float limit{0};
		std::vector<uint16_t> nodeIdxes;	// 先端の方が若い番号に入る.
	};
	std::vector<SIK> ikDatum;
	int32_t boneIdxUpper{-1};
#if defined(_DEBUG)
	void DebugDrawSphere(int32_t idx, const DirectX::XMMATRIX& matMotion, const DirectX::XMMATRIX& matL2W, CDebugFigure& debugFigure, float radius, const AFloatColor& color);
	void DebugDrawLine(int32_t from, int32_t to, const DirectX::XMMATRIX& matMotionFrom, const DirectX::XMMATRIX& matMotionTo, const DirectX::XMMATRIX& matL2W, CDebugFigure& debugFigure, float thickness, const AFloatColor& color);
#endif
};
class CLoaderSkeleton
{
public:
	CLoaderSkeleton();
	void Init();
	void Term();
	void Load(ELoadFile eFile, const SGameInfo& info);
	void Load(ELoadFile eFile, FILE* fp, const SGameInfo& info);
	void Unload();
	bool IsLoaded() const;
	std::weak_ptr<SSkeleton> GetSkeleton() const { return m_skeleton;}
private:
	ELoadFile										m_eFile;
	int16_t											m_nRefCount;
	std::shared_ptr<SSkeleton>	m_skeleton;
};

/**************************************************/
/*	モーションの管理.
***************************************************/
struct SMotion {
  struct SPlot {
    unsigned int frameNo{};
    DirectX::XMVECTOR quaternion;
		DirectX::XMFLOAT3 offset;
    DirectX::XMFLOAT2 p1;
    DirectX::XMFLOAT2 p2;
    SPlot(unsigned int fno, const DirectX::XMVECTOR& q, const DirectX::XMFLOAT3& o, const DirectX::XMFLOAT2& ip1, const DirectX::XMFLOAT2& ip2)
      : frameNo(fno), offset(o), quaternion(q), p1(ip1), p2(ip2)
    {}
  };
  std::map<size_t, std::vector<SPlot>> plots_by_bones;
	uint32_t duration{};
};
class CLoaderMotion
{
public:
	CLoaderMotion();
	void Init();
	void Term();
	void Load(ELoadFile eFile, const SGameInfo& info);
	void Unload();
	bool IsLoaded() const;
	std::weak_ptr<SMotion> GetMotion() const { return m_motion; }
private:
	ELoadFile									m_eFile;
	int16_t										m_nRefCount;
	std::shared_ptr<SMotion>	m_motion;						
};

/**************************************************/
/*	モーションセットの管理.
***************************************************/
class CLoaderMotionSet
{
public:
	CLoaderMotionSet();
	void Init();
	void Term();
	void Load(ELoadFile eFile, const SGameInfo& info);
	void Unload();
	bool IsLoaded() const;
	std::weak_ptr<std::vector<SMotion>> GetMotionSet() const { return m_motion_set; }
private:
	ELoadFile					m_eFile;
	int16_t						m_nRefCount;
	std::shared_ptr<std::vector<SMotion>>	m_motion_set;
};

/**************************************************/
/*	アクションの管理.
***************************************************/
struct SActionData {
	ghl::CDatabase<SCharacterActionData>		db_action;
	ghl::CDatabase<SCharacterActionTransit>	db_trans;
	ghl::CDatabase<SCharacterActionEvent>		db_event;
};
class CLoaderAction
{
public:
	CLoaderAction();
	void Init();
	void Term();
	void Load(ELoadFile eFile, const SGameInfo& info);
	void Unload();
	bool IsLoaded() const;
	std::weak_ptr<SActionData> GetData() const { return m_data; }
private:
	ELoadFile															m_eFile;
	int16_t																m_nRefCount;
	std::shared_ptr<SActionData>	m_data;
};

/**************************************************/
/*	ロード全体の管理.
*		参照カウンタ方式で多重ロードを避ける実装.
***************************************************/
class CLoadManager
{
public:
	CLoadManager();
	~CLoadManager();

	void Init();
	void Term();

	// テクスチャ.
	const CLoaderTexture* LoadTexture(ELoadFile eFile, const SGameInfo& info);
	const CLoaderTexture* LoadTexture(const char* pPath, const SGameInfo& info);
	void UnloadTexture(ELoadFile eFile);
	void UnloadTexture(const char* pPath);
	void UnloadTexture(size_t hash);
	bool IsTextureLoaded(ELoadFile eFile) const;
	bool IsTextureLoaded(const char* pPath) const;
	const CLoaderTexture* GetTexture(ELoadFile eFile) const;
	const CLoaderTexture* GetTexture(const char* pPath) const;

	// 頂点&インデックス.
	const CLoaderVertices* LoadVertices(ELoadFile eFile, const SGameInfo& info);
	const CLoaderVertices* LoadVertices(ELoadFile eFile, FILE* fp, const SGameInfo& info);
	void UnloadVertices(ELoadFile eFile);
	const CLoaderVertices* GetVertices(ELoadFile eFile) const;

	// スケルトン.
	const CLoaderSkeleton* LoadSkeleton(ELoadFile eFile, const SGameInfo& info);
	const CLoaderSkeleton* LoadSkeleton(ELoadFile eFile, FILE* fp, const SGameInfo& info);
	void UnloadSkeleton(ELoadFile eFile);
	const CLoaderSkeleton* GetSkeleton(ELoadFile eFile) const;

	// モーション.
	const CLoaderMotion* LoadMotion(ELoadFile eFile, const SGameInfo& info);
	void UnloadMotion(ELoadFile eFile);
	const CLoaderMotion* GetMotion(ELoadFile eFile) const;

	// モーションセット.
	const CLoaderMotionSet* LoadMotionSet(ELoadFile eFile, const SGameInfo& info);
	void UnloadMotionSet(ELoadFile eFile);
	const CLoaderMotionSet* GetMotionSet(ELoadFile eFile) const;

	// アクション.
	const CLoaderAction* LoadAction(ELoadFile eFile, const SGameInfo& info);
	void UnloadAction(ELoadFile eFile);
	const CLoaderAction* GetAction(ELoadFile eFile) const;

private:
	// ※mapの多用は断片化につながるので専用のアロケータを用意する.
	std::map<ELoadFile,CLoaderTexture>		m_textures;
	std::map<size_t,CLoaderTexture>				m_textures_managed_hash;
	std::map<ELoadFile,CLoaderVertices>		m_vertices;
	std::map<ELoadFile,CLoaderSkeleton>		m_skeletons;
	std::map<ELoadFile,CLoaderMotion>			m_motions;
	std::map<ELoadFile,CLoaderMotionSet>	m_motion_sets;
	std::map<ELoadFile,CLoaderAction>			m_actions;
};
