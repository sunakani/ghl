#pragma once

/************************************************/
/*  �����̊Ǘ�.
*************************************************/
class CLight
{
public:
	CLight();
	void Init();
	void Term();
	const DirectX::XMFLOAT3& GetParallelLightDirection() const { return m_parallel_light_dir; }
private:
	DirectX::XMFLOAT3	m_parallel_light_dir;
};
