#pragma once

#include "GHL.h"
#include "LoadFile.h"

enum class EScreenObjectType {
	eSimple,
	eSceneQuad,
};

enum class EScreenObjectDescriptorIndex {
	e0_Constant,
	e1_TexColor,
	e2_TexDepth,
};

class CScreenObject
{
public:
	CScreenObject();
	void Init(const SGameInfo& info, EScreenObjectType eType);
	void Term(const SGameInfo& info);

	void Update(const SGameUpdateParam& update_param);
	void PreDraw(const SGameUpdateParam& update_param);
	void Draw();

	void SetPos(float x, float y) { m_pos.x = x; m_pos.y = y; }
	void SetPos(const DirectX::XMFLOAT2& pos) { m_pos = pos; }
	void SetPosX(float x) { m_pos.x = x; }
	void SetPosY(float y) { m_pos.y = y; }
	void SetSize(float w, float h) { m_size.x = w; m_size.y = h; }
	void SetSize(const DirectX::XMFLOAT2& size) { m_size = size; }
	void SetSizeW(float w) { m_size.x = w; }
	void SetSizeH(float h) { m_size.y = h; }
	void SetColorTexture(ghl::HTextureBuffer hTex);
	void SetDepthTexture(ghl::HTextureBuffer hTex);

	const DirectX::XMFLOAT2& GetPos() const { return m_pos; }
	float GetPosX() const { return m_pos.x; }
	float GetPosY() const { return m_pos.y; }
	const DirectX::XMFLOAT2& GetSize() const { return m_size; }
	float GetSizeW() const { return m_size.x; }
	float GetSizeH() const { return m_size.y; }

private:
	EScreenObjectType		m_eType;
  ghl::HVertexBuffer  m_hVertexBuffer;
  ghl::HVertexView    m_hVertexView;
	ghl::HDescHeap			m_hDescHeap;
	ghl::HConstBuffer		m_hConstBuffer;
	ghl::HTextureBuffer	m_hTexColor;
	ghl::HTextureBuffer	m_hTexDepth;
	DirectX::XMFLOAT2		m_pos;
	DirectX::XMFLOAT2		m_size;
};
