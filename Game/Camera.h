#pragma once

#include "GHL.h"
#include "Graphic.h"
#include "Game_defs.h"

class CCharacter;

// カメラステータス.
struct SCameraStatus {
  DirectX::XMFLOAT3 eye {0.f, 10.f, -10.f};
  DirectX::XMFLOAT3 at  {0.f, 10.f, 0.f};
  DirectX::XMFLOAT3 up  {0.f, 1.f, 0.f};
  float fov             {DirectX::XM_PIDIV4};
  float aspect          {1.f};
  float clip_near       {1.f};
  float clip_far        {100.f};
};

// カメラステート.
class CCamera;
enum ECameraState : uint16_t
{
  eCameraStateDefault,
  eCameraStateLockOn,
};
enum ECameraEventId : uint16_t
{
  eCameraEventIdUpdate,
};
class CCameraStateDefault : public ghl::IState<CCamera>
{
public:
  CCameraStateDefault();
	virtual void OnBegin(CCamera& context) override;
	virtual void OnEnd(CCamera& context) override;
	virtual void OnEvent(CCamera& context, ghl::AStateEvent eEvent, const void* pArg) override;
	virtual void Transit(CCamera& context, ghl::IStateChanger& changer) override;
private:
  DirectX::XMFLOAT3 GetPositionToAttention(const CCharacter& chara) const;
  DirectX::XMFLOAT3 GetTrailOffset(float len, float angle_y, float angle_x) const;
private:
  struct SParam {
    float fLength{};
  };
  SParam m_param;
  float m_fAngleY{};
  float m_fAngleX{};
  bool m_bInit{};
};
class CCameraStateLockOn : public ghl::IState<CCamera>
{
public:
  CCameraStateLockOn();
	virtual void OnBegin(CCamera& context) override;
	virtual void OnEnd(CCamera& context) override;
	virtual void OnEvent(CCamera& context, ghl::AStateEvent eEvent, const void* pArg) override;
	virtual void Transit(CCamera& context, ghl::IStateChanger& changer) override;
private:

};

/************************************************/
/*  カメラクラス.
*   特色ごとにカメラステートと呼ばれるステートで分割して管理される.
*************************************************/
class CCamera
{
public:
  CCamera();

  // 初期化、終了.
  void Init();
  void Term();

  // 更新.
  void Update(const SGameUpdateParam& update);

  // ステータス.
  const SCameraStatus& GetStatus() const { return m_status; }
  void SetStatus  (const SCameraStatus& status)   { m_status = status; }
  void SetEye     (const DirectX::XMFLOAT3& eye)  { m_status.eye = eye; }
  void SetAt      (const DirectX::XMFLOAT3& at )  { m_status.at = at; }
  void SetUp      (const DirectX::XMFLOAT3& up )  { m_status.up = up; }
  void SetFov     (float fov)                     { m_status.fov = fov; }
  void SetAspect  (float aspect)                  { m_status.aspect = aspect; }
  void SetClipNear(float clip_near)               { m_status.clip_near = clip_near; }
  void SetClipFar (float clip_far)                { m_status.clip_far = clip_far; }

  // プレイヤー.
  void SetTargetPlayer(EPlayerIndex ePlayer) { m_target_player = ePlayer; }
  EPlayerIndex GetTargetPlayer() const { return m_target_player; }

private:
  SCameraStatus m_status;
  using AStateMachine = ghl::TStateMachine<
    std::tuple<
      CCameraStateDefault
    , CCameraStateLockOn
    >
    ,CCamera
  >;
  AStateMachine m_stateMachine;
  EPlayerIndex m_target_player;
};

