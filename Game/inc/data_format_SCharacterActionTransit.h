﻿#pragma once

//自動生成.
//From db_action_character.

struct SCharacterActionTransit {
  int32_t source;
  int32_t target;
  uint16_t key1;
  uint8_t keystate1;
  uint8_t keycond1;
  uint16_t key2;
  uint8_t keystate2;
  uint8_t keycond2;
  float interp_time;
};
