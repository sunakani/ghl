﻿#pragma once

//自動生成.
//From db_place_character.

struct SCharacterPlacementData {
  uint32_t UID;
  uint32_t CharaType;
  int32_t initial_pos_x;
  int32_t initial_pos_y;
  int32_t initial_pos_z;
  int16_t initial_angle_y;
  uint16_t dummy00;
};
