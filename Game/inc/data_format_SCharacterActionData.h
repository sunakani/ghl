﻿#pragma once

//自動生成.
//From db_action_character.

struct SCharacterActionData {
  int32_t id;
  int32_t motion_id;
  int32_t next;
  float next_interp_frame;
  float update_scale;
};
