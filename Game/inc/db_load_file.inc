// ロードファイル定義.
DEF_LOAD_FILE_START()
DEF_LOAD_FILE(MODEL_MIKU,387628,"Data/Model/初音ミク.pmd")
DEF_LOAD_FILE(MODEL_MIKU_METAL,5285,"Data/Model/初音ミクmetal.pmd")
DEF_LOAD_FILE(MODEL_RUKA,529062,"Data/Model/巡音ルカ.pmd")
DEF_LOAD_FILE(MODEL_RIN,307621,"Data/Model/鏡音リン_act2.pmd")
DEF_LOAD_FILE(DB_PLACE_CHARA,2686460,"Data/Database/db_place_character.bin")
DEF_LOAD_FILE(MOTSET_HUMAN,465958,"Data/motion/mot_human.bin")
DEF_LOAD_FILE(STAGE_FLOOR_TEX,47620276,"Data/Stage/floor.png")
DEF_LOAD_FILE(ACT_HUMAN,52867,"Data/Database/db_action_human.bin")
DEF_LOAD_FILE(UI_TEST,5890258,"Data/UI/ui_test.jpg")
DEF_LOAD_FILE_END()
