﻿#pragma once

//自動生成.
//From db_action_character.

struct SCharacterActionEvent {
  int32_t target_action;
  int32_t frame_begin;
  int32_t frame_end;
  int32_t event_type;
  float arg1;
  float arg2;
  float arg3;
  float arg4;
};
