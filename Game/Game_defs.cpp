#include "Game_defs.h"

//static const AFloatColor s_clear_color = { 0.5f, 0.5f, 0.5f, 1.f };
//static const AFloatColor s_clear_color = { 0.f, 0.f, 0.f, 1.f };
//static const AFloatColor s_clear_color = { 255.f/255.f, 165.f/265.f, 0.f, 1.f };
static const AFloatColor s_clear_color = { 100.f/255.f, 200.f/265.f, 100.f, 1.f };

const AFloatColor& GetClearColor() {
  return s_clear_color;
}

float adjust_angle_y(float angle_y)
{
  if (std::abs(angle_y) > DirectX::XM_2PI) {
    auto num = std::floorf(angle_y / DirectX::XM_2PI);
    angle_y = angle_y >= 0.f ? (angle_y - num * DirectX::XM_2PI) : (angle_y + num * DirectX::XM_2PI);
  }
  if (angle_y > DirectX::XM_PI) {
    angle_y -= DirectX::XM_2PI;
  }
  else if (angle_y < -DirectX::XM_PI) {
    angle_y += DirectX::XM_2PI;
  }
  return angle_y;
}

float calc_diff_to_close_target(float adjusted_target_angle, float adjusted_source_angle)
{
  auto target = adjusted_target_angle;
  auto source = adjusted_source_angle;
  auto diff = target - source;
  if (diff > DirectX::XM_PI) {
    diff -= DirectX::XM_2PI;
  }
  else if (diff < -DirectX::XM_PI) {
    diff += DirectX::XM_2PI;
  }
  return diff;
}
