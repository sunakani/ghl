#pragma once

#include "GHL.h"

// グラフィックインターフェースの参照.
// グラフィック関連の操作はこの参照を介して行うことが多い.
extern ghl::IGraphic& gi();

// パイプラインステート.
struct SPipeline {
  ghl::HShader          hVS;
  ghl::HShader          hPS;
  ghl::HShader          hGS;
  ghl::HPipelineState   hState;
  void Destroy();
};

// ルートシグネチャ.
struct SRootSignature {
  ghl::HRootSignatur    hRootSignature;
  void Destroy();
};

// キャラ描画時のカメラ関連の定数バッファ.
struct SConstantBufferForView {
  DirectX::XMMATRIX matView;
  DirectX::XMMATRIX matPersProj;
  DirectX::XMMATRIX matLightCamera;
  DirectX::XMMATRIX matShadow;
  DirectX::XMFLOAT3 eye;
};

// キャラの骨バッファ.
using ACharacterBoneMatrices = std::array<DirectX::XMMATRIX,256>;

// キャラ描画時のキャラ関連の定数バッファ.
struct SConstantBufferForChara
{
  DirectX::XMMATRIX matWorld;
  ACharacterBoneMatrices localBoneMatrices;
};

// キャラ描画時のキャラ部位関連の定数バッファ.
struct SConstantBufferForCharaParts
{
  DirectX::XMFLOAT3 diffuse{};
  float             alpha{};
  DirectX::XMFLOAT3 specular{};
  float             specularity{};
  DirectX::XMFLOAT3 ambient{};
};

// スクリーン矩形の頂点フォーマット.
struct SScreenQuadVertex {
  DirectX::XMFLOAT3 pos;
  DirectX::XMFLOAT2 uv;
};

// デバッグ図形の頂点フォーマット.
struct SDebugFigureVertex {
  DirectX::XMFLOAT3 pos;
};
// デバッグ図形の定数バッファ.
struct SConstBufDbgFigure {
  DirectX::XMMATRIX matWorld{};
  DirectX::XMMATRIX matView{};
  DirectX::XMMATRIX matPersProj{};
  DirectX::XMFLOAT3 diffuse{};
  float alpha{};
  float thickness{};
};

// ステージ床の頂点フォーマット.
struct SStageFloorVertex {
  DirectX::XMFLOAT3 pos;
  DirectX::XMFLOAT3 normal;
  DirectX::XMFLOAT2 uv;
};
// ステージ床の定数バッファ.
struct SConstantBufferForStageFloor {
  DirectX::XMMATRIX matWorld;
  DirectX::XMMATRIX matView;
  DirectX::XMMATRIX matPersProj;
  DirectX::XMMATRIX matLightCamera;
};

// UIの定数バッファ.
struct SConstantBufferForUI
{
  DirectX::XMMATRIX matPersProj{};
  DirectX::XMFLOAT2 pos;
  DirectX::XMFLOAT2 size;
};

