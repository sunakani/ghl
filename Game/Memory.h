#pragma once

/************************************************/
/*  ヒープの管理.
*   現状は確保された情報を把握しておくだけの実装.
*************************************************/
class CHeap
{
public:
	CHeap()
		: m_allocated_size()
    , m_allocated_count()
	{}
  void* Allocate(size_t size);
  void Deallocate(void* p);
	size_t GetAllocatedSize() const { return m_allocated_size; }
  size_t GetAllocatedCount() const { return m_allocated_count; }
private:
	void AddAllocation(size_t size) {
		m_allocated_size += size;
    m_allocated_count += 1;
	}
	void RemoveAllocation(size_t size) {
		m_allocated_size -= size;
    m_allocated_count -= 1;
	}
private:
	size_t m_allocated_size{};
  size_t m_allocated_count{};
};
CHeap& GetDefaultHeap();

/************************************************/
/*  DefaultHeapを使ったSTL用アロケータ.
*************************************************/
template<typename T>
class StlDefaultHeapAllocator
{
public:
  using value_type = T;
  StlDefaultHeapAllocator() = default;
  StlDefaultHeapAllocator(const T&){}
  template<class U> StlDefaultHeapAllocator(const U&){}
  T* allocate(std::size_t num) {
    return static_cast<T*>(GetDefaultHeap().Allocate(num * sizeof(T)));
  }
  void deallocate(T* p, std::size_t num) {
    GetDefaultHeap().Deallocate(p);
  }
};
template<class T, class U>
inline bool operator==(const StlDefaultHeapAllocator<T>& lhs, const StlDefaultHeapAllocator<U>& rhs) {
  return true;
}
template<class T, class U>
inline bool operator!=(const StlDefaultHeapAllocator<T>& lhs, const StlDefaultHeapAllocator<U>& rhs) {
  return false;
}
