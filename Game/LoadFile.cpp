#include "LoadFile.h"
#include "Game_defs.h"
#include "Graphic.h"
#include "pmd/pmd.h"

/************************************************/
/*  ファイルIDとそれが示すファイルパスのセットを管理.
*************************************************/
CFilePathManager::CFilePathManager()
{}
CFilePathManager::~CFilePathManager()
{}
void CFilePathManager::Init()
{
	//m_map.reserve(?);
#define DEF_LOAD_FILE_START()
#define DEF_LOAD_FILE(id, value, path) m_map.emplace(ELoadFile::id, path);
#define DEF_LOAD_FILE_END()
#include "inc/db_load_file.inc"
#undef DEF_LOAD_FILE_START
#undef DEF_LOAD_FILE
#undef DEF_LOAD_FILE_END
}
void CFilePathManager::Term()
{
	m_map.clear();
}
const char* CFilePathManager::GetPath(ELoadFile eFile) const
{
	auto found = m_map.find(eFile);
	if (found != m_map.end()) {
		return (*found).second;
	}
	return nullptr;
}

/************************************************/
/*  テクスチャをロードしてハンドルを保持.
*************************************************/
CLoaderTexture::CLoaderTexture()
	: m_eFile(ELoadFile::None)
	, m_nRefCount(0)
  , m_bManagedWithHash(false)
  , m_hash(0)
	, m_hBuffer()
#if defined(_DEBUG)
  , m_debug_path()
#endif
{}
void CLoaderTexture::Init()
{
  m_eFile = ELoadFile::None;
  m_nRefCount = 0;
  m_bManagedWithHash = false;
  m_hash = 0;
  m_hBuffer.Invalidate();
#if defined(_DEBUG)
  m_debug_path.clear();
  m_debug_path.shrink_to_fit();
#endif
}
void CLoaderTexture::Term()
{
  m_eFile = ELoadFile::None;
  m_nRefCount = 0;
  m_bManagedWithHash = false;
  m_hash = 0;
  if (m_hBuffer) {
    gi().Texture_DestroyBuffer(m_hBuffer);
  }
#if defined(_DEBUG)
  m_debug_path.clear();
  m_debug_path.shrink_to_fit();
#endif
}
void CLoaderTexture::Load(ELoadFile eFile, const SGameInfo& info)
{
  if (m_nRefCount == 0)
  {
    if (auto* pPath = info.pFilePathManager->GetPath(eFile))
    {
      std::ifstream ifs(pPath, std::ios::in | std::ios::binary);
      _DEBUG_BREAK(ifs);
      if (ifs) {
    	  ghl::SBinary bin;
        ifs.seekg(0, std::ios::end);
        size_t size = ifs.tellg();
        bin.bin.resize(size);
        ifs.seekg(0, std::ios::beg);
        ifs.read(bin.bin.data(),size);
        ifs.close();
        m_hBuffer = gi().Texture_CreateBufferFromMemory(bin.bin.data(), bin.bin.size(), ghl::ETextureFormat::eWIC);
        _DEBUG_BREAK(m_hBuffer);
        if (m_hBuffer) {
          ++m_nRefCount;
          m_eFile = eFile;
        }
      }
    }
  }
  else
  {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderTexture::Load(size_t hash, const char* pPath, const SGameInfo& info)
{
  if (m_nRefCount == 0)
  {
    if (pPath != nullptr)
    {
      std::ifstream ifs(pPath, std::ios::in | std::ios::binary);
      _DEBUG_BREAK(ifs);
      if (ifs) {
    	  ghl::SBinary bin;
        ifs.seekg(0, std::ios::end);
        size_t size = ifs.tellg();
        bin.bin.resize(size);
        ifs.seekg(0, std::ios::beg);
        ifs.read(bin.bin.data(),size);
        ifs.close();
        m_hBuffer = gi().Texture_CreateBufferFromMemory(bin.bin.data(), bin.bin.size(), ghl::ETextureFormat::eWIC);
        _DEBUG_BREAK(m_hBuffer);
        if (m_hBuffer) {
          ++m_nRefCount;
          m_hash = std::hash<std::string>()(pPath);
          m_bManagedWithHash = true;
#if defined(_DEBUG)
          m_debug_path = pPath;
#endif
        }
      }
    }
  }
  else
  {
    if (hash != 0) {
      if (m_hash == hash) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderTexture::Unload()
{
  --m_nRefCount;
  _DEBUG_BREAK(m_nRefCount >= 0);
  if (m_nRefCount == 0) {
    if (m_hBuffer) {
      gi().Texture_DestroyBuffer(m_hBuffer);
    }
    m_eFile = ELoadFile::None;
    m_nRefCount = 0;
    m_hash = 0;
    m_bManagedWithHash = false;
#if defined(_DEBUG)
    m_debug_path.clear();
    m_debug_path.shrink_to_fit();
#endif
  }
}
bool CLoaderTexture::IsLoaded() const
{
  if (m_hBuffer) {
    return true;
  }
  return false;
}

/************************************************/
/*  頂点バッファとインデックスバッファの管理.
*************************************************/
CLoaderVertices::CLoaderVertices()
  : m_eFile(ELoadFile::None)
  , m_nRefCount(0)
  , m_hVertexBuffer()
  , m_hIndexBuffer()
  , m_unVertexNum(0)
  , m_unIndexNum(0)
  , m_unOneVertexBytes(0)
  , m_unOneIndexBytes(0)
{}
void CLoaderVertices::Init()
{
  m_eFile = ELoadFile::None;
  m_nRefCount = 0;
  m_hVertexBuffer.Invalidate();
  m_hIndexBuffer.Invalidate();
  m_unVertexNum = 0;
  m_unIndexNum = 0;
  m_unOneVertexBytes = 0;
  m_unOneIndexBytes = 0;
}
void CLoaderVertices::Term()
{
}
void CLoaderVertices::Load(ELoadFile eFile, const SGameInfo& info)
{
  if (m_nRefCount == 0) {
    if (auto* pModelPath = info.pFilePathManager->GetPath(eFile)) {
      FILE* fp = nullptr;
      fopen_s(&fp, pModelPath, "rb");
      if (fp) {
        Load(eFile, fp, info);
        fclose(fp);
      }
    }
  }
  else {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderVertices::Load(ELoadFile eFile, FILE* fp, const SGameInfo& info)
{
  if (m_nRefCount == 0) {
    if (fp != nullptr) {
      char signature[3] = {};
      ghl::pmd::SHeaderFormat pmdheader;
      fread(signature, sizeof(signature), 1, fp);
      fread(&pmdheader, sizeof(pmdheader), 1, fp);
      constexpr size_t pmd_vertex_size = 38;
      uint32_t vertNum = 0;
      fread(&vertNum, sizeof(vertNum), 1, fp);
      std::vector<char> vertices(vertNum * pmd_vertex_size);
      fread(vertices.data(), vertices.size(), 1, fp);
      uint32_t indexNum = 0;
      fread(&indexNum, sizeof(indexNum), 1, fp);
      std::vector<unsigned short> indices(indexNum);
      fread(indices.data(), indices.size() * sizeof(indices[0]), 1, fp);
      // 頂点.
      m_hVertexBuffer     = gi().Vertex_CreateBuffer(pmd_vertex_size, vertNum);
      m_unVertexNum       = vertNum;
      m_unOneVertexBytes  = pmd_vertex_size;
      gi().Vertex_UpdateBuffer(m_hVertexBuffer, vertices.data(), pmd_vertex_size, vertNum);
      // インデックス.
      m_hIndexBuffer    = gi().Index_CreateBuffer(sizeof(unsigned short), indexNum);
      m_unIndexNum      = indexNum;
      m_unOneIndexBytes = sizeof(unsigned short);
      gi().Index_UpdateBuffer(m_hIndexBuffer, reinterpret_cast<const char*>(indices.data()), sizeof(unsigned short), indexNum);
      // ファイルID保存.
      m_eFile = eFile;
      ++m_nRefCount;
    }
  }
  else {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderVertices::Unload()
{
  --m_nRefCount;
  _DEBUG_BREAK(m_nRefCount >= 0);
  if (m_nRefCount == 0) {
    gi().Vertex_DestroyBuffer(m_hVertexBuffer);
    gi().Index_DestroyBuffer(m_hIndexBuffer);
    m_eFile = ELoadFile::None;
    m_unVertexNum = 0;
    m_unIndexNum = 0;
    m_unOneVertexBytes = 0;
    m_unOneIndexBytes = 0;
  }
}
bool CLoaderVertices::IsLoaded() const {
  if (m_eFile != ELoadFile::None) {
    return true;
  }
  return false;
}

/**************************************************/
/*	スケルトンの管理.
***************************************************/
CLoaderSkeleton::CLoaderSkeleton()
  : m_eFile(ELoadFile::None)
  , m_nRefCount(0)
  , m_skeleton()
{}
void CLoaderSkeleton::Init()
{}
void CLoaderSkeleton::Term()
{}
void CLoaderSkeleton::Load(ELoadFile eFile, const SGameInfo& info)
{
  if (m_nRefCount == 0) {
    if (auto* pModelPath = info.pFilePathManager->GetPath(eFile)) {
      FILE* fp = nullptr;
      fopen_s(&fp, pModelPath, "rb");
      if (fp) {
        Load(eFile, fp, info);
        fclose(fp);
      }
    }
  }
  else {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderSkeleton::Load(ELoadFile eFile, FILE* fp, const SGameInfo& info)
{
  if (m_nRefCount == 0) {
    if (fp != nullptr) {
      m_skeleton = std::make_shared<SSkeleton>();
      if (auto* pDst = m_skeleton.get())
      {
        fread(&pDst->boneNum, sizeof(pDst->boneNum), 1, fp);
        constexpr size_t pmd_bone_size = 39;
        std::vector<char> temp(pmd_bone_size * pDst->boneNum);
        fread(temp.data(), temp.size(), 1, fp);
        std::vector<ghl::pmd::SBoneFormat> pmdBones(pDst->boneNum);
        // 解析.
        for (uint32_t ui=0; ui<pDst->boneNum; ++ui) {
          auto& dst = pmdBones[ui];
          auto* pSrc = &temp[pmd_bone_size * ui];
          std::memcpy(&dst, pSrc, 24);
          std::memcpy(&dst.type, pSrc + 24, 1);
          std::memcpy(&dst.ikBoneNo, pSrc + 25, 2);
          std::memcpy(&dst.pos, pSrc + 27, 12);
        }
        // ノードテーブル作成.
        pDst->boneIdx2Node.resize(pDst->boneNum);
        pDst->bindMatrices.resize(pDst->boneNum);
        for (uint32_t ui=0; ui<pDst->boneNum; ++ui) {
          auto& pb = pmdBones[ui];
          auto hash = std::hash<std::string>()(pb.boneName);
          auto ib = pDst->boneNodeTable.insert(std::make_pair(hash,SSkeleton::SBoneNode()));
          _DEBUG_BREAK(ib.second);
          if (ib.second) {
            auto& dst = (*ib.first).second;
            dst.hash = hash;
            dst.boneIdx = ui;
            dst.boneType = static_cast<EBoneType>(pb.type);
            dst.ikParentBone = pb.ikBoneNo;
            dst.startPos = pb.pos;
            if (strstr(pb.boneName, "ひざ") != NULL) {
              dst.knee = true;
            }
            if (pDst->boneIdxUpper == -1) {
              if (strstr(pb.boneName, "上半身") != NULL) {
                pDst->boneIdxUpper = ui;
              }
            }
#if defined(_DEBUG)
            dst.debugName = pb.boneName;
#endif
#if defined(_DEBUG) && 0
            DebugPrintf("[%3d]%s\n", ui, pb.boneName);
#endif
            auto m = DirectX::XMMatrixTranslationFromVector(DirectX::XMLoadFloat3(&dst.startPos));
            pDst->bindMatrices[ui] = XMMatrixInverse(nullptr, m);
            pDst->boneIdx2Node[ui] = &dst;
          }
        }
        for (uint32_t ui=0; ui<pDst->boneNum; ++ui) {
          auto& pb = pmdBones[ui];
          if (pb.parentNo >= pmdBones.size()) {
            continue;
          }
          auto myHash = pDst->boneIdx2Node[ui]->hash;
          auto myNodeFound = pDst->boneNodeTable.find(myHash);
          if (myNodeFound != pDst->boneNodeTable.end()) {
            auto parentHash = pDst->boneIdx2Node[pb.parentNo]->hash;
            auto parentNodeFound = pDst->boneNodeTable.find(parentHash);
            if (parentNodeFound != pDst->boneNodeTable.end()) {
              auto& dst = (*parentNodeFound).second;
              dst.children.emplace_back(&(*myNodeFound).second);
              (*myNodeFound).second.parent = &dst;
            }
          }
        }
        // IK.
        uint16_t ikNum = 0;
        fread(&ikNum, sizeof(ikNum), 1, fp);
        pDst->ikDatum.resize(ikNum);
        for (auto& ik : pDst->ikDatum) {
		      fread(&ik.boneIdx, sizeof(ik.boneIdx), 1, fp);
		      fread(&ik.targetIdx, sizeof(ik.targetIdx), 1, fp);
          char temp[7];
          fread(temp, sizeof(temp), 1, fp);
		      uint8_t chainLen = 0;
          std::memcpy(&chainLen,      &temp[0], 1);
          std::memcpy(&ik.iterations, &temp[1], 2);
          std::memcpy(&ik.limit,      &temp[3], 4);
		      if (chainLen != 0) {
		        ik.nodeIdxes.resize(chainLen);
		        fread(ik.nodeIdxes.data(), sizeof(ik.nodeIdxes[0]), chainLen, fp);
          }
	      }
#if defined(_DEBUG) && 0
        for (auto& ik : pDst->ikDatum) {
          auto idx2name = [&](int idx)->std::string {
            if (idx >= 0 && idx < pDst->boneIdx2Node.size()) {
              if (auto* pNode = pDst->boneIdx2Node.at(idx)) {
                return pNode->debugName;
              }
            }
            return "";
          };
          DebugPrintf("【IK=%d:%s】\n", ik.boneIdx, idx2name(ik.boneIdx).c_str());
          DebugPrintf(" ・ターゲット:\n     =%2d:%s\n", ik.targetIdx, idx2name(ik.targetIdx).c_str());
          DebugPrintf(" ・接続:\n");
          int i=0;
          for (auto node : ik.nodeIdxes) {
            DebugPrintf("  [%d]=%2d:%s\n", i, node, idx2name(node).c_str());
            ++i;
          }
        }
        {
          auto hash = std::hash<std::string>()("センター");
          auto found = pDst->boneNodeTable.find(hash);
          if (found != pDst->boneNodeTable.end()) {
            if (auto* pRoot = &((*found).second)) {
              struct rec {
                static void Do(const SSkeleton::SBoneNode* pNode, int nIndent) {
                  if (pNode != nullptr) {
                    DebugPrintf("[%3d]", pNode->boneIdx);
                    for (int i=0; i<nIndent; ++i) {
                      DebugPrint(" ");
                    }
                    DebugPrintf("%s\n", pNode->debugName.c_str());
                    for (auto* pChild : pNode->children) {
                      Do(pChild, nIndent+1);
                    }
                  }
                }
              };
              rec::Do(pRoot, 0);
            }
          }
        }
#endif
        m_eFile = eFile;
        ++m_nRefCount;
      }
    }
  }
  else
  {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderSkeleton::Unload()
{
  --m_nRefCount;
  _DEBUG_BREAK(m_nRefCount >= 0);
  if (m_nRefCount == 0) {
    m_skeleton.reset();
    m_eFile = ELoadFile::None;
  }
}
bool CLoaderSkeleton::IsLoaded() const {
  if (m_skeleton) {
    return true;
  }
  return false;
}
#if defined(_DEBUG)
void SSkeleton::DebugDrawSphere(int32_t idx, const DirectX::XMMATRIX& matMotion, const DirectX::XMMATRIX& matL2W, CDebugFigure& debugFigure, float radius, const AFloatColor& color)
{
  if (idx >= 0 && idx < boneIdx2Node.size()) {
    if (auto* pNode = boneIdx2Node.at(idx)) {
      auto vec = DirectX::XMVector3Transform(XMLoadFloat3(&pNode->startPos), matMotion);
      vec = DirectX::XMVector3Transform(vec, matL2W);
      DirectX::XMFLOAT3 pos;
      DirectX::XMStoreFloat3(&pos, vec);
      debugFigure.AddSphere(pos, radius, color);
    }
  }
}
void SSkeleton::DebugDrawLine(int32_t from, int32_t to, const DirectX::XMMATRIX& matMotionFrom, const DirectX::XMMATRIX& matMotionTo, const DirectX::XMMATRIX& matL2W, CDebugFigure& debugFigure, float thickness, const AFloatColor& color)
{
  if (from >= 0 && from < boneIdx2Node.size()) {
    if (to >= 0 && to < boneIdx2Node.size()) {
      if (auto* pFrom = boneIdx2Node.at(from)) {
        if (auto* pTo = boneIdx2Node.at(to)) {
          auto vecFrom = DirectX::XMVector3Transform(XMLoadFloat3(&pFrom->startPos), matMotionFrom);
          vecFrom = DirectX::XMVector3Transform(vecFrom, matL2W);
          DirectX::XMFLOAT3 posFrom;
          DirectX::XMStoreFloat3(&posFrom, vecFrom);
          auto vecTo = DirectX::XMVector3Transform(XMLoadFloat3(&pTo->startPos), matMotionTo);
          vecTo = DirectX::XMVector3Transform(vecTo, matL2W);
          DirectX::XMFLOAT3 posTo;
          DirectX::XMStoreFloat3(&posTo, vecTo);
          debugFigure.AddLine(posFrom, posTo, color, thickness);
        }
      }
    }
  }
}
#endif

namespace {

void SetupMotion(SMotion& target, FILE* fp)
{
  char header[50];
  fread(header, 50, 1, fp);
  unsigned int motionDataNum = 0;
  fread(&motionDataNum, sizeof(motionDataNum), 1, fp);
  std::vector<ghl::pmd::VMDMotion> vmdMotionData(motionDataNum);
  for (auto& motion : vmdMotionData) {
    constexpr size_t vmd_motion_size = 111;
    std::vector<char> temp(vmd_motion_size);
    fread(temp.data(), vmd_motion_size, 1, fp);
    auto& dst = motion;
    auto* pSrc = temp.data();
    std::memcpy(&dst.boneName[0], pSrc, 15);
    std::memcpy(&dst.frameNo, pSrc + 15, 96);
    auto hash = std::hash<std::string>()(dst.boneName);
    auto ib = target.plots_by_bones.insert(std::make_pair(hash,std::vector<SMotion::SPlot>()));
    (*ib.first).second.push_back(
      SMotion::SPlot(
        dst.frameNo,
        DirectX::XMLoadFloat4(&dst.quaternion),
        dst.location,
        DirectX::XMFLOAT2(static_cast<float>(dst.bezier[ 3]) / 127.f, static_cast<float>(dst.bezier[ 7]) / 127.f),
        DirectX::XMFLOAT2(static_cast<float>(dst.bezier[11]) / 127.f, static_cast<float>(dst.bezier[15]) / 127.f)
      )
    );
    target.duration = std::max<uint32_t>(dst.frameNo, target.duration);
  }
  for (auto& bonemotion : target.plots_by_bones) {
    std::sort(bonemotion.second.begin(), bonemotion.second.end(), [](const SMotion::SPlot& lhs, const SMotion::SPlot& rhs){
      return lhs.frameNo <= rhs.frameNo;
      });
  }
}

}

/**************************************************/
/*	モーションの管理.
***************************************************/
CLoaderMotion::CLoaderMotion()
  : m_eFile(ELoadFile::None)
  , m_nRefCount(0)
  , m_motion()
{}
void CLoaderMotion::Init()
{}
void CLoaderMotion::Term()
{}
void CLoaderMotion::Load(ELoadFile eFile, const SGameInfo& info)
{
  if (m_nRefCount == 0) {
    if (eFile != ELoadFile::None) {
      if (auto* pPath = info.pFilePathManager->GetPath(eFile)) {
        FILE* fp = nullptr;
        fopen_s(&fp, pPath, "rb");
        if (fp != nullptr) {
          m_motion = std::make_shared<SMotion>();
          if (auto* pMotion = m_motion.get()) {
            SetupMotion(*pMotion, fp);
            m_eFile = eFile;
            ++m_nRefCount;
          }
          fclose(fp);
        }
      }
    }
  }
  else {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderMotion::Unload()
{
  --m_nRefCount;
  _DEBUG_BREAK(m_nRefCount >= 0);
  if (m_nRefCount == 0) {
    m_motion.reset();
    m_eFile = ELoadFile::None;
  }
}
bool CLoaderMotion::IsLoaded() const {
  if (m_motion) {
    return true;
  }
  return false;
}

/**************************************************/
/*	モーションセットの管理.
***************************************************/
CLoaderMotionSet::CLoaderMotionSet()
  : m_eFile(ELoadFile::None)
  , m_nRefCount(0)
  , m_motion_set()
{}
void CLoaderMotionSet::Init()
{
}
void CLoaderMotionSet::Term()
{
}
void CLoaderMotionSet::Load(ELoadFile eFile, const SGameInfo& info)
{
  if (m_nRefCount == 0) {
    if (eFile != ELoadFile::None) {
      if (auto* pPath = info.pFilePathManager->GetPath(eFile)) {
        FILE* fp = nullptr;
        fopen_s(&fp, pPath, "rb");
        if (fp != nullptr) {
          m_motion_set = std::make_shared<std::vector<SMotion>>();
          unsigned int motionDataNum = 0;
          fread(&motionDataNum, sizeof(motionDataNum), 1, fp);
          m_motion_set->resize(motionDataNum);
          std::vector<fpos_t> positions;
          positions.reserve(motionDataNum);
          for (unsigned int ui=0; ui<motionDataNum; ++ui) {
            unsigned int bytes_size = 0;
            fread(&bytes_size, sizeof(bytes_size), 1, fp);
            fpos_t p;
            fgetpos(fp, &p);
            positions.push_back(p);
            fseek(fp, bytes_size, SEEK_CUR);
          }
          for (unsigned int ui=0; ui<motionDataNum; ++ui) {
            fsetpos(fp, &positions[ui]);
            SetupMotion(m_motion_set->at(ui), fp);
          }
          fclose(fp);
          m_eFile = eFile;
          ++m_nRefCount;
        }
      }
    }
  }
  else {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderMotionSet::Unload()
{
  --m_nRefCount;
  _DEBUG_BREAK(m_nRefCount >= 0);
  if (m_nRefCount == 0) {
    m_motion_set.reset();
    m_eFile = ELoadFile::None;
  }
}
bool CLoaderMotionSet::IsLoaded() const
{
  if (m_motion_set) {
    return true;
  }
  return false;
}

/**************************************************/
/*	アクションの管理.
***************************************************/
CLoaderAction::CLoaderAction()
  : m_eFile(ELoadFile::None)
  , m_nRefCount(0)
  , m_data()
{}
void CLoaderAction::Init()
{}
void CLoaderAction::Term()
{}
void CLoaderAction::Load(ELoadFile eFile, const SGameInfo& info)
{
  if (m_nRefCount == 0) {
    if (eFile != ELoadFile::None) {
      if (auto* pPath = info.pFilePathManager->GetPath(eFile)) {
        FILE* fp = nullptr;
        fopen_s(&fp, pPath, "rb");
        if (fp != nullptr) {
          m_data = std::make_shared<SActionData>();

          uint32_t data_num = 0;
          fread(&data_num, sizeof(data_num), 1, fp);

          // 行動.
          uint32_t data_bytes = 0;
          fread(&data_bytes, sizeof(data_bytes), 1, fp);
          ghl::SBinary bin_data;
          bin_data.bin.resize(data_bytes);
          fread(bin_data.bin.data(), data_bytes, 1, fp);
          m_data->db_action.Init(bin_data.bin);

          // 遷移.
          fread(&data_bytes, sizeof(data_bytes), 1, fp);
          bin_data.bin.resize(data_bytes);
          fread(bin_data.bin.data(), data_bytes, 1, fp);
          m_data->db_trans.Init(bin_data.bin);

          // イベント.
          fread(&data_bytes, sizeof(data_bytes), 1, fp);
          bin_data.bin.resize(data_bytes);
          fread(bin_data.bin.data(), data_bytes, 1, fp);
          m_data->db_event.Init(bin_data.bin);

          fclose(fp);
          m_eFile = eFile;
          ++m_nRefCount;
        }
      }
    }
  }
  else {
    if (eFile != ELoadFile::None) {
      if (m_eFile == eFile) {
        ++m_nRefCount;
      }
    }
  }
}
void CLoaderAction::Unload()
{
  --m_nRefCount;
  _DEBUG_BREAK(m_nRefCount >= 0);
  if (m_nRefCount == 0) {
    m_data.reset();
    m_eFile = ELoadFile::None;
  }
}
bool CLoaderAction::IsLoaded() const
{
  if (m_data) {
    return true;
  }
  return false;
}

/************************************************/
/*  ロード管理.
*************************************************/
CLoadManager::CLoadManager()
{}
CLoadManager::~CLoadManager()
{}
void CLoadManager::Init()
{
  m_textures.clear();
  m_textures_managed_hash.clear();
  m_vertices.clear();
  m_skeletons.clear();
  m_motions.clear();
  m_motion_sets.clear();
  m_actions.clear();
}
namespace {
template<typename T> void TerminateMap(T& target_map) {
  for (auto& elem : target_map) {
    if (elem.second.IsLoaded()) {
      elem.second.Term();
    }
  }
  target_map.clear();
}
}
void CLoadManager::Term()
{
  TerminateMap(m_textures);
  TerminateMap(m_textures_managed_hash);
  TerminateMap(m_vertices);
  TerminateMap(m_skeletons);
  TerminateMap(m_motions);
  TerminateMap(m_motion_sets);
  TerminateMap(m_actions);
}
const CLoaderTexture* CLoadManager::LoadTexture(ELoadFile eFile, const SGameInfo& info)
{
  auto ib = m_textures.insert(std::make_pair(eFile,CLoaderTexture()));
  (*ib.first).second.Load(eFile, info);
  return &(*ib.first).second;
}
const CLoaderTexture* CLoadManager::LoadTexture(const char* pPath, const SGameInfo& info)
{
  auto hash = std::hash<std::string>()(pPath);
  auto ib = m_textures_managed_hash.insert(std::make_pair(hash,CLoaderTexture()));
  (*ib.first).second.Load(hash, pPath, info);
  return &(*ib.first).second;
}
void CLoadManager::UnloadTexture(ELoadFile eFile)
{
  auto found = m_textures.find(eFile);
  if (found != m_textures.end()) {
    (*found).second.Unload();
  }
}
void CLoadManager::UnloadTexture(const char* pPath)
{
  auto hash = std::hash<std::string>()(pPath);
  auto found = m_textures_managed_hash.find(hash);
  if (found != m_textures_managed_hash.end()) {
    (*found).second.Unload();
  }
}
void CLoadManager::UnloadTexture(size_t hash)
{
  auto found = m_textures_managed_hash.find(hash);
  if (found != m_textures_managed_hash.end()) {
    (*found).second.Unload();
  }
}
bool CLoadManager::IsTextureLoaded(ELoadFile eFile) const
{
  auto found = m_textures.find(eFile);
  if (found != m_textures.end()) {
    return (*found).second.IsLoaded();
  }
  return false;
}
bool CLoadManager::IsTextureLoaded(const char* pPath) const
{
  if (pPath != nullptr) {
    auto hash = std::hash<std::string>()(pPath);
    auto found = m_textures_managed_hash.find(hash);
    if (found != m_textures_managed_hash.end()) {
      return (*found).second.IsLoaded();
    }
  }
  return false;
}
const CLoaderTexture* CLoadManager::GetTexture(ELoadFile eFile) const
{
  auto found = m_textures.find(eFile);
  if (found != m_textures.end()) {
    return &(*found).second;
  }
  return nullptr;
}
const CLoaderTexture* CLoadManager::GetTexture(const char* pPath) const
{
  if (pPath != nullptr) {
    auto hash = std::hash<std::string>()(pPath);
    auto found = m_textures_managed_hash.find(hash);
    if (found != m_textures_managed_hash.end()) {
      return &(*found).second;
    }
  }
  return nullptr;
}
const CLoaderVertices* CLoadManager::LoadVertices(ELoadFile eFile, const SGameInfo& info)
{
  auto ib = m_vertices.insert(std::make_pair(eFile,CLoaderVertices()));
  (*ib.first).second.Load(eFile, info);
  return &(*ib.first).second;
}
const CLoaderVertices* CLoadManager::LoadVertices(ELoadFile eFile, FILE* fp, const SGameInfo& info)
{
  auto ib = m_vertices.insert(std::make_pair(eFile,CLoaderVertices()));
  (*ib.first).second.Load(eFile, fp, info);
  return &(*ib.first).second;
}
void CLoadManager::UnloadVertices(ELoadFile eFile)
{
  auto found = m_vertices.find(eFile);
  if (found != m_vertices.end()) {
    (*found).second.Unload();
  }
}
const CLoaderVertices* CLoadManager::GetVertices(ELoadFile eFile) const
{
  auto found = m_vertices.find(eFile);
  if (found != m_vertices.end()) {
    return &(*found).second;
  }
  return nullptr;
}
const CLoaderSkeleton* CLoadManager::LoadSkeleton(ELoadFile eFile, const SGameInfo& info)
{
  auto ib = m_skeletons.insert(std::make_pair(eFile,CLoaderSkeleton()));
  (*ib.first).second.Load(eFile, info);
  return &(*ib.first).second;
}
const CLoaderSkeleton* CLoadManager::LoadSkeleton(ELoadFile eFile, FILE* fp, const SGameInfo& info)
{
  auto ib = m_skeletons.insert(std::make_pair(eFile,CLoaderSkeleton()));
  (*ib.first).second.Load(eFile, fp, info);
  return &(*ib.first).second;
}
void CLoadManager::UnloadSkeleton(ELoadFile eFile)
{
  auto found = m_skeletons.find(eFile);
  if (found != m_skeletons.end()) {
    (*found).second.Unload();
  }
}
const CLoaderSkeleton* CLoadManager::GetSkeleton(ELoadFile eFile) const
{
  auto found = m_skeletons.find(eFile);
  if (found != m_skeletons.end()) {
    return &(*found).second;
  }
  return nullptr;
}
const CLoaderMotion* CLoadManager::LoadMotion(ELoadFile eFile, const SGameInfo& info)
{
  auto ib = m_motions.insert(std::make_pair(eFile,CLoaderMotion()));
  (*ib.first).second.Load(eFile, info);
  return &(*ib.first).second;
}
void CLoadManager::UnloadMotion(ELoadFile eFile)
{
  auto found = m_motions.find(eFile);
  if (found != m_motions.end()) {
    (*found).second.Unload();
  }
}
const CLoaderMotion* CLoadManager::GetMotion(ELoadFile eFile) const
{
  auto found = m_motions.find(eFile);
  if (found != m_motions.end()) {
    return &(*found).second;
  }
  return nullptr;
}
const CLoaderMotionSet* CLoadManager::LoadMotionSet(ELoadFile eFile, const SGameInfo& info)
{
  auto ib = m_motion_sets.insert(std::make_pair(eFile,CLoaderMotionSet()));
  (*ib.first).second.Load(eFile, info);
  return &(*ib.first).second;
}
void CLoadManager::UnloadMotionSet(ELoadFile eFile)
{
  auto found = m_motion_sets.find(eFile);
  if (found != m_motion_sets.end()) {
    (*found).second.Unload();
  }
}
const CLoaderMotionSet* CLoadManager::GetMotionSet(ELoadFile eFile) const
{
  auto found = m_motion_sets.find(eFile);
  if (found != m_motion_sets.end()) {
    return &(*found).second;
  }
  return nullptr;
}
const CLoaderAction* CLoadManager::LoadAction(ELoadFile eFile, const SGameInfo& info) {
  auto ib = m_actions.insert(std::make_pair(eFile,CLoaderAction()));
  (*ib.first).second.Load(eFile, info);
  return &(*ib.first).second;
}
void CLoadManager::UnloadAction(ELoadFile eFile) {
  auto found = m_actions.find(eFile);
  if (found != m_actions.end()) {
    (*found).second.Unload();
  }
}
const CLoaderAction* CLoadManager::GetAction(ELoadFile eFile) const {
  auto found = m_actions.find(eFile);
  if (found != m_actions.end()) {
    return &(*found).second;
  }
  return nullptr;
}


