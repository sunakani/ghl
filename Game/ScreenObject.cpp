#include "ScreenObject.h"
#include "Graphic.h"

CScreenObject::CScreenObject()
  : m_eType(EScreenObjectType::eSimple)
  , m_hVertexBuffer()
  , m_hVertexView()
  , m_hDescHeap()
  , m_hConstBuffer()
  , m_hTexColor()
  , m_hTexDepth()
  , m_pos{}
  , m_size{100.f, 100.f}
{}

void CScreenObject::Init(const SGameInfo& info, EScreenObjectType eType)
{
  m_eType = eType;
  SScreenQuadVertex sv[4] = {
    {{0, 1, 0.1f}, {0.f, 0.f}}, // 左上.
    {{1, 1, 0.1f}, {1.f, 0.f}}, // 右上.
    {{0, 0, 0.1f}, {0.f, 1.f}}, // 左下.
    {{1, 0, 0.1f}, {1.f, 1.f}}, // 右下.
  };
  m_hVertexBuffer = gi().Vertex_CreateBuffer(sizeof(sv[0]), _countof(sv));
  m_hVertexView = gi().Vertex_CreateView(m_hVertexBuffer, sizeof(sv[0]), _countof(sv));
  gi().Vertex_UpdateBuffer(m_hVertexBuffer, sv, sizeof(sv[0]), _countof(sv));

  // ディスクリプタ.
  uint16_t unDescHeapNum = m_eType == EScreenObjectType::eSceneQuad ? 3 : 2;
  m_hDescHeap = gi().DescHeap_Create(ghl::EDescriptorHeapType::eCBV_SRV_UAV, unDescHeapNum, true);
  {
    // 定数バッファ.
    m_hConstBuffer = gi().Const_CreateBuffer(sizeof(SConstantBufferForUI));
    SConstantBufferForUI tmp{};
    gi().Const_UpdateBuffer(m_hConstBuffer, &tmp, sizeof(tmp));
    gi().DescHeap_PlaceConstView(m_hDescHeap, m_hConstBuffer, e2ut(EScreenObjectDescriptorIndex::e0_Constant));
  }
}

void CScreenObject::Term(const SGameInfo& info)
{
  if (m_hConstBuffer) {
    gi().Const_DestroyBuffer(m_hConstBuffer);
  }
  if (m_hDescHeap) {
    gi().DescHeap_Destroy(m_hDescHeap);
  }
  if (m_hVertexView) {
    gi().Vertex_DestroyView(m_hVertexBuffer, m_hVertexView);
  }
  if (m_hVertexBuffer) {
    gi().Vertex_DestroyBuffer(m_hVertexBuffer);
  }
  m_hTexColor.Invalidate();
  m_hTexDepth.Invalidate();
}

void CScreenObject::SetColorTexture(ghl::HTextureBuffer hTex)
{
  m_hTexColor = hTex;
  gi().DescHeap_PlaceTextureView(m_hDescHeap, m_hTexColor, e2ut(EScreenObjectDescriptorIndex::e1_TexColor));
}

void CScreenObject::SetDepthTexture(ghl::HTextureBuffer hTex)
{
  m_hTexDepth = hTex;
  gi().DescHeap_PlaceTextureView(m_hDescHeap, m_hTexDepth, e2ut(EScreenObjectDescriptorIndex::e2_TexDepth), ghl::EGraphInfraFormat::eR32_Float);
}

void CScreenObject::Update(const SGameUpdateParam& update_param)
{
}

void CScreenObject::PreDraw(const SGameUpdateParam& update_param)
{
  SConstantBufferForUI buf;
  buf.matPersProj = update_param.matOrthProj;
  buf.pos = m_pos;
  buf.size = m_size;
  gi().Const_UpdateBuffer(m_hConstBuffer, &buf, sizeof(buf));
}

void CScreenObject::Draw()
{
  gi().Cmd_Vertex_Set(m_hVertexBuffer, m_hVertexView);
  gi().Cmd_DescHeap_Set(m_hDescHeap);
  gi().Cmd_DescHeap_SetRootParam(m_hDescHeap, 0, 0);
  gi().Cmd_DescHeap_SetRootParam(m_hDescHeap, 1, 1);
  gi().Cmd_Draw_Instanced(4, 1, 0, 0);
}
