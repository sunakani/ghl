#pragma once

#include "Graphic.h"
#include "ScreenObject.h"
#include "LoadFile.h"

class CScreenObjectManager
{
public:
	CScreenObjectManager();

	void Init(const SGameInfo& info, EGameDisplayMode eDispMode);
	void Term(const SGameInfo& info);

	void Update(const SGameUpdateParam& update_param);

	void PreDraw(const SGameUpdateParam& update_param);
	void Draw();
	void DrawSceneQuad();

	void ChangeDispModeDefault();
	void ChangeDispModeWithDebug();

private:
	void InitPipeline();
	void TermPipeline();

private:
	SPipeline				m_pso;
	SRootSignature	m_rootsig;

	CScreenObject				m_test_object;
	ghl::HTextureBuffer	m_hTestTexture;

	struct SAutoTransit
	{
		bool bProcess{};
		DirectX::XMFLOAT2	pos_prev{};
		DirectX::XMFLOAT2	pos_next{};
		DirectX::XMFLOAT2	size_prev{};
		DirectX::XMFLOAT2	size_next{};
		float duration{};
		float elapsed{};
		void Init() {
			bProcess = false;
			pos_prev.x	= pos_prev.y	= 0.f;
			pos_next.x	= pos_next.y	= 0.f;
			size_prev.x	= size_prev.y	= 0.f;
			size_next.x	= size_next.y	= 0.f;
			duration = 0.f;
			elapsed = 0.f;
		}
	};
	SAutoTransit m_auto_transit;

	// シーンを描画したテクスチャを張る矩形.
	CScreenObject m_scene_quad;

	// 表示モード切り替え.
	EGameDisplayMode	m_curr_disp_mode;
	EGameDisplayMode	m_next_disp_mode;
};

