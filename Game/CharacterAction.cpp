#include "CharacterAction.h"
#include "Player.h"
#include "Character.h"
#include "Camera.h"

CCharacterActionController::CCharacterActionController(CCharacter* pOwner)
	: m_pOwner(pOwner)
	, m_actions()
	, m_state()
	, m_action_uid(0)
{}

void CCharacterActionController::Init(std::weak_ptr<SActionData> action)
{
	auto shared_action = action.lock();
	if (auto* pAction = shared_action.get()) {
		m_actions.resize(pAction->db_action.Num());
		for (size_t i=0; i<m_actions.size(); ++i) {
			m_actions[i].pData = pAction->db_action.GetByIndex(i);
			auto actionId = m_actions[i].pData->id;
			for (int j=0; j<pAction->db_trans.Num(); ++j) {
				if (auto* pTrans = pAction->db_trans.GetByIndex(j)) {
					if (pTrans->source == actionId) {
						m_actions[i].trans.push_back(pTrans);
					}
				}
			}
			for (int j=0; j<pAction->db_event.Num(); ++j) {
				if (auto* pEvent = pAction->db_event.GetByIndex(j)) {
					if (pEvent->target_action == actionId) {
						m_actions[i].events.push_back(pEvent);
					}
				}
			}
		}
	}
	if (m_actions.size() > 0) {
		OnBeginAction(m_actions[0], 0.f);
	}
}

void CCharacterActionController::Term()
{
	m_actions.clear();
	m_actions.shrink_to_fit();
}

void CCharacterActionController::UpdateTransit(const SGameUpdateParam& update_param)
{
	if (m_state.pCurrent != nullptr)
	{
		bool bTransit = false;
		if (bTransit == false) {
			// モーションが終了した.
			if (m_pOwner->GetAnimator().IsCurrentMotionFinished(m_state.pCurrent->pData->motion_id)) {
				auto next = m_state.pCurrent->pData->next;
				bool bLoop = m_state.pCurrent->pData->motion_id == next;
				float interp_frame = m_state.pCurrent->pData->next_interp_frame;
				OnEndAction(*m_state.pCurrent);
				if (next >= 0 && next < m_actions.size()) {
					OnBeginAction(m_actions[next], interp_frame);
				}
				bTransit = true;
			}
		}
		if (bTransit == false) {
			// 遷移ルールを満たした.
			for (auto* pRule  : m_state.pCurrent->trans) {
				if (pRule != nullptr) {
					if (CheckRule(*pRule, update_param)) {
						auto next = pRule->target;
						OnEndAction(*m_state.pCurrent);
						if (next >= 0 && next < m_actions.size()) {
							OnBeginAction(m_actions[next], pRule->interp_time);
						}
						bTransit = true;
						break;
					}
				}
			}
		}
	}
}

namespace {

	float GetFrontAngleByCamera(const CCamera& camera) {
		auto& camera_status = camera.GetStatus();
		auto dir = camera_status.at - camera_status.eye;
		dir.y = 0.f;
		auto normal = DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&dir));
		auto angle_y = std::acosf(DirectX::XMVectorGetZ(normal));
		if (dir.x < 0.f) {
			angle_y = -angle_y;
		}
		return angle_y;
	}

}

void CCharacterActionController::UpdateEvent(const SGameUpdateParam& update_param)
{
	if (m_state.pCurrent != nullptr)
	{
		for (auto* pEvent : m_state.pCurrent->events) {
			if (pEvent != nullptr) {
				if (0){}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::MOVE) {
					if (m_pOwner != nullptr) {
						m_pOwner->MoveFront(0.1f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_FRONT) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_LEFT) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					angle_y -= DirectX::XM_PIDIV2;
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_RIGHT) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					angle_y += DirectX::XM_PIDIV2;
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_BACK) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					angle_y += DirectX::XM_PI;
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_FL) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					angle_y -= DirectX::XM_PIDIV4;
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_BL) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					angle_y -= (DirectX::XM_PIDIV4 * 3.f);
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_BR) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					angle_y += (DirectX::XM_PIDIV4 * 3.f);
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
				else if (static_cast<EActionEvent>(pEvent->event_type) == EActionEvent::ROT_FR) {
					auto angle_y = GetFrontAngleByCamera(*update_param.pCamera);
					angle_y += DirectX::XM_PIDIV4;
					if (m_pOwner != nullptr) {
						m_pOwner->SetDesiredAngleY(angle_y,0.01f);
					}
				}
			}
		}
	}
}

int32_t CCharacterActionController::GetCurrentActionId() const
{
	if (auto* pCurrent = GetActionState().pCurrent) {
		if (pCurrent->pData != nullptr) {
			return pCurrent->pData->id;
		}
	}
	return -1;
}

bool CCharacterActionController::CheckRule(const SCharacterActionTransit& rule, const SGameUpdateParam& update_param) const
{
	if (static_cast<ghl::EKey>(rule.key1) != ghl::EKey::NONE) {
		bool bSatisfied = false;
		if (m_pOwner->GetPlayer() != EPlayerIndex::None) {
			if (auto* pInput = update_param.pGameInfo->pPlayerManager->GetActivePlayerInput(m_pOwner->GetPlayer())) {
				bool bKeyCondition = pInput->IsKeybordKeyState(static_cast<ghl::EKey>(rule.key1), static_cast<ghl::EKeyState>(rule.keystate1));
				if (bKeyCondition == static_cast<bool>(rule.keycond1)) {
					bSatisfied = true;
				}
			}
		}
		if (bSatisfied == false) {
			return false;
		}
	}
	if (static_cast<ghl::EKey>(rule.key2) != ghl::EKey::NONE) {
		bool bSatisfied = false;
		if (m_pOwner->GetPlayer() != EPlayerIndex::None) {
			if (auto* pInput = update_param.pGameInfo->pPlayerManager->GetActivePlayerInput(m_pOwner->GetPlayer())) {
				bool bKeyCondition = pInput->IsKeybordKeyState(static_cast<ghl::EKey>(rule.key2), static_cast<ghl::EKeyState>(rule.keystate2));
				if (bKeyCondition == static_cast<bool>(rule.keycond2)) {
					bSatisfied = true;
				}
			}
		}
		if (bSatisfied == false) {
			return false;
		}
	}
	return true;
}

void CCharacterActionController::OnBeginAction(const SAction& action, float interp_time)
{
	m_state.pCurrent		= &action;
	m_state.uid					= m_action_uid;
	m_state.interp_time	= interp_time;
	++m_action_uid;
	if (m_action_uid == 0xFFFFFFFF) {
		m_action_uid = 0;
	}
#if defined(_DEBUG) && 0
	DebugPrintf("[Action] begin = %d.\n", action.id);
#endif
}

void CCharacterActionController::OnEndAction(const SAction& action)
{
#if defined(_DEBUG) && 0
	DebugPrintf("[Action] end = %d.\n", action.id);
#endif
	m_state.pCurrent		= nullptr;
	m_state.uid					= 0xFFFFFFFF;
	m_state.interp_time	= 0.f;
}

CCharacterActionController::SAction* CCharacterActionController::GetAction(int32_t id)
{
	for (auto& elem : m_actions) {
		if (elem.pData->id == id) {
			return &elem;
		}
	}
	return nullptr;
}
