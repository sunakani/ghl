#pragma once

#include "input/Input.h"
#include "Game_defs.h"
#include "LoadFile.h"

class CCharacter;

// アクションイベント定義.
enum class EActionEvent
{
	NONE,
	MOVE,				// 移動.
	ROT_FRONT,	// カメラに対して前方に回転.
	ROT_LEFT,		// カメラに対して左方に回転.
	ROT_BACK,		// カメラに対して後方に回転.
	ROT_RIGHT,	// カメラに対して右方に回転.
	ROT_FL,			// カメラに対して前左方向に回転.
	ROT_BL,			// カメラに対して後左方向に回転.
	ROT_BR,			// カメラに対して後右方向に回転.
	ROT_FR,			// カメラに対して前右方向に回転.
};

/************************************************/
/*  キャラクターのアクションを管理.
*		キャラクターの状態をアクションという名のステートに分割し、
*		アクションごとに特色を持たせたり、
*		アクション間のつながりを司るクラス.
*
*		◆行動(Action)
*				キャラクターのステートの最小単位.
*		◆遷移(Transit)
*				ひとつの行動から別の行動につながるための条件.
*				例えば、待機行動の時に移動入力を行うことで走り行動に遷移する、といった具合.
*		◆イベント(Event)
*				行動のモーション中に特定フレーム中だけ何かしらの効果を持たせたりする機能.
*				例えば、走り行動中は進む方向に体を回転させ続ける、といった効果の実装.
* 
*************************************************/
class CCharacterActionController
{
public:
	CCharacterActionController() = delete;
	CCharacterActionController(CCharacter* pOwner);
	void Init(std::weak_ptr<SActionData> action);
	void Term();

	void UpdateTransit(const SGameUpdateParam& update_param);
	void UpdateEvent(const SGameUpdateParam& update_param);
	
	struct SAction
	{
		const SCharacterActionData*								pData{};
		std::list<const SCharacterActionTransit*>	trans{};
		std::list<const SCharacterActionEvent*>		events{};
	};
	struct SActionState {
		const SAction*			pCurrent{};
		uint32_t						uid{0xFFFFFFFF};
		float								interp_time{};
	};
	const SActionState& GetActionState() const { return m_state; }
	int32_t GetCurrentActionId() const;

private:
	bool CheckRule(const SCharacterActionTransit& rule, const SGameUpdateParam& update_param) const;
	SAction* GetAction(int32_t id);
	void OnBeginAction(const SAction& action, float interp_time);
	void OnEndAction(const SAction& action);

private:
	CCharacter* const			m_pOwner{};
	std::vector<SAction>	m_actions;
	SActionState					m_state;
	uint32_t							m_action_uid;
};



