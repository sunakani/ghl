#pragma once

#include "GHL.h"	
#include "Graphic.h"
#include "Game_defs.h"

/************************************************************/
/*	ステージ関連のオブジェクトをまとめるクラス.
*************************************************************/
class CStageManager
{
public:
	CStageManager();
	void Init(const SGameInfo& info);
	void Term(const SGameInfo& info);
	void Update(SGameUpdateParam& update_param);
	void Draw();

private:
	void InitPSO();
	void TermPSO();
	void InitFloor(const SGameInfo& info);
	void TermFloor(const SGameInfo& info);

private:
	SRootSignature	m_root_signature;
	SPipeline				m_pso;

	// 床.
	struct SFloor {
		ghl::HVertexBuffer	hVB;
		ghl::HVertexView		hVV;
		ghl::HDescHeap			hDescHeap;
		ghl::HConstBuffer		hCB;
		ghl::HTextureBuffer	hTex;
		SConstantBufferForStageFloor	cbuffer{};
	};
	SFloor	m_floor;
};
