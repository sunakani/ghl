#pragma once

#include "GHL.h"
#include "Graphic.h"
#include "Game_defs.h"

/************************************************************/
/*	デバッグ用の３Ｄ図形表示クラス.
*************************************************************/
class CDebugFigure
{
public:
	CDebugFigure();
	void Init();
	void Term();

	// 古いリクエストを削除.
	// フレームの序盤で呼び出す.
	void Update1st(const SGameUpdateParam& update_param);

	// 今フレームのリクエストをもとに描画の準備を行わせる.
	void Update2nd(const SGameUpdateParam& update_param);

	// 描画.
	void Draw();

	// 表示リクエスト.
	void AddSphere(const DirectX::XMFLOAT3& pos, float radius, const AFloatColor& color);
	void AddLine(const DirectX::XMFLOAT3& start, const DirectX::XMFLOAT3& end, const AFloatColor& color, float thickness);

private:
	void InitPipeline();
	void TermPipeline();
	void InitSphere(uint8_t unSliceX, uint8_t unSliceY);
	void TermSphere();
	void InitLine();
	void TermLine();

private:
	SPipeline				m_pso_triangle;
	SPipeline				m_pso_line;
	SRootSignature	m_rs_triangle;
	SRootSignature	m_rs_line;

	// 球.
	struct SSphere {
		ghl::HVertexBuffer	hVertexBuffer;
		ghl::HVertexView		hVertexView;
		uint32_t						unVertxNum{0};
		ghl::HIndexBuffer		hIndexBuffer;
		ghl::HIndexView			hIndexView;
		uint32_t						unIndexNum{0};
		ghl::HDescHeap			hDescHeap;
		struct SUnit {
			bool								bValid{false};
			DirectX::XMFLOAT3		pos{};
			float								radius{};
			AFloatColor					color{};
		};
		std::vector<SUnit>							units;
		std::vector<ghl::HConstBuffer>	hConstantBuffer;
	};
	SSphere	m_spheres;

	// 線分.
	struct SLine {
		ghl::HDescHeap			hDescHeap;
		struct SVertex {
			ghl::HVertexBuffer	hVertexBuffer;
			ghl::HVertexView		hVertexView;
			uint32_t						unVertxNum{0};
		};
		std::vector<SVertex>						vertices;
		struct SUnit {
			bool								bValid{false};
			DirectX::XMFLOAT3		pos_start	{0,0,0};
			DirectX::XMFLOAT3		pos_end		{0,0,0};
			float								thickness	{0.01f};
			AFloatColor					color{};
		};
		std::vector<SUnit>							units;
		std::vector<ghl::HConstBuffer>	hConstantBuffer;
	};
	SLine m_lines;
};
