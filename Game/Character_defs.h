#pragma once

#include "LoadFile.h"
#include "Graphic.h"

// キャラクターID.
#define DEF_CHARACTER_START() enum class ECharacter { \
  NONE = 0,
#define DEF_CHARACTER(id, value, ...) id = value,
#define DEF_CHARACTER_END() };
#include "inc/db_def_character.inc"
#undef DEF_CHARACTER_START
#undef DEF_CHARACTER
#undef DEF_CHARACTER_END

// キャラデータベースのデータ.
// リソースの情報などが含まれる.
struct SCharacterData {
	ECharacter	eChara				{ECharacter::NONE};
	ELoadFile   eFileModel    {ELoadFile::None};
  ELoadFile   eFileMotionSet{ELoadFile::None};
  ELoadFile   eFileAction   {ELoadFile::None};
	SCharacterData(
      ECharacter chara
    , ELoadFile model
    , ELoadFile motion_set
    , ELoadFile action
  )
		: eChara(chara)
    , eFileModel(model)
    , eFileMotionSet(motion_set)
    , eFileAction(action)
	{}
};

// 生成に必要な情報.
// これの準備が整っていればキャラクターを生成できる.
// ロードしたリソースのハンドルなどが含まれる.
struct SCharacterArchetype
{
  ghl::HVertexBuffer  vertices;
  ghl::HIndexBuffer   indices;
  uint32_t            unVerticesNum{0};
  uint32_t            unIndicesNum{0};
  uint16_t            unOneVertexSize{0};
  uint16_t            unOneIndexSize{0};
  struct SPart {
    SConstantBufferForCharaParts  material{};
    ghl::HTextureBuffer texColor;
    ghl::HTextureBuffer texSphirical;
    ghl::HTextureBuffer texAdditionalSpherical;
    ghl::HTextureBuffer texToon;
    size_t              key2TexColor{0};
    size_t              key2TexSphirical{0};
    size_t              key2TexAdditionalSpherical{0};
    size_t              key2TexToon{0};
    uint32_t            unIndicesNum{0};
    bool                bEdgeFlag{false};
  };
  std::vector<SPart>        parts;
  std::weak_ptr<SSkeleton>  skeleton;
  std::weak_ptr<std::vector<SMotion>> motion_set;
  std::weak_ptr<SActionData>  action;
};

// キャラクターを生成するときの引数.
struct SCharacterCreateParam {
  ECharacter        eId           {ECharacter::NONE};
  DirectX::XMFLOAT3 initial_pos   {0.f, 0.f, 0.f};
  float             initial_rot_y {0.f};
};
