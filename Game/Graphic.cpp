#include "Graphic.h"

ghl::IGraphic& gi() { return ghl::GraphicInterface(); }

void SPipeline::Destroy()
{
  if (hVS) {
    gi().Shader_Destroy(hVS);
  }
  if (hPS) {
    gi().Shader_Destroy(hPS);
  }
  if (hGS) {
    gi().Shader_Destroy(hGS);
  }
  if (hState) {
    gi().PipeState_Destroy(hState);
  }
}

void SRootSignature::Destroy()
{
  if (hRootSignature) {
    gi().RootSig_Destroy(hRootSignature);
  }
}
