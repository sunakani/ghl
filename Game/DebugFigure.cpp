#include "DebugFigure.h"

CDebugFigure::CDebugFigure()
{}

void CDebugFigure::Init()
{
	InitPipeline();
	InitSphere(12, 12);
	InitLine();
}

void CDebugFigure::Term()
{
	TermSphere();
	TermLine();
	TermPipeline();
}

void CDebugFigure::InitPipeline()
{
	// 三角形用.
	{
		auto& pso = m_pso_triangle;
		// ルートシグネチャ.
		ghl::SCreateRootSignatureParam root_sig_param;
		{
			auto& rp = root_sig_param.root_parameters;
			rp.resize(1);
			rp[0].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 0, 1}); // b0.
			auto& smp = root_sig_param.sampler_parameters;
		}
		m_rs_triangle.hRootSignature = gi().RootSig_Create(root_sig_param);

		// シェーダ.
		LPCWSTR shader_dir = L"Shader\\";
		WCHAR path[256];
		swprintf_s(path, L"%s\\%s", shader_dir, L"DebugFigure.hlsl");
		pso.hVS = gi().Shader_CreateByCompileFile(path, "VS", "vs_5_0");
		swprintf_s(path, L"%s\\%s", shader_dir, L"DebugFigure.hlsl");
		pso.hPS = gi().Shader_CreateByCompileFile(path, "PS", "ps_5_0");

		// パイプラインステート.
		ghl::SCreatePiplelineStateParam pipeline_state_param;
		{
			auto& vi = pipeline_state_param.vertex_input_rayout_parameters;
			vi.push_back({"POSITION", 0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0});
			pipeline_state_param.hVS = pso.hVS;
			pipeline_state_param.hPS = pso.hPS;
			pipeline_state_param.hRootSignature = m_rs_triangle.hRootSignature;
			pipeline_state_param.eFillMode = ghl::EFillMode::eWireFrame;
		}
		pso.hState = gi().PipeState_Create(pipeline_state_param);
	}
	// ライン用.
	{
		auto& pso = m_pso_line;
		// ルートシグネチャ.
		ghl::SCreateRootSignatureParam root_sig_param;
		{
			auto& rp = root_sig_param.root_parameters;
			rp.resize(1);
			rp[0].table.push_back({ghl::EDescriptorType::eConstantBufferView, 0, 0, 1}); // b0.
			auto& smp = root_sig_param.sampler_parameters;
		}
		m_rs_line.hRootSignature = gi().RootSig_Create(root_sig_param);

		// シェーダ.
		LPCWSTR shader_dir = L"Shader\\";
		WCHAR path[256];
		swprintf_s(path, L"%s\\%s", shader_dir, L"DebugFigure.hlsl");
		pso.hVS = gi().Shader_CreateByCompileFile(path, "VS_Line", "vs_5_0");
		swprintf_s(path, L"%s\\%s", shader_dir, L"DebugFigure.hlsl");
		pso.hPS = gi().Shader_CreateByCompileFile(path, "PS", "ps_5_0");
		swprintf_s(path, L"%s\\%s", shader_dir, L"DebugFigure.hlsl");
		pso.hGS = gi().Shader_CreateByCompileFile(path, "GS_Line", "gs_5_0");

		// パイプラインステート.
		ghl::SCreatePiplelineStateParam pipeline_state_param;
		{
			auto& vi = pipeline_state_param.vertex_input_rayout_parameters;
			vi.push_back({"POSITION", 0, ghl::EGraphInfraFormat::eR32G32B32_Float, 0});
			pipeline_state_param.hVS = pso.hVS;
			pipeline_state_param.hPS = pso.hPS;
			pipeline_state_param.hGS = pso.hGS;
			pipeline_state_param.hRootSignature = m_rs_line.hRootSignature;
			pipeline_state_param.eCullMode = ghl::ECullMode::eNone;
			pipeline_state_param.ePrimitiveTopologyType = ghl::EPrimitiveTopologyType::eLine;
		}
		pso.hState = gi().PipeState_Create(pipeline_state_param);
	}
}

void CDebugFigure::TermPipeline()
{
	m_pso_triangle.Destroy();
	m_pso_line.Destroy();
	m_rs_triangle.Destroy();
	m_rs_line.Destroy();
}

void CDebugFigure::InitSphere(uint8_t unSliceX, uint8_t unSliceY)
{
	std::vector<SDebugFigureVertex> vertices((unSliceX + 1) * (unSliceY + 1));
	for (uint8_t x=0; x<=unSliceX; ++x) {
		for (uint8_t y=0; y<=unSliceY; ++y) {
			auto index = x * (unSliceY + 1) + y;
			auto& dst = vertices.at(index);
			float theta	= DirectX::XM_2PI * x / unSliceX;
			float fai		= DirectX::XM_2PI * y / unSliceY;
			dst.pos = {
				std::cosf(theta) * std::sinf(fai),
				std::cosf(fai),
				std::sinf(theta) * std::sinf(fai)
			};
		}
	}
	std::vector<unsigned short> indices((unSliceX * unSliceY) * 6);
	for (uint8_t x=0; x<unSliceX; ++x) {
		for (uint8_t y=0; y<unSliceY; ++y) {
			size_t i = (static_cast<size_t>(x) * unSliceX + y) * 6;
			indices.at(i + 0) = x * (unSliceY + 1) + y;
			indices.at(i + 1) = x * (unSliceY + 1) + y + 1;
			indices.at(i + 2) = (x + 1) * (unSliceY + 1) + y;
			indices.at(i + 3) = indices.at(i + 2);
			indices.at(i + 4) = indices.at(i + 1);
			indices.at(i + 5) = (x + 1) * (unSliceY + 1) + y + 1;
		}
	}
	m_spheres.hVertexBuffer	= gi().Vertex_CreateBuffer(sizeof(SDebugFigureVertex), vertices.size());
	m_spheres.hVertexView		= gi().Vertex_CreateView(m_spheres.hVertexBuffer, sizeof(SDebugFigureVertex), vertices.size());
	gi().Vertex_UpdateBuffer(m_spheres.hVertexBuffer, vertices.data(), sizeof(SDebugFigureVertex), vertices.size());
	m_spheres.unVertxNum	= static_cast<uint32_t>(vertices.size());

	m_spheres.hIndexBuffer	= gi().Index_CreateBuffer(sizeof(unsigned short), indices.size());
	m_spheres.hIndexView		= gi().Index_CreateView(m_spheres.hIndexBuffer, sizeof(unsigned short), indices.size());
	gi().Index_UpdateBuffer(m_spheres.hIndexBuffer, indices.data(), sizeof(unsigned short), indices.size());
	m_spheres.unIndexNum	= static_cast<uint32_t>(indices.size());

	m_spheres.hDescHeap	= gi().DescHeap_Create(ghl::EDescriptorHeapType::eCBV_SRV_UAV, 256, true);
	m_spheres.hConstantBuffer.resize(256);
	for (int i=0; i<256; ++i) {
		m_spheres.hConstantBuffer[i] = gi().Const_CreateBuffer(sizeof(SConstBufDbgFigure));
		gi().DescHeap_PlaceConstView(m_spheres.hDescHeap, m_spheres.hConstantBuffer[i], i);
	}
	m_spheres.units.reserve(256);
}

void CDebugFigure::TermSphere()
{
	if (m_spheres.hVertexView) {
		gi().Vertex_DestroyView(m_spheres.hVertexBuffer, m_spheres.hVertexView);
	}
	if (m_spheres.hVertexBuffer) {
		gi().Vertex_DestroyBuffer(m_spheres.hVertexBuffer);
	}
	m_spheres.unVertxNum = 0;
	if (m_spheres.hIndexView) {
		gi().Index_DestroyView(m_spheres.hIndexBuffer, m_spheres.hIndexView);
	}
	if (m_spheres.hIndexBuffer) {
		gi().Index_DestroyBuffer(m_spheres.hIndexBuffer);
	}
	m_spheres.unIndexNum = 0;
	if (m_spheres.hDescHeap) {
		gi().DescHeap_Destroy(m_spheres.hDescHeap);
	}
	for (auto& elem : m_spheres.hConstantBuffer) {
		gi().Const_DestroyBuffer(elem);
	}
	m_spheres.hConstantBuffer.clear();
	m_spheres.hConstantBuffer.shrink_to_fit();
	m_spheres.units.clear();
	m_spheres.units.shrink_to_fit();
}

void CDebugFigure::InitLine()
{
	m_lines.vertices.resize(256);
	for (int i=0; i<256; ++i) {
		auto& elem = m_lines.vertices.at(i);
		elem.hVertexBuffer	= gi().Vertex_CreateBuffer(sizeof(SDebugFigureVertex), 2);
		elem.hVertexView		= gi().Vertex_CreateView(elem.hVertexBuffer, sizeof(SDebugFigureVertex), 2);
		elem.unVertxNum			= 2;
	}
	m_lines.hDescHeap	= gi().DescHeap_Create(ghl::EDescriptorHeapType::eCBV_SRV_UAV, 256, true);
	m_lines.hConstantBuffer.resize(256);
	for (int i=0; i<256; ++i) {
		m_lines.hConstantBuffer[i] = gi().Const_CreateBuffer(sizeof(SConstBufDbgFigure));
		gi().DescHeap_PlaceConstView(m_lines.hDescHeap, m_lines.hConstantBuffer[i], i);
	}
	m_lines.units.reserve(256);
}

void CDebugFigure::TermLine()
{
	for (auto& elem : m_lines.vertices) {
		if (elem.hVertexView) {
			gi().Vertex_DestroyView(elem.hVertexBuffer, elem.hVertexView);
		}
		if (elem.hVertexBuffer) {
			gi().Vertex_DestroyBuffer(elem.hVertexBuffer);
		}
		elem.unVertxNum = 0;
	}
	m_lines.vertices.clear();
	m_lines.vertices.shrink_to_fit();
	if (m_lines.hDescHeap) {
		gi().DescHeap_Destroy(m_lines.hDescHeap);
	}
	for (auto& elem : m_lines.hConstantBuffer) {
		gi().Const_DestroyBuffer(elem);
	}
	m_lines.hConstantBuffer.clear();
	m_lines.hConstantBuffer.shrink_to_fit();
	m_lines.units.clear();
	m_lines.units.shrink_to_fit();
}

void CDebugFigure::Update1st(const SGameUpdateParam& update_param)
{
	m_spheres.units.clear();
	m_lines.units.clear();
}

void CDebugFigure::Update2nd(const SGameUpdateParam& update_param)
{
	for (int i=0; i<m_spheres.units.size(); ++i)
	{
		auto& unit = m_spheres.units[i];
		SConstBufDbgFigure buf;
		buf.matView = update_param.matView;
		buf.matPersProj = update_param.matPersProj;
		buf.matWorld=	DirectX::XMMatrixScaling(unit.radius, unit.radius, unit.radius) *	DirectX::XMMatrixTranslation(unit.pos.x, unit.pos.y, unit.pos.z);
		buf.diffuse	= {unit.color.at(0), unit.color.at(1), unit.color.at(2)};
		buf.alpha		= unit.color.at(3);
		gi().Const_UpdateBuffer(m_spheres.hConstantBuffer[i], &buf, sizeof(buf));
	}
	for (int i=0; i<m_lines.units.size(); ++i)
	{
		auto& unit = m_lines.units[i];
		SConstBufDbgFigure buf;
		buf.matView		= update_param.matView;
		buf.matPersProj		= update_param.matPersProj;
		buf.matWorld	=	DirectX::XMMatrixIdentity();
		buf.diffuse		= {unit.color.at(0), unit.color.at(1), unit.color.at(2)};
		buf.alpha			= unit.color.at(3);
		buf.thickness	= unit.thickness;
		gi().Const_UpdateBuffer(m_lines.hConstantBuffer[i], &buf, sizeof(buf));

		auto& vert = m_lines.vertices[i];
		SDebugFigureVertex vertices[] = {
			{{unit.pos_start}},
			{{unit.pos_end}},
		};
		gi().Vertex_UpdateBuffer(vert.hVertexBuffer, vertices, sizeof(SDebugFigureVertex), 2);
	};
}

void CDebugFigure::Draw()
{
	// 三角形.
  gi().Cmd_RootSig_Set(m_rs_triangle.hRootSignature);
  gi().Cmd_PipeState_Set(m_pso_triangle.hState);
	gi().Cmd_Set_PrimitiveTopology(ghl::EPrimitiveTopology::eTriangleList);
  gi().Cmd_Vertex_Set(m_spheres.hVertexBuffer, m_spheres.hVertexView);
  gi().Cmd_Index_Set(m_spheres.hIndexBuffer, m_spheres.hIndexView);
  gi().Cmd_DescHeap_Set(m_spheres.hDescHeap);
	for (int i=0; i<m_spheres.units.size(); ++i)
	{
	  gi().Cmd_DescHeap_SetRootParam(m_spheres.hDescHeap, 0, i);
    gi().Cmd_Draw_IndexedInstanced(m_spheres.unIndexNum, 1, 0, 0, 0);
  };

	// ライン.
	gi().Cmd_RootSig_Set(m_rs_line.hRootSignature);
  gi().Cmd_PipeState_Set(m_pso_line.hState);
	gi().Cmd_Set_PrimitiveTopology(ghl::EPrimitiveTopology::eLineList);
  gi().Cmd_DescHeap_Set(m_lines.hDescHeap);
	for (int i=0; i<m_lines.units.size(); ++i)
	{
		auto& elem = m_lines.vertices[i];
    gi().Cmd_Vertex_Set(elem.hVertexBuffer, elem.hVertexView);
    gi().Cmd_DescHeap_SetRootParam(m_lines.hDescHeap, 0, i);
    gi().Cmd_Draw_Instanced(2, 1, 0, 0);
  };
}

void CDebugFigure::AddSphere(const DirectX::XMFLOAT3& pos, float radius, const AFloatColor& color)
{
	if (m_spheres.units.size() >= m_spheres.units.capacity()) {
		return;
	}
	m_spheres.units.push_back(SSphere::SUnit());
	auto& dst = m_spheres.units.back();
	dst.bValid = true;
	dst.pos = pos;
	dst.radius = radius;
	dst.color = color;
}

void CDebugFigure::AddLine(const DirectX::XMFLOAT3& start, const DirectX::XMFLOAT3& end, const AFloatColor& color, float thickness)
{
	if (m_lines.units.size() >= m_lines.units.capacity()) {
		return;
	}
	m_lines.units.push_back(SLine::SUnit());
	auto& dst = m_lines.units.back();
	dst.bValid		= true;
	dst.pos_start = start;
	dst.pos_end		= end;
	dst.thickness	= thickness;
	dst.color			= color;
}
