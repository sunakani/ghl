#pragma once

#include "ghl.h"
#include "Game_defs.h"
#include "Character_defs.h"
#include "Graphic.h"
#include "LoadFile.h"
#include "CharacterAnimator.h"
#include "CharacterAction.h"

/************************************************/
/*  キャラクタークラス.
*   モーション→CCharacterAnimator.
*   アクション→CCharacterActionController.
*   にそれぞれ委譲.
*************************************************/
class CCharacter
{
public:
  CCharacter();
  ~CCharacter();

  // キャラ生成.
  // 配置情報とリソース情報を引数に初期化される.
  void Create(const SCharacterCreateParam& create_param, const SCharacterArchetype& archetype, const SGameInfo& info);

  // キャラ破棄.
  // 参照を忘れ、生成したものは破棄する.
  void Destroy();

  // 更新.
  void Update(const SGameUpdateParam& update_param);

  // 描画.
  void PreDraw(SGameUpdateParam& update_param);
  void DrawShadow();
  void Draw();
  void DrawOutline();

  // コンポネント.
  const CCharacterAnimator& GetAnimator() const { return m_animator; }
  const CCharacterActionController& GetActionController() const { return m_actionController; }

  // プレイヤー関連.
  void SetPlayer(EPlayerIndex ePlayer) { m_ePlayer = ePlayer; }
  EPlayerIndex GetPlayer() const { return m_ePlayer; }
  bool IsPlayer() const { return GetPlayer() != EPlayerIndex::None; }

  // 位置、姿勢.
  const DirectX::XMFLOAT3& GetPosition() const { return m_position; }
  const DirectX::XMFLOAT3& GetUpperPosition() const { return m_position_upper; }
  float GetAngleY() const { return m_fAngleY; }
  DirectX::XMFLOAT3 GetFrontDir() const;
  void SetDesiredAngleY(float angleY, float duration);
  void MoveFront(float duration);

private:
  // デバッグ.
#if defined(_DEBUG)
  void DebugDrawBone(const SGameUpdateParam& update_param);
#endif

  void UpdateUpperPosition();

private:
  DirectX::XMFLOAT3 m_position;
  DirectX::XMFLOAT3 m_position_upper;
  float             m_fAngleY;
  float             m_fDesiredAngleY;
  float             m_fValidFrameToRotateDesired;
  float             m_fValidFrameToMoveFront;
  EPlayerIndex      m_ePlayer;

  // 頂点.
  ghl::HVertexBuffer    m_hVertexBufferRef;
  ghl::HVertexView      m_hVertexView;
  uint32_t              m_unIndexNum;

  // インデックス.
  ghl::HIndexBuffer     m_hIndexBufferRef;
  ghl::HIndexView       m_hIndexView;

  // ディスクリプタヒープ.
  ghl::HDescHeap        m_hDescHeapForView;

  // ディスクリプタの配置順.
  enum EDescUsage {
    eDU_Material,
    eDU_TexColor,
    eDU_TexSpherical,
    eDU_TexAdditionalSpherical,
    eDU_Toon,
    eDU_TexShadow,
    NumOfDescUsage
  };

  // 部位.
  struct SPart {
    ghl::HConstBuffer hConstBufferForMaterial;
    UINT              uiIndexNum{0};
    bool              bEdgeFlag{1};
  };
  std::vector<SPart>        m_parts;

  // 骨.
  std::weak_ptr<SSkeleton>  m_skeleton;
  int32_t                   m_boneIdxUpper{-1};

  // 定数バッファ.
  ghl::HConstBuffer         m_hConstView;
  ghl::HConstBuffer         m_hConstChara;

  // 定数バッファ転送元.
  SConstantBufferForView    m_cbView;
  SConstantBufferForChara   m_cbChara;

  // アニメーター.
  CCharacterAnimator        m_animator;

  // アクションコントロール.
  CCharacterActionController  m_actionController;
};
