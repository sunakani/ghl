#include "Character.h"
#include "pmd/pmd.h"
#include "Graphic.h"
#include "Camera.h"
#include "Light.h"
#include "DebugFigure.h"
#include "DebugMenu.h"

CCharacter::CCharacter()
  : m_position{0.f, 0.f, 0.f}
  , m_position_upper{m_position}
  , m_fAngleY()
  , m_fDesiredAngleY()
  , m_fValidFrameToRotateDesired()
  , m_fValidFrameToMoveFront()
  , m_ePlayer(EPlayerIndex::None)
  , m_hVertexBufferRef()
  , m_hVertexView()
  , m_unIndexNum(0)
  , m_hIndexBufferRef()
  , m_hIndexView()
  , m_parts()
  , m_boneIdxUpper(-1)
  , m_cbView{}
  , m_cbChara{}
  , m_animator()
  , m_actionController(this)
{}
CCharacter::~CCharacter()
{}
void CCharacter::Create(const SCharacterCreateParam& create_param, const SCharacterArchetype& archetype, const SGameInfo& info)
{
  // アーキタイプからバッファのハンドルをコピーし、
  // それを配置するためのヒープとビューを作成する.
  // 一部バッファも作成する.
  auto& a = archetype;
  
  // 部位数をコピー.
  m_parts.resize(a.parts.size());

  // 頂点ビュー.
  m_hVertexBufferRef = a.vertices;
  m_hVertexView = gi().Vertex_CreateView(a.vertices, a.unOneVertexSize, a.unVerticesNum);

  // インデックスビュー.
  m_hIndexBufferRef = a.indices;
  m_hIndexView  = gi().Index_CreateView(a.indices, a.unOneIndexSize, a.unIndicesNum);

  // ヒープ.
  m_hDescHeapForView = gi().DescHeap_Create(ghl::EDescriptorHeapType::eCBV_SRV_UAV, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage + 2), true);

  // 各部位の定数バッファビューとテクスチャビュー.
  for (size_t i=0; i<m_parts.size(); ++i)
  {
    auto& param = a.parts.at(i);
    auto& dst = m_parts.at(i);

    // ディスクリプタ配置位置.
    uint16_t pos = static_cast<uint16_t>(i * NumOfDescUsage);

    // 定数バッファを確保し配置.
    dst.hConstBufferForMaterial = gi().Const_CreateBuffer(sizeof(SConstantBufferForCharaParts));
    gi().Const_UpdateBuffer(dst.hConstBufferForMaterial, &param.material, sizeof(param.material));
    gi().DescHeap_PlaceConstView(m_hDescHeapForView, dst.hConstBufferForMaterial, pos + eDU_Material);

    // テクスチャ.
    {
      auto handle = param.texColor;
      if (!handle) {
        handle = gi().Texture_GetWhite();
      }
      gi().DescHeap_PlaceTextureView(m_hDescHeapForView, handle, pos + eDU_TexColor);
    }
    {
      auto handle = param.texSphirical;
      if (!handle) {
        handle = gi().Texture_GetWhite();
      }
      gi().DescHeap_PlaceTextureView(m_hDescHeapForView, handle, pos + eDU_TexSpherical);
    }
    {
      auto handle = param.texAdditionalSpherical;
      if (!handle) {
        handle = gi().Texture_GetBlack();
      }
      gi().DescHeap_PlaceTextureView(m_hDescHeapForView, handle, pos + eDU_TexAdditionalSpherical);
    }
    {
      auto handle = param.texToon;
      if (!handle) {
        handle = gi().Texture_GetGrayGradation();
      }
      gi().DescHeap_PlaceTextureView(m_hDescHeapForView, handle, pos + eDU_Toon);
    }
    {
      auto handle = info.hShadowDepthBuffer;
      if (handle) {
        gi().DescHeap_PlaceTextureView(m_hDescHeapForView, handle, pos + eDU_TexShadow, ghl::EGraphInfraFormat::eR32_Float);
      }
    }

    // インデックス数.
    dst.uiIndexNum = param.unIndicesNum;
    dst.bEdgeFlag = param.bEdgeFlag;
    m_unIndexNum += dst.uiIndexNum;
  }

  // スケルトン.
  m_skeleton = archetype.skeleton;
  auto shared_skeleton = m_skeleton.lock();
  if (auto* pSkeleton = shared_skeleton.get()) {
    m_boneIdxUpper = pSkeleton->boneIdxUpper;
  }
  std::fill(m_cbChara.localBoneMatrices.begin(), m_cbChara.localBoneMatrices.end(), DirectX::XMMatrixIdentity());

  // アニメーター.
  m_animator.Init(archetype.motion_set, archetype.skeleton);
  m_animator.UpdateBone(m_cbChara.localBoneMatrices);

  // アクション.
  m_actionController.Init(archetype.action);

  // 定数バッファ.
  m_hConstView = gi().Const_CreateBuffer(sizeof(SConstantBufferForView));
  gi().DescHeap_PlaceConstView(m_hDescHeapForView, m_hConstView, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage + 0));
  m_hConstChara = gi().Const_CreateBuffer(sizeof(SConstantBufferForChara));
  gi().DescHeap_PlaceConstView(m_hDescHeapForView, m_hConstChara, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage + 1));

  // 座標、向き.
  m_position  = create_param.initial_pos;
  m_fAngleY   = create_param.initial_rot_y;
  m_cbChara.matWorld  = DirectX::XMMatrixIdentity();
  m_cbChara.matWorld *= DirectX::XMMatrixRotationY(m_fAngleY + DirectX::XM_PI);
  m_cbChara.matWorld *= DirectX::XMMatrixTranslation(m_position.x, m_position.y, m_position.z);
  UpdateUpperPosition();
}
void CCharacter::Destroy()
{
  m_animator.Term();
  m_actionController.Term();

  // 頂点.
  if (m_hVertexBufferRef && m_hVertexView) {
    gi().Vertex_DestroyView(m_hVertexBufferRef, m_hVertexView);
  }
  m_hVertexBufferRef.Invalidate();
  // インデックス.
  if (m_hIndexBufferRef && m_hIndexView) {
    gi().Index_DestroyView(m_hIndexBufferRef, m_hIndexView);
  }
  m_hIndexBufferRef.Invalidate();
  // 部位.
  for (auto& p : m_parts) {
    gi().Const_DestroyBuffer(p.hConstBufferForMaterial);
  }
  // 定数バッファ.
  gi().Const_DestroyBuffer(m_hConstView);
  gi().Const_DestroyBuffer(m_hConstChara);
  // ヒープ.
  gi().DescHeap_Destroy(m_hDescHeapForView);
}

void CCharacter::Update(const SGameUpdateParam& update_param)
{
  // アクション.
  m_actionController.UpdateTransit(update_param);
  m_actionController.UpdateEvent(update_param);

#if defined(_DEBUG)
  if (IsPlayer()) {
    CDebugDisp::Inst().SetPosition(1200, 0);
    CDebugDisp::Inst().SetColor({0.f, 0.f, 0.f, 1.f});
    CDebugDisp::Inst().Printf("[Action ] Id=%d\n", m_actionController.GetCurrentActionId());
  }
#endif

  // 移動など.
  m_fAngleY = adjust_angle_y(m_fAngleY);
  if (m_fValidFrameToRotateDesired > 0.f) {
    m_fValidFrameToRotateDesired -= update_param.fDeltaTime;
    auto diff_to_close = calc_diff_to_close_target(m_fDesiredAngleY, m_fAngleY);
    m_fAngleY += diff_to_close * 0.1f;
    if (m_fValidFrameToRotateDesired < 0.f) {
      m_fValidFrameToRotateDesired = 0.f;
      m_fDesiredAngleY = 0.f;
    }
  }
  if (m_fValidFrameToMoveFront > 0.f) {
    m_fValidFrameToMoveFront -= update_param.fDeltaTime;

    DirectX::XMFLOAT4 front = {0.f, 0.f, 1.f, 0.f};
    auto move_dir = DirectX::XMVector3Transform(DirectX::XMLoadFloat4(&front), DirectX::XMMatrixRotationY(m_fAngleY));
    float base_speed = 50.f;
    float speed_rate = 1.f;
    auto& debug = update_param.pGameInfo->pDebugMenu->GetPlayerPhysical();
    if (debug.bEnabled) {
      speed_rate = debug.contents.move_speed_rate;
    }
    float speed = base_speed * speed_rate;

    m_position.x += DirectX::XMVectorGetX(move_dir) * speed * update_param.fDeltaTime;
    m_position.z += DirectX::XMVectorGetZ(move_dir) * speed * update_param.fDeltaTime;

    if (m_fValidFrameToMoveFront < 0.f) {
      m_fValidFrameToMoveFront = 0.f;
    }
  }


  // アニメーター.
  m_animator.Update(update_param, *this);
}

void CCharacter::PreDraw(SGameUpdateParam& update_param)
{
  m_cbView.matView = update_param.matView;
  m_cbView.matPersProj = update_param.matPersProj;

  // カメラ.
  if (auto* pCamera = update_param.pCamera) {
    auto& cs = pCamera->GetStatus();
    m_cbView.eye = cs.eye;
  }

  // ライト.
  if (auto* pLight = update_param.pLight) {
    using namespace DirectX;
    XMFLOAT4 planeVec(0.f, 1.f, 0.f, 0.f);
    XMFLOAT3 paraLightInverse = {-pLight->GetParallelLightDirection().x, -pLight->GetParallelLightDirection().y, -pLight->GetParallelLightDirection().z};
    m_cbView.matShadow = XMMatrixShadow(XMLoadFloat4(&planeVec), XMLoadFloat3(&paraLightInverse));
    m_cbView.matLightCamera = update_param.matLightCamera;
  }

  // キャラ.
  m_cbChara.matWorld  = DirectX::XMMatrixIdentity();
  m_cbChara.matWorld *= DirectX::XMMatrixRotationY(m_fAngleY + DirectX::XM_PI);
  m_cbChara.matWorld *= DirectX::XMMatrixTranslation(m_position.x, m_position.y, m_position.z);

  auto draw_pos = m_animator.GetRootTranslate();
  draw_pos = DirectX::XMVector3Transform(draw_pos, m_cbChara.matWorld);
  draw_pos.m128_f32[1] = 0.f;
  draw_pos = DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&m_position), draw_pos);
  m_cbChara.matWorld *= DirectX::XMMatrixTranslation(draw_pos.m128_f32[0], draw_pos.m128_f32[1], draw_pos.m128_f32[2]);
#if defined(_DEBUG) && 0
  if (IsPlayer()) {
    DebugPrintf("pos: %.2f, %.2f, %.2f\n", m_cbChara.matWorld.r[3].m128_f32[0], m_cbChara.matWorld.r[3].m128_f32[1], m_cbChara.matWorld.r[3].m128_f32[2]);
  }
#endif

  // アニメーター.
  std::fill(m_cbChara.localBoneMatrices.begin(), m_cbChara.localBoneMatrices.end(), DirectX::XMMatrixIdentity());
  m_animator.UpdateBone(m_cbChara.localBoneMatrices);
#if defined(_DEBUG) && 0
  DebugDrawBone(update_param);
#endif

  // 骨座標取得.
  UpdateUpperPosition();

  // バッファにコピー.
  gi().Const_UpdateBuffer(m_hConstView, &m_cbView, sizeof(m_cbView));
  gi().Const_UpdateBuffer(m_hConstChara, &m_cbChara, sizeof(m_cbChara));

  // デバッグ.
#if defined(_DEBUG)
  if (GetPlayer() != EPlayerIndex::None)
  {
    auto& debug_figure = *update_param.pGameInfo->pDebugFigure;
    DirectX::XMFLOAT3 axis_z = {0.f,0.f,1.f};
    auto front = DirectX::XMVector3Transform(DirectX::XMLoadFloat3(&axis_z), DirectX::XMMatrixRotationY(m_fAngleY));
    DirectX::XMFLOAT3 l_begin = {m_position.x, m_position.y + 12.f, m_position.z};
    DirectX::XMFLOAT3 l_end   = {l_begin.x + front.m128_f32[0] * 20.f, l_begin.y + front.m128_f32[1] * 20.f, l_begin.z + front.m128_f32[2] * 20.f};
    debug_figure.AddLine(l_begin, l_end, {1.f, 1.f, 0.f, 1.f}, 2.f);
    debug_figure.AddSphere(l_end,1.f,{1.f,1.f,0.f,1.f});
  }
#endif
}

void CCharacter::DrawShadow()
{
  if (m_hVertexBufferRef && m_hVertexView && m_hIndexBufferRef && m_hIndexView) {
    // 頂点.
    gi().Cmd_Vertex_Set(m_hVertexBufferRef, m_hVertexView);
    // インデックス.
    gi().Cmd_Index_Set(m_hIndexBufferRef, m_hIndexView);
    // ヒープ.
    gi().Cmd_DescHeap_Set(m_hDescHeapForView);
    // ルートパラム0.
    gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 0, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage) + 0);
    gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 1, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage) + 1);
    // まとめて描画.
    gi().Cmd_Draw_IndexedInstanced(m_unIndexNum, 1, 0, 0, 0);
  }
}

void CCharacter::Draw()
{
  if (m_hVertexBufferRef && m_hVertexView && m_hIndexBufferRef && m_hIndexView) {
    // 頂点.
    gi().Cmd_Vertex_Set(m_hVertexBufferRef, m_hVertexView);
    // インデックス.
    gi().Cmd_Index_Set(m_hIndexBufferRef, m_hIndexView);
    // ヒープ.
    gi().Cmd_DescHeap_Set(m_hDescHeapForView);
    // ルートパラム0.
    gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 0, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage) + 0);
    gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 1, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage) + 1);
    // マテリアル単位で描画.
    UINT index_offset = 0;
    for (size_t s=0; s<m_parts.size(); ++s) {
      auto count = m_parts[s].uiIndexNum;
    // ルートパラム1.
      gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 2, static_cast<uint16_t>(s * NumOfDescUsage));
      UINT instance_num = 1;
      gi().Cmd_Draw_IndexedInstanced(count, instance_num, index_offset, 0, 0);
      index_offset += count;
    }
  }
}

void CCharacter::DrawOutline()
{
  if (m_hVertexBufferRef && m_hVertexView && m_hIndexBufferRef && m_hIndexView) {
    // 頂点.
    gi().Cmd_Vertex_Set(m_hVertexBufferRef, m_hVertexView);
    // インデックス.
    gi().Cmd_Index_Set(m_hIndexBufferRef, m_hIndexView);
    // ヒープ.
    gi().Cmd_DescHeap_Set(m_hDescHeapForView);
    // ルートパラム0.
    gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 0, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage) + 0);
    gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 1, static_cast<uint16_t>(m_parts.size() * NumOfDescUsage) + 1);
    // マテリアル単位で描画.
    UINT index_offset = 0;
    for (size_t s=0; s<m_parts.size(); ++s) {
      if (m_parts[s].bEdgeFlag != 0) {
        auto count = m_parts[s].uiIndexNum;
        // ルートパラム1.
        gi().Cmd_DescHeap_SetRootParam(m_hDescHeapForView, 2, static_cast<uint16_t>(s * NumOfDescUsage));
        UINT instance_num = 1;
        gi().Cmd_Draw_IndexedInstanced(count, instance_num, index_offset, 0, 0);
        index_offset += count;
      }
    }
  }
}

#if defined(_DEBUG)
void CCharacter::DebugDrawBone(const SGameUpdateParam& update_param)
{
  if (update_param.pGameInfo == nullptr) {
    return;
  }
  if (auto* pDebugFigure = update_param.pGameInfo->pDebugFigure)
  {
    auto shared_skeleton = m_skeleton.lock();
    if (auto* pSkeleton = shared_skeleton.get())
    {
      // IKエフェクタとターゲット.
      for (auto& elem : pSkeleton->ikDatum) {
        pSkeleton->DebugDrawSphere(elem.targetIdx,  m_cbChara.localBoneMatrices[elem.targetIdx],  m_cbChara.matWorld, *pDebugFigure, 0.3f, {1.f, 0.f, 1.f, 1.f});
        pSkeleton->DebugDrawSphere(elem.boneIdx,    m_cbChara.localBoneMatrices[elem.boneIdx],    m_cbChara.matWorld, *pDebugFigure, 0.3f, {0.4f, 1.f, 0.4f, 1.f});

        auto prev = elem.boneIdx;
        for (auto& chain : elem.nodeIdxes) {
          pSkeleton->DebugDrawSphere(chain, m_cbChara.localBoneMatrices[chain],  m_cbChara.matWorld, *pDebugFigure, 0.4f, {1.f, 1.f, 0.f, 1.f});
          pSkeleton->DebugDrawLine(prev, chain, m_cbChara.localBoneMatrices[prev], m_cbChara.localBoneMatrices[chain], m_cbChara.matWorld, *pDebugFigure, 0.1f, {1.f, 1.f, 0.f, 1.f});
          prev = chain;
        }
      }
    }
  }
}
#endif

void CCharacter::UpdateUpperPosition()
{
  m_position_upper = m_position;
  if (m_boneIdxUpper >= 0 && m_boneIdxUpper < m_cbChara.localBoneMatrices.size()) {
    auto shared_skeleton = m_skeleton.lock();
  if (auto* pSkeleton = shared_skeleton.get()) {
      if (auto* pNode = pSkeleton->boneIdx2Node[m_boneIdxUpper]) {
        auto vec = XMLoadFloat3(&pNode->startPos);
        vec = DirectX::XMVector3Transform(vec, m_cbChara.localBoneMatrices[m_boneIdxUpper]);
        vec = DirectX::XMVector3Transform(vec, m_cbChara.matWorld);
        DirectX::XMStoreFloat3(&m_position_upper, vec);
      }
    }
  }
}

void CCharacter::SetDesiredAngleY(float angleY, float duration)
{
  m_fDesiredAngleY = adjust_angle_y(angleY);
  m_fValidFrameToRotateDesired = duration;
}

void CCharacter::MoveFront(float duration)
{
  m_fValidFrameToMoveFront = duration;
}

DirectX::XMFLOAT3 CCharacter::GetFrontDir() const
{
  DirectX::XMFLOAT3 dir = {0.f,0.f,1.f};
  auto front = DirectX::XMVector3Transform(DirectX::XMLoadFloat3(&dir), DirectX::XMMatrixRotationY(m_fAngleY));
  DirectX::XMStoreFloat3(&dir, front);
  return dir;
}
