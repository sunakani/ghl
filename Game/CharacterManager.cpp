#include "CharacterManager.h"
#include "Character.h"
#include "pmd/pmd.h"

CCharacterManager::CCharacterManager()
  : m_datum()
  , m_characters()
  , m_player_characters{}
  , m_update_threads()
{}
CCharacterManager::~CCharacterManager()
{}
void CCharacterManager::Init()
{
#define DEF_CHARACTER_START()
#define DEF_CHARACTER(id, value, model, motion_set, action) m_datum.insert(std::make_pair(ECharacter::id,SUnit(ECharacter::id,ELoadFile::model,ELoadFile::motion_set,ELoadFile::action)));
#define DEF_CHARACTER_END()
#include "inc/db_def_character.inc"
#undef DEF_CHARACTER_START
#undef DEF_CHARACTER
#undef DEF_CHARACTER_END
  m_player_characters.fill(nullptr);
  // 更新スレッド.
  for (auto& elem : m_update_threads) {
    elem.Init();
  }
}
void CCharacterManager::Term(SGameInfo& info)
{
  for (auto& elem : m_update_threads) {
    elem.Term();
  }
  for (auto& elem : m_datum) {
    auto& unit = elem.second;
    _DEBUG_BREAK(unit.count == 0);  // 解放漏れ.
    if (unit.count != 0) {
      _UnloadCharacter(unit, info);
      unit.count = 0;
    }
  }
	m_datum.clear();
  for (auto*& pChara : m_characters) {
    if (pChara != nullptr) {
      pChara->Destroy();
      delete pChara;
      pChara = nullptr;
    }
  }
  m_characters.clear();
}
void CCharacterManager::Update(SGameUpdateParam& update_param)
{
#if 1
  for (auto* pChara : m_characters) {
    if (pChara != nullptr) {
      pChara->Update(update_param);
      pChara->PreDraw(update_param);
    }
  }
#else
  for (auto& elem : m_update_threads) {
    if (elem.GetTargetNum() > 0) {
      elem.SetUpdateParam(update_param);
      elem.SetBegin();
    }
  }
  for (auto& elem : m_update_threads) {
    if (elem.GetTargetNum() > 0) {
      elem.WaitEnd();
    }
  }
#endif
}
void CCharacterManager::DrawShadow()
{
  for (auto* pChara : m_characters) {
    if (pChara != nullptr) {
      pChara->DrawShadow();
    }
  }
}
void CCharacterManager::Draw()
{
  for (auto* pChara : m_characters) {
    if (pChara != nullptr) {
      pChara->Draw();
    }
  }
}
void CCharacterManager::DrawOutline()
{
  for (auto* pChara : m_characters) {
    if (pChara != nullptr) {
      pChara->DrawOutline();
    }
  }
}
void CCharacterManager::CreateCharacter(const SCharacterCreateParam& param, const SGameInfo& info)
{
  auto found = m_datum.find(param.eId);
  if (found != m_datum.end()) {
	  if (auto* pNewChara = new CCharacter()) {
      m_characters.push_back(pNewChara);
#if defined(_DEBUG)
      DebugPrintf(u8"◆CreateCharacter{type=%d pos={%.1f,%.1f,%.1f}}\n", e2ut(param.eId), param.initial_pos.x, param.initial_pos.y, param.initial_pos.z);
#endif
      pNewChara->Create(param, (*found).second.archetype, info);

      size_t smallest = std::numeric_limits<size_t>::max();
      CCharacterUpdateThread* pTarget = nullptr;
      for (auto& elem : m_update_threads) {
        if (elem.GetTargetNum() < smallest) {
          smallest = elem.GetTargetNum();
          pTarget = &elem;
        }
      }
      _DEBUG_BREAK(pTarget != nullptr);
      if (pTarget != nullptr) {
        pTarget->AddTarget(pNewChara);
      }
    }
  }
  else {
    _DEBUG_BREAK(0);
  }
}

void CCharacterManager::LoadCharacter(ECharacter eChara, SGameInfo& info)
{
  auto found = m_datum.find(eChara);
  if (found != m_datum.end()) {
    auto& unit = (*found).second;
    _LoadCharacter(unit, info);
  }
  else {
    _DEBUG_BREAK(0);
  }
}

void CCharacterManager::_LoadCharacter(SUnit& unit, SGameInfo& info)
{
  if (unit.count == 0)
  {
    LoadModel(unit, info);
    LoadMotion(unit, info);
    LoadAction(unit, info);
    unit.count += 1;
  }
  else
  {
    unit.count += 1;
  }
}
void CCharacterManager::UnloadCharacter(ECharacter eChara, SGameInfo& info)
{
  auto found = m_datum.find(eChara);
  if (found != m_datum.end()) {
    auto& unit = (*found).second;
    unit.count -= 1;
    if (unit.count == 0) {
      _UnloadCharacter(unit, info);
    }
  }
}
void CCharacterManager::_UnloadCharacter(SUnit& unit, SGameInfo& info)
{
  if (auto* pLoadManager = info.pLoadManager) {
    auto& arche = unit.archetype;
    if (arche.vertices) {
      pLoadManager->UnloadVertices(unit.data.eFileModel);
    }
    arche.vertices.Invalidate();
    arche.indices.Invalidate();
    arche.unVerticesNum = 0;
    arche.unIndicesNum = 0;
    arche.unOneVertexSize = 0;
    arche.unOneIndexSize = 0;
    for (auto& p : arche.parts) {
      auto unload_tex = [pLoadManager](ghl::HTextureBuffer& h, size_t& k) {
        if (h && k != 0) {
          pLoadManager->UnloadTexture(k);
        }
        h.Invalidate();
        k = 0;
      };
      unload_tex(p.texColor, p.key2TexColor);
      unload_tex(p.texSphirical, p.key2TexSphirical);
      unload_tex(p.texAdditionalSpherical, p.key2TexAdditionalSpherical);
      unload_tex(p.texToon, p.key2TexToon);
      p.unIndicesNum = 0;
    }
    arche.parts.clear();
    arche.parts.shrink_to_fit();
    pLoadManager->UnloadSkeleton(unit.data.eFileModel);
    arche.skeleton.reset();
    pLoadManager->UnloadMotionSet(unit.data.eFileMotionSet);
    arche.motion_set.reset();
    pLoadManager->UnloadAction(unit.data.eFileAction);
    arche.action.reset();
  }
}
void CCharacterManager::LoadModel(SUnit& unit, SGameInfo& info)
{
  auto& arche = unit.archetype;
  if (auto* pLoadManager = info.pLoadManager) {
    // モデル読み込み.
    if (unit.data.eFileModel != ELoadFile::None) {
      if (auto* pModelPath = info.pFilePathManager->GetPath(unit.data.eFileModel)) {
        FILE* fp = nullptr;
        fopen_s(&fp, pModelPath, "rb");
        if (fp) {
          // 一度解析して先頭座標を保存する.
          fpos_t pos_vertex   = std::numeric_limits<fpos_t>::max();
          fpos_t pos_material = std::numeric_limits<fpos_t>::max();
          fpos_t pos_skeleton = std::numeric_limits<fpos_t>::max();
          fpos_t pos_ik       = std::numeric_limits<fpos_t>::max();
          int result = -1;
          {
            // 頂点.
            result = fgetpos(fp, &pos_vertex);
            _DEBUG_BREAK(result == 0);
            char signature[3] = {};
            ghl::pmd::SHeaderFormat pmdheader{};
            fseek(fp, sizeof(signature), SEEK_CUR);
            fseek(fp, sizeof(pmdheader), SEEK_CUR);
            constexpr size_t pmd_vertex_size = 38;
            uint32_t vertNum = 0;
            fread(&vertNum, sizeof(vertNum), 1, fp);
            fseek(fp, (vertNum * pmd_vertex_size), SEEK_CUR);
            constexpr size_t pmd_index_size = sizeof(unsigned short);
            uint32_t indexNum = 0;
            fread(&indexNum, sizeof(indexNum), 1, fp);
            fseek(fp, (indexNum * pmd_index_size), SEEK_CUR);
            // マテリアル.
            result = fgetpos(fp, &pos_material);
            _DEBUG_BREAK(result == 0);
            uint32_t materialNum = 0;
            fread(&materialNum, sizeof(materialNum), 1, fp);
            constexpr size_t pmd_material_size = 70;
            fseek(fp, (materialNum * pmd_material_size), SEEK_CUR);
            // スケルトン.
            result = fgetpos(fp, &pos_skeleton);
            _DEBUG_BREAK(result == 0);
            uint16_t boneNum = 0;
            fread(&boneNum, sizeof(boneNum), 1, fp);
            constexpr size_t pmd_bone_size = 39;
            fseek(fp, (boneNum * pmd_bone_size), SEEK_CUR);
            // IK.
            result = fgetpos(fp, &pos_ik);
            _DEBUG_BREAK(result == 0);
            uint16_t ikNum = 0;
            fread(&ikNum, sizeof(ikNum), 1, fp);
          }
          // 頂点バッファとインデックスバッファ作成.
          result = fsetpos(fp, &pos_vertex);
          _DEBUG_BREAK(result == 0);
          auto* pVerticesLoader = pLoadManager->LoadVertices(unit.data.eFileModel, fp, info);
          if (pVerticesLoader != nullptr && pVerticesLoader->IsLoaded()) {
            arche.vertices        = pVerticesLoader->GetVertexBuffer();
            arche.unVerticesNum   = pVerticesLoader->GetVertexNum();
            arche.unOneVertexSize = pVerticesLoader->GetOneVertexBytes();
            arche.indices         = pVerticesLoader->GetIndexBuffer();
            arche.unIndicesNum    = pVerticesLoader->GetIndexNum();
            arche.unOneIndexSize  = pVerticesLoader->GetOneIndexBytes();
          }
          // マテリアルを解析してマテリアル定数とテクスチャバッファを作成.
          // マテリアル定数のバッファは利用者側で変更できるよう、作成しない.
          result = fsetpos(fp, &pos_material);
          _DEBUG_BREAK(result == 0);
          uint32_t materialNum = 0;
          fread(&materialNum, sizeof(materialNum), 1, fp);
          constexpr size_t pmd_material_size = 70;
          std::vector<char> temp(pmd_material_size * materialNum);  // パディング問題回避のため、一度char型で受け取り分解していく.
          fread(temp.data(), temp.size(), 1, fp);
          arche.parts.resize(materialNum);  // 受け取り側を拡張.
          for (uint32_t i=0; i<materialNum; ++i) {
            ghl::pmd::SMaterialFormat mat{};
            auto* pSrc = &temp[pmd_material_size * i];
            std::memcpy(&mat.diffuse, pSrc, 46);
            std::memcpy(&mat.indicesNum, pSrc + 46, 24);
            auto& dst = arche.parts.at(i);
            // マテリアル定数.
            dst.material.diffuse    = mat.diffuse;
            dst.material.alpha      = mat.alpha;
            dst.material.specular   = mat.specular;
            dst.material.specularity= mat.specularity;
            dst.material.ambient    = mat.ambient;
            dst.unIndicesNum        = mat.indicesNum;
            dst.bEdgeFlag           = mat.edgeFlg != 0;
            // マテリアルテクスチャ.
            if (mat.texFilePath[0] != '\0') {
              std::string source = mat.texFilePath;
              int try_max = 10;
              while (try_max > 0) {
                --try_max;
                auto index_of_delim = source.find('*');
                std::string current;
                if (index_of_delim != std::string::npos) {
                  current = source.substr(0, index_of_delim);
                  source = source.substr(index_of_delim + 1, source.length() - 1);
                }
                else {
                  current = source;
                  try_max = 0;
                }
                if (current.length() > 0) {
                  auto index_of_extension = current.find_last_of('.');
                  if (index_of_extension != std::string::npos) {
                    // 用途.
                    auto eTextureUsage = ghl::ETextureUsage::eColor;
                    struct SUsagePicker {
                      const char* pExtension;
                      ghl::ETextureUsage eUsage;
                    };
                    const SUsagePicker usage_picker[] = {
                      { "SPH",  ghl::ETextureUsage::eSpherical            },
                      { "SPA",  ghl::ETextureUsage::eAdditionalSpherical  },
                    };
                    // 拡張子(大文字化).
                    std::string extension = current.substr(index_of_extension + 1, current.length() - 1);
                    std::transform(extension.begin(), extension.end(), extension.begin(), std::toupper);
                    for (auto& p : usage_picker) {
                      if (extension == p.pExtension) {
                        eTextureUsage = p.eUsage;
                        break;
                      }
                    }
                    // ロード.
                    char texture_path[256] = {'\0'};
                    sprintf_s(texture_path, "%s%s", CFilePathManager::GetModelDir(), current.c_str());
                    auto* pTextureLoader = pLoadManager->LoadTexture(texture_path, info);
                    if (pTextureLoader != nullptr) {
                      if (eTextureUsage == ghl::ETextureUsage::eColor) {
                        dst.texColor = pTextureLoader->GetBuffer();
                        dst.key2TexColor = pTextureLoader->GetHash();
                      }
                      else if (eTextureUsage == ghl::ETextureUsage::eSpherical) {
                        dst.texSphirical = pTextureLoader->GetBuffer();
                        dst.key2TexSphirical = pTextureLoader->GetHash();
                      }
                      else if (eTextureUsage == ghl::ETextureUsage::eAdditionalSpherical) {
                        dst.texAdditionalSpherical = pTextureLoader->GetBuffer();
                        dst.key2TexAdditionalSpherical = pTextureLoader->GetHash();
                      }
                    }
                  }
                }
              }
            }
            // Toon.
            if (mat.toonIdx >= 0 && mat.toonIdx < 10) {
              auto number = mat.toonIdx + 1;
              char texture_path[256] = {'\0'};
              sprintf_s(texture_path, "%stoon%02d.bmp", CFilePathManager::GetToonDir(), number);
              auto* pTextureLoader = pLoadManager->LoadTexture(texture_path, info);
              if (pTextureLoader != nullptr) {
                dst.texToon = pTextureLoader->GetBuffer();
                dst.key2TexToon = pTextureLoader->GetHash();
              }
            }
          }
          // スケルトン.
          result = fsetpos(fp, &pos_skeleton);
          _DEBUG_BREAK(result == 0);
          if (auto* pSkeletonLoader = pLoadManager->LoadSkeleton(unit.data.eFileModel, fp, info)) {
            arche.skeleton = pSkeletonLoader->GetSkeleton();
          }

          // 閉じる.
          fclose(fp);
        }
      }
    }
  }
}
void CCharacterManager::LoadMotion(SUnit& unit, SGameInfo& info)
{
  auto& arche = unit.archetype;
  if (auto* pLoadManager = info.pLoadManager) {
    // モーションセット読み込み.
    if (unit.data.eFileMotionSet != ELoadFile::None) {
      if (auto* pMotionSetLoader = pLoadManager->LoadMotionSet(unit.data.eFileMotionSet, info)) {
        arche.motion_set = pMotionSetLoader->GetMotionSet();
      }
    }
  }
}
void CCharacterManager::SetPlayerByIndex(size_t index, EPlayerIndex ePlayer)
{
  size_t count = 0;
  for (auto* pChara : m_characters) {
    if (count == index) {
      if (pChara != nullptr) {
        pChara->SetPlayer(ePlayer);
        auto index = e2ut(ePlayer);
        if (index >= 0 && index < m_player_characters.size()) {
          m_player_characters.at(index) = pChara;
        }
      }
      break;
    }
    ++count;
  }
}
const CCharacter* CCharacterManager::GetPlayerCharacter(EPlayerIndex ePlayer) const
{
  auto index = e2ut(ePlayer);
  if (index >= 0 && index < m_player_characters.size()) {
    return m_player_characters.at(index);
  }
  return nullptr;
}
void CCharacterManager::LoadAction(SUnit& unit, SGameInfo& info)
{
  auto& arche = unit.archetype;
  if (auto* pLoadManager = info.pLoadManager) {
    // アクション読み込み.
    if (unit.data.eFileAction != ELoadFile::None) {
      if (auto* pActionLoader = pLoadManager->LoadAction(unit.data.eFileAction, info)) {
        arche.action = pActionLoader->GetData();
      }
    }
  }
}
