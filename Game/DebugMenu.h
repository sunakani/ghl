#pragma once

#include "GHL.h"
#include "imgui.h"

namespace Debug {

template<typename T>
struct SOption {
	bool bEnabled{false};
	T contents{};
	void Init(bool b) {
		bEnabled = b;
		contents.Init();
	}
};

// カメラ.
struct SCamera {
	float fov{};
	void Init() {
		fov = 45.f;
	}
};

// プレイヤーキャラの身体性能.
struct SPlayerPhysical {
	float move_speed_rate{1.f};
	void Init() {
		move_speed_rate = 1.f;
	}
};


}//namespace Debug.

class CDebugMenu
{
public:
	CDebugMenu();
	~CDebugMenu();
	void Init(HWND hWnd);
	void Term();

	void UpdateImGui();
	void LogImGui(const char* contents);

	const Debug::SOption<Debug::SCamera>&					GetCamera() const						{ return m_camera;					}
	const Debug::SOption<Debug::SPlayerPhysical>&	GetPlayerPhysical() const		{ return m_player_physical;	}

private:
	void _UpdateImGui();

private:

	Debug::SOption<Debug::SCamera>					m_camera;
	Debug::SOption<Debug::SPlayerPhysical>	m_player_physical;

	class ExampleAppLog;
	std::unique_ptr<ExampleAppLog> m_pimplOfLog;
};

// ImGuiによるデバッグメニュー更新.
void DebugMenuImGui();
