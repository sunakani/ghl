﻿#include "GHL.h"
#include "graphic/DirectX.h"

namespace ghl {

CDirectX* s_directx = nullptr;
bool s_bFirstTick = true;
LARGE_INTEGER s_freq;
LARGE_INTEGER s_curr;
LARGE_INTEGER s_prev;
float s_delta_time = 0.f;
float s_fps = 60.f;
float s_delta_time_max = 1.f / s_fps;
float s_elapsed_time = 0.f;
float s_raw_fps = 0.f;

bool InitLibrary(const SInitParam& param)
{
  s_directx = new CDirectX();
  if (s_directx == nullptr) {
    return false;
  }
  if (s_directx->Init(param) == false) {
    return false;
  }
  return true;
}

void TermLibrary()
{
  if (s_directx != nullptr) {
    s_directx->Term();
    delete s_directx;
    s_directx = nullptr;
  }
}

void UpdateLibrary(std::function<void(float)> updater)
{
  if (s_bFirstTick) {
    s_bFirstTick = false;
    QueryPerformanceFrequency(&s_freq);
    QueryPerformanceCounter(&s_curr);
    s_prev = s_curr;
  }
  if (updater) {
    s_prev = s_curr;
    QueryPerformanceCounter(&s_curr);
    s_delta_time = static_cast<float>(s_curr.QuadPart - s_prev.QuadPart) / s_freq.QuadPart;
    s_delta_time = std::min(s_delta_time, s_delta_time_max);
    s_elapsed_time += s_delta_time;
    if (s_delta_time > FLT_EPSILON) {
      s_raw_fps = 1.f / s_delta_time;
    }
    updater(s_delta_time);
  }
}

float GetElapsedTime() {
  return s_elapsed_time;
}
float GetFps() {
  return s_raw_fps;
}

IGraphic& GraphicInterface() {
  return *s_directx;
}

bool ImGuiInit(HWND hWnd) {
  if (s_directx != nullptr) {
    return s_directx->ImGuiInit(hWnd);
  }
  return false;
}
void ImGuiTerm() {
  if (s_directx != nullptr) {
    s_directx->ImGuiTerm();
  }
}
void ImGuiNewFrame() {
  if (s_directx != nullptr) {
    s_directx->ImGuiNewFrame();
  }
}
void ImGuiRender() {
  if (s_directx != nullptr) {
    s_directx->ImGuiRender();
  }
}

#if 0
#include "memory\PoolAllocator.h"
#include "memory\Allocator.h"

class TestObj {
public:
  TestObj(int a) : m_a(a) {
    //std::cout << "ctor(" << m_a << ")\n";
  }
  ~TestObj() {
    //std::cout << "dtor(" << m_a << ")\n";
  }
private:
  int m_a;
};

enum {
  TestNum = 20000000,
};

#define USE_POOL_ALLCATOR

int main()
{
#if defined(USE_POOL_ALLCATOR)
  using TestAllocator = ghl::PoolAllocator<TestObj, ghl::NewDeleteAllocator<TestObj>>;
  std::vector<TestAllocator::CWrap> wraps(TestNum);
  TestAllocator pool;
  std::cout << "TotalAllocateSize = " << ghl::NewDeleteAllocatorTotalSize << "\n";
  pool.Init(TestNum);
  std::cout << "TotalAllocateSize = " << ghl::NewDeleteAllocatorTotalSize << "\n";
#else
  std::vector<TestObj*> wraps(TestNum);
#endif

  auto start = std::chrono::system_clock::now();
  for (int i = 0; i < TestNum; ++i) {
#if defined(USE_POOL_ALLCATOR)
    wraps.at(i) = pool.GetFree(i);
#else
    wraps.at(i) = new TestObj(i);
#endif
  }
  for (auto& w : wraps) {
#if defined(USE_POOL_ALLCATOR)
    pool.SetFree(w);
#else
    delete w;
#endif
  }
  auto end = std::chrono::system_clock::now();

#if defined(USE_POOL_ALLCATOR)
  pool.Term();
  std::cout << "TotalAllocateSize = " << ghl::NewDeleteAllocatorTotalSize << "\n";
#endif

  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
  std::cout << "Elapsed = " << elapsed << "\n";
}
#endif


}
