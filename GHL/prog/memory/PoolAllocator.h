#pragma once

namespace ghl {

/************************************************************/
/*	プールアロケータ.
*   使用可能な前方向リストを作成し、
*   必要な場合にその先頭を差し出し、使用中リストとして把握する。
*   返却されたものは使用中リストの結びを変えた後、使用可能リストの先頭に配置する。
*************************************************************/
template<
  typename T,
  template<typename> typename Allocator
>
class PoolAllocator
{
  enum {
    eBufferByte = sizeof(T),
    eBufferSize = eBufferByte * 8,
  };

  struct SUnit {
    char buffer[eBufferByte];
    T* ptr;
    SUnit* pPrev;
    SUnit* pNext;
    void Init()
    {
      buffer[0] = '\0';
      ptr = reinterpret_cast<T*>(buffer);
      pPrev = nullptr;
      pNext = nullptr;
    }
  };

public:
  class CWrap
  {
  public:
    CWrap()
      : m_pUnit(nullptr)
    {}
    CWrap(SUnit* pUnit)
      : m_pUnit(pUnit)
    {}
    CWrap(const CWrap& rhs)
      : m_pUnit(rhs.m_pUnit)
    {}
    CWrap& operator=(const CWrap& rhs) {
      m_pUnit = rhs.m_pUnit;
      return *this;
    }
    T& operator*() {
      return *m_pUnit->ptr;
    }
    const T& operator*() const {
      return *m_pUnit->ptr;
    }
    T* operator->() {
      return m_pUnit->ptr;
    }
    const T* operator->() const {
      return m_pUnit->ptr;
    }
    explicit operator bool() const {
      return m_pUnit != nullptr;
    }
    bool operator==(const CWrap& rhs) const {
      return m_pUnit == rhs.m_pUnit;
    }
    bool operator!=(const CWrap& rhs) const {
      return m_pUnit != rhs.m_pUnit;
    }
    bool operator==(const SUnit* pUnit) const {
      return m_pUnit == pUnit;
    }
    bool operator!=(const SUnit* pUnit) const {
      return m_pUnit != pUnit;
    }
    SUnit* GetUnit() {
      return m_pUnit;
    }
    void Clear() {
      m_pUnit = nullptr;
    }
  private:
    SUnit* m_pUnit;
  };

public:
  PoolAllocator()
    : m_pool(0)
    , m_free_head(nullptr)
    , m_use_head(nullptr)
    , m_use_tail(nullptr)
    , m_use_num(0)
  {}

  void Init(int n)
  {
    m_pool.resize(n);
    SUnit* pPrev = nullptr;
    for (auto& elem : m_pool) {
      elem.Init();
      if (pPrev != nullptr) {
        pPrev->pNext = &elem;
      }
      pPrev = &elem;
    }
    m_free_head = &m_pool.at(0);
    m_use_head = nullptr;
    m_use_tail = nullptr;
    m_use_num = 0;
  }

  void Term()
  {
    m_free_head = nullptr;
    m_use_head = nullptr;
    m_use_tail = nullptr;
    m_use_num = 0;
    m_pool.clear();
    m_pool.shrink_to_fit();
  }

  void Clear()
  {
    m_free_head = &m_pool.at(0);
    m_use_head = nullptr;
    m_use_tail = nullptr;
    m_use_num = 0;
  }

  template <typename... Args>
  CWrap GetFree(Args... args)
  {
    if (auto* pFree = m_free_head) {
      // 返すUnitをFreeListから外す.
      auto* pNewHead = pFree->pNext;
      pFree->pNext = nullptr;
      m_free_head = pNewHead;
      // 返すUnitをUseListの先頭にする.
      if (m_use_head != nullptr) {
        // UseListは双方向.
        auto* pOldTail = m_use_tail;
        m_use_tail = pFree;
        pOldTail->pNext = m_use_tail;
        m_use_tail->pPrev = pOldTail;
      }
      else {
        m_use_head = pFree;
        m_use_tail = pFree;
      }
      ++m_use_num;
      new(pFree->buffer) T(args...);
      return CWrap(pFree);
    }
    return CWrap(nullptr);
  }

  void SetFree(CWrap& wrap)
  {
    if (auto* pUnit = wrap.GetUnit()) {
      // UseListから除外.
      if (pUnit == m_use_tail) {
        m_use_tail = pUnit->pPrev;
      }
      if (pUnit == m_use_head) {
        m_use_head = pUnit->pNext;
      }
      // 前後の結び替え.
      if (pUnit->pPrev != nullptr) {
        pUnit->pPrev->pNext = pUnit->pNext;
      }
      if (pUnit->pNext != nullptr) {
        pUnit->pNext->pPrev = pUnit->pPrev;
      }
      pUnit->pPrev = nullptr;
      pUnit->pNext = nullptr;
      // デストラクタ.
      pUnit->ptr->~T();
      pUnit->Init();
      --m_use_num;
      // FreeListの先頭にする.
      auto* pOldFreeHead = m_free_head;
      m_free_head = pUnit;
      m_free_head->pNext = pOldFreeHead;
      // 引数はクリア.
      wrap.Clear();
    }
  }

  template<typename Fun>
  void ForEach(Fun fun)
  {
    auto* pTarget = m_use_head;
    while (pTarget != nullptr) {
      fun(*pTarget->ptr);
      pTarget = pTarget->pNext;
    }
  }

private:
  std::vector<SUnit, Allocator<SUnit>> m_pool;
  SUnit* m_free_head;
  SUnit* m_use_head;
  SUnit* m_use_tail;
  int32_t m_use_num;
};

}
