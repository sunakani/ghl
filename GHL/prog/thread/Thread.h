#pragma once

namespace ghl {

/************************************************************/
/*	スレッドを立ち上げ、スレッド関数の更新を行うクラス.
*************************************************************/
class CThread
{
public:
	CThread()
		: m_handle()
		, m_id()
		, m_begin_event()
		, m_end_event()
	{}
	using AThreadFunc = unsigned (void*);
	void Init(AThreadFunc* pFunc, PCWSTR name) {
		m_handle = reinterpret_cast<HANDLE>(_beginthreadex(nullptr, 0, pFunc, this, CREATE_SUSPENDED, &m_id));

		wchar_t event_name[64];
		_snwprintf_s(event_name, sizeof(event_name), L"ThreadBeginEvent%u", m_id);
		m_begin_event	= CreateEvent(NULL, TRUE, FALSE, event_name);

		_snwprintf_s(event_name, sizeof(event_name), L"ThreadEndEvent%u", m_id);
		m_end_event		= CreateEvent(NULL, TRUE, FALSE, event_name);

		SetThreadDescription(m_handle, name);

		ResumeThread(m_handle);
	}
	void Term() {
		CloseHandle(m_handle);
		m_handle = NULL;
		CloseHandle(m_begin_event);
		m_begin_event = NULL;
		CloseHandle(m_end_event);
		m_end_event = NULL;
	}

	void SetBegin() {
		SetEvent(m_begin_event);
		ResetEvent(m_end_event);
	}
	void SetEnd() {
		ResetEvent(m_begin_event);
		SetEvent(m_end_event);
	}

	void WaitBegin() {
		WaitForSingleObject(m_begin_event, INFINITE);
	}
	void WaitEnd() {
		WaitForSingleObject(m_end_event, INFINITE);
	}

	unsigned int GetId() const { return m_id; }

private:
	HANDLE				m_handle{};
	unsigned int	m_id{};
	HANDLE				m_begin_event{};
	HANDLE				m_end_event{};
};

#if 0
class CThreadTest : public ghl::CThread
{
public:
  using Super = ghl::CThread;
  CThreadTest()
    : ghl::CThread()
    , m_args()
  {}
  void Init(PCWSTR name) {
    Super::Init(ThreadFunc, name);
    m_args.clear();
  }

  void Add(int value) { m_args.push_back(value); }
  const std::vector<int>& Get() const { return m_args; }
  void Reset() { m_args.clear(); }

	// スレッド関数.
	// 呼び出し元のスレッドから、開始イベントと終了イベントを受け取ることで動作する.
  static unsigned ThreadFunc(void* pArgs)
  {
    while(1) {
      CThreadTest* pTest = reinterpret_cast<CThreadTest*>(pArgs);
      pTest->WaitBegin();
      for (auto value : pTest->Get()) {
        DebugPrintf("%d\n", value);
      }
      pTest->SetEnd();
    }
    return 0;
  }

private:
  std::vector<int> m_args;
};
#endif

}
