#include "DirectX_defs.h"

namespace ghl {

// Utility.
D3D12_DESCRIPTOR_RANGE_TYPE ToDxType(EDescriptorType type) {
  return
    type == EDescriptorType::eShaderResourceView  ? D3D12_DESCRIPTOR_RANGE_TYPE_SRV :
    type == EDescriptorType::eUnorderedAccessView  ? D3D12_DESCRIPTOR_RANGE_TYPE_UAV :
    type == EDescriptorType::eConstantBufferView  ? D3D12_DESCRIPTOR_RANGE_TYPE_CBV :
    D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER;
}
D3D12_DESCRIPTOR_HEAP_TYPE ToDxType(EDescriptorHeapType type) {
  return
    type == EDescriptorHeapType::eCBV_SRV_UAV  ? D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV :
    type == EDescriptorHeapType::eSampler      ? D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER :
    type == EDescriptorHeapType::eRTV          ? D3D12_DESCRIPTOR_HEAP_TYPE_RTV :
    type == EDescriptorHeapType::eDSV          ? D3D12_DESCRIPTOR_HEAP_TYPE_DSV :
    D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
}

D3D12_TEXTURE_ADDRESS_MODE ToDxType(ETextureAddressMode type) {
  return
    type == ETextureAddressMode::eWrap    ? D3D12_TEXTURE_ADDRESS_MODE_WRAP :
    type == ETextureAddressMode::eMirror  ? D3D12_TEXTURE_ADDRESS_MODE_MIRROR :
    type == ETextureAddressMode::eClamp    ? D3D12_TEXTURE_ADDRESS_MODE_CLAMP :
    type == ETextureAddressMode::eBorder  ? D3D12_TEXTURE_ADDRESS_MODE_BORDER :
    D3D12_TEXTURE_ADDRESS_MODE_MIRROR_ONCE;
}
DXGI_FORMAT ToDxType(EGraphInfraFormat type) {
  static const DXGI_FORMAT sk_list[] = {
    DXGI_FORMAT_R32G32B32_FLOAT,
    DXGI_FORMAT_R32G32_FLOAT,
    DXGI_FORMAT_R16G16_UINT,
    DXGI_FORMAT_R8_UINT,
    DXGI_FORMAT_R8G8B8A8_UNORM,
    DXGI_FORMAT_D32_FLOAT,
    DXGI_FORMAT_R32_TYPELESS,
    DXGI_FORMAT_R32_FLOAT,
    DXGI_FORMAT_UNKNOWN,
  };
  static_assert(_countof(sk_list) == e2ut(EGraphInfraFormat::Num),"Format numbers don't match.");
  auto index = e2ut(type);
  if (index >= 0 && index < e2ut(EGraphInfraFormat::Num)) {
    return sk_list[index];
  }
  _DEBUG_BREAK(0);  // ����`.
  return DXGI_FORMAT_UNKNOWN;
}
D3D_PRIMITIVE_TOPOLOGY ToDxType(EPrimitiveTopology type) {
  return
    type == EPrimitiveTopology::eTriangleList ? D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST :
    type == EPrimitiveTopology::eTriangleStrip ? D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP :
    type == EPrimitiveTopology::ePointList ? D3D_PRIMITIVE_TOPOLOGY_POINTLIST :
    type == EPrimitiveTopology::eLineList ? D3D_PRIMITIVE_TOPOLOGY_LINELIST :
    type == EPrimitiveTopology::eLineStrip ? D3D_PRIMITIVE_TOPOLOGY_LINESTRIP :
    D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
}
D3D12_HEAP_TYPE ToDxType(EResourceHeapType type) {
  return
    type == EResourceHeapType::eDefault    ? D3D12_HEAP_TYPE_DEFAULT :
    type == EResourceHeapType::eUpload    ? D3D12_HEAP_TYPE_UPLOAD :
    type == EResourceHeapType::eReadback  ? D3D12_HEAP_TYPE_READBACK :
    type == EResourceHeapType::eCustom    ? D3D12_HEAP_TYPE_CUSTOM :
    D3D12_HEAP_TYPE_DEFAULT;
}
D3D12_CPU_PAGE_PROPERTY ToDxType(EResourceCpuPageProp type) {
  return
    type == EResourceCpuPageProp::eUnknown      ? D3D12_CPU_PAGE_PROPERTY_UNKNOWN :
    type == EResourceCpuPageProp::eNotAvailable  ? D3D12_CPU_PAGE_PROPERTY_NOT_AVAILABLE :
    type == EResourceCpuPageProp::eWriteCombine  ? D3D12_CPU_PAGE_PROPERTY_WRITE_COMBINE :
    type == EResourceCpuPageProp::eWriteBack    ? D3D12_CPU_PAGE_PROPERTY_WRITE_BACK :
    D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
}
D3D12_MEMORY_POOL ToDxType(EResourceMemoryPool type) {
  return
    type == EResourceMemoryPool::eUnknown    ? D3D12_MEMORY_POOL_UNKNOWN :
    type == EResourceMemoryPool::eL0        ? D3D12_MEMORY_POOL_L0 :
    type == EResourceMemoryPool::eL1        ? D3D12_MEMORY_POOL_L1 :
    D3D12_MEMORY_POOL_UNKNOWN;
}
D3D12_RESOURCE_STATES ToDxType(EResourceState type) {
  if (type == EResourceState::eRenderTarget) {
    return D3D12_RESOURCE_STATE_RENDER_TARGET;
  }
  if (type == EResourceState::ePixcelShaderResource) {
    return D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
  }
  _DEBUG_BREAK(0);  // ����`.
  return D3D12_RESOURCE_STATE_COMMON;
}
D3D12_FILTER ToDxType(EFilter type) {
  static const D3D12_FILTER sk_list[] = {
    D3D12_FILTER_MIN_MAG_MIP_POINT,
    D3D12_FILTER_COMPARISON_MIN_MAG_MIP_POINT,
    D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR,
  };
  static_assert(_countof(sk_list) == e2ut(EFilter::Num),"Filter numbers don't match.");
  auto index = e2ut(type);
  if (index >= 0 && index < e2ut(EFilter::Num)) {
    return sk_list[index];
  }
  _DEBUG_BREAK(0);  // ����`.
  return D3D12_FILTER_MIN_MAG_MIP_POINT;
}
D3D12_COMPARISON_FUNC ToDxType(EComparisonFunc type) {
  static const D3D12_COMPARISON_FUNC sk_list[] = {
    D3D12_COMPARISON_FUNC_NEVER,
    D3D12_COMPARISON_FUNC_LESS,
    D3D12_COMPARISON_FUNC_EQUAL,
    D3D12_COMPARISON_FUNC_LESS_EQUAL,
    D3D12_COMPARISON_FUNC_GREATER,
    D3D12_COMPARISON_FUNC_NOT_EQUAL,
    D3D12_COMPARISON_FUNC_GREATER_EQUAL,
    D3D12_COMPARISON_FUNC_ALWAYS,
  };
  static_assert(_countof(sk_list) == e2ut(EComparisonFunc::Num),"ComparisonFunc numbers don't match.");
  auto index = e2ut(type);
  if (index >= 0 && index < e2ut(EComparisonFunc::Num)) {
    return sk_list[index];
  }
  _DEBUG_BREAK(0);  // ����`.
  return D3D12_COMPARISON_FUNC_NEVER;
}
D3D12_PRIMITIVE_TOPOLOGY_TYPE  ToDxType(EPrimitiveTopologyType type) {
  static const D3D12_PRIMITIVE_TOPOLOGY_TYPE sk_list[] = {
    D3D12_PRIMITIVE_TOPOLOGY_TYPE_UNDEFINED,
    D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT,
    D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE,
    D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE,
    D3D12_PRIMITIVE_TOPOLOGY_TYPE_PATCH,
  };
  static_assert(_countof(sk_list) == e2ut(EPrimitiveTopologyType::Num),"PrimitiveTopologyType numbers don't match.");
  auto index = e2ut(type);
  if (index >= 0 && index < e2ut(EPrimitiveTopologyType::Num)) {
    return sk_list[index];
  }
  _DEBUG_BREAK(0);  // ����`.
  return D3D12_PRIMITIVE_TOPOLOGY_TYPE_UNDEFINED;
}
D3D12_CULL_MODE ToDxType(ECullMode type) {
  static const D3D12_CULL_MODE sk_list[] = {
    D3D12_CULL_MODE_NONE,
    D3D12_CULL_MODE_FRONT,
    D3D12_CULL_MODE_BACK,
  };
  static_assert(_countof(sk_list) == e2ut(ECullMode::Num),"CullMode numbers don't match.");
  auto index = e2ut(type);
  if (index >= 0 && index < e2ut(ECullMode::Num)) {
    return sk_list[index];
  }
  _DEBUG_BREAK(0);  // ����`.
  return D3D12_CULL_MODE_NONE;
}
D3D12_FILL_MODE ToDxType(EFillMode type) {
  static const D3D12_FILL_MODE sk_list[] = {
    D3D12_FILL_MODE_WIREFRAME,
    D3D12_FILL_MODE_SOLID,
  };
  static_assert(_countof(sk_list) == e2ut(EFillMode::Num),"FillMode numbers don't match.");
  auto index = e2ut(type);
  if (index >= 0 && index < e2ut(EFillMode::Num)) {
    return sk_list[index];
  }
  _DEBUG_BREAK(0);  // ����`.
  return D3D12_FILL_MODE_SOLID;
}


}//namespace ghl.
