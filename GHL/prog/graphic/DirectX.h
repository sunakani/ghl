#pragma once

#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXTex.h>
#include <d3dx12.h>

#include "GHL.h"
#include "DirectX_defs.h"

#include "SpriteFont.h"
#include "ResourceUploadBatch.h"

#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"

namespace ghl {

// フェンス.
struct SFence {
  ID3D12Fence* object = nullptr;
  UINT64 value = 0;
  void Init() {
    object = nullptr;
    value = 0;
  }
};

//------------------------------------------------
// DX描画クラス.
//------------------------------------------------
class CDirectX : public IGraphic
{
public:
  CDirectX();
  bool Init(const SInitParam& param);
  void Term();

  // ImGui.
  bool ImGuiInit(HWND hWnd);
  void ImGuiTerm();
  void ImGuiNewFrame();
  void ImGuiRender();

  // ルートシグネチャ.
  virtual HRootSignatur RootSig_Create  (const SCreateRootSignatureParam& param) override;
  virtual void          RootSig_Destroy (HRootSignatur& hRoot) override;
  virtual void          Cmd_RootSig_Set (HRootSignatur hRoot) override;

  // パイプラインステート.
  virtual HPipelineState  PipeState_Create  (const SCreatePiplelineStateParam& param) override;
  virtual void            PipeState_Destroy (HPipelineState& hPipe) override;
  virtual void            Cmd_PipeState_Set (HPipelineState hPipe) override;

  // 頂点.
  virtual HVertexBuffer Vertex_CreateBuffer   (size_t one_vertex_bytes, size_t vertex_num) override;
  virtual void          Vertex_DestroyBuffer  (HVertexBuffer& hBuffer) override;
  virtual void          Vertex_UpdateBuffer   (HVertexBuffer hBuffer, const void* pBin, size_t one_vertex_bytes, size_t vertex_num) override;
  virtual HVertexView   Vertex_CreateView     (HVertexBuffer hBuffer, size_t one_vertex_bytes, size_t vertex_num) override;
  virtual void          Vertex_DestroyView    (HVertexBuffer hBuffer, HVertexView& hView) override;
  virtual void          Cmd_Vertex_Set        (HVertexBuffer hBuffer, HVertexView hView) override;

  // インデックス.
  virtual HIndexBuffer  Index_CreateBuffer    (size_t one_index_bytes, size_t index_num) override;
  virtual void          Index_DestroyBuffer   (HIndexBuffer& hIndex) override;
  virtual void          Index_UpdateBuffer    (HIndexBuffer hIndex, const void* pBin, size_t one_index_bytes, size_t index_num) override;
  virtual HIndexView    Index_CreateView      (HIndexBuffer hBuffer, size_t one_index_bytes, size_t index_num) override;
  virtual void          Index_DestroyView     (HIndexBuffer hBuffer, HIndexView& hView) override;
  virtual void          Cmd_Index_Set         (HIndexBuffer hBuffer, HIndexView hView) override;

  // ディスクリプタヒープ.
  virtual HDescHeap     DescHeap_Create               (EDescriptorHeapType type, uint16_t descNum, bool bShaderVisible) override;
  virtual void          DescHeap_Destroy              (HDescHeap& hDesc) override;
  virtual void          DescHeap_PlaceConstView       (HDescHeap hDesc, HConstBuffer hConst, uint16_t pos) override;
  virtual void          DescHeap_PlaceTextureView     (HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos) override;
  virtual void          DescHeap_PlaceTextureView     (HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos, EGraphInfraFormat eFormat) override;
  virtual void          DescHeap_PlaceRenderTargetView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos) override;
  virtual void          DescHeap_PlaceDepthStencilView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos, EGraphInfraFormat eFormat) override;
  virtual void          Cmd_DescHeap_Set              (HDescHeap hDesc) override;
  virtual void          Cmd_DescHeap_Set              (HDescHeap hDesc0, HDescHeap hDesc1) override;
  virtual void          Cmd_DescHeap_SetRootParam     (HDescHeap hDesc, uint16_t nRootParamIdx, uint16_t nTargetIndex) override;

  // 定数.
  virtual HConstBuffer Const_CreateBuffer  (size_t bytes) override;
  virtual void         Const_DestroyBuffer (HConstBuffer& hConst) override;
  virtual void         Const_UpdateBuffer  (HConstBuffer hConst, const void* pBin, size_t bytes) override;

  // テクスチャ.
  virtual HTextureBuffer  Texture_CreateBufferFromMemory  (const void* pBin, size_t bytes, ETextureFormat format) override;
  virtual HTextureBuffer  Texture_CreateBuffer            (uint32_t nWidth, uint32_t nHeight) override;
  virtual HTextureBuffer  Texture_CreateBuffer            (const SCreateTextureBufferParam& param) override;
  virtual HTextureBuffer  Texture_CreateDepthStencilBuffer(uint32_t nWidth, uint32_t nHeight, EGraphInfraFormat eFormat) override;
  virtual void            Texture_UpdateBuffer            (HTextureBuffer hTex, const void* pBin, size_t SrcRowPitch, size_t SrcDepthPitch) override;
  virtual void            Texture_DestroyBuffer           (HTextureBuffer& hTex) override;
  virtual HTextureBuffer  Texture_GetWhite                () override { return m_hWhiteTextureBuffer; }
  virtual HTextureBuffer  Texture_GetBlack                () override { return m_hBlackTextureBuffer; }
  virtual HTextureBuffer  Texture_GetGrayGradation        () override { return m_hGrayGradationTextureBuffer; }
  virtual void            Cmd_Texture_TransState          (HTextureBuffer hTex, EResourceState eBefore, EResourceState eAfter) override;

  // 深度バッファ.
  virtual void Cmd_DepthStencil_Clear(HDescHeap hDesc, uint16_t pos) override;

  // バックバッファ.
  virtual void Cmd_BackBuffer_Clear(float r, float g, float b, float a) override;
  virtual void Cmd_BackBuffer_Clear(const std::array<float,4>& color) override;
  virtual void Cmd_BackBuffer_ChangeStateFromPresentToRenderTarget() override;
  virtual void Cmd_BackBuffer_ChangeStateFromRenderTargetToPresent() override;
  virtual void Cmd_BackBuffer_SetToRenderTarget(HDescHeap hDescDsv, uint16_t pos) override;

  // レンダーターゲット.
  virtual void Cmd_RenderTarget_Set   (HDescHeap hDescRtv, uint16_t posRtv, HDescHeap hDescDsv, uint16_t posDsv) override;
  virtual void Cmd_RenderTarget_Clear (HDescHeap hDesc, const std::array<float,4>& color) override;

  // シェーダ.
  virtual HShader Shader_CreateByCompileFile  (LPCWSTR pFilename, LPCSTR pEntrypoint, LPCSTR pTarget) override;
  virtual void    Shader_Destroy              (HShader& hShader) override;

  // 描画設定.
  virtual void Cmd_Set_PrimitiveTopology  (EPrimitiveTopology eTopology) override;
  virtual void Cmd_Set_Viewports          (const SViewPort& vp) override;
  virtual void Cmd_Set_ScissorRects       (const SRect& r) override;

  // 描画.
  virtual void Cmd_Draw_Instanced         (UINT VertexCountPerInstance, UINT InstanceCount, UINT StartVertexLocation, INT StartInstanceLocation) override;
  virtual void Cmd_Draw_IndexedInstanced  (UINT IndexCountPerInstance, UINT InstanceCount, UINT StartIndexLocation, INT BaseVertexLocation, UINT StartInstanceLocation) override;

  // スワップチェイン.
  virtual void SwapChain_Present(bool bEnableVSync) override;

  // コマンド.
  virtual void CloseCommand() override;
  virtual void ExecuteCommand() override;
  virtual void WaitCommandFinished() override;
  virtual void ClearCommand() override;

  // デバッグフォント.
  virtual void DebugFont_Begin(const SViewPort& vp) override;
  virtual void DebugFont_DrawString(const char* str, const DirectX::XMFLOAT2& position, const DirectX::XMFLOAT4& color, float size) override;
  virtual void DebugFont_End() override;

  // その他.
  virtual void ClearGraphicMemory() override;

  ID3D12GraphicsCommandList* GetCommandList() { return m_cmdList; }
  ID3D12CommandQueue* GetCommandQueue() { return m_cmdQueue; }
  ID3D12Device* GetDevice() { return m_device; }
  IDXGIFactory6* GetGIFactory() { return m_dxgiFactory; }

private:
  void EnableDebugLayer();
  static UINT64 GenNextFenceValue(UINT64 current);
  static void WaitQueueFinished(ID3D12CommandQueue* queue, SFence& fence);

  void CreateWhiteTextureBuffer();
  void CreateBlackTextureBuffer();
  void CreateGrayGradationTextureBuffer();

private:

  IDXGIFactory6*  m_dxgiFactory;
  ID3D12Device*   m_device;

  // グラフィックコマンド関連.
  ID3D12CommandAllocator*     m_cmdAllocator;
  ID3D12GraphicsCommandList*  m_cmdList;
  ID3D12CommandQueue*         m_cmdQueue;

  // フェンス.
  SFence m_fence;

  // パイプライン.
  ID3D12PipelineState* m_pipeline_state;

  // ルートシグネチャ.
  ID3D12RootSignature* m_rootsignature;

  // スワップチェイン.
  IDXGISwapChain4*            m_pSwapChain;
  std::vector<HTextureBuffer> m_hBackBuffers;
  HDescHeap                   m_hBackBufferDescHeap;

  // テクスチャ.
  HTextureBuffer  m_hWhiteTextureBuffer;
  HTextureBuffer  m_hBlackTextureBuffer;
  HTextureBuffer  m_hGrayGradationTextureBuffer;

  // ルートシグネチャ.
  THandleContainer<SRootSignature>  m_root_sig_management;

  // パイプラインステート.
  THandleContainer<SPipelineState>  m_pipe_state_management;

  // 頂点管理.
  THandleContainer<SVertexBuffer>   m_vertex_management;

  // インデックス管理.
  THandleContainer<SIndexBuffer>    m_index_management;

  // ディスクリプタヒープ管理.
  THandleContainer<SDescHeap>       m_desc_heap_management;

  // 定数管理.
  THandleContainer<SConst>          m_const_management;

  // テクスチャ管理.
  THandleContainer<STexture_>       m_texture_management;

  // シェーダ管理.
  THandleContainer<SShader>         m_shader_management;

  // フォント.
  HDescHeap                 m_hDescHeapForFont;
  DirectX::GraphicsMemory*  m_pGraphicMemory;
  DirectX::SpriteFont*      m_pSpriteFont;
  DirectX::SpriteBatch*     m_pSpriteBatch;

  // imgui.
  ghl::HDescHeap      m_hDescHeapForImgui;
  ImGuiContext*       m_pImguiContext;
};

}