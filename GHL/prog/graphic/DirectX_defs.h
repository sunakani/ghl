#pragma once

#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXTex.h>
#include <d3dx12.h>

#include "GHL.h"

namespace ghl {

class CDirectX;

/***********************************************/
/*  DXクラス内で使用するもの.
/***********************************************/
// DirectXをオーナーとするコンポネントクラスの基底.
class CDirectXComponent
{
public:
  CDirectXComponent(CDirectX* pOwner)
    : m_pOwner(pOwner)
  {}
  CDirectX* OwnerPtr() { return m_pOwner; }
  const CDirectX* OwnerPtr() const { return m_pOwner; }
  CDirectX& Owner() { return *m_pOwner; }
  const CDirectX& Owner() const { return *m_pOwner; }
private:
  CDirectX* const m_pOwner;
};

/***********************************************/
/*  ハンドル.
/***********************************************/
// ハンドルオブジェクト基底クラス.
template<EHandleType type>
struct SHandleBase {
  THandle<type> handle;
};
// 複数のハンドルオブジェクトをまとめて扱うコンテナ.
template<typename T>
class THandleContainer
{
public:
  THandleContainer()
    : m_contents()
    , m_descs()
  {}
  void Init(uint32_t nReserve)
  {
    m_contents.reserve(nReserve);
    m_descs.reserve(nReserve);
  }
  void Term()
  {
    m_contents.clear();
    m_contents.shrink_to_fit();
    m_descs.clear();
    m_descs.shrink_to_fit();
  }
  template<typename F>
  void IterateAllContents(F fun) {
    for (int i=0; i<m_contents.size(); ++i) {
      fun(m_contents[i], m_descs[i].bValid);
    }
  }
  T* New() {
    for (int i=0; i<m_descs.size(); ++i) {
      if (m_descs[i].bValid == false) {
        m_descs[i].bValid = true;
        m_contents[i].handle._Set(i);
        return &m_contents[i];
      }
    }
    auto back_index = static_cast<uint32_t>(m_descs.size());
    m_descs.push_back({true});
    m_contents.push_back(T());
    auto& back = m_contents.back();
    back.handle._Set(back_index);
    return &back;
  }
  void Delete(T* pTarget) {
    if (pTarget != nullptr) {
      auto index = pTarget->handle._Get();
      if (index >= 0 && index < m_descs.size()) {
        m_descs[index].bValid = false;
        m_contents[index].handle.Invalidate();
      }
    }
  }
  template<typename H>
  void Delete(H handle) {
    if (handle) {
      if (auto* pTarget = Get(handle._Get())) {
        Delete(pTarget);
      }
    }
  }
  T* Get(uint32_t index) {
    if (index >= 0 && index < m_descs.size()) {
      return &m_contents[index];
    }
    return nullptr;
  }
  template<typename H>
  T* Get(H h) {
    if (h) {
      return Get(h._Get());
    }
    return nullptr;
  }
private:
  struct SDescription {
    bool bValid{false};
  };
  std::vector<T>            m_contents;
  std::vector<SDescription> m_descs;
};

/***********************************************/
/*  ルートシグネチャ.
/***********************************************/
struct SRootSignature : public SHandleBase<EHandleType::eRootSignatur>
{
  ID3D12RootSignature*  pRootSignature{};
  void Init() {
    pRootSignature = nullptr;
  }
  void Term() {
    SafeRelease(pRootSignature);
  }
};

/***********************************************/
/*  パイプラインステート.
/***********************************************/
struct SPipelineState : public SHandleBase<EHandleType::ePipelineState>
{
  ID3D12PipelineState*  pPipelineState{};
  void Init() {
    pPipelineState = nullptr;
  }
  void Term() {
    SafeRelease(pPipelineState);
  }
};

/***********************************************/
/*  頂点管理.
/***********************************************/
// 頂点ビュー.
struct SVertexView : public SHandleBase<EHandleType::eVertexView>
{
  D3D12_VERTEX_BUFFER_VIEW view{};
  void Init() {
    view.BufferLocation = 0;
    view.SizeInBytes = 0;
    view.StrideInBytes = 0;
  }
  void Term() {
    Init();
  }
};
// 頂点バッファ.
struct SVertexBuffer : public SHandleBase<EHandleType::eVertexBuffer>
{
  ID3D12Resource*               pBuffer{nullptr};
  THandleContainer<SVertexView> views{};
  void Init()
  {
    pBuffer = nullptr;
    views.Init(0);
  }
  void Term()
  {
    SafeRelease(pBuffer);
    views.Term();
  }
};

/***********************************************/
/*  インデックス管理.
/***********************************************/
// インデックスビュー.
struct SIndexView : public SHandleBase<EHandleType::eIndexView>
{
  D3D12_INDEX_BUFFER_VIEW view{};
  void Init() {
    view.BufferLocation = 0;
    view.SizeInBytes = 0;
    view.Format = DXGI_FORMAT_UNKNOWN;
  }
  void Term() {
    Init();
  }
};
// インデックスバッファ.
struct SIndexBuffer : public SHandleBase<EHandleType::eIndexBuffer>
{
  ID3D12Resource*               pBuffer{nullptr};
  THandleContainer<SIndexView>  views{};
  void Init()
  {
    pBuffer = nullptr;
    views.Init(0);
  }
  void Term()
  {
    SafeRelease(pBuffer);
    views.Term();
  }
};

/***********************************************/
/*  ディスクリプタヒープ管理.
/***********************************************/
struct SDescHeap : public SHandleBase<EHandleType::eDescHeap>
{
  ID3D12DescriptorHeap*         pHeap{nullptr};
  D3D12_DESCRIPTOR_HEAP_TYPE    type{D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV};
  void Init()
  {
    pHeap = nullptr;
    type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
  }
  void Term()
  {
    SafeRelease(pHeap);
    type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
  }
};

/***********************************************/
/*  定数管理.
/***********************************************/
struct SConst : public SHandleBase<EHandleType::eConstBuffer>
{
  ID3D12Resource*               pBuffer{nullptr};
  void Init()
  {
    pBuffer = nullptr;
  }
  void Term()
  {
    SafeRelease(pBuffer);
  }
};

/***********************************************/
/*  テクスチャ管理.
/***********************************************/
struct STexture_ : public SHandleBase<EHandleType::eTextureBuffer>
{
  ID3D12Resource*               pBuffer{nullptr};
  void Init()
  {
    pBuffer = nullptr;
  }
  void Term()
  {
    SafeRelease(pBuffer);
  }
};

/***********************************************/
/*  シェーダ管理.
/***********************************************/
struct SShader : public SHandleBase<EHandleType::eShader>
{
  ID3DBlob*                     pBlob{nullptr};
  void Init()
  {
    pBlob = nullptr;
  }
  void Term()
  {
    SafeRelease(pBlob);
  }
};

// Utility.
extern D3D12_DESCRIPTOR_RANGE_TYPE    ToDxType(EDescriptorType type);
extern D3D12_DESCRIPTOR_HEAP_TYPE     ToDxType(EDescriptorHeapType type);
extern D3D12_TEXTURE_ADDRESS_MODE     ToDxType(ETextureAddressMode type);
extern DXGI_FORMAT                    ToDxType(EGraphInfraFormat type);
extern D3D_PRIMITIVE_TOPOLOGY         ToDxType(EPrimitiveTopology type);
extern D3D12_HEAP_TYPE                ToDxType(EResourceHeapType type);
extern D3D12_CPU_PAGE_PROPERTY        ToDxType(EResourceCpuPageProp type);
extern D3D12_MEMORY_POOL              ToDxType(EResourceMemoryPool type);
extern D3D12_RESOURCE_STATES          ToDxType(EResourceState type);
extern D3D12_FILTER                   ToDxType(EFilter type);
extern D3D12_COMPARISON_FUNC          ToDxType(EComparisonFunc type);
extern D3D12_PRIMITIVE_TOPOLOGY_TYPE  ToDxType(EPrimitiveTopologyType type);
extern D3D12_CULL_MODE                ToDxType(ECullMode type);
extern D3D12_FILL_MODE                ToDxType(EFillMode type);

}//namespace ghl.
