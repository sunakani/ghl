#include "DirectX.h"

#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "DirectXTex.lib")
#pragma comment(lib, "DirectXTK12.lib")

namespace ghl {

// CDirectX.
CDirectX::CDirectX()
  : m_dxgiFactory(nullptr)
  , m_device(nullptr)
  , m_cmdAllocator(nullptr)
  , m_cmdList(nullptr)
  , m_cmdQueue(nullptr)
  , m_fence()
  , m_pipeline_state(nullptr)
  , m_rootsignature(nullptr)
  , m_pSwapChain(nullptr)
  , m_hBackBuffers()
  , m_hBackBufferDescHeap()
  , m_hWhiteTextureBuffer()
  , m_hBlackTextureBuffer()
  , m_hGrayGradationTextureBuffer()
  , m_root_sig_management()
  , m_vertex_management()
  , m_index_management()
  , m_desc_heap_management()
  , m_const_management()
  , m_texture_management()
  , m_shader_management()
  , m_hDescHeapForFont()
  , m_pGraphicMemory(nullptr)
  , m_pSpriteFont(nullptr)
  , m_pSpriteBatch(nullptr)
  , m_hDescHeapForImgui()
{}

bool CDirectX::Init(const SInitParam& param)
{
  m_fence.Init();

  m_root_sig_management   .Init(16);
  m_pipe_state_management .Init(16);
  m_vertex_management     .Init(256);
  m_index_management      .Init(256);
  m_desc_heap_management  .Init(256);
  m_const_management      .Init(256);
  m_texture_management    .Init(256);
  m_shader_management     .Init(128);

#if defined(_DEBUG)
  EnableDebugLayer();
#endif

  //DirectX12まわり初期化
  HRESULT result = S_OK;
#if defined(_DEBUG)
  if (FAILED(CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS(&m_dxgiFactory)))) {
    if (FAILED(CreateDXGIFactory2(0, IID_PPV_ARGS(&m_dxgiFactory)))) {
      return false;
    }
  }
#else
  if (FAILED(CreateDXGIFactory2(0, IID_PPV_ARGS(&m_dxgiFactory)))) {
    return false;
  }
#endif

  // アダプタを列挙する.
  enum { eMaxAdapterNum = 10 };
  struct SAdapterInfo {
    IDXGIAdapter1* pRef{nullptr};
    bool bNvidia{false};
  };
  std::array<SAdapterInfo, eMaxAdapterNum> adapters = {};
  {
    for (int i = 0; i < eMaxAdapterNum; ++i) {
      IDXGIAdapter1* tmpAdapter = nullptr;
      if (m_dxgiFactory->EnumAdapters1(i, &tmpAdapter) != DXGI_ERROR_NOT_FOUND) {
        DXGI_ADAPTER_DESC1 desc = {};
        tmpAdapter->GetDesc1(&desc);
        if ((desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) == 0) {
          bool bNvidia = false;
          if (std::wstring(desc.Description).find(L"NVIDIA") != std::string::npos) {
            bNvidia = true;
          }
          adapters.at(i) = { tmpAdapter, bNvidia };
        }
        else {
          SafeRelease(tmpAdapter);
        }
      }
    }
  }

  // 今はNVIDIAのものを優先的に使う.
  // 将来的にはより適切な選択ルールにするか、選択式にする.
  IDXGIAdapter* pTargetAdapter = nullptr;
  for (auto& elem : adapters) {
    if (elem.pRef != nullptr) {
      if (elem.bNvidia) {
        pTargetAdapter = elem.pRef;
        break;
      }
    }
  }
  if (pTargetAdapter == nullptr) {
    for (auto& elem : adapters) {
      if (elem.pRef != nullptr) {
        pTargetAdapter = elem.pRef;
        break;
      }
    }
  }

  //Direct3Dデバイスの初期化
  //フィーチャレベル列挙
  D3D_FEATURE_LEVEL levels[] = {
    D3D_FEATURE_LEVEL_12_1,
    D3D_FEATURE_LEVEL_12_0,
    D3D_FEATURE_LEVEL_11_1,
    D3D_FEATURE_LEVEL_11_0,
  };
  D3D_FEATURE_LEVEL featureLevel;
  for (auto l : levels) {
    if (D3D12CreateDevice(pTargetAdapter, l, IID_PPV_ARGS(&m_device)) == S_OK) {
      featureLevel = l;
      break;
    }
  }
  for (auto& elem : adapters) {
    SafeRelease(elem.pRef);
  }

  // コマンド関連初期化.
  const D3D12_COMMAND_LIST_TYPE cmdListType = D3D12_COMMAND_LIST_TYPE_DIRECT;
  result = m_device->CreateCommandAllocator(cmdListType, IID_PPV_ARGS(&m_cmdAllocator));
  _DEBUG_BREAK(SUCCEEDED(result));
  result = m_device->CreateCommandList(0, cmdListType, m_cmdAllocator, nullptr, IID_PPV_ARGS(&m_cmdList));
  //_cmdList->Close();
  _DEBUG_BREAK(SUCCEEDED(result));

  D3D12_COMMAND_QUEUE_DESC cmdQueueDesc = {};
  cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
  cmdQueueDesc.NodeMask = 0;
  cmdQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
  cmdQueueDesc.Type = cmdListType;
  result = m_device->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&m_cmdQueue));
  _DEBUG_BREAK(SUCCEEDED(result));

  // スワップチェイン.
  {
    // バッファ.
    DXGI_SWAP_CHAIN_DESC1 swapchainDesc = {};
    swapchainDesc.Width               = param.width;
    swapchainDesc.Height              = param.height;
    swapchainDesc.Format              = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapchainDesc.Stereo              = false;
    swapchainDesc.SampleDesc.Count    = 1;
    swapchainDesc.SampleDesc.Quality  = 0;
    swapchainDesc.BufferUsage         = DXGI_USAGE_BACK_BUFFER;
    swapchainDesc.BufferCount         = param.unBackBufferNum;
    swapchainDesc.Scaling             = DXGI_SCALING_STRETCH;
    swapchainDesc.SwapEffect          = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    swapchainDesc.AlphaMode           = DXGI_ALPHA_MODE_UNSPECIFIED;
    swapchainDesc.Flags               = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    auto result = m_dxgiFactory->CreateSwapChainForHwnd(
      m_cmdQueue, param.hWnd, &swapchainDesc, nullptr, nullptr,
      reinterpret_cast<IDXGISwapChain1**>(&m_pSwapChain)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    DXGI_SWAP_CHAIN_DESC swcDesc = {};
    result = m_pSwapChain->GetDesc(&swcDesc);
    _DEBUG_BREAK(SUCCEEDED(result));
    m_hBackBuffers.resize(swcDesc.BufferCount);
    for (UINT ui=0; ui<swcDesc.BufferCount; ++ui) {
      ID3D12Resource* pResource = nullptr;
      result = m_pSwapChain->GetBuffer(ui, IID_PPV_ARGS(&pResource));
      _DEBUG_BREAK(SUCCEEDED(result));
      if (auto* pNew = m_texture_management.New()) {
        pNew->Init();
        pNew->pBuffer = pResource;
        m_hBackBuffers[ui] = pNew->handle;
      }
    }
    // ディスクリプタヒープ.
    m_hBackBufferDescHeap = DescHeap_Create(EDescriptorHeapType::eRTV, swcDesc.BufferCount, false);
    // ビュー.
    if (auto* pDescHeap = m_desc_heap_management.Get(m_hBackBufferDescHeap)) {
      if (pDescHeap->pHeap != nullptr) {
        for (UINT ui=0; ui<swcDesc.BufferCount; ++ui) {
          if (auto* pBuffer = m_texture_management.Get(m_hBackBuffers[ui])) {
            if (pBuffer->pBuffer != nullptr) {
              D3D12_CPU_DESCRIPTOR_HANDLE handle = pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
              handle.ptr += (m_device->GetDescriptorHandleIncrementSize(pDescHeap->type) * ui);
              m_device->CreateRenderTargetView(pBuffer->pBuffer, nullptr, handle);
            }
          }
        }
      }
    }
  }

  // フェンス作成.
  result = m_device->CreateFence(m_fence.value, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence.object));

  // テクスチャ.
  CreateWhiteTextureBuffer();
  CreateBlackTextureBuffer();
  CreateGrayGradationTextureBuffer();

  // フォント.
  m_pGraphicMemory = new DirectX::GraphicsMemory(m_device);
  DirectX::ResourceUploadBatch resUploadBatch(m_device);
  resUploadBatch.Begin();
  DirectX::RenderTargetState rtState(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_FORMAT_D32_FLOAT);
	DirectX::SpriteBatchPipelineStateDescription pd(rtState);
	m_pSpriteBatch = new DirectX::SpriteBatch(m_device, resUploadBatch, pd);
  m_hDescHeapForFont = DescHeap_Create(EDescriptorHeapType::eCBV_SRV_UAV, 1, true);
  if (auto* pDescHeap = m_desc_heap_management.Get(m_hDescHeapForFont)) {
    if (pDescHeap->pHeap != nullptr) {
	    m_pSpriteFont = new DirectX::SpriteFont(
        m_device,
		    resUploadBatch,
		    L"Data/Font/bizud.spritefont",
		    pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart(),
		    pDescHeap->pHeap->GetGPUDescriptorHandleForHeapStart()
      );
    }
  }
	auto future = resUploadBatch.End(m_cmdQueue);
  WaitCommandFinished();
	future.wait();

  return true;
}

void CDirectX::Term()
{
  WaitCommandFinished();

  SafeDelete(m_pSpriteFont);
  if (m_hDescHeapForFont) {
    DescHeap_Destroy(m_hDescHeapForFont);
  }
  SafeDelete(m_pSpriteBatch);
  SafeDelete(m_pGraphicMemory);

  Texture_DestroyBuffer(m_hWhiteTextureBuffer);
  Texture_DestroyBuffer(m_hBlackTextureBuffer);
  Texture_DestroyBuffer(m_hGrayGradationTextureBuffer);

  for (auto& elem : m_hBackBuffers) {
    Texture_DestroyBuffer(elem);
  }
  m_hBackBuffers.clear();
  m_hBackBuffers.shrink_to_fit();
  DescHeap_Destroy(m_hBackBufferDescHeap);
  SafeRelease(m_pSwapChain);

  m_root_sig_management.Term();
  m_pipe_state_management.Term();
  m_vertex_management.Term();
  m_index_management.Term();
  m_desc_heap_management.Term();
  m_const_management.Term();
  m_texture_management.Term();
  m_shader_management.Term();

  SafeRelease(m_rootsignature);
  SafeRelease(m_pipeline_state);

  SafeRelease(m_fence.object);
  m_fence.Init();
  SafeRelease(m_cmdQueue);
  SafeRelease(m_cmdList);
  SafeRelease(m_cmdAllocator);
  SafeRelease(m_dxgiFactory);

#if 0
  if (m_device != nullptr) {
    ID3D12DebugDevice* debugInterface;
    if (SUCCEEDED(m_device->QueryInterface(&debugInterface)))
    {
      debugInterface->ReportLiveDeviceObjects(D3D12_RLDO_DETAIL | D3D12_RLDO_IGNORE_INTERNAL);
      debugInterface->Release();
    }
  }
#endif
  SafeRelease(m_device);
}

bool CDirectX::ImGuiInit(HWND hWnd)
{
  m_hDescHeapForImgui = DescHeap_Create(ghl::EDescriptorHeapType::eCBV_SRV_UAV, 1, true);
  m_pImguiContext = ImGui::CreateContext();
  bool bResult = false;
  bResult = ImGui_ImplWin32_Init(hWnd);
  _DEBUG_BREAK(bResult);
  bResult = false;
  if (auto* pDescHeap = m_desc_heap_management.Get(m_hDescHeapForImgui)) {
    if (pDescHeap->pHeap != nullptr) {
      bResult = ImGui_ImplDX12_Init(
        m_device,
        3,
        DXGI_FORMAT_R8G8B8A8_UNORM,
        pDescHeap->pHeap,
        pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart(),
        pDescHeap->pHeap->GetGPUDescriptorHandleForHeapStart()
      );
    }
  }
  _DEBUG_BREAK(bResult);
  return true;
}
void CDirectX::ImGuiTerm()
{
  ImGui_ImplDX12_Shutdown();
  ImGui_ImplWin32_Shutdown();
  if (m_pImguiContext != nullptr) {
    ImGui::DestroyContext(m_pImguiContext);
    m_pImguiContext = nullptr;
  }
  if (m_hDescHeapForImgui) {
    DescHeap_Destroy(m_hDescHeapForImgui);
  }
}
void CDirectX::ImGuiNewFrame() {
  ImGui_ImplDX12_NewFrame();
  ImGui_ImplWin32_NewFrame();
  ImGui::NewFrame();
}
void CDirectX::ImGuiRender()
{
  ImGui::Render();
  if (auto* pDescHeap = m_desc_heap_management.Get(m_hDescHeapForImgui)) {
    if (pDescHeap->pHeap != nullptr) {
      m_cmdList->SetDescriptorHeaps(1, &pDescHeap->pHeap);
      ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), m_cmdList);
    }
  }
}

void CDirectX::EnableDebugLayer()
{
  ID3D12Debug* debugLayer = nullptr;
  if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugLayer)))) {
    if (debugLayer != nullptr) {
      debugLayer->EnableDebugLayer();
      debugLayer->Release();
    }
  }
}

// static.
UINT64 CDirectX::GenNextFenceValue(UINT64 current)
{
  auto next = current + 1;
  if (next == std::numeric_limits<UINT64>::max()) {
    next = 0;
  }
  return next;
}

// static.
void CDirectX::WaitQueueFinished(ID3D12CommandQueue* queue, SFence& fence)
{
  if (queue != nullptr) {
    fence.value = GenNextFenceValue(fence.value);
    queue->Signal(fence.object, fence.value);
    if (fence.object->GetCompletedValue() != fence.value) {
      if (auto event = CreateEvent(nullptr, false, false, nullptr)) {
        fence.object->SetEventOnCompletion(fence.value, event);
        WaitForSingleObject(event, INFINITE);
        CloseHandle(event);
      }
    }
  }
}

HShader CDirectX::Shader_CreateByCompileFile(LPCWSTR pFilename, LPCSTR pEntrypoint, LPCSTR pTarget)
{
  if (auto* pNew = m_shader_management.New()) {
    pNew->Init();
    ID3DBlob* pErrorBlob = nullptr;
    auto result = D3DCompileFromFile(
      pFilename,
      nullptr,
      D3D_COMPILE_STANDARD_FILE_INCLUDE,
      pEntrypoint,
      pTarget,
      D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION,  // ※リリースの時は最適化をする. そもそも実行時コンパイルをやめる.
      0,
      &pNew->pBlob,
      &pErrorBlob
    );
    if (pErrorBlob != nullptr) {
			const char* pContents = reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer());
      DebugPrint(pContents);
    }
    _DEBUG_BREAK(SUCCEEDED(result));
    SafeRelease(pErrorBlob);
    if (SUCCEEDED(result) && pNew->pBlob != nullptr) {
      return pNew->handle;
    }
    Shader_Destroy(pNew->handle);
  }
  return HShader();
}
void CDirectX::Shader_Destroy(HShader& hShader)
{
  if (auto* pTarget = m_shader_management.Get(hShader)) {
    pTarget->Term();
    m_shader_management.Delete(pTarget);
    hShader.Invalidate();
  }
}

void CDirectX::Cmd_Draw_Instanced(UINT VertexCountPerInstance, UINT InstanceCount, UINT StartVertexLocation, INT StartInstanceLocation)
{
  if (m_cmdList != nullptr) {
    m_cmdList->DrawInstanced(VertexCountPerInstance, InstanceCount, StartVertexLocation, StartInstanceLocation);
  }
}

void CDirectX::Cmd_Draw_IndexedInstanced(UINT IndexCountPerInstance, UINT InstanceCount, UINT StartIndexLocation,  INT BaseVertexLocation, UINT StartInstanceLocation)
{
  if (m_cmdList != nullptr) {
    m_cmdList->DrawIndexedInstanced(IndexCountPerInstance, InstanceCount, StartIndexLocation, BaseVertexLocation, StartInstanceLocation);
  }
}

void CDirectX::Cmd_Set_PrimitiveTopology(EPrimitiveTopology eTopology)
{
  if (m_cmdList != nullptr) {
    auto topology = ToDxType(eTopology);
    m_cmdList->IASetPrimitiveTopology(topology);
  }
}

void CDirectX::CloseCommand()
{
  if (m_cmdList != nullptr) {
    m_cmdList->Close();
  }
}

void CDirectX::ExecuteCommand()
{
  ID3D12CommandList* cmdlists[] = {
    m_cmdList
  };
  if (m_cmdQueue != nullptr) {
    m_cmdQueue->ExecuteCommandLists(static_cast<UINT>(std::size(cmdlists)), cmdlists);
  }
}

void CDirectX::WaitCommandFinished()
{
  WaitQueueFinished(m_cmdQueue, m_fence);
}

void CDirectX::ClearCommand()
{
  if (m_cmdAllocator != nullptr) {
    m_cmdAllocator->Reset();
  }
  if (m_cmdList != nullptr) {
    m_cmdList->Reset(m_cmdAllocator, nullptr);
  }
}

void CDirectX::Cmd_DepthStencil_Clear(HDescHeap hDesc, uint16_t pos)
{
  if (m_cmdList != nullptr) {
    if (hDesc) {
      if (auto* pDescHeap = m_desc_heap_management.Get(hDesc)) {
        if (pDescHeap->pHeap != nullptr) {
          auto dsvH = pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
          dsvH.ptr += static_cast<ULONG_PTR>(pos * m_device->GetDescriptorHandleIncrementSize(pDescHeap->type));
          m_cmdList->ClearDepthStencilView(dsvH, D3D12_CLEAR_FLAG_DEPTH, 1.f, 0, 0, nullptr);
        }
      }
    }
  }
}

void CDirectX::Cmd_BackBuffer_Clear(float r, float g, float b, float a)
{
  if (m_cmdList != nullptr && m_pSwapChain != nullptr) {
    if (auto* pDescHeap = m_desc_heap_management.Get(m_hBackBufferDescHeap)) {
      if (pDescHeap->pHeap != nullptr) {
        auto rtvH = pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
        auto idx = m_pSwapChain->GetCurrentBackBufferIndex();
        rtvH.ptr += static_cast<ULONG_PTR>(idx * m_device->GetDescriptorHandleIncrementSize(pDescHeap->type));
        float color[4] = { r, g, b, a };
        m_cmdList->ClearRenderTargetView(rtvH, color, 0, nullptr);
      }
    }
  }
}
void CDirectX::Cmd_BackBuffer_Clear(const std::array<float,4>& color)
{
  Cmd_BackBuffer_Clear(color[0], color[1], color[2], color[3]);
}
void CDirectX::Cmd_BackBuffer_ChangeStateFromPresentToRenderTarget()
{
  if (m_cmdList != nullptr) {
    if (m_pSwapChain != nullptr) {
      auto bbIdx = m_pSwapChain->GetCurrentBackBufferIndex();
      if (auto* pTarget = m_texture_management.Get(m_hBackBuffers[bbIdx])) {
        D3D12_RESOURCE_BARRIER BarrierDesc = {};
        BarrierDesc.Type                    = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
        BarrierDesc.Flags                    = D3D12_RESOURCE_BARRIER_FLAG_NONE;
        BarrierDesc.Transition.pResource    = pTarget->pBuffer;
        BarrierDesc.Transition.Subresource  = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
        BarrierDesc.Transition.StateBefore  = D3D12_RESOURCE_STATE_PRESENT;
        BarrierDesc.Transition.StateAfter    = D3D12_RESOURCE_STATE_RENDER_TARGET;
        m_cmdList->ResourceBarrier(1, &BarrierDesc);
      }
    }
  }
}
void CDirectX::Cmd_BackBuffer_ChangeStateFromRenderTargetToPresent()
{
  if (m_cmdList != nullptr) {
    if (m_pSwapChain != nullptr) {
      auto bbIdx = m_pSwapChain->GetCurrentBackBufferIndex();
      if (auto* pTarget = m_texture_management.Get(m_hBackBuffers[bbIdx])) {
        D3D12_RESOURCE_BARRIER BarrierDesc = {};
        BarrierDesc.Type                    = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
        BarrierDesc.Flags                    = D3D12_RESOURCE_BARRIER_FLAG_NONE;
        BarrierDesc.Transition.pResource    = pTarget->pBuffer;
        BarrierDesc.Transition.Subresource  = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
        BarrierDesc.Transition.StateBefore  = D3D12_RESOURCE_STATE_RENDER_TARGET;
        BarrierDesc.Transition.StateAfter    = D3D12_RESOURCE_STATE_PRESENT;
        m_cmdList->ResourceBarrier(1, &BarrierDesc);
      }
    }
  }
}
void CDirectX::Cmd_BackBuffer_SetToRenderTarget(HDescHeap hDescDsv, uint16_t pos)
{
  if (m_cmdList != nullptr && m_pSwapChain != nullptr) {
    if (auto* pDescHeapRtv = m_desc_heap_management.Get(m_hBackBufferDescHeap)) {
      if (pDescHeapRtv->pHeap != nullptr) {
        auto rtvH = pDescHeapRtv->pHeap->GetCPUDescriptorHandleForHeapStart();
        auto idx = m_pSwapChain->GetCurrentBackBufferIndex();
        rtvH.ptr += static_cast<ULONG_PTR>(idx * m_device->GetDescriptorHandleIncrementSize(pDescHeapRtv->type));
        bool bApply = false;
        if (hDescDsv) {
          if (auto* pDescHeapDsv = m_desc_heap_management.Get(hDescDsv)) {
            if (pDescHeapDsv->pHeap != nullptr) {
              auto dsvH = pDescHeapDsv->pHeap->GetCPUDescriptorHandleForHeapStart();
              dsvH.ptr += static_cast<ULONG_PTR>(pos * m_device->GetDescriptorHandleIncrementSize(pDescHeapDsv->type));
              m_cmdList->OMSetRenderTargets(1, &rtvH, false, &dsvH);
              bApply = true;
            }
          }
        }
        if (bApply == false) {
          m_cmdList->OMSetRenderTargets(1, &rtvH, false, nullptr);
        }
      }
    }
  }
}

// レンダーターゲット.
void CDirectX::Cmd_RenderTarget_Set(HDescHeap hDescRtv, uint16_t posRtv, HDescHeap hDescDsv, uint16_t posDsv)
{
  if (m_cmdList != nullptr) {
    D3D12_CPU_DESCRIPTOR_HANDLE rtv = {};
    D3D12_CPU_DESCRIPTOR_HANDLE dsv = {};
    if (hDescRtv) {
      if (auto* pDescHeap = m_desc_heap_management.Get(hDescRtv)) {
        if (pDescHeap->pHeap != nullptr) {
          rtv = pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
          rtv.ptr += static_cast<ULONG_PTR>(posRtv * m_device->GetDescriptorHandleIncrementSize(pDescHeap->type));
        }
      }
    }
    if (hDescDsv) {
      if (auto* pDescHeap = m_desc_heap_management.Get(hDescDsv)) {
        if (pDescHeap->pHeap != nullptr) {
          dsv = pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
          dsv.ptr += static_cast<ULONG_PTR>(posDsv * m_device->GetDescriptorHandleIncrementSize(pDescHeap->type));
        }
      }
    }
    if (rtv.ptr == 0) {
      m_cmdList->OMSetRenderTargets(0, nullptr, false, dsv.ptr != 0 ? &dsv : nullptr);
    }
    else {
      m_cmdList->OMSetRenderTargets(1, &rtv, false, dsv.ptr != 0 ? &dsv : nullptr);
    }
  }
}
void CDirectX::Cmd_RenderTarget_Clear(HDescHeap hDesc, const std::array<float,4>& color)
{
  if (m_cmdList != nullptr) {
    if (auto* pDescHeap = m_desc_heap_management.Get(hDesc)) {
      if (pDescHeap->pHeap != nullptr) {
        auto rtvH = pDescHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
        float set_color[4] = { color.at(0), color.at(1), color.at(2), color.at(3) };
        m_cmdList->ClearRenderTargetView(rtvH, set_color, 0, nullptr);
      }
    }
  }
}

void CDirectX::Cmd_Set_Viewports(const SViewPort& vp)
{
  if (m_cmdList != nullptr) {
    D3D12_VIEWPORT viewport = {vp.TopLeftX, vp.TopLeftY, vp.Width, vp.Height, vp.MinDepth, vp.MaxDepth};
    m_cmdList->RSSetViewports(1, &viewport);
  }
}

void CDirectX::Cmd_Set_ScissorRects(const SRect& r)
{
  if (m_cmdList != nullptr) {
    D3D12_RECT rect = {static_cast<LONG>(r.left), static_cast<LONG>(r.top), static_cast<LONG>(r.right), static_cast<LONG>(r.bottom)};
    m_cmdList->RSSetScissorRects(1, &rect);
  }
}

void CDirectX::SwapChain_Present(bool bEnableVSync)
{
  if (m_pSwapChain != nullptr) {
    m_pSwapChain->Present(bEnableVSync ? 1 : 0, 0);
  }
}

void CDirectX::CreateWhiteTextureBuffer()
{
  enum {
    eTextureWidth    = 4,
    eTextureHeight  = 4,
  };
  m_hWhiteTextureBuffer = Texture_CreateBuffer(eTextureWidth, eTextureHeight);
  std::array<unsigned int, eTextureWidth * eTextureHeight> data;
  data.fill(0xFFFFFFFF);
  Texture_UpdateBuffer(m_hWhiteTextureBuffer, data.data(), sizeof(unsigned int) * eTextureWidth, sizeof(data));
}

void CDirectX::CreateBlackTextureBuffer()
{
  enum {
    eTextureWidth    = 4,
    eTextureHeight  = 4,
  };
  m_hBlackTextureBuffer = Texture_CreateBuffer(eTextureWidth, eTextureHeight);
  std::array<unsigned int, eTextureWidth * eTextureHeight> data;
  data.fill(0x00000000);
  Texture_UpdateBuffer(m_hBlackTextureBuffer, data.data(), sizeof(unsigned int) * eTextureWidth, sizeof(data));
}

void CDirectX::CreateGrayGradationTextureBuffer()
{
  enum {
    eTextureWidth    = 4,
    eTextureHeight  = 256,
  };
  m_hGrayGradationTextureBuffer = Texture_CreateBuffer(eTextureWidth, eTextureHeight);
  std::array<unsigned int, eTextureWidth * eTextureHeight> data;
  unsigned int c = 0xFF;
  for (auto it = data.begin(); it != data.end(); it += 4) {
    auto color = (0xFF << 24) | (c << 16) | (c << 8) | c;
    std::fill(it, it+4, color);  // 1行ずつcolorで埋める.
    --c;
  }
  Texture_UpdateBuffer(m_hGrayGradationTextureBuffer, data.data(), sizeof(unsigned int) * eTextureWidth, sizeof(data));
}

HRootSignatur CDirectX::RootSig_Create(const SCreateRootSignatureParam& param)
{
  if (auto* pNew = m_root_sig_management.New()) {
    pNew->Init();

    D3D12_ROOT_SIGNATURE_DESC desc = {};
    desc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;

    // ルートパラメータ.  
    std::vector<std::vector<D3D12_DESCRIPTOR_RANGE>> desc_table_range(param.root_parameters.size());
    std::vector<D3D12_ROOT_PARAMETER> root_params(param.root_parameters.size());
    for (int i=0; i<root_params.size(); ++i) {
      // レンジ.
      auto& ranges = desc_table_range[i];
      ranges.resize(param.root_parameters[i].table.size());
      for (int j=0; j<ranges.size(); ++j) {
        auto& r = ranges[j];
        auto& s = param.root_parameters[i].table[j];
        r.NumDescriptors = s.num;
        r.RangeType = ToDxType(s.type);
        r.BaseShaderRegister = s.shaderRegister;
        //r.OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;
        r.OffsetInDescriptorsFromTableStart = s.offset;
        r.RegisterSpace = 0;
      }
      // ルートパラメータ.
      root_params[i].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
      root_params[i].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
      root_params[i].DescriptorTable.pDescriptorRanges = ranges.data();
      root_params[i].DescriptorTable.NumDescriptorRanges = static_cast<UINT>(ranges.size());
    }
    desc.pParameters = root_params.data();
    desc.NumParameters = static_cast<UINT>(root_params.size());

    // サンプラー.
    std::vector<CD3DX12_STATIC_SAMPLER_DESC> sampler_params(param.sampler_parameters.size());
    for (int i=0; i<sampler_params.size(); ++i) {
      auto& s = param.sampler_parameters[i];
      sampler_params[i].Init(
        s.shaderRegister,
        ToDxType(s.filter),
        ToDxType(s.addressU),
        ToDxType(s.addressV),
        ToDxType(s.addressW),
        0.f,
        s.maxAnisotropy,
        ToDxType(s.comparison),
        D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK,
        0.f,
        D3D12_FLOAT32_MAX,
        D3D12_SHADER_VISIBILITY_PIXEL,
        0
      );
    }
    desc.pStaticSamplers = sampler_params.data();
    desc.NumStaticSamplers = static_cast<UINT>(sampler_params.size());

    ID3DBlob* rootSigBlob = nullptr;
    ID3DBlob* errorBlob = nullptr;
    auto result = D3D12SerializeRootSignature(
      &desc, D3D_ROOT_SIGNATURE_VERSION_1_0, &rootSigBlob, &errorBlob
    );
    if (errorBlob != nullptr) {
      const char* pContents = reinterpret_cast<const char*>(errorBlob->GetBufferPointer());
      printf(pContents);
    }
    _DEBUG_BREAK(SUCCEEDED(result));
    result = m_device->CreateRootSignature(
      0,
      rootSigBlob->GetBufferPointer(),
      rootSigBlob->GetBufferSize(),
      IID_PPV_ARGS(&pNew->pRootSignature)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    SafeRelease(rootSigBlob);
    SafeRelease(errorBlob);
    if (SUCCEEDED(result) && pNew->pRootSignature != nullptr) {
      return pNew->handle;
    }
    RootSig_Destroy(pNew->handle);
  }
  return HRootSignatur();
}
void CDirectX::RootSig_Destroy(HRootSignatur& hRoot)
{
  if (auto* pTarget = m_root_sig_management.Get(hRoot)) {
    pTarget->Term();
    m_root_sig_management.Delete(pTarget);
    hRoot.Invalidate();
  }
}
void CDirectX::Cmd_RootSig_Set(HRootSignatur hRoot)
{
  if (m_cmdList != nullptr) {
    if (auto* pTarget = m_root_sig_management.Get(hRoot)) {
      m_cmdList->SetGraphicsRootSignature(pTarget->pRootSignature);
    }
  }
}

// パイプラインステート.
HPipelineState CDirectX::PipeState_Create(const SCreatePiplelineStateParam& param)
{
  if (auto* pNew = m_pipe_state_management.New()) {
    D3D12_GRAPHICS_PIPELINE_STATE_DESC gpipeline = {};
    if (auto* pTarget = m_root_sig_management.Get(param.hRootSignature)) {
      gpipeline.pRootSignature = pTarget->pRootSignature;
    }
    if (auto* pVS = m_shader_management.Get(param.hVS)) {
      if (pVS->pBlob != nullptr) {
        gpipeline.VS.pShaderBytecode  = pVS->pBlob->GetBufferPointer();
        gpipeline.VS.BytecodeLength   = pVS->pBlob->GetBufferSize();
      }
    }
    if (auto* pPS = m_shader_management.Get(param.hPS)) {
      if (pPS->pBlob != nullptr) {
        gpipeline.PS.pShaderBytecode  = pPS->pBlob->GetBufferPointer();
        gpipeline.PS.BytecodeLength   = pPS->pBlob->GetBufferSize();
      }
    }
    if (auto* pGS = m_shader_management.Get(param.hGS)) {
      if (pGS->pBlob != nullptr) {
        gpipeline.GS.pShaderBytecode  = pGS->pBlob->GetBufferPointer();
        gpipeline.GS.BytecodeLength   = pGS->pBlob->GetBufferSize();
      }
    }
    // サンプルマスク.
    gpipeline.SampleMask = D3D12_DEFAULT_SAMPLE_MASK;
    // ブレンドパラメータ.
#if 1
    gpipeline.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
    if (param.bEnableBlend) {
      gpipeline.BlendState.RenderTarget[0].BlendEnable = true;
    }
#else
    gpipeline.BlendState.AlphaToCoverageEnable = false;
    gpipeline.BlendState.IndependentBlendEnable = false;
    D3D12_RENDER_TARGET_BLEND_DESC renderTargetBlendDesc = {};
    renderTargetBlendDesc.BlendEnable = false;
    renderTargetBlendDesc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
    renderTargetBlendDesc.LogicOpEnable = false;
    gpipeline.BlendState.RenderTarget[0] = renderTargetBlendDesc;
#endif
    // ラスタライザ.
    gpipeline.RasterizerState.MultisampleEnable     = false;
    gpipeline.RasterizerState.CullMode              = ToDxType(param.eCullMode);
    gpipeline.RasterizerState.FillMode              = ToDxType(param.eFillMode);
    gpipeline.RasterizerState.DepthClipEnable       = true;
    gpipeline.RasterizerState.FrontCounterClockwise = false;
    gpipeline.RasterizerState.DepthBias             = D3D12_DEFAULT_DEPTH_BIAS;
    gpipeline.RasterizerState.DepthBiasClamp        = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
    gpipeline.RasterizerState.SlopeScaledDepthBias  = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
    gpipeline.RasterizerState.AntialiasedLineEnable = false;
    gpipeline.RasterizerState.ForcedSampleCount     = 0;
    gpipeline.RasterizerState.ConservativeRaster    = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
    // 深度、 ステンシル.
    gpipeline.DepthStencilState.DepthEnable     = true;
    gpipeline.DepthStencilState.DepthWriteMask  = D3D12_DEPTH_WRITE_MASK_ALL;
    gpipeline.DepthStencilState.DepthFunc       = D3D12_COMPARISON_FUNC_LESS;
    gpipeline.DepthStencilState.StencilEnable   = false;
    gpipeline.DSVFormat                         = DXGI_FORMAT_D32_FLOAT;
    // 頂点レイアウト.
    std::vector<D3D12_INPUT_ELEMENT_DESC> inputElementDesc(param.vertex_input_rayout_parameters.size());
    for (int i=0; i<inputElementDesc.size(); ++i) {
      auto& s = param.vertex_input_rayout_parameters[i];
      inputElementDesc[i] = { s.semanticsName, s.semanticeIndex, ToDxType(s.format), s.slot, D3D12_APPEND_ALIGNED_ELEMENT,  D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,  0 };
    }
    gpipeline.InputLayout.pInputElementDescs  = inputElementDesc.data();
    gpipeline.InputLayout.NumElements         = static_cast<UINT>(inputElementDesc.size());

    gpipeline.IBStripCutValue       = D3D12_INDEX_BUFFER_STRIP_CUT_VALUE_DISABLED;//ストリップ時のカットなし
    gpipeline.PrimitiveTopologyType = ToDxType(param.ePrimitiveTopologyType);
    gpipeline.NumRenderTargets      = param.unRenderTargetNum;
    gpipeline.RTVFormats[0]         = ToDxType(param.eGIFormatRT);
    //gpipeline.SampleDesc.Count      = static_cast<UINT>(param.unSamplerNum);
    gpipeline.SampleDesc.Count      = 1;
    gpipeline.SampleDesc.Quality    = 0;

    auto result = m_device->CreateGraphicsPipelineState(&gpipeline, IID_PPV_ARGS(&pNew->pPipelineState));
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pPipelineState != nullptr) {
      return pNew->handle;
    }
    PipeState_Destroy(pNew->handle);
  }
  return HPipelineState();
}
void CDirectX::PipeState_Destroy(HPipelineState& hPipe)
{
  if (auto* pTarget = m_pipe_state_management.Get(hPipe)) {
    pTarget->Term();
    m_pipe_state_management.Delete(pTarget);
    hPipe.Invalidate();
  }
}
void CDirectX::Cmd_PipeState_Set(HPipelineState hPipe)
{
  if (m_cmdList != nullptr) {
    if (auto* pTarget = m_pipe_state_management.Get(hPipe)) {
      m_cmdList->SetPipelineState(pTarget->pPipelineState);
    }
  }
}

HVertexBuffer CDirectX::Vertex_CreateBuffer(size_t one_vertex_bytes, size_t vertex_num)
{
  if (auto* pNew = m_vertex_management.New()) {
    pNew->Init();

    D3D12_HEAP_PROPERTIES heapprop = {};
    heapprop.Type                 = D3D12_HEAP_TYPE_UPLOAD;
    heapprop.CPUPageProperty      = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    heapprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;

    D3D12_RESOURCE_DESC resdesc = {};
    resdesc.Dimension         = D3D12_RESOURCE_DIMENSION_BUFFER;
    resdesc.Width             = one_vertex_bytes * vertex_num;
    resdesc.Height            = 1;
    resdesc.DepthOrArraySize  = 1;
    resdesc.MipLevels         = 1;
    resdesc.Format            = DXGI_FORMAT_UNKNOWN;
    resdesc.SampleDesc.Count  = 1;
    resdesc.Flags             = D3D12_RESOURCE_FLAG_NONE;
    resdesc.Layout            = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

    auto result = m_device->CreateCommittedResource(
      &heapprop,
      D3D12_HEAP_FLAG_NONE,
      &resdesc,
      D3D12_RESOURCE_STATE_GENERIC_READ,
      nullptr,
      IID_PPV_ARGS(&pNew->pBuffer)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pBuffer != nullptr) {
      return pNew->handle;
    }

    Vertex_DestroyBuffer(pNew->handle);
  }
  return HVertexBuffer();
}
void CDirectX::Vertex_DestroyBuffer(HVertexBuffer& hBuffer)
{
  if (auto* pTarget = m_vertex_management.Get(hBuffer)) {
    pTarget->Term();
    m_vertex_management.Delete(pTarget);
    hBuffer.Invalidate();
  }
}
void CDirectX::Vertex_UpdateBuffer(HVertexBuffer hBuffer, const void* pBin, size_t one_vertex_bytes, size_t vertex_num)
{
  if (auto* pTarget = m_vertex_management.Get(hBuffer)) {
    if (pTarget->pBuffer != nullptr) {
      char* pDest = nullptr;
      if (SUCCEEDED(pTarget->pBuffer->Map(0, nullptr, (void**)&pDest))) {
        std::memcpy(pDest, pBin, one_vertex_bytes * vertex_num);
        pTarget->pBuffer->Unmap(0, nullptr);
      }
    }
  }
}

HVertexView CDirectX::Vertex_CreateView(HVertexBuffer hBuffer, size_t one_vertex_bytes, size_t vertex_num)
{
  if (auto* pTarget = m_vertex_management.Get(hBuffer)) {
    if (pTarget->pBuffer != nullptr) {
      if (auto* pView = pTarget->views.New()) {
        pView->Init();
        pView->view.BufferLocation = pTarget->pBuffer->GetGPUVirtualAddress();
        pView->view.SizeInBytes    = static_cast<UINT>(one_vertex_bytes * vertex_num);
        pView->view.StrideInBytes  = static_cast<UINT>(one_vertex_bytes);
        return pView->handle;
      }
    }
  }
  return HVertexView();
}

void CDirectX::Vertex_DestroyView(HVertexBuffer hBuffer, HVertexView& hView)
{
  if (auto* pTarget = m_vertex_management.Get(hBuffer)) {
    pTarget->views.Delete(hView);
    hView.Invalidate();
  }
}

void CDirectX::Cmd_Vertex_Set(HVertexBuffer hBuffer, HVertexView hView)
{
  if (m_cmdList != nullptr) {
    if (auto* pTarget = m_vertex_management.Get(hBuffer)) {
      if (auto* pView = pTarget->views.Get(hView)) {
        m_cmdList->IASetVertexBuffers(0, 1, &pView->view);
      }
    }
  }
}

// インデックス.
HIndexBuffer CDirectX::Index_CreateBuffer(size_t one_index_bytes, size_t index_num)
{
  if (auto* pNew = m_index_management.New()) {
    pNew->Init();

    D3D12_HEAP_PROPERTIES heapprop = {};
    heapprop.Type                  = D3D12_HEAP_TYPE_UPLOAD;
    heapprop.CPUPageProperty      = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    heapprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;

    D3D12_RESOURCE_DESC resdesc = {};
    resdesc.Dimension          = D3D12_RESOURCE_DIMENSION_BUFFER;
    resdesc.Width              = one_index_bytes * index_num;
    resdesc.Height            = 1;
    resdesc.DepthOrArraySize  = 1;
    resdesc.MipLevels          = 1;
    resdesc.Format            = DXGI_FORMAT_UNKNOWN;
    resdesc.SampleDesc.Count  = 1;
    resdesc.Flags              = D3D12_RESOURCE_FLAG_NONE;
    resdesc.Layout            = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;

    auto result = m_device->CreateCommittedResource(
      &heapprop,
      D3D12_HEAP_FLAG_NONE,
      &resdesc,
      D3D12_RESOURCE_STATE_GENERIC_READ,
      nullptr,
      IID_PPV_ARGS(&pNew->pBuffer)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pBuffer != nullptr) {
      return pNew->handle;
    }

    Index_DestroyBuffer(pNew->handle);
  }
  return HIndexBuffer();
}
void CDirectX::Index_DestroyBuffer(HIndexBuffer& hIndex)
{
  if (auto* pTarget = m_index_management.Get(hIndex)) {
    pTarget->Term();
    m_index_management.Delete(pTarget);
    hIndex.Invalidate();
  }
}
void CDirectX::Index_UpdateBuffer(HIndexBuffer hIndex, const void* pBin, size_t one_index_bytes, size_t index_num)
{
  if (auto* pTarget = m_index_management.Get(hIndex)) {
    if (pTarget->pBuffer != nullptr) {
      char* pDest = nullptr;
      if (SUCCEEDED(pTarget->pBuffer->Map(0, nullptr, (void**)&pDest))) {
        std::memcpy(pDest, pBin, one_index_bytes * index_num);
        pTarget->pBuffer->Unmap(0, nullptr);
      }
    }
  }
}

HIndexView CDirectX::Index_CreateView(HIndexBuffer hBuffer, size_t one_index_bytes, size_t index_num)
{
  if (auto* pTarget = m_index_management.Get(hBuffer)) {
    if (pTarget->pBuffer != nullptr) {
      if (auto* pView = pTarget->views.New()) {
        pView->Init();
        pView->view.BufferLocation  = pTarget->pBuffer->GetGPUVirtualAddress();
        pView->view.SizeInBytes      = static_cast<UINT>(one_index_bytes * index_num);
        pView->view.Format          = DXGI_FORMAT_R16_UINT;
        return pView->handle;
      }
    }
  }
  return HIndexView();
}
void CDirectX::Index_DestroyView(HIndexBuffer hBuffer, HIndexView& hView)
{
  if (auto* pTarget = m_index_management.Get(hBuffer)) {
    pTarget->views.Delete(hView);
  }
}

void CDirectX::Cmd_Index_Set(HIndexBuffer hBuffer, HIndexView hView)
{
  if (m_cmdList != nullptr) {
    if (auto* pTarget = m_index_management.Get(hBuffer)) {
      if (auto* pView = pTarget->views.Get(hView)) {
        m_cmdList->IASetIndexBuffer(&pView->view);
      }
    }
  }
}

  // ディスクリプタヒープ.
HDescHeap CDirectX::DescHeap_Create(EDescriptorHeapType type, uint16_t descNum, bool bShaderVisible)
{
  if (auto* pNew = m_desc_heap_management.New()) {
    pNew->Init();
    D3D12_DESCRIPTOR_HEAP_DESC descHeapDesc = {};
    descHeapDesc.Flags = bShaderVisible ? D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE : D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    descHeapDesc.NumDescriptors = static_cast<UINT>(descNum);
    descHeapDesc.Type = ToDxType(type);
    auto result = m_device->CreateDescriptorHeap(&descHeapDesc, IID_PPV_ARGS(&pNew->pHeap));
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pHeap != nullptr) {
      pNew->type = descHeapDesc.Type;
      return pNew->handle;
    }
    DescHeap_Destroy(pNew->handle);
  }
  return HDescHeap();
}

void CDirectX::DescHeap_Destroy(HDescHeap& hDesc)
{
  if (auto* pTarget = m_desc_heap_management.Get(hDesc)) {
    pTarget->Term();
    m_desc_heap_management.Delete(pTarget);
    hDesc.Invalidate();
  }
}
void CDirectX::DescHeap_PlaceConstView(HDescHeap hDesc, HConstBuffer hConst, uint16_t pos)
{
  if (auto* pHeap = m_desc_heap_management.Get(hDesc)) {
    if (pHeap->pHeap != nullptr) {
      if (auto* pBuffer = m_const_management.Get(hConst)) {
        if (pBuffer->pBuffer != nullptr) {
          // 設定.
          D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
          cbvDesc.BufferLocation  = pBuffer->pBuffer->GetGPUVirtualAddress();
          cbvDesc.SizeInBytes      = static_cast<UINT>(pBuffer->pBuffer->GetDesc().Width);
          // 位置.
          auto heapHandle = pHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
          auto inc = m_device->GetDescriptorHandleIncrementSize(pHeap->type);
          heapHandle.ptr += (inc * pos);
          // 生成.
          m_device->CreateConstantBufferView(&cbvDesc, heapHandle);
        }
      }
    }
  }
}
void CDirectX::DescHeap_PlaceTextureView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos)
{
  if (auto* pHeap = m_desc_heap_management.Get(hDesc)) {
    if (pHeap->pHeap != nullptr) {
      if (auto* pTexture = m_texture_management.Get(hTex)) {
        if (pTexture->pBuffer != nullptr) {
          D3D12_SHADER_RESOURCE_VIEW_DESC texViewDesc = {};
          texViewDesc.Format                  = pTexture->pBuffer->GetDesc().Format;
          texViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
          texViewDesc.ViewDimension           = D3D12_SRV_DIMENSION_TEXTURE2D;
          texViewDesc.Texture2D.MipLevels     = 1;
          auto hHeap = pHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
          auto inc = m_device->GetDescriptorHandleIncrementSize(pHeap->type);
          hHeap.ptr += (inc * pos);
          m_device->CreateShaderResourceView(pTexture->pBuffer, &texViewDesc, hHeap);
        }
      }
    }
  }
}
void CDirectX::DescHeap_PlaceTextureView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos, EGraphInfraFormat eFormat)
{
  if (auto* pHeap = m_desc_heap_management.Get(hDesc)) {
    if (pHeap->pHeap != nullptr) {
      if (auto* pTexture = m_texture_management.Get(hTex)) {
        if (pTexture->pBuffer != nullptr) {
          D3D12_SHADER_RESOURCE_VIEW_DESC texViewDesc = {};
          texViewDesc.Format                  = ToDxType(eFormat);
          texViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
          texViewDesc.ViewDimension           = D3D12_SRV_DIMENSION_TEXTURE2D;
          texViewDesc.Texture2D.MipLevels     = 1;
          auto hHeap = pHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
          auto inc = m_device->GetDescriptorHandleIncrementSize(pHeap->type);
          hHeap.ptr += (inc * pos);
          m_device->CreateShaderResourceView(pTexture->pBuffer, &texViewDesc, hHeap);
        }
      }
    }
  }
}
void CDirectX::DescHeap_PlaceRenderTargetView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos)
{
  if (auto* pHeap = m_desc_heap_management.Get(hDesc)) {
    if (pHeap->pHeap != nullptr) {
      if (auto* pTexture = m_texture_management.Get(hTex)) {
        D3D12_RENDER_TARGET_VIEW_DESC desc = {};
        desc.Format          = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.ViewDimension   = D3D12_RTV_DIMENSION_TEXTURE2D;
        auto hHeap = pHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
        auto inc = m_device->GetDescriptorHandleIncrementSize(pHeap->type);
        hHeap.ptr += (inc * pos);
        m_device->CreateRenderTargetView(pTexture->pBuffer, &desc, hHeap);
      }
    }
  }
}
void CDirectX::DescHeap_PlaceDepthStencilView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos, EGraphInfraFormat eFormat)
{
  if (auto* pHeap = m_desc_heap_management.Get(hDesc)) {
    if (pHeap->pHeap != nullptr) {
      if (auto* pTexture = m_texture_management.Get(hTex)) {
        D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
        dsvDesc.Format          = ToDxType(eFormat);
        dsvDesc.ViewDimension   = D3D12_DSV_DIMENSION_TEXTURE2D;
        dsvDesc.Flags           = D3D12_DSV_FLAG_NONE;
        auto hHeap = pHeap->pHeap->GetCPUDescriptorHandleForHeapStart();
        auto inc = m_device->GetDescriptorHandleIncrementSize(pHeap->type);
        hHeap.ptr += (inc * pos);
        m_device->CreateDepthStencilView(pTexture->pBuffer, &dsvDesc, hHeap);
      }
    }
  }
}
void CDirectX::Cmd_DescHeap_Set(HDescHeap hDesc)
{
  if (m_cmdList != nullptr) {
    if (auto* pTarget = m_desc_heap_management.Get(hDesc)) {
      if (pTarget->pHeap != nullptr) {
        m_cmdList->SetDescriptorHeaps(1, &pTarget->pHeap);
      }
    }
  }
}
void CDirectX::Cmd_DescHeap_Set(HDescHeap hDesc0, HDescHeap hDesc1)
{
  if (m_cmdList != nullptr) {
    ID3D12DescriptorHeap* pHeaps[] = {
      nullptr,
      nullptr,
    };
    HDescHeap handles[] = {
      hDesc0,
      hDesc1,
    };
    for (int i=0; i<_countof(handles); ++i) {
      if (auto* pTarget = m_desc_heap_management.Get(handles[i])) {
        if (pTarget->pHeap != nullptr) {
          pHeaps[i] = pTarget->pHeap;
        }
      }
    }
    m_cmdList->SetDescriptorHeaps(_countof(pHeaps), pHeaps);
  }
}
void CDirectX::Cmd_DescHeap_SetRootParam (HDescHeap hDesc, uint16_t nRootParamIdx, uint16_t nTargetIndex)
{
  if (m_cmdList != nullptr) {
    if (auto* pTarget = m_desc_heap_management.Get(hDesc)) {
      if (pTarget->pHeap != nullptr) {
        auto handle = pTarget->pHeap->GetGPUDescriptorHandleForHeapStart();
        auto inc = m_device->GetDescriptorHandleIncrementSize(pTarget->type);
        handle.ptr += (inc * nTargetIndex);
        m_cmdList->SetGraphicsRootDescriptorTable(nRootParamIdx, handle);
      }
    }
  }
}

// 定数.
HConstBuffer CDirectX::Const_CreateBuffer(size_t bytes)
{
  if (auto* pNew = m_const_management.New()) {
    pNew->Init();
    auto heapProp = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
    auto resDesc = CD3DX12_RESOURCE_DESC::Buffer((bytes + 0xff) & ~0xff);
    auto result = m_device->CreateCommittedResource(
      &heapProp,
      D3D12_HEAP_FLAG_NONE,
      &resDesc,
      D3D12_RESOURCE_STATE_GENERIC_READ,
      nullptr,
      IID_PPV_ARGS(&pNew->pBuffer)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pBuffer != nullptr) {
      return pNew->handle;
    }
    Const_DestroyBuffer(pNew->handle);
  }
  return HConstBuffer();
}
void CDirectX::Const_DestroyBuffer(HConstBuffer& hConst)
{
  if (auto* pTarget = m_const_management.Get(hConst)) {
    pTarget->Term();
    m_const_management.Delete(pTarget);
    hConst.Invalidate();
  }
}
void CDirectX::Const_UpdateBuffer(HConstBuffer hConst, const void* pBin, size_t bytes)
{
  if (auto* pTarget = m_const_management.Get(hConst)) {
    if (pTarget->pBuffer != nullptr) {
      char* pDest = nullptr;
      if (SUCCEEDED(pTarget->pBuffer->Map(0, nullptr, reinterpret_cast<void**>(&pDest)))) {
        std::memcpy(pDest, pBin, bytes);
        pTarget->pBuffer->Unmap(0, nullptr);
      }
    }
  }
}

// テクスチャ.
HTextureBuffer CDirectX::Texture_CreateBufferFromMemory(const void* pBin, size_t bytes, ETextureFormat format)
{
  if (auto* pNew = m_texture_management.New()) {
    DirectX::ScratchImage image;
    HRESULT result = E_FAIL;
    if (format == ETextureFormat::eWIC) {
      result = DirectX::LoadFromWICMemory(pBin, bytes, DirectX::WIC_FLAGS_NONE, nullptr, image);
    }
    else if (format == ETextureFormat::eTGA) {
      result = DirectX::LoadFromTGAMemory(pBin, bytes, nullptr, image);
    }
    else if (format == ETextureFormat::eDDS) {
      result = DirectX::LoadFromDDSMemory(pBin, bytes, DirectX::DDS_FLAGS_NONE, nullptr, image);
    }
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result)) {
      if (auto* pImg = image.GetImage(0, 0, 0))
      {
        auto& metadata = image.GetMetadata();

        // ヒープ設定
        D3D12_HEAP_PROPERTIES texHeapProp = {};
        texHeapProp.Type                  = D3D12_HEAP_TYPE_CUSTOM;//特殊な設定なのでdefaultでもuploadでもなく
        texHeapProp.CPUPageProperty        = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;//ライトバックで
        texHeapProp.MemoryPoolPreference  = D3D12_MEMORY_POOL_L0;//転送がL0つまりCPU側から直で
        texHeapProp.CreationNodeMask      = 0;//単一アダプタのため0
        texHeapProp.VisibleNodeMask        = 0;//単一アダプタのため0

        // 確保するリソースの説明.
        D3D12_RESOURCE_DESC resDesc = {};
        resDesc.Format            = metadata.format;
        resDesc.Width              = static_cast<UINT>(metadata.width);
        resDesc.Height            = static_cast<UINT>(metadata.height);
        resDesc.DepthOrArraySize  = static_cast<uint16_t>(metadata.arraySize);
        resDesc.SampleDesc.Count  = 1;
        resDesc.SampleDesc.Quality= 0;
        resDesc.MipLevels          = static_cast<uint16_t>(metadata.mipLevels);
        resDesc.Dimension          = static_cast<D3D12_RESOURCE_DIMENSION>(metadata.dimension);
        resDesc.Layout            = D3D12_TEXTURE_LAYOUT_UNKNOWN;
        resDesc.Flags              = D3D12_RESOURCE_FLAG_NONE;

        // リソース作成.
        result = m_device->CreateCommittedResource(
          &texHeapProp,
          D3D12_HEAP_FLAG_NONE,//特に指定なし
          &resDesc,
          D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,//テクスチャ用(ピクセルシェーダから見る用)
          nullptr,
          IID_PPV_ARGS(&pNew->pBuffer)
        );
        _DEBUG_BREAK(SUCCEEDED(result));
        if (SUCCEEDED(result) && pNew->pBuffer != nullptr) {
          // 転送(CPU->GPU).
          result = pNew->pBuffer->WriteToSubresource(
            0,
            nullptr,//全領域へコピー
            pImg->pixels,//元データアドレス
            static_cast<UINT>(pImg->rowPitch),//1ラインサイズ
            static_cast<UINT>(pImg->slicePitch)//全サイズ
          );
          _DEBUG_BREAK(SUCCEEDED(result));
          if (SUCCEEDED(result)) {
            return pNew->handle;
          }
        }
      }
    }
    Texture_DestroyBuffer(pNew->handle);
  }
  return HTextureBuffer();
}
HTextureBuffer CDirectX::Texture_CreateBuffer(uint32_t nWidth, uint32_t nHeight)
{
  if (auto* pNew = m_texture_management.New()) {
    pNew->Init();
    // ヒープ設定
    D3D12_HEAP_PROPERTIES texHeapProp = {};
    texHeapProp.Type                  = D3D12_HEAP_TYPE_CUSTOM;
    texHeapProp.CPUPageProperty        = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
    texHeapProp.MemoryPoolPreference  = D3D12_MEMORY_POOL_L0;
    texHeapProp.CreationNodeMask      = 0;
    texHeapProp.VisibleNodeMask        = 0;
    // 確保するリソースの説明.
    D3D12_RESOURCE_DESC resDesc = {};
    resDesc.Format            = DXGI_FORMAT_R8G8B8A8_UNORM;
    resDesc.Width              = nWidth;
    resDesc.Height            = nHeight;
    resDesc.DepthOrArraySize  = 1;
    resDesc.SampleDesc.Count  = 1;
    resDesc.SampleDesc.Quality= 0;
    resDesc.MipLevels          = 1;
    resDesc.Dimension          = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    resDesc.Layout            = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    resDesc.Flags              = D3D12_RESOURCE_FLAG_NONE;
    // リソース作成.
    auto result = m_device->CreateCommittedResource(
      &texHeapProp,
      D3D12_HEAP_FLAG_NONE,//特に指定なし
      &resDesc,
      D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,//テクスチャ用(ピクセルシェーダから見る用)
      nullptr,
      IID_PPV_ARGS(&pNew->pBuffer)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pBuffer != nullptr) {
      return pNew->handle;
    }
    Texture_DestroyBuffer(pNew->handle);
  }
  return HTextureBuffer();
}
HTextureBuffer CDirectX::Texture_CreateBuffer(const SCreateTextureBufferParam& param)
{
  if (auto* pNew = m_texture_management.New()) {
    pNew->Init();
    // ヒープ設定
    D3D12_HEAP_PROPERTIES texHeapProp = {};
    texHeapProp.Type                  = ToDxType(param.eHeapType);
    texHeapProp.CPUPageProperty        = ToDxType(param.eCpuPageProp);
    texHeapProp.MemoryPoolPreference  = ToDxType(param.eMemoryPool);
    texHeapProp.CreationNodeMask      = 0;
    texHeapProp.VisibleNodeMask        = 0;
    // 確保するリソースの説明.
    D3D12_RESOURCE_DESC resDesc = {};
    resDesc.Format            = ToDxType(param.eGIFormat);
    resDesc.Width              = param.unWidth;
    resDesc.Height            = param.unHeight;
    resDesc.DepthOrArraySize  = 1;
    resDesc.SampleDesc.Count  = 1;
    resDesc.SampleDesc.Quality= 0;
    resDesc.MipLevels          = 1;
    resDesc.Dimension          = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    resDesc.Layout            = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    resDesc.Flags              = static_cast<D3D12_RESOURCE_FLAGS>(param.flag);
    D3D12_CLEAR_VALUE* pClearValue = nullptr;
    D3D12_CLEAR_VALUE clearValue = {};
    if (param.pClearColor != nullptr) {
      float value[4] = { param.pClearColor->at(0), param.pClearColor->at(1), param.pClearColor->at(2), param.pClearColor->at(3)};
      clearValue = CD3DX12_CLEAR_VALUE(DXGI_FORMAT_R8G8B8A8_UNORM, value);
      pClearValue = &clearValue;
    }
    // リソース作成.
    auto result = m_device->CreateCommittedResource(
      &texHeapProp,
      D3D12_HEAP_FLAG_NONE,
      &resDesc,
      ToDxType(param.eState),
      pClearValue,
      IID_PPV_ARGS(&pNew->pBuffer)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pBuffer != nullptr) {
      return pNew->handle;
    }
    Texture_DestroyBuffer(pNew->handle);
  }
  return HTextureBuffer();
}
HTextureBuffer CDirectX::Texture_CreateDepthStencilBuffer(uint32_t nWidth, uint32_t nHeight, EGraphInfraFormat eFormat)
{
  if (auto* pNew = m_texture_management.New()) {
    pNew->Init();
    // リソース.
    D3D12_HEAP_PROPERTIES depthHeapProp = {};
    depthHeapProp.Type                  = D3D12_HEAP_TYPE_DEFAULT;
    depthHeapProp.CPUPageProperty       = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    depthHeapProp.MemoryPoolPreference  = D3D12_MEMORY_POOL_UNKNOWN;
    D3D12_RESOURCE_DESC depthResDesc = {};
    depthResDesc.Dimension        = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    depthResDesc.Width            = nWidth;
    depthResDesc.Height           = nHeight;
    depthResDesc.DepthOrArraySize = 1;
    depthResDesc.Format           = ToDxType(eFormat);
    depthResDesc.SampleDesc.Count = 1;
    depthResDesc.Flags            = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
    D3D12_CLEAR_VALUE depthClearValue = {};
    depthClearValue.DepthStencil.Depth  = 1.f; //最大値でクリア.
    depthClearValue.Format              = DXGI_FORMAT_D32_FLOAT;
    auto result = m_device->CreateCommittedResource(
      &depthHeapProp,
      D3D12_HEAP_FLAG_NONE,
      &depthResDesc,
      D3D12_RESOURCE_STATE_DEPTH_WRITE,  // 深度書き込みに使用.
      &depthClearValue,
      IID_PPV_ARGS(&pNew->pBuffer)
    );
    _DEBUG_BREAK(SUCCEEDED(result));
    if (SUCCEEDED(result) && pNew->pBuffer != nullptr) {
      return pNew->handle;
    }
    Texture_DestroyBuffer(pNew->handle);
  }
  return HTextureBuffer();
}
void CDirectX::Texture_UpdateBuffer(HTextureBuffer hTex, const void* pBin, size_t SrcRowPitch, size_t SrcDepthPitch)
{
  if (auto* pTarget = m_texture_management.Get(hTex)) {
    if (pTarget->pBuffer != nullptr) {
      auto result = pTarget->pBuffer->WriteToSubresource(
        0, nullptr, pBin, static_cast<UINT>(SrcRowPitch), static_cast<UINT>(SrcDepthPitch)
      );
      _DEBUG_BREAK(SUCCEEDED(result));
    }
  }
}
void CDirectX::Texture_DestroyBuffer(HTextureBuffer& hTex)
{
  if (auto* pTarget = m_texture_management.Get(hTex)) {
    pTarget->Term();
    m_texture_management.Delete(pTarget);
    hTex.Invalidate();
  }
}

void CDirectX::Cmd_Texture_TransState(HTextureBuffer hTex, EResourceState eBefore, EResourceState eAfter)
{
  if (m_cmdList != nullptr) {
    if (auto* pTarget = m_texture_management.Get(hTex)) {
      D3D12_RESOURCE_BARRIER BarrierDesc = {};
      BarrierDesc.Type                    = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
      BarrierDesc.Flags                   = D3D12_RESOURCE_BARRIER_FLAG_NONE;
      BarrierDesc.Transition.pResource    = pTarget->pBuffer;
      BarrierDesc.Transition.Subresource  = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
      BarrierDesc.Transition.StateBefore  = ToDxType(eBefore);
      BarrierDesc.Transition.StateAfter   = ToDxType(eAfter);
      m_cmdList->ResourceBarrier(1, &BarrierDesc);
    }
  }
}

// デバッグフォント.
void CDirectX::DebugFont_Begin(const SViewPort& vp)
{
  Cmd_DescHeap_Set(m_hDescHeapForFont);
  if (m_pSpriteBatch != nullptr) {
    D3D12_VIEWPORT viewport = {vp.TopLeftX, vp.TopLeftY, vp.Width, vp.Height, vp.MinDepth, vp.MaxDepth};
    m_pSpriteBatch->SetViewport(viewport);
    m_pSpriteBatch->Begin(m_cmdList);
  }
}
void CDirectX::DebugFont_DrawString(const char* str, const DirectX::XMFLOAT2& position, const DirectX::XMFLOAT4& color, float size)
{
  if (m_pSpriteFont != nullptr)
  {
    DirectX::FXMVECTOR      color_    = DirectX::XMLoadFloat4(&color);
    float                   rotation  = 0;
    DirectX::XMFLOAT2       origin    = {0.f, 0.f};
    float                   scale     = size;
    DirectX::SpriteEffects  effects   = DirectX::SpriteEffects_None;
    float                   layerDepth= 0.f;
    m_pSpriteFont->DrawString(m_pSpriteBatch, str, position, color_, rotation, origin, scale, effects, layerDepth);
  }
}
void CDirectX::DebugFont_End()
{
  if (m_pSpriteBatch != nullptr) {
    m_pSpriteBatch->End();
  }
}

void CDirectX::ClearGraphicMemory()
{
  if (m_pGraphicMemory != nullptr) {
    m_pGraphicMemory->Commit(m_cmdQueue);
  }
}

}//namespace ghl