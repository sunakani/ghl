#pragma once

namespace ghl {

// ステートを識別するID.
using AStateId		= uint16_t;

// イベントの種類を区別する番号.
using AStateEvent	= uint16_t;

// ステート変更装置.
class IStateChanger {
public:
	virtual void RequestNext(AStateId next) = 0;
};

// ステートインターフェース.
template<typename T>
class IState {
public:
	// ステート開始時.
	virtual void OnBegin(T& context) = 0;
	// ステート終了時.
	virtual void OnEnd(T& context) = 0;
	// ステートイベント受信時.
	virtual void OnEvent(T& context, AStateEvent eEvent, const void* pArg) = 0;
	// 他のステートへの変化.
	virtual void Transit(T& context, IStateChanger& changer) = 0;
};

// ステート管理オブジェクト.
// 各ステートはtemplate引数でタプル形式で指定される.
// 例えばステートクラスA,B,Cがあったとき、テンプレート第一引数にstd::tuple<A,B,C>として渡す.
// Tはステートのオーナーとなる.
template<typename StatesTuple, typename T>
class TStateMachine : public IStateChanger
{
public:
	enum {
		eStateNum			= std::tuple_size<StatesTuple>::value,
		eInvalidState	= std::numeric_limits<AStateId>::max(),
	};
	TStateMachine(T* pContext)
		: m_pContext(pContext)
		, m_tuple()
		, m_states()
		, m_pCurrent(nullptr)
		, m_next(eInvalidState)
	{
		Setup<0>();
	}
	void Init(AStateId first) {
		if (m_pCurrent == nullptr) {
			if (auto* pNew = Get(first)) {
				pNew->OnBegin(*m_pContext);
				m_pCurrent = pNew;
			}
		}
	}
	void Term() {
		if (m_pCurrent != nullptr) {
			m_pCurrent->OnEnd(*m_pContext);
			m_pCurrent = nullptr;
		}
	}
	// リクエストされた遷移はUpdateのタイミングで実行される.
	virtual void RequestNext(AStateId next) override {
		m_next = next;
	}
	// リクエストされた遷移を実行.
	void Update() {
		if (m_next != eInvalidState) {
			if (auto* pNext = Get(m_next)) {
				if (m_pCurrent != nullptr) {
					m_pCurrent->OnEnd(*m_pContext);
					m_pCurrent = nullptr;
				}
				pNext->OnBegin(*m_pContext);
				m_pCurrent = pNext;
			}
			m_next = eInvalidState;
		}
	}
	// 他のステートに変化したい場合はリクエストさせる.
	void Transit() {
		if (m_pCurrent != nullptr) {
			m_pCurrent->Transit(*m_pContext, *this);
		}
	}
	// ステートに各種イベントを実行させる.
	// 毎フレ行われる更新などもイベントの一種として扱う.
	// 必要な引数はcontextに持たせておく.
	void Event(AStateEvent eEvent, const void* pArg) {
		if (m_pCurrent != nullptr) {
			m_pCurrent->OnEvent(*m_pContext,eEvent,pArg);
		}
	}
private:
	template<int i> void Setup()
	{
		m_states.at(i) = &std::get<i>(m_tuple);
		Setup<i+1>();
	}
	template<> void Setup<eStateNum>() {}
	IState<T>* Get(AStateId id) const {
		if (id < m_states.size()) {
			return m_states.at(id);
		}
		return nullptr;
	}
private:
	T* m_pContext;
	StatesTuple m_tuple;
	std::array<IState<T>*,eStateNum> m_states;
	IState<T>* m_pCurrent;
	AStateId m_next;
};

#if 0
namespace test {

enum EStateId : AStateId {
	eStateA,
	eStateB,
	eStateC,
};
enum EStateEvent : AStateEvent {
	eEventUpdate,
};

class Hoge {
};
class A : public IState<Hoge>
{
public:
  A(){}
  virtual void OnBegin(Hoge& hoge) override {
		DebugPrint("begin A.\n");
	};
	virtual void OnEnd(Hoge& hoge) override {
		DebugPrint("end A.\n");
	};
	virtual void OnEvent(Hoge& hoge, AStateEvent eEvent, const void* pArg) override {
		if (eEvent == eEventUpdate) {
			DebugPrint("update A.\n");
		}
	};
	virtual void Transit(Hoge& hoge, IStateChanger& changer) override {
		changer.RequestNext(eStateB);
	}
};
class B : public IState<Hoge>
{
public:
  B(){}
  virtual void OnBegin(Hoge& hoge) override {
		DebugPrint("begin B.\n");
	};
	virtual void OnEnd(Hoge& hoge) override {
		DebugPrint("end B.\n");
	};
	virtual void OnEvent(Hoge& hoge, AStateEvent eEvent, const void* pArg) override {
		if (eEvent == eEventUpdate) {
			DebugPrint("update B.\n");
		}
	};
	virtual void Transit(Hoge& hoge, IStateChanger& changer) override {
		changer.RequestNext(eStateC);
	}
};
class C : public IState<Hoge>
{
public:
  C(){}
  virtual void OnBegin(Hoge& hoge) override {
		DebugPrint("begin C.\n");
	};
	virtual void OnEnd(Hoge& hoge) override {
		DebugPrint("end C.\n");
	};
	virtual void OnEvent(Hoge& hoge, AStateEvent eEvent, const void* pArg) override {
		if (eEvent == eEventUpdate) {
			DebugPrint("update C.\n");
		}
	};
	virtual void Transit(Hoge& hoge, IStateChanger& changer) override {
		changer.RequestNext(eStateA);
	}
};

inline void func()
{
	Hoge hoge;
	TStateMachine<
		std::tuple<A,B,C>
		, Hoge
	> sm(&hoge);

	sm.Init(eStateA);
	sm.Event(eEventUpdate);

	sm.Transit();
	sm.Update();

	sm.Transit();
	sm.Update();

	sm.Event(eEventUpdate);

	sm.Term();
}

}
#endif

}//namespace ghl
