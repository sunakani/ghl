#pragma once

#include "global.h"

namespace ghl {

/************************************************************/
/*	T型の配列となっているバイナリを保持するクラス.
*************************************************************/
template<typename T>
class CDatabase
{
public:
	CDatabase()
		: m_datum()
	{}
	void Init(const std::vector<char>& bin) {
		Init(bin.data(), bin.size());
	}
	void Init(const char* pBin, size_t nSize) {
		_DEBUG_BREAK((nSize % sizeof(T)) == 0);
		auto num = nSize / (sizeof(T));
		m_datum.resize(num);
		std::memcpy(m_datum.data(), pBin, nSize);
	}
	void Term() {
		m_datum.clear();
		m_datum.shrink_to_fit();
	}
	size_t Num() const {
		return m_datum.size();
	}
	const T* GetByIndex(size_t index) const {
		if (index < m_datum.size()) {
			return &m_datum.at(index);
		}
		return nullptr;
	}

private:
	std::vector<T> m_datum;
};


}//namespace ghl.
