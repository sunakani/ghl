#pragma once

// Windows.
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// DirectX.
#include <DirectXMath.h>
#include <DirectXColors.h>

// Standard.
#include <vector>
#include <list>
#include <map>
#include <unordered_map>
#include <cstdint>
#include <array>
#include <string>
#include <limits>
#include <iostream>
#include <chrono>
#include <memory>
#include <functional>
#include <algorithm>
#include <fstream>
#include <stdio.h>
#include <stdarg.h>
#include <tchar.h>
#include <process.h>

// GHL.
#include "memory/PoolAllocator.h"
#include "math/math_util.h"
#include "utility/State.h"
#include "thread/Thread.h"

// コピー禁止.
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
TypeName(TypeName&) = delete; \
void operator=(TypeName) = delete;

// デバッグ用.
void DebugPrint(const wchar_t* pContents);
void DebugPrint(const char* pContents);
void DebugPrintf(const wchar_t* pFormat, ...);
void DebugPrintf(const char* pFormat, ...);

#define _DEBUG_BREAK(b) if (!(b)) { IsDebuggerPresent() ? __debugbreak() : assert(b); }

// Enum関連.
template<typename E> constexpr auto e2ut(E e) noexcept
{
  return static_cast<std::underlying_type_t<E>>(e);
}

template<typename T> T ClampT(T src, T _min, T _max) {
  return std::min(_max, std::max(_min, src));
}

// ライブラリ.
namespace ghl {
template<typename T> void SafeRelease(T*& o) {
  if (o != nullptr) {
    o->Release();
    o = nullptr;
  }
}
template<typename T> void SafeDelete(T*& o) {
  if (o != nullptr) {
    delete o;
    o = nullptr;
  }
}

// 標準出力以外の出力先を追加(ImGuiなど).
void AddOutputStringFunc(void (*func)(const char*));

}

// 算術.
inline DirectX::XMFLOAT3 operator+(const DirectX::XMFLOAT3& a, const DirectX::XMFLOAT3& b) {
  return DirectX::XMFLOAT3{a.x + b.x, a.y + b.y, a.z + b.z};
}
inline DirectX::XMFLOAT3 operator-(const DirectX::XMFLOAT3& a, const DirectX::XMFLOAT3& b) {
  return DirectX::XMFLOAT3{a.x - b.x, a.y - b.y, a.z - b.z};
}
inline DirectX::XMFLOAT3 operator*(const DirectX::XMFLOAT3& a, float k) {
  return DirectX::XMFLOAT3{a.x * k, a.y * k, a.z * k};
}
inline DirectX::XMFLOAT3& operator+=(DirectX::XMFLOAT3& a,const DirectX::XMFLOAT3& b) {
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  return a;
}
