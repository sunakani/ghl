#pragma once

#include "GHL.h"

/************************************************************/
/*	PMDはMMDで扱われるモデルデータの拡張子.
*   ここにある定義はそのデータを読み解くためのフォーマットなど.
*************************************************************/

namespace ghl {
namespace pmd {

struct SHeaderFormat {
  float version;
  char mode_name[20];
  char comment[256];
};

// マテリアル.
struct SMaterialFormat {
  DirectX::XMFLOAT3 diffuse;  // 12.
  float alpha;                // 4.
  float specularity;          // 4.
  DirectX::XMFLOAT3 specular; // 12.
  DirectX::XMFLOAT3 ambient;  // 12.
  unsigned char toonIdx;      // 1.
  unsigned char edgeFlg;      // 1.
  unsigned char pad[2];       // 2.
  unsigned int indicesNum;    // 4.
  char texFilePath[20];       // 20.
};

struct SBoneFormat {
  char boneName[20];
  unsigned short parentNo;
  unsigned short nextNo;
  unsigned char type;
  unsigned short ikBoneNo;
  DirectX::XMFLOAT3 pos;
};

struct VMDMotion {
  char boneName[15];
  unsigned int frameNo;
  DirectX::XMFLOAT3 location;
  DirectX::XMFLOAT4 quaternion;
  unsigned char bezier[64];
};

}
}
