#pragma once

//
// ゲーム側にインクルードするヘッダー.
//

#include "global.h"

namespace ghl {

// ディスクリプタータイプ.
enum class EDescriptorType {
  eShaderResourceView,
  eUnorderedAccessView,
  eConstantBufferView,
  eSampler,
};
// ディスクリプターヒープタイプ.
enum class EDescriptorHeapType {
  eCBV_SRV_UAV,
  eSampler,
  eRTV,
  eDSV,
};
// ディスクリプターレンジ.
struct SDescriptorRange {
  EDescriptorType type{};
  UINT offset{};
  UINT shaderRegister{};
  UINT num{};
};
// ルートパラメータ設定.
struct SRootParameter {
  std::vector<SDescriptorRange> table;
};
// テクスチャ端の扱い.
enum class ETextureAddressMode {
  eWrap,
  eMirror,
  eClamp,
  eBorder,
  eMirrorOnce,
};
// フォーマット.
enum class EGraphInfraFormat {
  eR32G32B32_Float,
  eR32G32_Float,
  eR16G16_UInt,
  eR8_UInt,
  eR8G8B8A8_UNorm,
  eD32_Float,
  eR32_Typeless,
  eR32_Float,
  eUnknown,
  Num
};
// 頂点入力レイアウト.
struct SVertexInputRayout {
  LPCSTR              semanticsName{};
  UINT                semanticeIndex{};
  EGraphInfraFormat   format{};
  UINT                slot{};
};
// ビューポート.
struct SViewPort {
  float TopLeftX;
  float TopLeftY;
  float Width;
  float Height;
  float MinDepth;
  float MaxDepth;
};
// 矩形.
struct SRect {
  uint32_t  left;
  uint32_t  top;
  uint32_t  right;
  uint32_t  bottom;
};
// フィルター.
enum class EFilter {
  eMinMagMipPoint,
  eCompMinMagMipPoint,
  eCompMinMagMipLinear,
  Num
};
// 比較関数.
enum class EComparisonFunc {
  eNever,
  eLess,
  eEqual,
  eLessEqual,
  eGreater,
  eNotEqual,
  eGreaterEqual,
  eAlways,
  Num
};

// サンプラー設定.
struct SSamplerParameter {
  UINT shaderRegister{};
  ETextureAddressMode addressU{};
  ETextureAddressMode addressV{};
  ETextureAddressMode addressW{};
  EFilter filter{EFilter::eMinMagMipPoint};
  EComparisonFunc comparison{EComparisonFunc::eNever};
  uint8_t maxAnisotropy{0};
};

//-------------------------------------------------
//  ライブラリの初期化、破棄.
//-------------------------------------------------
// ライブラリ初期化に使うパラメータ.
struct SInitParam {
  uint32_t  width{1920};
  uint32_t  height{1080};
  HWND      hWnd{0};
  LPCWSTR   data_dir{};
  std::function<void(float)> callback_update;
  uint8_t   unBackBufferNum{2};
};
bool InitLibrary(const SInitParam& param);
void TermLibrary();

//-------------------------------------------------
//  ライブラリの更新.
//-------------------------------------------------
void UpdateLibrary(std::function<void(float)> updater);

//-------------------------------------------------
//  その他ライブラリ関連.
//-------------------------------------------------
float GetElapsedTime();
float GetFps();

//-------------------------------------------------
//  テクスチャ.
//-------------------------------------------------
enum class ETextureFormat {
  eWIC,
  eTGA,
  eDDS,
};

//-------------------------------------------------
//  マテリアル.
//-------------------------------------------------
struct SBinary {
  std::vector<char> bin;
};
enum class ETextureUsage {
  eColor,
  eSpherical,
  eAdditionalSpherical,
  eToon,
};
struct SPartsTexturesRelation {
  UINT          part{};
  size_t        texture_search_key{};
  ETextureUsage usage{};
  int16_t       toon_index{-1};
  SPartsTexturesRelation(UINT p, size_t t, ETextureUsage u)
    : part(p), texture_search_key(t), usage(u), toon_index{-1}
  {}
};
struct SCreateMaterialParam
{
  UINT                              parts_num{};
  struct STextureData {
    SBinary bin;
    ETextureFormat format{ETextureFormat::eWIC};
  };
  std::map<size_t, STextureData>    texture_binaries{};
  std::list<SPartsTexturesRelation> relations{};
};

//-------------------------------------------------
//  頂点.
//-------------------------------------------------
// レイアウト.
struct SVertexLayout {
  DirectX::XMFLOAT4 pos;
  DirectX::XMFLOAT2 uv;
};

//-------------------------------------------------
//  描画.
//-------------------------------------------------
// プリミティブの種類.
enum class EPrimitiveTopologyType {
  eUndefined,
  ePoint,
  eLine,
  eTriangle,
  ePatch,
  Num
};
// 塗り方.
enum class EPrimitiveTopology {
  eTriangleList,
  eTriangleStrip,
  ePointList,
  eLineList,
  eLineStrip,
};
// リソース生成関連.
enum class EResourceHeapType {
  eDefault,
  eUpload,
  eReadback,
  eCustom,
};
enum class EResourceCpuPageProp {
  eUnknown,
  eNotAvailable,
  eWriteCombine,
  eWriteBack,
};
enum class EResourceMemoryPool {
  eUnknown,
  eL0,
  eL1,
};
enum class EResourceState {
  eRenderTarget,
  ePixcelShaderResource,
};
enum EResourceFlag {
  eRES_FLAG_NONE                        = 0,
  eRES_FLAG_ALLOW_RENDER_TARGET         = 0x1,
  eRES_FLAG_ALLOW_DEPTH_STENCIL         = 0x2,
  eRES_FLAG_ALLOW_UNORDERED_ACCESS      = 0x4,
  eRES_FLAG_DENY_SHADER_RESOURCE        = 0x8,
  eRES_FLAG_ALLOW_CROSS_ADAPTER         = 0x10,
  eRES_FLAG_ALLOW_SIMULTANEOUS_ACCESS   = 0x20,
  eRES_FLAG_VIDEO_DECODE_REFERENCE_ONLY = 0x40,
};
// 面カリング.
enum class ECullMode {
  eNone,
  eFront,
  eBack,
  Num
};
// 塗りつぶしタイプ.
enum class EFillMode {
  eWireFrame,
  eSolid,
  Num
};

//-------------------------------------------------
//  ハンドル.
//-------------------------------------------------
enum class EHandleType {
  eVertexBuffer,
  eVertexView,
  eIndexBuffer,
  eIndexView,
  eDescHeap,
  eConstBuffer,
  eTextureBuffer,
  eShader,
  eRootSignatur,
  ePipelineState,
};
template<EHandleType>
class THandle {
  enum {
    InvalidHandle = std::numeric_limits<uint32_t>::max()
  };
public:
  THandle():m_handle(InvalidHandle){}
  bool IsValid() const {
    return m_handle != InvalidHandle;
  }
  bool IsInvalid() const {
    return m_handle == InvalidHandle;
  }
  void Invalidate() {
    m_handle = InvalidHandle;
  }
  explicit operator bool() const noexcept { return IsValid(); }
  bool operator!() const noexcept { return IsInvalid(); }
  // システム用.
  uint32_t _Get() const { return m_handle; }
  void _Set(uint32_t h) { m_handle = h; }
private:
  uint32_t m_handle;
};
using HVertexBuffer   = THandle<EHandleType::eVertexBuffer>;
using HVertexView     = THandle<EHandleType::eVertexView>;
using HIndexBuffer    = THandle<EHandleType::eIndexBuffer>;
using HIndexView      = THandle<EHandleType::eIndexView>;
using HDescHeap       = THandle<EHandleType::eDescHeap>;
using HConstBuffer    = THandle<EHandleType::eConstBuffer>;
using HTextureBuffer  = THandle<EHandleType::eTextureBuffer>;
using HShader         = THandle<EHandleType::eShader>;
using HRootSignatur   = THandle<EHandleType::eRootSignatur>;
using HPipelineState  = THandle<EHandleType::ePipelineState>;


// バッファ生成パラメータ.
struct SCreateTextureBufferParam {
  uint32_t                    unWidth       {};
  uint32_t                    unHeight      {};
  ghl::EResourceHeapType      eHeapType     {ghl::EResourceHeapType::eDefault};
  ghl::EResourceCpuPageProp   eCpuPageProp  {ghl::EResourceCpuPageProp::eUnknown};
  ghl::EResourceMemoryPool    eMemoryPool   {ghl::EResourceMemoryPool::eUnknown};
  ghl::EGraphInfraFormat      eGIFormat     {ghl::EGraphInfraFormat::eR8G8B8A8_UNorm};
  uint32_t                    flag          {}; // EResourceFlag.
  ghl::EResourceState         eState        {ghl::EResourceState::ePixcelShaderResource};
  const std::array<float,4>*  pClearColor   {nullptr};
};

// ルートシグネチャ生成パラメータ.
struct SCreateRootSignatureParam {
  std::vector<SRootParameter>     root_parameters     {};
  std::vector<SSamplerParameter>  sampler_parameters  {};
};

// パイプラインステート生成パラメータ.
struct SCreatePiplelineStateParam {
  EPrimitiveTopologyType          ePrimitiveTopologyType            {EPrimitiveTopologyType::eTriangle};
  HShader                         hVS                               {};
  HShader                         hPS                               {};
  HShader                         hGS                               {};
  HRootSignatur                   hRootSignature                    {};
  std::vector<SVertexInputRayout> vertex_input_rayout_parameters    {};
  uint8_t                         unRenderTargetNum                 {1};
  ghl::EGraphInfraFormat          eGIFormatRT                       {ghl::EGraphInfraFormat::eR8G8B8A8_UNorm};
  ECullMode                       eCullMode                         {ECullMode::eBack};
  EFillMode                       eFillMode                         {EFillMode::eSolid};
  bool                            bEnableBlend                      {false};
};

// グラフィック関連のインターフェース.
class IGraphic
{
public:
  // ルートシグネチャ.
  virtual HRootSignatur RootSig_Create  (const SCreateRootSignatureParam& param) = 0;
  virtual void          RootSig_Destroy (HRootSignatur& hRoot) = 0;
  virtual void          Cmd_RootSig_Set (HRootSignatur hRoot) = 0;

  // パイプラインステート.
  virtual HPipelineState  PipeState_Create  (const SCreatePiplelineStateParam& param) = 0;
  virtual void            PipeState_Destroy (HPipelineState& hPipe) = 0;
  virtual void            Cmd_PipeState_Set (HPipelineState hPipe) = 0;

  // 頂点.
  virtual HVertexBuffer Vertex_CreateBuffer   (size_t one_vertex_bytes, size_t vertex_num) = 0;
  virtual void          Vertex_DestroyBuffer  (HVertexBuffer& handle) = 0;
  virtual void          Vertex_UpdateBuffer   (HVertexBuffer handle, const void* pBin, size_t one_vertex_bytes, size_t vertex_num) = 0;
  virtual HVertexView   Vertex_CreateView     (HVertexBuffer hBuffer, size_t one_vertex_bytes, size_t vertex_num) = 0;
  virtual void          Vertex_DestroyView    (HVertexBuffer hBuffer, HVertexView& hView) = 0;
  virtual void          Cmd_Vertex_Set        (HVertexBuffer hBuffer, HVertexView hView) = 0;

  // インデックス.
  virtual HIndexBuffer  Index_CreateBuffer    (size_t one_index_bytes, size_t index_num) = 0;
  virtual void          Index_DestroyBuffer   (HIndexBuffer& hIndex) = 0;
  virtual void          Index_UpdateBuffer    (HIndexBuffer hIndex, const void* pBin, size_t one_index_bytes, size_t index_num) = 0;
  virtual HIndexView    Index_CreateView      (HIndexBuffer hBuffer, size_t one_index_bytes, size_t index_num) = 0;
  virtual void          Index_DestroyView     (HIndexBuffer hBuffer, HIndexView& hView) = 0;
  virtual void          Cmd_Index_Set         (HIndexBuffer hBuffer, HIndexView hView) = 0;
  
  // ディスクリプタヒープ(定数やテクスチャを配置する場所).
  virtual HDescHeap     DescHeap_Create               (EDescriptorHeapType type, uint16_t descNum, bool bShaderVisible) = 0;
  virtual void          DescHeap_Destroy              (HDescHeap& hDesc) = 0;
  virtual void          DescHeap_PlaceConstView       (HDescHeap hDesc, HConstBuffer hConst, uint16_t pos) = 0;
  virtual void          DescHeap_PlaceTextureView     (HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos) = 0;
  virtual void          DescHeap_PlaceTextureView     (HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos, EGraphInfraFormat eFormat) = 0;
  virtual void          DescHeap_PlaceRenderTargetView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos) = 0;
  virtual void          DescHeap_PlaceDepthStencilView(HDescHeap hDesc, HTextureBuffer hTex, uint16_t pos, EGraphInfraFormat eFormat) = 0;
  virtual void          Cmd_DescHeap_Set              (HDescHeap hDesc) = 0;
  virtual void          Cmd_DescHeap_Set              (HDescHeap hDesc0, HDescHeap hDesc1) = 0;
  virtual void          Cmd_DescHeap_SetRootParam     (HDescHeap hDesc, uint16_t nRootParamIdx, uint16_t nTargetIndex) = 0;

  // 定数.
  virtual HConstBuffer  Const_CreateBuffer  (size_t bytes) = 0;
  virtual void          Const_DestroyBuffer (HConstBuffer& hConst) = 0;
  virtual void          Const_UpdateBuffer  (HConstBuffer hConst, const void* pBin, size_t bytes) = 0;

  // テクスチャ.
  virtual HTextureBuffer  Texture_CreateBufferFromMemory  (const void* pBin, size_t bytes, ETextureFormat format) = 0;
  virtual HTextureBuffer  Texture_CreateBuffer            (uint32_t nWidth, uint32_t nHeight) = 0;
  virtual HTextureBuffer  Texture_CreateBuffer            (const SCreateTextureBufferParam& param) = 0;
  virtual HTextureBuffer  Texture_CreateDepthStencilBuffer(uint32_t nWidth, uint32_t nHeight, EGraphInfraFormat eFormat) = 0;
  virtual void            Texture_UpdateBuffer            (HTextureBuffer hTex, const void* pBin, size_t SrcRowPitch, size_t SrcDepthPitch) = 0;
  virtual void            Texture_DestroyBuffer           (HTextureBuffer& hTex) = 0;
  virtual HTextureBuffer  Texture_GetWhite                () = 0;
  virtual HTextureBuffer  Texture_GetBlack                () = 0;
  virtual HTextureBuffer  Texture_GetGrayGradation        () = 0;
  virtual void            Cmd_Texture_TransState          (HTextureBuffer hTex, EResourceState eBefore, EResourceState eAfter) = 0;

  // 深度バッファ.
  virtual void Cmd_DepthStencil_Clear(HDescHeap hDesc, uint16_t pos) = 0;

  // バックバッファ.
  virtual void Cmd_BackBuffer_Clear(float r, float g, float b, float a) = 0;
  virtual void Cmd_BackBuffer_Clear(const std::array<float,4>& color) = 0;
  virtual void Cmd_BackBuffer_ChangeStateFromPresentToRenderTarget() = 0;
  virtual void Cmd_BackBuffer_ChangeStateFromRenderTargetToPresent() = 0;
  virtual void Cmd_BackBuffer_SetToRenderTarget(HDescHeap hDescDsv, uint16_t pos) = 0;

  // レンダーターゲット.
  virtual void Cmd_RenderTarget_Set   (HDescHeap hDescRtv, uint16_t posRtv, HDescHeap hDescDsv, uint16_t posDsv) = 0;
  virtual void Cmd_RenderTarget_Clear (HDescHeap hDesc, const std::array<float,4>& color) = 0;

  // シェーダ.
  virtual HShader Shader_CreateByCompileFile  (LPCWSTR pFilename, LPCSTR pEntrypoint, LPCSTR pTarget) = 0;
  virtual void    Shader_Destroy              (HShader& hShader) = 0;

  // 描画設定.
  virtual void Cmd_Set_PrimitiveTopology  (EPrimitiveTopology eTopology) = 0;
  virtual void Cmd_Set_Viewports          (const SViewPort& vp) = 0;
  virtual void Cmd_Set_ScissorRects       (const SRect& r) = 0;

  // 描画.
  virtual void Cmd_Draw_Instanced         (UINT VertexCountPerInstance, UINT InstanceCount, UINT StartVertexLocation, INT StartInstanceLocation) = 0;
  virtual void Cmd_Draw_IndexedInstanced  (UINT IndexCountPerInstance, UINT InstanceCount, UINT StartIndexLocation,  INT BaseVertexLocation, UINT StartInstanceLocation) = 0;

  // スワップチェイン.
  virtual void SwapChain_Present(bool bEnableVSync) = 0;

  // コマンド.
  virtual void CloseCommand() = 0;
  virtual void ExecuteCommand() = 0;
  virtual void WaitCommandFinished() = 0;
  virtual void ClearCommand() = 0;

  // デバッグフォント.
  virtual void DebugFont_Begin(const SViewPort& vp) = 0;
  virtual void DebugFont_DrawString(const char* str, const DirectX::XMFLOAT2& position, const DirectX::XMFLOAT4& color, float size) = 0;
  virtual void DebugFont_End() = 0;

  // その他.
  virtual void ClearGraphicMemory() = 0;

};
IGraphic& GraphicInterface();

// imgui.
bool ImGuiInit(HWND hWnd);
void ImGuiTerm();
void ImGuiNewFrame();
void ImGuiRender();


}




