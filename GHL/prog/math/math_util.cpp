#include "math_util.h"

namespace ghl {

using namespace DirectX;

DirectX::XMMATRIX LookAtMatrix(const DirectX::XMVECTOR& dir_z, const DirectX::XMFLOAT3& up, const DirectX::XMFLOAT3& right)
{
	auto z = XMVector3Normalize(dir_z);
	auto y = XMVector3Normalize(XMLoadFloat3(&up));
	auto x = XMVector3Normalize(XMVector3Cross(z,z));
	y = XMVector3Normalize(XMVector3Cross(z,x));

	float dot_yz = XMVector3Dot(y,z).m128_f32[0];
	if (DirectX::XMScalarNearEqual(dot_yz, 1.f, FLT_EPSILON)) {
		x = XMVector3Normalize(XMLoadFloat3(&right));
		y = XMVector3Normalize(XMVector3Cross(z,x));
		x = XMVector3Normalize(XMVector3Cross(y,z));
	}

	XMMATRIX ret = XMMatrixIdentity();
	ret.r[0] = x;
	ret.r[1] = y;
	ret.r[2] = z;
	return ret;
}

DirectX::XMMATRIX LookAtMatrix(const DirectX::XMVECTOR& origin, const DirectX::XMVECTOR& lookat, const DirectX::XMFLOAT3& up, const DirectX::XMFLOAT3& right)
{
	return XMMatrixTranspose(
		LookAtMatrix(origin, up, right) * LookAtMatrix(lookat, up, right)
	);
}

/// クォータニオンからオイラー角としてのXYZの角度を求める関数.
DirectX::XMFLOAT3 GetQuaternionToEulerXYZ(const DirectX::XMVECTOR& quat)
{
  using namespace DirectX;
  XMFLOAT4 tmp; XMStoreFloat4(&tmp, quat);
  XMFLOAT3 ret;

  // XYZ回転角を求める.
  float x2 = tmp.x + tmp.x;
  float y2 = tmp.y + tmp.y;
  float z2 = tmp.z + tmp.z;
  float xz2 = tmp.x * z2;
  float wy2 = tmp.w * y2;
  float temp = -(xz2 - wy2);
  if (temp >= 1.0f) { temp = 1.0f; }
  else if (temp <= -1.0f) { temp = -1.0f; }
  float yRadian = asinf(temp);

  float xx2 = tmp.x * x2;
  float xy2 = tmp.x * y2;
  float zz2 = tmp.z * z2;
  float wz2 = tmp.w * z2;
  if (yRadian < XM_PI * 0.5f) {
    if (yRadian > -XM_PI * 0.5f) {
      float yz2 = tmp.y * z2;
      float wx2 = tmp.w * x2;
      float yy2 = tmp.y * y2;
      ret.x = atan2f((yz2 + wx2), (1.0f - (xx2 + yy2)));
      ret.y = yRadian;
      ret.z = atan2f((xy2 + wz2), (1.0f - (yy2 + zz2)));
    }
    else {
      ret.x = -atan2f((xy2 - wz2), (1.0f - (xx2 + zz2)));
      ret.y = yRadian;
      ret.z = 0.0f;
    }
  }
  else {
    ret.x = atan2f((xy2 - wz2), (1.0f - (xx2 + zz2)));
    ret.y = yRadian;
    ret.z = 0.0f;
  }
  return ret;
}

// オイラー角からクォータニオンを生成
DirectX::XMVECTOR MakeQuaternionEuler(const DirectX::XMFLOAT3 & eulerXYZ)
{
  float xRadian = eulerXYZ.x * 0.5f;
  float yRadian = eulerXYZ.y * 0.5f;
  float zRadian = eulerXYZ.z * 0.5f;
  float sinX = sinf(xRadian);
  float cosX = cosf(xRadian);
  float sinY = sinf(yRadian);
  float cosY = cosf(yRadian);
  float sinZ = sinf(zRadian);
  float cosZ = cosf(zRadian);

  DirectX::XMFLOAT4 newQuaternion;
  newQuaternion.x = sinX * cosY * cosZ - cosX * sinY * sinZ;
  newQuaternion.y = cosX * sinY * cosZ + sinX * cosY * sinZ;
  newQuaternion.z = cosX * cosY * sinZ - sinX * sinY * cosZ;
  newQuaternion.w = cosX * cosY * cosZ + sinX * sinY * sinZ;

  return DirectX::XMLoadFloat4(&newQuaternion);
}

}//namespace ghl.
