#pragma once

#include <DirectXMath.h>

namespace ghl {

DirectX::XMMATRIX LookAtMatrix(const DirectX::XMVECTOR& dir_z,	const DirectX::XMFLOAT3& up,			const DirectX::XMFLOAT3& right);
DirectX::XMMATRIX LookAtMatrix(const DirectX::XMVECTOR& origin,	const DirectX::XMVECTOR& lookat,	const DirectX::XMFLOAT3& up, const DirectX::XMFLOAT3& right);

DirectX::XMFLOAT3 GetQuaternionToEulerXYZ(const DirectX::XMVECTOR& quat);
DirectX::XMVECTOR MakeQuaternionEuler(const DirectX::XMFLOAT3 & eulerXYZ);

}//namespace ghl
