#include "Input.h"

namespace ghl {

constexpr EKey all_EKey[] = {
	EKey::NONE,
	EKey::ESC,
	EKey::W,
	EKey::A,
	EKey::S,
	EKey::D,
	EKey::MOUSE_L,
	EKey::MOUSE_R,
	EKey::O,
};
static_assert(_countof(all_EKey) == e2ut(EKey::Num), "Numbers don't match.");

static const int sk_keybord_virtualkey[] = {
	0,
	VK_ESCAPE,
	0x57,	// W.
	0x41,	// A.
	0x53,	// S.
	0x44,	// D.
	VK_LBUTTON,
	VK_RBUTTON,
	0x4F,	// O.
};
static_assert(_countof(sk_keybord_virtualkey) == e2ut(EKey::Num), "Numbers don't match.");

CInput::CInput()
	: m_keyboard_states()
	, m_mouse_state()
{}

void CInput::Init()
{}

void CInput::Term()
{}

void CInput::UpdateKeybordState()
{
	for (auto e : all_EKey)
	{
		if (e == EKey::NONE) {
			continue;
		}
		auto idx = e2ut(e);
		auto& target = m_keyboard_states.at(idx);

		// 前回を保存.
		auto prev = target;

		// 新しく状態を確認.
		SHORT state = GetAsyncKeyState(sk_keybord_virtualkey[idx]);

		// 押している、押した瞬間、離した瞬間、を確認.
		bool bPushed = (state & 0x8000) != 0;
		bool bPressed = false;
		bool bReleased = false;
		if (prev.bPushed == false && bPushed) {
			bPressed = true;
		}
		if (prev.bPressed && bPushed == false) {
			bReleased = true;
		}
		target.bPushed = bPushed;
		target.bPressed = bPressed;
		target.bReleased = bReleased;
	}
}

bool CInput::IsKeybordKeyPushed(EKey key) const {
	return IsKeybordKeyState(key, EKeyState::Pushed);
}
bool CInput::IsKeybordKeyPressed(EKey key) const {
	return IsKeybordKeyState(key, EKeyState::Pressed);
}
bool CInput::IsKeybordKeyReleased(EKey key) const {
	return IsKeybordKeyState(key, EKeyState::Released);
}

bool CInput::IsKeybordKeyState(EKey key, EKeyState state) const
{
	auto& target = m_keyboard_states.at(e2ut(key));
	if (0
		|| (state == EKeyState::Pushed && target.bPushed)
		|| (state == EKeyState::Pressed && target.bPressed)
		|| (state == EKeyState::Released && target.bReleased)
		)
	{
		return true;
	}
	return false;
}

void CInput::UpdateMouseState(HWND hWnd)
{
	auto prev = m_mouse_state.point;

  GetCursorPos(&m_mouse_state.point);
	if (ScreenToClient(hWnd, &m_mouse_state.point)) {
		m_mouse_state.nMoveX = m_mouse_state.point.x - prev.x;
		m_mouse_state.nMoveY = m_mouse_state.point.y - prev.y;
	}
	else {
		m_mouse_state.point = prev;
	}

	//RECT r;
	//GetWindowRect(hWnd,&r);
	//int x = (r.right - r.left) / 2;
	//int y = (r.bottom - r.top) / 2;
	//SetCursorPos(x, y);
	//m_mouse_state.point.x = x;
	//m_mouse_state.point.y = y;
}

void CInput::GetMouseMove(int32_t* pX, int32_t* pY) const
{
	if (pX != nullptr) {
		*pX = m_mouse_state.nMoveX;
	}
	if (pY != nullptr) {
		*pY = m_mouse_state.nMoveY;
	}
}


}
