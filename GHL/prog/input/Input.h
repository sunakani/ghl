#pragma once

#include "global.h"

namespace ghl {

// 入力.
enum class EKey
{
	NONE,
	ESC,
	W,
	A,
	S,
	D,
	MOUSE_L,
	MOUSE_R,
	O,
	Num
};

// 入力の判定種類.
enum class EKeyState
{
	None,
	Pushed,
	Pressed,
	Released,
};

/************************************************************/
/*	入力を確認、保持するクラス.
*************************************************************/
class CInput
{
public:
	CInput();
	void Init();
	void Term();

	void UpdateKeybordState();
	bool IsKeybordKeyPushed(EKey key) const;
	bool IsKeybordKeyPressed(EKey key) const;
	bool IsKeybordKeyReleased(EKey key) const;
	bool IsKeybordKeyState(EKey key, EKeyState state) const;

	void UpdateMouseState(HWND hWnd);
	void GetMouseMove(int32_t* pX, int32_t* pY) const;
	int32_t GetMouseMoveX() const { return m_mouse_state.nMoveX; }
	int32_t GetMouseMoveY() const { return m_mouse_state.nMoveY; }

private:

	enum {
		eKeyboarKeyNum = e2ut(EKey::Num),
	};

	struct SKeyState {
		bool bPushed{};		// 押している.
		bool bPressed{};	// 押した瞬間.
		bool bReleased{};	// 離れた瞬間.
	};
	std::array<SKeyState, eKeyboarKeyNum> m_keyboard_states;

	struct SMouseState {
		POINT point{};
		int32_t nMoveX{};
		int32_t nMoveY{};
	};
	SMouseState m_mouse_state;

};




}
