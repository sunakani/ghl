#include "global.h"

namespace ghl {

void (*DebugPrintFunc)(const char*) = nullptr;
void AddOutputStringFunc(void (*func)(const char*))
{
	DebugPrintFunc = func;
}

}

void _OutputString(const char* pContents)
{
	OutputDebugStringA(pContents);
	if (ghl::DebugPrintFunc != nullptr) {
    (*ghl::DebugPrintFunc)(pContents);
	}
}

void DebugPrint(const wchar_t* pContents) {
  OutputDebugString(pContents);
}

void DebugPrint(const char* pContents) {
  _OutputString(pContents);
}

void DebugPrintf(const wchar_t* pFormat, ...)
{
  wchar_t pszBuf[512];
  va_list argp;
  va_start(argp, pFormat);
  _vstprintf(pszBuf, 512, pFormat, argp);
  va_end(argp);
  OutputDebugString(pszBuf);
}

void DebugPrintf(const char* pFormat, ...)
{
  char pszBuf[512];
  va_list argp;
  va_start(argp, pFormat);
  vsprintf_s(pszBuf, pFormat, argp);
  va_end(argp);
  _OutputString(pszBuf);
}
