﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace GameAddin
{
  public partial class ThisAddIn
  {
    public enum DataType {
      eUNKNOWN,
      eUINT8,
      eUINT16,
      eUINT32,
      eINT8,
      eINT16,
      eINT32,
      eFLOAT,
    };



    public DataType Str2Type(string param) {
      if (param == "uint8")   { return DataType.eUINT8;   }
      if (param == "uint16")  { return DataType.eUINT16;  }
      if (param == "uint32")  { return DataType.eUINT32;  }
      if (param == "int8")    { return DataType.eINT8;    }
      if (param == "int16")   { return DataType.eINT16;   }
      if (param == "int32")   { return DataType.eINT32;   }
      if (param == "float")   { return DataType.eFLOAT;   }
      return DataType.eUNKNOWN;
    }

    public int Type2ByteSize(DataType type) {
      if (type == DataType.eUINT8)  { return 1; }
      if (type == DataType.eUINT16) { return 2; }
      if (type == DataType.eUINT32) { return 4; }
      if (type == DataType.eINT8)   { return 1; }
      if (type == DataType.eINT16)  { return 2; }
      if (type == DataType.eINT32)  { return 4; }
      if (type == DataType.eFLOAT)  { return 4; }
      return 0;
    }

    public string Type2CppTypeName(DataType type) {
      if (type == DataType.eUINT8)  { return "uint8_t"; }
      if (type == DataType.eUINT16) { return "uint16_t"; }
      if (type == DataType.eUINT32) { return "uint32_t"; }
      if (type == DataType.eINT8)   { return "int8_t"; }
      if (type == DataType.eINT16)  { return "int16_t"; }
      if (type == DataType.eINT32)  { return "int32_t"; }
      if (type == DataType.eFLOAT)  { return "float"; }
      return "";
    }

    public void AddByteList(ref List<byte> dst, dynamic value, DataType type) {
      if (type == DataType.eUINT8) {
        dst.Add((byte)value);
      }
      if (type == DataType.eUINT16) {
        var bytes = BitConverter.GetBytes((ushort)value);
        dst.AddRange(bytes);
      }
      if (type == DataType.eUINT32) {
        var bytes = BitConverter.GetBytes((uint)value);
        dst.AddRange(bytes);
      }
      if (type == DataType.eINT8) {
        dst.Add((byte)value);
      }
      if (type == DataType.eINT16) {
        var bytes = BitConverter.GetBytes((short)value);
        dst.AddRange(bytes);
      }
      if (type == DataType.eINT32) {
        var bytes = BitConverter.GetBytes((int)value);
        dst.AddRange(bytes);
      }
      if (type == DataType.eFLOAT) {
        var bytes = BitConverter.GetBytes((float)value);
        dst.AddRange(bytes);
      }
    }


    public Excel.Worksheet FindWorksheetByName(Excel.Workbook wb, string name) {
      if (wb != null) {
        foreach(Excel.Worksheet ws in wb.Worksheets) {
          if (ws.Name == name) {
            return ws;
          }
        }
      }
      return null;
    }

    public string GetRootDirectory(Excel.Workbook wb) {
      if (wb != null) {
        string check_dir = Path.GetDirectoryName(wb.Path);
        for (int i=0; i<30; ++i) {
          if (check_dir == null) {
            break;
          }
          bool bFound = false;
          foreach (string file in Directory.GetFiles(check_dir, "root_mark*")) {
            bFound = true;
            break;
          }
          if (bFound) {
            return check_dir + "\\";
          }
          check_dir = Path.GetDirectoryName(check_dir);
        }
      }
      return null;
    }

  }
}
