﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace GameAddin
{
  public partial class ThisAddIn
  {
    private void ThisAddIn_Startup(object sender, System.EventArgs e)
    {
    }

    private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
    {
    }

    #region VSTO で生成されたコード

    /// <summary>
    /// デザイナーのサポートに必要なメソッドです。
    /// コード エディターで変更しないでください。
    /// </summary>
    private void InternalStartup()
    {
      this.Startup += new System.EventHandler(ThisAddIn_Startup);
      this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
    }

    #endregion

    // バイナリ出力.
    public void OutputBinary()
    {
      Excel.Workbook wb = (Excel.Workbook)Application.ActiveWorkbook;
      Excel.Worksheet ws = (Excel.Worksheet)Application.ActiveSheet;

      int colHeader = 1;
      int rowSheetType = 1;

      // 出力シートかを確認.
      if (ws.Cells[rowSheetType, colHeader].Value.ToString() != "出力") {
        return;
      }

      // データ名(=Book名).
      string DataName = Path.GetFileNameWithoutExtension(wb.Name);

      // ヘッダー情報がある行を確認.
      int rowHeaderStart = -1;
      int rowHeaderEnd = -1;
      for (int i=2; i<50; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "Header") {
          rowHeaderStart = i;
        }
        else if (name == "Contents") {
          rowHeaderEnd = i;
        }
      }
      if (rowHeaderStart < 0 || rowHeaderEnd < 0) {
        return;
      }

      // データのルートディレクトリを確認.
      string RootDirectory = GetRootDirectory(wb);

      // ヘッダー情報を解析.
      string BinaryOutputDir = "";
      string BinaryOutputName = DataName;
      for (int i = rowHeaderStart; i< rowHeaderEnd; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "バイナリ出力") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            BinaryOutputDir = rvalue.ToString();
            BinaryOutputDir = BinaryOutputDir.Replace("<Root>",RootDirectory);
          }
        }
        else if (name == "バイナリ名") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            BinaryOutputName = rvalue.ToString();
          }
        }
      }

      // バイナリ出力パス.
      string BinaryOutputPath = BinaryOutputDir + BinaryOutputName + ".bin";

      // データ内容の解析.
      {
        int rowContentsStart = rowHeaderEnd;
        int rowContentsName   = rowContentsStart + 1;
        int rowContentsType   = rowContentsStart + 2;
        int rowContentsValue  = rowContentsStart + 3;
        var dataHeaderList = new List<(string, DataType)>();
        int colValidation = 2;  // 2列目は有効な行とするか
        int colDataStart = 3;
        int colData = colDataStart;
        while(true) {
          var value = ws.Cells[rowContentsName, colData].Value;
          if (value == null) {
            break;
          }
          string name = value.ToString();
          string typeName = ws.Cells[rowContentsType, colData].Value.ToString();
          dataHeaderList.Add((name, Str2Type(typeName)));
          colData += 1;
        }

        // 1行のデータサイズを確定(自動でパディングは考慮しない).
        int one_row_byte_size = 0;
        foreach (var value in dataHeaderList) {
          int byte_size = Type2ByteSize(value.Item2);
          one_row_byte_size += byte_size;
        }

        // 何行あるか(左端の列が1なら有効な行、0なら無効な行、それ以外なら終わり).
        int num_of_valid_rows = 0;
        for (int r=rowContentsValue; ; ++r) {
          var value = ws.Cells[r, colValidation].Value;
          if (value == null) {
            break;
          }
          var value_str = value.ToString();
          if (value_str == "") {
          }
          else if (value == 1) {
            num_of_valid_rows += 1;
          }
          else if (value == 0) {
          }
          else {
            break;
          }
        }

        // 全データを取り込むメモリ領域を確保.
        int total_byte_size = one_row_byte_size * num_of_valid_rows;
        List<byte> binary = new List<byte>(total_byte_size);

        // １行ずつバイトリストを作っていく.
        for (int r=rowContentsValue; ; ++r) {
          var value = ws.Cells[r, colValidation].Value;
          if (value == null) {
            break;
          }
          var value_str = value.ToString();
          if (value_str == "") {
          }
          else if (value == 1) {
            for (int c=0; c<dataHeaderList.Count; ++c) {
              int col = colDataStart + c;
              AddByteList(ref binary, ws.Cells[r, col].Value, dataHeaderList[c].Item2);
            }
          }
          else if (value == 0) {
          }
          else {
            break;
          }
        }

        // 問題ないかチェック.
        Debug.Assert(binary.Count == total_byte_size);

        // ファイルに出力.
        using (var fs = File.OpenWrite(BinaryOutputPath)) {
          fs.SetLength(0);
          fs.Write(binary.ToArray(), 0, binary.Count);
        }

        // メッセージ.
        MessageBox.Show("Output Binary File\n" + BinaryOutputPath);
      }
    }

    // INC出力.
    public void OutputInc()
    {
      Excel.Workbook wb = (Excel.Workbook)Application.ActiveWorkbook;
      Excel.Worksheet ws = (Excel.Worksheet)Application.ActiveSheet;

      int colHeader = 1;
      int rowSheetType = 1;

      // 出力シートかを確認.
      if (ws.Cells[rowSheetType, colHeader].Value.ToString() != "出力") {
        return;
      }

      // データ名(=Book名).
      string DataName = Path.GetFileNameWithoutExtension(wb.Name);

      // ヘッダー情報がある行を確認.
      int rowHeaderStart = -1;
      int rowHeaderEnd = -1;
      for (int i=2; i<50; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "Header") {
          rowHeaderStart = i;
        }
        else if (name == "Contents") {
          rowHeaderEnd = i;
        }
      }
      if (rowHeaderStart < 0 || rowHeaderEnd < 0) {
        return;
      }

      // データのルートディレクトリを確認.
      string RootDirectory = GetRootDirectory(wb);

      // ヘッダー情報を解析.
      string MacroString = "";
      string OutputDir = "";
      string OutputName = "";
      for (int i = rowHeaderStart; i< rowHeaderEnd; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "マクロ") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            MacroString = rvalue.ToString();
          }
        }
        else if (name == "出力フォルダ") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            OutputDir = rvalue.ToString();
            OutputDir = OutputDir.Replace("<Root>",RootDirectory);
          }
        }
        else if (name == "出力名") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            OutputName = rvalue.ToString();
          }
        }
      }

      // 出力パス.
      string OutputPath = OutputDir + OutputName;

      // データ内容の解析.
      {
        int rowContentsStart  = rowHeaderEnd;
        int rowContentsHeader = rowContentsStart + 1;
        int rowContentsValue  = rowContentsStart + 2;
        int colValidation = 2;  // 2列目は有効な行とするか
        int colDataStart = 3;
        int colData = colDataStart;

        // 有効な列を抽出.
        List<int> valid_columns = new List<int>();
        for (int c=colDataStart; c<100; ++c) {
          var value = ws.Cells[rowContentsHeader, c].Value;
          if (value == null) {
            break;
          }
          if (value == 1) {
            valid_columns.Add(c);
          }
        }

        // 書き込む内容を準備.
        List<string> contents_list = new List<string>();
        for (int r=rowContentsValue; ; ++r) {
          var value = ws.Cells[r, colValidation].Value;
          if (value == null) {
            break;
          }
          if (value == 1) {
            string dst = "";
            foreach (int c in valid_columns) {
              var cell_value = ws.Cells[r, c].Value;
              if (dst.Length > 0) {
                dst += ",";
              }
              dst += cell_value.ToString();
            }
            contents_list.Add(dst);
          }
          else if (value == 0) {
          }
          else {
            break;
          }
        }

        // ファイルに出力.
        using (StreamWriter sw = new StreamWriter(OutputPath, false, Encoding.UTF8)) {
          sw.WriteLine(MacroString + "_START()");
          foreach (var contents in contents_list) {
            sw.WriteLine(MacroString + "(" + contents + ")");
          }
          sw.WriteLine(MacroString + "_END()");
        }

        // メッセージ.
        MessageBox.Show("Output Inc File\n" + OutputPath);
      }
    }

    //
    public void UpdateDefines()
    {
      Excel.Workbook wb = (Excel.Workbook)Application.ActiveWorkbook;

      // 同じ階層.
      string CurrentDir = Path.GetDirectoryName(wb.FullName);

      // 開くべきブック.
      string SourceBookPath = CurrentDir + "\\" + "定義.xlsm";

      // 開く.
      Excel.Workbook SourceBook = null;
      foreach (Excel.Workbook b in Application.Workbooks) {
        if (b.FullName == SourceBookPath) {
          SourceBook = b;
          break;
        }
      }
      if (SourceBook == null) {
        SourceBook = Application.Workbooks.Open(SourceBookPath);
      }
      if (SourceBook == null) {
        return;
      }

      // コピー元.
      Excel.Worksheet SourceSheet = FindWorksheetByName(SourceBook, "定義");
      if (SourceSheet == null) {
        return;
      }

      // コピー先.
      Excel.Worksheet DestSheet = FindWorksheetByName(wb, "定義");
      if (DestSheet == null) {
        DestSheet = wb.Worksheets.Add();
        DestSheet.Name = "定義";
      }

      // 3列ずつ解析.
      List<string> SourceList = new List<string>();
      List<string> DestList = new List<string>();
      int nRowOfHeader = 1;
      for (int c=1; ; c+=3) {
        var value = SourceSheet.Cells[nRowOfHeader, c].Value;
        if (value == null) {
          break;
        }
        string name = value.ToString();
        SourceList.Add(name);
      }
      for (int c=1; ; c+=3) {
        var value = DestSheet.Cells[nRowOfHeader, c].Value;
        if (value == null) {
          break;
        }
        string name = value.ToString();
        DestList.Add(name);
      }

      int nRowOfDataStart = 3;
      for (int i=0; i<SourceList.Count; ++i) {
        int c = i * 3 + 1;
        int nIndex = DestList.IndexOf(SourceList[i]);
        if (nIndex != -1) {
          int dc = nIndex * 3 + 1;
          var range = DestSheet.Range[
            DestSheet.Cells[nRowOfDataStart,dc],
            DestSheet.Cells[DestSheet.UsedRange.Rows.Count,dc]
            ];
          range.Clear();

          for (int r=nRowOfDataStart; ; ++r) {
            var value = SourceSheet.Cells[r, c].Value;
            if (value == null) {
              break;
            }
            DestSheet.Cells[r, dc]    = value;
            DestSheet.Cells[r, dc+1]  = SourceSheet.Cells[r, c+1].Value;
            DestSheet.Cells[r, dc+2]  = SourceSheet.Cells[r, c+2].Value;
          }
        }
      }
    }

    /// <summary>
    /// 構造体出力.
    /// </summary>
    public void OutputStructure()
    {
      Excel.Workbook wb = (Excel.Workbook)Application.ActiveWorkbook;
      Excel.Worksheet ws = (Excel.Worksheet)Application.ActiveSheet;

      int colHeader = 1;
      int rowSheetType = 1;

      // 出力シートかを確認.
      if (ws.Cells[rowSheetType, colHeader].Value.ToString() != "出力") {
        return;
      }

      // データ名(=Book名).
      string DataName = Path.GetFileNameWithoutExtension(wb.Name);

      // ヘッダー情報がある行を確認.
      int rowHeaderStart = -1;
      int rowHeaderEnd = -1;
      for (int i=2; i<50; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "Header") {
          rowHeaderStart = i;
        }
        else if (name == "Contents") {
          rowHeaderEnd = i;
        }
      }
      if (rowHeaderStart < 0 || rowHeaderEnd < 0) {
        return;
      }

      // データのルートディレクトリを確認.
      string RootDirectory = GetRootDirectory(wb);

      // ヘッダー情報を解析.
      string StructureOutputDir   = "";
      string StructureOutputName  = "";
      for (int i = rowHeaderStart; i< rowHeaderEnd; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "構造体出力") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            StructureOutputDir = rvalue.ToString();
            StructureOutputDir = StructureOutputDir.Replace("<Root>",RootDirectory);
          }
        }
        if (name == "構造体名") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            StructureOutputName = rvalue.ToString();
          }
        }
      }

      // 出力パス.
      string OutputPath = StructureOutputDir + "\\data_format_" + StructureOutputName + ".h";

      // データ内容の解析.
      {
        int rowContentsStart  = rowHeaderEnd;
        int rowContentsName   = rowContentsStart + 1;
        int rowContentsType   = rowContentsStart + 2;
        int rowContentsValue  = rowContentsStart + 3;
        var dataHeaderList = new List<(string, DataType)>();
        int colDataStart = 3;
        int colData = colDataStart;
        while(true) {
          var value = ws.Cells[rowContentsName, colData].Value;
          if (value == null) {
            break;
          }
          string name = value.ToString();
          string typeName = ws.Cells[rowContentsType, colData].Value.ToString();
          dataHeaderList.Add((name, Str2Type(typeName)));
          colData += 1;
        }

        // ファイルに出力.
        using (StreamWriter sw = new StreamWriter(OutputPath, false, Encoding.UTF8)) {
          sw.WriteLine($"#pragma once");
          sw.WriteLine($"");
          sw.WriteLine($"//自動生成.");
          sw.WriteLine($"//From {DataName}.");
          sw.WriteLine($"");
          sw.WriteLine($"struct {StructureOutputName} {{");
          foreach (var data in dataHeaderList) {
            string typename = Type2CppTypeName(data.Item2);
            if (typename != null && typename != "") {
              sw.WriteLine($"  {typename} {data.Item1};");
            }
          }
          sw.WriteLine($"}};");
        }

        // メッセージ.
        MessageBox.Show("Output Header File\n" + OutputPath + "\n" + "struct " + StructureOutputName);
      }
    }

    /// <summary>
    /// モーション結合.
    /// </summary>
    public void LinkMotionDatum()
    {
      Excel.Workbook wb = (Excel.Workbook)Application.ActiveWorkbook;
      Excel.Worksheet ws = (Excel.Worksheet)Application.ActiveSheet;

      int colHeader = 1;
      int rowSheetType = 1;

      // 出力シートかを確認.
      if (ws.Cells[rowSheetType, colHeader].Value.ToString() != "モーション") {
        return;
      }

      // データ名(=Book名).
      string DataName = Path.GetFileNameWithoutExtension(wb.Name);

      // ヘッダー情報がある行を確認.
      int rowHeaderStart = -1;
      int rowHeaderEnd = -1;
      for (int i=2; i<50; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "Header") {
          rowHeaderStart = i;
        }
        else if (name == "Contents") {
          rowHeaderEnd = i;
        }
      }
      if (rowHeaderStart < 0 || rowHeaderEnd < 0) {
        return;
      }

      // データのルートディレクトリを確認.
      string RootDirectory = GetRootDirectory(wb);

      // ヘッダー情報を解析.
      string BinaryOutputDir = "";
      string BinaryOutputName = "";
      for (int i = rowHeaderStart; i< rowHeaderEnd; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "出力先") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            BinaryOutputDir = rvalue.ToString();
            BinaryOutputDir = BinaryOutputDir.Replace("<Root>",RootDirectory);
          }
        }
        else if (name == "出力名") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            BinaryOutputName = rvalue.ToString();
          }
        }
      }

      // バイナリ出力パス.
      string BinaryOutputPath = BinaryOutputDir + "\\" + BinaryOutputName;

      // 現在のディレクトリ.
      string CurrentDir = Path.GetDirectoryName(wb.FullName);

      // データ内容の解析.
      {
        int rowContentsStart  = rowHeaderEnd;
        int rowContentsLabel  = rowContentsStart + 1;
        int rowContentsValue  = rowContentsStart + 2;
        var dataHeaderList = new List<(string, DataType)>();
        int colDataId = 2;
        int colDataRes = 4;
        int nMotionNum = 0;
        for (int r=rowContentsValue; ; ++r) {
          var value = ws.Cells[r, colDataId].Value;
          if (value == null) {
            break;
          }
          nMotionNum += 1;
        }
        if (nMotionNum <= 0) {
          return;
        }

        //
        var contents = new List<(uint, List<byte>)>();
        for (int r=rowContentsValue; ; ++r) {
          var value = ws.Cells[r, colDataId].Value;
          if (value == null) {
            break;
          }
          string mot_path = CurrentDir + "\\" + ws.Cells[r, colDataRes].Value.ToString();

          using (var fs = new System.IO.FileStream(mot_path, System.IO.FileMode.Open, System.IO.FileAccess.Read))
          {
            byte[] tmp = new byte[(int)fs.Length];
            int result = fs.Read(tmp, 0, (int)fs.Length);
            contents.Add(((uint)fs.Length, new List<byte>(tmp)));
          }
        }

        // ファイルに出力.
        using (var fs = File.OpenWrite(BinaryOutputPath)) {
          fs.SetLength(0);
          fs.Write(BitConverter.GetBytes((uint)nMotionNum), 0, 4);
          foreach(var value in contents) {
            fs.Write(BitConverter.GetBytes(value.Item1), 0, 4);
            fs.Write(value.Item2.ToArray(), 0, (int)value.Item1);
          }
        }
      }
    }


    public void LinkBinaries()
    {
      Excel.Workbook wb = (Excel.Workbook)Application.ActiveWorkbook;
      Excel.Worksheet ws = (Excel.Worksheet)Application.ActiveSheet;

      int colHeader = 1;
      int rowSheetType = 1;

      // 出力シートかを確認.
      if (ws.Cells[rowSheetType, colHeader].Value.ToString() != "リンク") {
        return;
      }

      // リンク対象のファイル.
      List<string> link_targets = new List<string>();

      // 出力ファイル.
      string output_path = "";

      // データのルートディレクトリを確認.
      string RootDirectory = GetRootDirectory(wb);

      // ヘッダー情報を解析.
      for (int i = 1; i< 50; ++i)
      {
        var value = ws.Cells[i, colHeader].Value;
        if (value == null) {
          continue;
        }
        string name = value.ToString();
        if (name == "Input") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            string path = rvalue.ToString();
            path = path.Replace("<Root>",RootDirectory);
            link_targets.Add(path);
          }
        }
        else if (name == "Output") {
          var rvalue = ws.Cells[i, colHeader + 1].Value;
          if (rvalue != null) {
            string path = rvalue.ToString();
            path = path.Replace("<Root>",RootDirectory);
            output_path = path;
          }
        }
      }

      // 現在のディレクトリ.
      string CurrentDir = Path.GetDirectoryName(wb.FullName);

      // データ準備.
      var contents = new List<(uint, List<byte>)>();
      foreach (var item in link_targets)
      {
        using (var fs = new System.IO.FileStream(item, System.IO.FileMode.Open, System.IO.FileAccess.Read))
        {
          byte[] tmp = new byte[(int)fs.Length];
          int result = fs.Read(tmp, 0, (int)fs.Length);
          contents.Add(((uint)fs.Length, new List<byte>(tmp)));
        }
      }

      // ファイルに出力.
      using (var fs = File.OpenWrite(output_path)) {
        fs.SetLength(0);
        fs.Write(BitConverter.GetBytes((uint)link_targets.Count), 0, 4);
        foreach(var value in contents) {
          fs.Write(BitConverter.GetBytes(value.Item1), 0, 4);
          fs.Write(value.Item2.ToArray(), 0, (int)value.Item1);
        }
      }

      // メッセージ.
      string msg = "Link Binaries\n";
      foreach (var item in link_targets)
      {
        msg += item.ToString() + "\n";
      }
      msg += output_path + "\n";
      MessageBox.Show(msg);
    }
  }
}
