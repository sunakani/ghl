﻿namespace GameAddin
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
      this.tab1 = this.Factory.CreateRibbonTab();
      this.group1 = this.Factory.CreateRibbonGroup();
      this.OutputButton = this.Factory.CreateRibbonButton();
      this.OutputStructure = this.Factory.CreateRibbonButton();
      this.Link = this.Factory.CreateRibbonButton();
      this.group2 = this.Factory.CreateRibbonGroup();
      this.OutputIncButton = this.Factory.CreateRibbonButton();
      this.group3 = this.Factory.CreateRibbonGroup();
      this.UpdateDefines = this.Factory.CreateRibbonButton();
      this.group4 = this.Factory.CreateRibbonGroup();
      this.LinkMotionDatum = this.Factory.CreateRibbonButton();
      this.tab1.SuspendLayout();
      this.group1.SuspendLayout();
      this.group2.SuspendLayout();
      this.group3.SuspendLayout();
      this.group4.SuspendLayout();
      this.SuspendLayout();
      // 
      // tab1
      // 
      this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
      this.tab1.Groups.Add(this.group1);
      this.tab1.Groups.Add(this.group2);
      this.tab1.Groups.Add(this.group3);
      this.tab1.Groups.Add(this.group4);
      this.tab1.Label = "Database";
      this.tab1.Name = "tab1";
      // 
      // group1
      // 
      this.group1.Items.Add(this.OutputButton);
      this.group1.Items.Add(this.OutputStructure);
      this.group1.Items.Add(this.Link);
      this.group1.Label = "データベース";
      this.group1.Name = "group1";
      // 
      // OutputButton
      // 
      this.OutputButton.Label = "バイナリ出力";
      this.OutputButton.Name = "OutputButton";
      this.OutputButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.OutputButton_Click);
      // 
      // OutputStructure
      // 
      this.OutputStructure.Label = "構造体出力";
      this.OutputStructure.Name = "OutputStructure";
      this.OutputStructure.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.OutputStructure_Click);
      // 
      // Link
      // 
      this.Link.Label = "リンク";
      this.Link.Name = "Link";
      this.Link.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Link_Click);
      // 
      // group2
      // 
      this.group2.Items.Add(this.OutputIncButton);
      this.group2.Label = "プログラム";
      this.group2.Name = "group2";
      // 
      // OutputIncButton
      // 
      this.OutputIncButton.Label = "INC出力";
      this.OutputIncButton.Name = "OutputIncButton";
      this.OutputIncButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.OutputIncButton_Click);
      // 
      // group3
      // 
      this.group3.Items.Add(this.UpdateDefines);
      this.group3.Label = "定義";
      this.group3.Name = "group3";
      // 
      // UpdateDefines
      // 
      this.UpdateDefines.Label = "定義更新";
      this.UpdateDefines.Name = "UpdateDefines";
      this.UpdateDefines.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UpdateDefines_Click);
      // 
      // group4
      // 
      this.group4.Items.Add(this.LinkMotionDatum);
      this.group4.Label = "モーション";
      this.group4.Name = "group4";
      // 
      // LinkMotionDatum
      // 
      this.LinkMotionDatum.Label = "モーションデータ結合";
      this.LinkMotionDatum.Name = "LinkMotionDatum";
      this.LinkMotionDatum.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.LinkMotionDatum_Click);
      // 
      // Ribbon1
      // 
      this.Name = "Ribbon1";
      this.RibbonType = "Microsoft.Excel.Workbook";
      this.Tabs.Add(this.tab1);
      this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
      this.tab1.ResumeLayout(false);
      this.tab1.PerformLayout();
      this.group1.ResumeLayout(false);
      this.group1.PerformLayout();
      this.group2.ResumeLayout(false);
      this.group2.PerformLayout();
      this.group3.ResumeLayout(false);
      this.group3.PerformLayout();
      this.group4.ResumeLayout(false);
      this.group4.PerformLayout();
      this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton OutputButton;
    internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
    internal Microsoft.Office.Tools.Ribbon.RibbonButton OutputIncButton;
    internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
    internal Microsoft.Office.Tools.Ribbon.RibbonButton UpdateDefines;
    internal Microsoft.Office.Tools.Ribbon.RibbonButton OutputStructure;
    internal Microsoft.Office.Tools.Ribbon.RibbonGroup group4;
    internal Microsoft.Office.Tools.Ribbon.RibbonButton LinkMotionDatum;
    internal Microsoft.Office.Tools.Ribbon.RibbonButton Link;
  }

  partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
