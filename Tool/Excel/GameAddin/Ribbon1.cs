﻿using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAddin
{
  public partial class Ribbon1
  {
    private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
    {

    }

    private void OutputButton_Click(object sender, RibbonControlEventArgs e)
    {
      Globals.ThisAddIn.OutputBinary();
    }

    private void OutputIncButton_Click(object sender, RibbonControlEventArgs e)
    {
      Globals.ThisAddIn.OutputInc();
    }

    private void UpdateDefines_Click(object sender, RibbonControlEventArgs e)
    {
      Globals.ThisAddIn.UpdateDefines();
    }

    private void OutputStructure_Click(object sender, RibbonControlEventArgs e)
    {
      Globals.ThisAddIn.OutputStructure();
    }

    private void LinkMotionDatum_Click(object sender, RibbonControlEventArgs e)
    {
      Globals.ThisAddIn.LinkMotionDatum();
    }

    private void Link_Click(object sender, RibbonControlEventArgs e)
    {
      Globals.ThisAddIn.LinkBinaries();
    }
  }
}
