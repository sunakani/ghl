﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;

namespace GameAddin
{

[ComVisible(true)]
public interface IAddInUtilities
{
    void LinkMotionData();
}

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.None)]
public class AddInUtilities : IAddInUtilities
{
    public void LinkMotionData()
    {
      MessageBox.Show("LinkMotionData.");
    }
}
}
